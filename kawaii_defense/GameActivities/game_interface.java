package nexidea.kawaii_defense.GameActivities;


import android.graphics.Bitmap;
import android.graphics.Canvas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import nexidea.kawaii_defense.MyRandom;
import nexidea.kawaii_defense.Projectiles.Ball;
import nexidea.kawaii_defense.Projectiles.Projectile;
import nexidea.kawaii_defense.Projectiles.Projectile_bomb;
import nexidea.kawaii_defense.Projectiles.Projectile_heart;
import nexidea.kawaii_defense.Projectiles.Projectile_laser;
import nexidea.kawaii_defense.Projectiles.Projectile_stage1_boss;
import nexidea.kawaii_defense.Projectiles.Projectile_stage2_boss;
import nexidea.kawaii_defense.Wave;

public interface game_interface {

    //init
    void init();


    void load_projectiles();
    void update(long elapsed);
    void draw(Canvas canvas);
    void adjust_game_difficulty();
    void kaboom();

    //convert a drawable to a Bitmap image
    Bitmap getBitmap(int drawableRes);

    //get inventories
    HashMap<String, HashMap<String, Integer>> getMultiProjectile();

    //projectiles in game
    HashMap<String, ConcurrentHashMap<Integer, Projectile>> getProjectile_in_game();
    ConcurrentHashMap<Integer, Projectile_heart> getHeart_in_game();
    ConcurrentHashMap<Integer, Projectile_bomb> getBomb_in_game();
    ConcurrentHashMap<Integer, Projectile_stage1_boss> getstage1_Boss_in_game();
    ConcurrentHashMap<Integer, Projectile_stage2_boss> getstage2_Boss_in_game();

    //get_obj_counter/obj_types
    HashMap<String, Integer> getObj_counter();
    ArrayList<String> getObj_types();

    //laser in game/ laser types
    ConcurrentHashMap<Integer, Projectile_laser> getLaser_in_game();
    Wave getCurrent_wave();

    //set ball
    void setBall(Ball ball);

    //creation of objects
    void createLives(int k);
    void createBombs(Integer i);
    void createInks(Integer j);

    //count obj_in_screen
    int getCount_obj_in_screen();
    void setCount_obj_in_screen(int count_obj_in_screen);


    //pause and load wave
    int getWave_counter();
    void setWave_counter(int wave_counter);

    //getrandom
     MyRandom getMy_random();

     //setpaused
    void setPaused(boolean paused);

    //draw ink above character
    void draw_ink(Canvas canvas);

    //reset trackers
    void setKill_qty_tracker(int kill_qty_tracker);
    void setScore_tracker(int score_tracker);
}
