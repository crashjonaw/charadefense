package nexidea.kawaii_defense.GameActivities;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import nexidea.kawaii_defense.Projectiles.Ball;
import nexidea.kawaii_defense.Game;
import nexidea.kawaii_defense.MainActivity_stage;
import nexidea.kawaii_defense.MyRandom;
import nexidea.kawaii_defense.Projectiles.Projectile;
import nexidea.kawaii_defense.Projectiles.Projectile_bomb;
import nexidea.kawaii_defense.Projectiles.Projectile_diamond;
import nexidea.kawaii_defense.Projectiles.Projectile_heart;
import nexidea.kawaii_defense.Projectiles.Projectile_laser;
import nexidea.kawaii_defense.Projectiles.Projectile_stage1_boss;
import nexidea.kawaii_defense.Projectiles.Projectile_stage2_boss;
import nexidea.kawaii_defense.R;
import nexidea.kawaii_defense.Wave;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;
import static java.lang.Math.min;
import static nexidea.kawaii_defense.MenuView.MY_PREFS_NAME;
import static nexidea.kawaii_defense.MenuView.menuviewcontext;

public class GameActivity_stage3 implements game_interface {
    //declare variables
    private SurfaceHolder holder;
    private Resources resources;
    private Ball ball;
    private int s_width;
    private int s_height;
    private Game game;
    private int kill_qty_tracker;
    private int score_tracker;
    private boolean paused;

    public static MyRandom my_random;

    private boolean new_wave_begins;

    //stage 1 normal projectile characteristics
    private long spawn_interval;
    private long acc_time;
    private int max_projectiles;
    private float projSpeed;
    private int error_tolerance;
    private int count_obj_in_screen;
    private int live_creation_count;
    private int bomb_creation_count;

    //store spawn_start_info
    public String spawn_all_start;

    //hashmap to store Wave information
    public HashMap<Integer, Wave> wave_info = new HashMap<>();

    //wave counter
    public int total_wave_count;

    //pointer go current Wave in game;
    public Wave current_wave;
    //wave counter
    public int wave_counter;

    //spawn_rate
    private HashMap<String, Integer> spawn_rate;

    //zone 1 stuff
    public HashMap<String, Integer > obj_counter = new HashMap<>();

    //hashmap to hold , asteroid,
    public HashMap<String, HashMap<String, Integer>> multiProjectile = new HashMap<>();

    //list to store names of projectiles
    public ArrayList<String> obj_types = new ArrayList<>();
    //Arraylist to actually display
    public HashMap<String, ConcurrentHashMap<Integer, Projectile>> projectile_in_game = new HashMap<>();

    //Arraylist for hearts
    public ConcurrentHashMap<Integer, Projectile_heart> heart_in_game = new ConcurrentHashMap<>();

    //Arraylist for bombs
    public ConcurrentHashMap<Integer, Projectile_bomb> bomb_in_game = new ConcurrentHashMap<>();

    //boss_in_game
    public ConcurrentHashMap<Integer, Projectile_stage1_boss> boss_in_game = new ConcurrentHashMap<>();

    //zone 3
    public ConcurrentHashMap<Integer, Projectile_diamond> diamond_in_game = new ConcurrentHashMap<>();



    //constructor
    public GameActivity_stage3(int s_width, int s_height,
                               Resources resources, SurfaceHolder holder,
                               Game game,
                               int error_tolerance,
                               float game_scale,
                               int start_wave){

        //random_initiation
        my_random = new MyRandom();
        my_random.add_dim(s_width, s_height);
        //Log.d(TAG, "GameActivity_stage1: test random " + my_random.random_map.get("75,150").next());

        this.s_width = s_width;
        this.s_height = s_height;
        this.error_tolerance = error_tolerance;

        this.holder = holder;
        this.resources = resources;
        this.game = game;

        //accumulated time for spawn
        acc_time = 0;
        this.acc_time = 0;
        this.projSpeed =0.20f;
        wave_counter = start_wave;


    }

    @Override
    public void init(){
        load_stage3();
        new_wave_begins = false;
        count_obj_in_screen = 0;

        create_waves();
        current_wave = wave_info.get(wave_counter);
        current_wave.load_game(this.game);
        current_wave.get_dim(s_width, s_height);
        setup_wave_to_game(current_wave);
        game.create_star_calculator(wave_counter);
//        Log.d(TAG, "GameActivity_stage3: stage 3 stuff is loaded!");
    }

    @Override
    public void load_projectiles() {
        //projectile_in_game
        my_random.add_stage3_random_starts(10000);
        current_wave.load_stage3_projectiles(1000, projectile_in_game, diamond_in_game);
    }

    @Override
    public void update(long elapsed) {
        acc_time += elapsed;
        //hearts and bombs
        for (Map.Entry<Integer, Projectile_heart> entry : heart_in_game.entrySet()) {
            if (entry.getValue().out) {
//                heart_in_game.remove(entry.getKey());
            } else {
                entry.getValue().update(elapsed);
                check_heart_collision(entry.getValue());
            }
        }
        for (Map.Entry<Integer, Projectile_bomb> entry : bomb_in_game.entrySet()) {
            if (entry.getValue().out) {
//                bomb_in_game.remove(entry.getKey());
            } else {
                entry.getValue().update(elapsed);
            }
        }

        if (new_wave_begins) {
//            Log.d(TAG, "update: " + Long.toString(acc_time) + "spawn timer " + Long.toString(current_wave.spawn_interval));
//            reset obj_counter to reload projectiles from the list
            Integer lcc = new Integer(live_creation_count);
            for (int h = 0; h < lcc; h++) {
                float[] rand_start = my_random.random_start_map.get(spawn_all_start).next();
                obj_counter.put("heart", obj_counter.get("heart") + 1);
                Projectile_heart heart = new Projectile_heart(s_width, s_height,
                        rand_start[0],
                        rand_start[1],
                        projSpeed * randomNumberInRange(75,150)  / 100,
                        projSpeed * randomNumberInRange(75,150)  / 100);
                heart_in_game.put(obj_counter.get("heart"), heart);
                heart_in_game.get(obj_counter.get("heart")).
                        load_projectile("heart", game);
                live_creation_count -= 1;
            }


            Integer bcc = new Integer(bomb_creation_count);
            for (int b = 0; b < bcc; b++) {
                float[] rand_start = my_random.random_start_map.get(spawn_all_start).next();
                obj_counter.put("bomb", obj_counter.get("bomb") + 1);
                Projectile_bomb bomb = new Projectile_bomb(s_width, s_height,
                        rand_start[0],
                        rand_start[1],
                        projSpeed * randomNumberInRange(75,150)  / 100,
                        projSpeed * randomNumberInRange(75,150)  / 100);
                bomb_in_game.put(obj_counter.get("bomb"), bomb);
                bomb_in_game.get(obj_counter.get("bomb")).
                        load_projectile("bomb", game);
                bomb_creation_count -= 1;
            }
            //zone3
            for (Map.Entry<Integer, Projectile_diamond> entry : diamond_in_game.entrySet()) {
                if (entry.getValue().out) {
                } else {
                    entry.getValue().update(elapsed);
                    check_diamond_collision(entry.getValue());
                }
            }

            if (!current_wave.check_wave_completion()) {
                if (!current_wave.boss_stage) {
                    if (acc_time > spawn_interval && count_obj_in_screen < max_projectiles) {
                        acc_time = 0;
                        //spawn fireballs
                        for (int i = 0; i < my_random.random_map.get(Integer.toString(wave_counter) + "fireball").next(); i++) {
                            count_obj_in_screen += 1;
                            obj_counter.put("fireball", obj_counter.get("fireball") + 1);
                            diamond_in_game.get(obj_counter.get("fireball")).load_projectile();
                        }
                    }


                } else if (current_wave.boss_stage) {
                    for (Map.Entry<Integer, Projectile_stage1_boss> entry : boss_in_game.entrySet()) {
                        if (entry.getValue().out) {
                        } else {
                            entry.getValue().update(elapsed);
                            check_boss_collision(entry.getValue());
                        }
                    }
                    if (!current_wave.boss_spawned) {
                        for (int i = 0; i < current_wave.boss_count; i++) {
                            obj_counter.put("boss", obj_counter.get("boss") + 1);
                            Projectile_stage1_boss boss = new Projectile_stage1_boss(
                                    s_width,
                                    s_height,
                                    randomNumberInRange(s_width / 10, s_width * 9 / 10),
                                    0,
                                    0,
                                    randomNumberInRange(3, 5),
                                    "boss",
                                    game
                            );
                            boss.setDifficulty(current_wave.boss_difficulty);
                            boss_in_game.put(obj_counter.get("boss"), boss);
                            boss_in_game.get(obj_counter.get("boss")).
                                    load_projectile();
                        }
                        current_wave.boss_spawned = true;
                    }

                }
            } else if (count_obj_in_screen > 0) {
            } else {
                end_wave();
                acc_time = 0;
            }
        } else if (acc_time > current_wave.wave_wait) {
            new_wave_begins = true;
            acc_time = 0;
//            Log.d(TAG, "update: wave " + Integer.toString(wave_counter)+" start!");
        } else {
        }
    }

    @Override
    public void draw(Canvas canvas) {
        //draws
//        for (int j = 0; j < obj_types.size(); j++) {
//            if (projectile_in_game.get(obj_types.get(j)) == null) {
//            } else {
//                //Iterator it = projectile_in_game.get(obj_types.get(j)).entrySet().iterator();
//                for (Map.Entry<Integer, Projectile> entry : projectile_in_game.get(obj_types.get(j)).entrySet()) {
//                    if (entry.getValue().out) {
//                    } else {
//                        entry.getValue().draw(canvas);
//                    }
//                }
//            }
//        }

        for (Map.Entry<Integer, Projectile_heart> entry : heart_in_game.entrySet()) {
            if (entry.getValue().out) {
            } else {
                entry.getValue().draw(canvas);
            }
        }
        for (Map.Entry<Integer, Projectile_bomb> entry : bomb_in_game.entrySet()) {
            if (entry.getValue().out) {
            } else {
                entry.getValue().draw(canvas);
            }
        }

        for (Map.Entry<Integer, Projectile_diamond> entry : diamond_in_game.entrySet())
            if (entry.getValue().out|| !entry.getValue().spawned) {
            } else {
                entry.getValue().draw(canvas);
            }


//        for (Map.Entry<Integer, Projectile_stage1_boss> entry : boss_in_game.entrySet()) {
//            if (entry.getValue().out) {
//            } else {
//                entry.getValue().draw_boss(canvas);
//            }
//        }

    }

    @Override
    public void adjust_game_difficulty() {
    }
    @Override
    public Bitmap getBitmap(int drawableRes) {
        Drawable drawable = ResourcesCompat.getDrawable(resources,drawableRes,null);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }
    @Override
    public void kaboom() {
        for (Map.Entry<Integer, Projectile_diamond> entry : diamond_in_game.entrySet()) {
            if (entry.getValue().dead) {
            } else {
                entry.getValue().destroyed(game);
            }

        }
    }

    @Override
    public HashMap<String, HashMap<String, Integer>> getMultiProjectile() {
        return multiProjectile;
    }

    //creating projectiles and objects
    //create hearts
    @Override
    public void createLives(int k) {
        live_creation_count += k;
    }
    //create bomb
    @Override
    public void createBombs(Integer i){
        bomb_creation_count += i;
    }
    //create inks
    @Override
    public void createInks(Integer j) { }

    //get obj_counter and types
    @Override
    public HashMap<String, Integer> getObj_counter() {
        return obj_counter;
    }
    @Override
    public ArrayList<String> getObj_types() {
        return obj_types;
    }
    @Override
    public ConcurrentHashMap<Integer, Projectile_laser> getLaser_in_game() {
        return null;
    }

    //projectiles_in_game
    @Override
    public HashMap<String, ConcurrentHashMap<Integer, Projectile>> getProjectile_in_game() {
        return projectile_in_game;
    }
    @Override
    public ConcurrentHashMap<Integer, Projectile_heart> getHeart_in_game() {
        return heart_in_game;
    }
    @Override
    public ConcurrentHashMap<Integer, Projectile_bomb> getBomb_in_game() {
        return bomb_in_game;
    }
    @Override
    public ConcurrentHashMap<Integer, Projectile_stage1_boss> getstage1_Boss_in_game() {
        return null;
    }
    @Override
    public ConcurrentHashMap<Integer, Projectile_stage2_boss> getstage2_Boss_in_game() {
        return null;
    }
    //set ball
    @Override
    public void setBall(Ball ball) {
        this.ball = ball;
    }

    //get current wave
    @Override
    public Wave getCurrent_wave() {
        return current_wave;
    }
    @Override
    public int getCount_obj_in_screen() {
        return count_obj_in_screen;
    }
    @Override
    public void setCount_obj_in_screen(int count_obj_in_screen) {
        this.count_obj_in_screen = count_obj_in_screen;
    }
    @Override
    public int getWave_counter() {
        return wave_counter;
    }
    @Override
    public void setWave_counter(int wave_counter) {
        this.wave_counter = wave_counter;
    }
    @Override
    public MyRandom getMy_random() {
        return my_random;
    }
    @Override
    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    @Override
    public void draw_ink(Canvas canvas){
    }
    @Override
    public void setKill_qty_tracker(int kill_qty_tracker) {
        this.kill_qty_tracker = kill_qty_tracker;
    }
    @Override
    public void setScore_tracker(int score_tracker) {
        this.score_tracker = score_tracker;
    }

    //boss
    public void check_boss_collision(Projectile_stage1_boss enemy) {
        if (enemy.dead) {
        } else {
            Rect screenRect = enemy.getScreenRect();
            if (ball.getX() + ball.getScreenRect().width() / 2 >= screenRect.left &&
                    ball.getX() + ball.getScreenRect().width() / 2 <= screenRect.right &&
                    ball.getY() + ball.getScreenRect().height() / 2 >= screenRect.top &&
                    ball.getY() + ball.getScreenRect().height() / 2 <= screenRect.bottom) {
                enemy.destroyed_by_collision(game);
            }
        }
    }

    public void check_heart_collision(Projectile_heart enemy) {
        if (enemy.dead) {
        } else {
            Rect screenRect = enemy.getScreenRect();
            if (ball.getX() + ball.getRect().width() / 2 >= screenRect.left &&
                    ball.getX() + ball.getRect().width() / 2 <= screenRect.right &&
                    ball.getY() + ball.getRect().height() / 2 >= screenRect.top &&
                    ball.getY() + ball.getRect().height() / 2  / 2 <= screenRect.bottom) {
                enemy.destroyed_by_collision(game);
            }
        }
    }

    public void check_diamond_collision(Projectile_diamond enemy){
        if (enemy.dead) {
        } else {
            Rect screenRect = enemy.getScreenRect();
            if (ball.getX() + ball.getRect().width() / 2 >= screenRect.left &&
                    ball.getX() + ball.getRect().width() / 2 <= screenRect.right &&
                    ball.getY() + ball.getRect().height() / 2 >= screenRect.top &&
                    ball.getY() + ball.getRect().height() / 2  / 2 <= screenRect.bottom) {
                enemy.destroyed_by_collision(game);
            }
        }
    }


    public SurfaceHolder getHolder() {
        return holder;
    }

    public static int randomNumberInRange(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    //wave setting
    public void create_waves() {
//        Log.d(TAG, "create_waves: starting to create waves");
        total_wave_count = 1;
        int base_wave_interval = 1000;
        create_fireball_waves(2,1,new double[] {0.8,0.1,0.1}, 0.4f,5,5,0,0,0,4,base_wave_interval);
        create_fireball_waves(2,1,new double[] {0.0,0.5,0.5}, 0.45f,0,0,5,5,0,5,base_wave_interval);
        create_fireball_waves(2,1,new double[] {0.3,0.3,0.4}, 0.5f,5,5,5,0,5,5,base_wave_interval);
        create_fireball_waves(2,1,new double[] {0.3,0.3,0.4}, 0.5f,5,5,5,5,5,6,base_wave_interval);
        create_fireball_waves(2,1,new double[] {0.8,0.1,0.1}, 0.6f,10,10,10,10,10,7,base_wave_interval);
        //create wave according to difficulty

        // add one more boss here
    }

    public void create_fireball_waves(int fireball_waves, int start_count,
                                      double[] size_probabilities,float projectile_speed,
                                      int blue,int purple, int red, int yellow, int green,
                                      int max_projectiles,
                                      int wave_interval) {
        for (int x = 1; x < fireball_waves + 1; x++) {
            HashMap<String, Integer> wave_completion = set_wave_completion(
                    ((x - 1) + randomNumberInRange(0,1)+ start_count)*blue,
                    ((x - 1)  + randomNumberInRange(0,1)+  start_count)*purple,
                    ((x - 1) + randomNumberInRange(0,1)+  start_count)*red,
                    ((x - 1)  + randomNumberInRange(0,1)+  start_count)*yellow,
                    ((x - 1)  + randomNumberInRange(0,1)+  start_count)*green,
                    0,
                    0);
//            Log.d(TAG, "create_waves: finished set_wave_completion");
            Wave wave = new Wave(
                    wave_completion,
                    projectile_speed,
                    set_spawn_rate(min(1 + (x - 1 + start_count) * randomNumberInRange(1, 2), 5)),
                    max_projectiles,
                    randomNumberInRange(wave_interval-500, wave_interval+500),
                    "top",
                    2000,
                    my_random,
                    size_probabilities
            );

            //stage3 wave settings update
            wave.set_stage3_projectile_diamond_types();

            //create random spawn rates
            add_random_spawn_rate_to_random_map(wave);
            wave_info.put(total_wave_count, wave);
            total_wave_count = total_wave_count + 1;
        }
    }

    public void create_boss_wave(int boss_count, int boss_difficulty) {
//        for (int x = 1 ; x <= boss_waves; x++){
//        HashMap<String, Integer> wave_completion = set_wave_completion(
//                0,
//                0,
//                0);
//        Log.d(TAG, "create_waves: finished set_wave_completion");
//        Wave wave = new Wave(
//                wave_completion,
//                0.4f,
//                set_spawn_rate(randomNumberInRange(3, 4) - 1),
//                10,
//                randomNumberInRange(1500, 2500),
//                "irrelevant",
//                5000,
//                my_random,
//                null
//        );
////        wave.set_bosses(boss_count, boss_difficulty);
//        //create random spawn rates
//        add_random_spawn_rate_to_random_map(wave);
//        wave_info.put(total_wave_count, wave);
//        total_wave_count = total_wave_count + 1;
//        }
    }

    public void end_wave() {
        game.loaded = false;
        game.change_char_offense(-1);
//        if(wave_counter == total_wave_count-1){
//            game.stage_completed();
//        }
        if(wave_counter == 1){
            game.stage_completed();
        }
        else {
            count_obj_in_screen = 0;
            //stars
            current_wave.stars = game.getStar_calculator().eval_stars(game.score-score_tracker);
//            Log.d(TAG, "end_wave: current stars" + current_wave.stars);
            update_stars(current_wave);
            show_wave_end_log(current_wave.stars, wave_counter);

            new_wave_begins = false;
            wave_counter++;
            //stars
            game.create_star_calculator(wave_counter);
            if (wave_counter % 10 == 0) {
                createLives(randomNumberInRange(1, 2));
            }
            current_wave = wave_info.get(wave_counter);
            setup_wave_to_game(current_wave);
            //refresh randoms
            my_random.add_stage1_random_starts(5000);

            //clear
            for (int j = 0; j < obj_types.size(); j++) {
                obj_counter.put(obj_types.get(j),0);
            }
            current_wave.load_game(game);
            current_wave.get_dim(s_width, s_height);
            current_wave.load_stage3_projectiles(1000, projectile_in_game,diamond_in_game);
        }
        System.gc();
    }

    public void setup_wave_to_game(Wave current_wave) {
        spawn_interval = current_wave.spawn_interval;
        spawn_rate = current_wave.spawn_rate;
        max_projectiles = current_wave.max_projectiles;
//        Log.d(TAG, "setup_wave_to_game: " + max_projectiles);
        projSpeed = current_wave.projSpeed;
        ((MainActivity_stage) game.getContext()).updateWaveText(wave_counter);
        ((MainActivity_stage) game.getContext()).updatereq(current_wave.wave_completion, game.current_stage);
    }

    //1: red, 2: blue, 3: purple, 4: yellow, 5: green
    public HashMap<String, Integer> set_wave_completion(int blue,int purple, int red, int yellow, int green, int score, int boss_count) {
        HashMap<String, Integer> wave_completion = new HashMap<>();
        wave_completion.put("blue",blue);
        wave_completion.put("purple",purple);
        wave_completion.put("red",red);
        wave_completion.put("yellow",yellow);
        wave_completion.put("green",green);
        wave_completion.put("score", score);
        wave_completion.put("boss", boss_count);
        return wave_completion;
    }

    public HashMap<String, Integer> set_spawn_rate( int fireball) {
        HashMap<String, Integer> spawn_rate = new HashMap<>();
        spawn_rate.put("fireball", fireball);
        return spawn_rate;
    }

    public void load_stage3(){
        obj_counter.put("heart", 0);
        obj_counter.put("bomb", 0);
        obj_counter.put("boss", 0);
        obj_counter.put("fireball",0);

        spawn_all_start = "top_down_left_right";
        live_creation_count = 0;
        bomb_creation_count = 0;


        //zone3 files
        HashMap<String, Integer> boss = new HashMap<>();
        boss.put("damage", 1);
        boss.put("hits", 10);
        boss.put("p_score", 100);
        boss.put("wall_bounce_lives", null);
        multiProjectile.put("boss", boss);

        HashMap<String, Integer> heart = new HashMap<>();
        heart.put("damage", -1);
        heart.put("hits", 1);
        heart.put("p_score", 0);
        heart.put("wall_bounce_lives", 10);
        multiProjectile.put("heart", heart);

        HashMap<String, Integer> bomb = new HashMap<>();
        bomb.put("damage", 0);
        bomb.put("hits", 1);
        bomb.put("p_score", 3);
        bomb.put("wall_bounce_lives", 10);
        multiProjectile.put("bomb", bomb);

        HashMap<String,Integer> fireball = new HashMap<>();
        fireball.put("damage",1);
        fireball.put("hits",999);
        fireball.put("p_score",1);
        multiProjectile.put("fireball",fireball);


        for (int j = 0; j < obj_types.size(); j++) {
            ConcurrentHashMap<Integer, Projectile> temp = new ConcurrentHashMap<>();
            projectile_in_game.put(obj_types.get(j), temp);
        }
    }

    public void update_stars(Wave current_wave) {
        HashMap<Integer,Integer> wave_stars = new HashMap<>();
        SharedPreferences local_db = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String stage3_wave_stars_local_store = local_db.getString("stage3_wave_stars_local_store", "");
        if (stage3_wave_stars_local_store == "") {
            for (int i = 0; i < total_wave_count; i++) {
                wave_stars.put(i + 1, 0);
            }
//            Log.d(TAG, "update_stars: cannot find local database");
            SharedPreferences.Editor editor = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
            Type type = new TypeToken<HashMap<Integer, Integer>>() {
            }.getType();
            String json = gson.toJson(wave_stars, type);
            editor.putString("stage3_wave_stars_local_store", json);
            editor.apply();
        } else {
            HashMap<Integer, Integer> stage3_wave_stars = new Gson().fromJson(stage3_wave_stars_local_store,
                    new TypeToken<HashMap<Integer, Integer>>() {}.getType());
            if(current_wave.stars > stage3_wave_stars.get(wave_counter)){
//                Log.d(TAG, "update_stars: done update for wave" + wave_counter);
                SharedPreferences.Editor editor = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                stage3_wave_stars.put(wave_counter,current_wave.stars);
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
                Type type = new TypeToken<HashMap<Integer, Integer>>() {
                }.getType();
                String json = gson.toJson(stage3_wave_stars, type);
                editor.putString("stage3_wave_stars_local_store", json);
                editor.apply();
            }
        }
    }

    private void add_random_spawn_rate_to_random_map(Wave wave){
        my_random.add_random_map(Integer.toString(total_wave_count) + "fireball",
                wave.spawn_rate.get("fireball") / 2,
                wave.spawn_rate.get("fireball"), 1000);
    }

    public void create_wave_end_log(final int stars, final int wave_counter){
        paused = true;
        //now let's do the dialog creation
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(game.getContext());
        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(game.getContext());
        View mView = inflater.inflate(R.layout.activity_wave_end, null);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();

        //set stars
        ImageView s1 = mView.findViewById(R.id.wave_end_star_achievement_1);
        ImageView s2 = mView.findViewById(R.id.wave_end_star_achievement_2);
        ImageView s3 = mView.findViewById(R.id.wave_end_star_achievement_3);
        HashMap<Integer,ImageView> go_stars = new HashMap<>();
        go_stars.put(1,s1);
        go_stars.put(2,s2);
        go_stars.put(3,s3);

        HashMap<Integer,Integer> star_images = new HashMap<>();
        star_images.put(1,R.drawable.star1);
        star_images.put(2,R.drawable.star2);
        star_images.put(3,R.drawable.star3);
        star_images.put(0,R.drawable.star0);

        for (int i = 1; i<4; i++) {
            if (i <= stars) {
                update_image_view(go_stars.get(i), star_images.get(i));
            } else {
                update_image_view(go_stars.get(i), star_images.get(0));
            }
        }

        //set statistics - score/kill/tokens
        TextView game_over_score = mView.findViewById(R.id.wave_end_score_achieved);
        game_over_score.setText(Integer.toString(game.score-score_tracker));
        TextView kill_count = mView.findViewById(R.id.wave_end_kill_qty);
        kill_count.setText(" X " +Integer.toString(game.kill_qty - kill_qty_tracker));

        //set title
        TextView wave_end_title = mView.findViewById(R.id.wave_end_title);
        wave_end_title.setText("Wave "+Integer.toString(wave_counter) + " CompletE!");

        //reset kill/score
        kill_qty_tracker = game.kill_qty;
        score_tracker = game.score;

        Button next_wave = mView.findViewById(R.id.wave_end_next_wave);
        next_wave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paused=false;
                dialog.dismiss();

            }
        });

        Button main_menu = mView.findViewById(R.id.wave_end_end_game);
        main_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                game.record_scores();
                dialog.dismiss();
            }
        });


        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    private void show_wave_end_log(final int stars, final int wave_counter) {
        //initiate SharedPreferences to collect scores + name
        game.activity_handler = new Handler(Looper.getMainLooper());
        game.activity_handler.postAtTime(new Runnable() {
            @Override
            public void run() {
                synchronized(game.getContext()) {
                    create_wave_end_log(stars, wave_counter);
//                    Log.d(TAG, "run: handler fired! show_wave_end_log_stage3");
                }
            }
        },500);

//        MainActivity_stage.this.runOnUiThread(new Runnable()

    }

    private void update_image_view(ImageView image, int id){
        image.setImageResource(id);
        image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        image.setBackgroundColor(Color.TRANSPARENT);
    }
}

