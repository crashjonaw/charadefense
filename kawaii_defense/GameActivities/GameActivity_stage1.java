package nexidea.kawaii_defense.GameActivities;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import nexidea.kawaii_defense.Projectiles.Ball;
import nexidea.kawaii_defense.Game;
import nexidea.kawaii_defense.MainActivity_stage;
import nexidea.kawaii_defense.MyRandom;
import nexidea.kawaii_defense.Projectiles.Projectile;
import nexidea.kawaii_defense.Projectiles.Projectile_bomb;
import nexidea.kawaii_defense.Projectiles.Projectile_char_offense;
import nexidea.kawaii_defense.Projectiles.Projectile_heart;
import nexidea.kawaii_defense.Projectiles.Projectile_ink;
import nexidea.kawaii_defense.Projectiles.Projectile_laser;
import nexidea.kawaii_defense.Projectiles.Projectile_stage1_boss;
import nexidea.kawaii_defense.Projectiles.Projectile_stage2_boss;
import nexidea.kawaii_defense.R;
import nexidea.kawaii_defense.Wave;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;
import static java.lang.Math.min;
import static nexidea.kawaii_defense.MenuView.MY_PREFS_NAME;
import static nexidea.kawaii_defense.MenuView.menuviewcontext;

public class GameActivity_stage1 implements game_interface {
    //declare variables
    private Resources resources;
    private Ball ball;
    private int s_width;
    private int s_height;
    private Game game;
    private int kill_qty_tracker;
    private int score_tracker;
    private boolean paused;

    public static MyRandom my_random;

    private boolean new_wave_begins;

    private long spawn_interval;
    private long acc_time;
    private HashMap<String, Integer> spawn_rate = new HashMap<>();
    private int max_projectiles;
    private float projSpeed;
    private int error_tolerance;
    private int count_obj_in_screen;

    private int live_creation_count;
    private int bomb_creation_count;
    private int ink_creation_count;

    //store spawn_start_info
    public String spawn_all_start;

    //hashmap to store Wave information
    public HashMap<Integer, Wave> wave_info = new HashMap<>();

    //wave counter
    public int wave_counter;
    public int total_wave_count;

    //pointer go current Wave in game;
    public Wave current_wave;

    //zone 1 stuff
    public HashMap<String, Integer> obj_counter = new HashMap<>();

    //hashmap to hold coconut, banana, heart, bomb
    public HashMap<String, HashMap<String, Integer>> multiProjectile = new HashMap<>();

    //list to store names of projectiles
    public ArrayList<String> obj_types = new ArrayList<>();

    //Arraylist for projectiles
    public HashMap<String, ConcurrentHashMap<Integer, Projectile>> projectile_in_game = new HashMap<>();

    //Arraylist for inks
    public ConcurrentHashMap<Integer, Projectile_ink> ink_in_game = new ConcurrentHashMap<>();

    //Arraylist for hearts
    public ConcurrentHashMap<Integer, Projectile_heart> heart_in_game = new ConcurrentHashMap<>();

    //Arraylist for bombs
    public ConcurrentHashMap<Integer, Projectile_bomb> bomb_in_game = new ConcurrentHashMap<>();

    //berry_in_game
    public ConcurrentHashMap<Integer, Projectile_stage1_boss> boss_in_game = new ConcurrentHashMap<>();


    //constructor
    public GameActivity_stage1(int s_width, int s_height,
                               Resources resources,
                               Game game,
                               int error_tolerance,
                               float game_scale,
                               int start_wave) {

        //random_initiation
        my_random = new MyRandom();
        my_random.add_dim(s_width, s_height);
        //Log.d(TAG, "GameActivity_stage1: test random " + my_random.random_map.get("75,150").next());

        this.s_width = s_width;
        this.s_height = s_height;
        this.error_tolerance = error_tolerance;

        this.resources = resources;
        this.game = game;
        wave_counter = start_wave;

    }

    @Override
    public void init(){
        kill_qty_tracker =0;
        score_tracker=0;
        paused = false;
        //accumulated time for spawn
        acc_time = 0;

        load_stage1();
        new_wave_begins = false;
        count_obj_in_screen = 0;

        create_waves();
        current_wave = wave_info.get(wave_counter);
        current_wave.load_game(this.game);
        current_wave.get_dim(s_width, s_height);
        setup_wave_to_game(current_wave);

        game.tutorial.show_tutorial_log(game,"sword");
        game.tutorial.show_tutorial_log(game,"shield");
        game.tutorial.show_tutorial_log(game,"heart");


        game.create_star_calculator(wave_counter);
    }


    @Override
    public void load_projectiles() {
        my_random.add_stage1_random_starts(5000);
        current_wave.load_stage1_projectiles(500, projectile_in_game);
    }

    @Override
    public void update(long elapsed) {
        if(!paused) {
            acc_time += elapsed;
        }

        //hearts and bombs
        for (Map.Entry<Integer, Projectile_heart> entry : heart_in_game.entrySet()) {
            if (entry.getValue().out) {
//                heart_in_game.remove(entry.getKey());
            } else {
                entry.getValue().update(elapsed);
                check_heart_collision(entry.getValue());
            }
        }
        for (Map.Entry<Integer, Projectile_bomb> entry : bomb_in_game.entrySet()) {
            if (entry.getValue().out) {
//                bomb_in_game.remove(entry.getKey());
            } else {
                entry.getValue().update(elapsed);
//                check_bomb_collision(entry.getValue());
            }
        }

        for (Map.Entry<Integer, Projectile_ink> entry : ink_in_game.entrySet()) {
            if (entry.getValue().out) {
//                bomb_in_game.remove(entry.getKey());
            } else {
                entry.getValue().update(elapsed);
//                check_bomb_collision(entry.getValue());
            }
        }

        if (new_wave_begins) {
            Log.d(TAG, "update: " + Long.toString(acc_time) + "spawn timer " + Long.toString(current_wave.spawn_interval));
//            reset obj_counter to reload projectiles from the list
            Integer lcc = new Integer(live_creation_count);
            for (int h = 0; h < lcc; h++) {
                float[] rand_start = my_random.random_start_map.get(spawn_all_start).next();
                obj_counter.put("heart", obj_counter.get("heart") + 1);
                Projectile_heart heart = new Projectile_heart(s_width, s_height,
                        rand_start[0],
                        rand_start[1],
                        projSpeed * randomNumberInRange(75,150) / 100,
                        projSpeed * randomNumberInRange(75,150)  / 100);
                heart_in_game.put(obj_counter.get("heart"), heart);
                heart_in_game.get(obj_counter.get("heart")).
                        load_projectile("heart", game);
                live_creation_count -= 1;
            }


            Integer bcc = new Integer(bomb_creation_count);
            for (int b = 0; b < bcc; b++) {
                float[] rand_start = my_random.random_start_map.get(spawn_all_start).next();
                obj_counter.put("bomb", obj_counter.get("bomb") + 1);
                Projectile_bomb bomb = new Projectile_bomb(s_width, s_height,
                        rand_start[0],
                        rand_start[1],
                        projSpeed * randomNumberInRange(75,150)  / 100,
                        projSpeed * randomNumberInRange(75,150) / 100);
                bomb_in_game.put(obj_counter.get("bomb"), bomb);
                bomb_in_game.get(obj_counter.get("bomb")).
                        load_projectile("bomb", game);
                bomb_creation_count -= 1;
            }


            Integer icc = new Integer(ink_creation_count);
            for (int i = 0; i < icc; i++) {
                obj_counter.put("ink", obj_counter.get("ink") + 1);
                Projectile_ink ink = new Projectile_ink(s_width, s_height,
                        0,
                        0,
                        0,
                        0,
                        game,
                        current_wave);
                ink_in_game.put(obj_counter.get("ink"), ink);
                ink_in_game.get(obj_counter.get("ink")).
                        load_projectile();
                ink_creation_count -= 1;

            }

            //zone1
            // banana, coconut
            for (int j = 0; j < obj_types.size(); j++) {
                for (Map.Entry<Integer, Projectile> entry : projectile_in_game.get(obj_types.get(j)).entrySet()) {
                    if (entry.getValue().out) {
//                        projectile_in_game.get(obj_types.get(j)).remove(entry.getKey());
                    } else {
                        entry.getValue().update(elapsed);
                        check_collision(entry.getValue());
                    }
                }
            }
            if (!current_wave.check_wave_completion()) {
                if (!current_wave.boss_stage) {
                    if (acc_time > spawn_interval && count_obj_in_screen < max_projectiles) {
                        acc_time = 0;

                        for (int j = 0; j < obj_types.size(); j++) {
                            for (int i = 0; i < my_random.random_map.get(Integer.toString(wave_counter) + obj_types.get(j)).next(); i++) {
                                count_obj_in_screen += 1;
                                obj_counter.put(obj_types.get(j), obj_counter.get(obj_types.get(j)) + 1);
                                projectile_in_game.get(obj_types.get(j)).get(obj_counter.get(obj_types.get(j))).
                                        load_projectile();
                            }
                        }
                    }

                } else if (current_wave.boss_stage) {
                    for (Map.Entry<Integer, Projectile_stage1_boss> entry : boss_in_game.entrySet()) {
                        if (entry.getValue().out) {
                        } else {
                            entry.getValue().update(elapsed);
                            check_boss_collision(entry.getValue());
                        }
                    }
                    if (!current_wave.boss_spawned) {
                        for (int i = 0; i < current_wave.tree_boss_count; i++) {
                            obj_counter.put("boss", obj_counter.get("boss") + 1);
                            Projectile_stage1_boss boss = new Projectile_stage1_boss(
                                    s_width,
                                    s_height,
                                    randomNumberInRange(s_width / 10, s_width * 9 / 10),
                                    0,
                                    0,
                                    randomNumberInRange(3, 5),
                                    "tree_boss",
                                    game
                            );
                            boss.setDifficulty(current_wave.boss_difficulty);
                            boss_in_game.put(obj_counter.get("boss"), boss);
                            boss_in_game.get(obj_counter.get("boss")).
                                    load_projectile();
                        }

                        for (int i = 0; i < current_wave.urchin_boss_count; i++) {
                            obj_counter.put("boss", obj_counter.get("boss") + 1);
                            Projectile_stage1_boss boss = new Projectile_stage1_boss(
                                    s_width,
                                    s_height,
                                    randomNumberInRange(s_width / 10, s_width * 9 / 10),
                                    0,
                                    0,
                                    randomNumberInRange(3, 5),
                                    "urchin_boss",
                                    game
                            );
                            boss.setDifficulty(current_wave.boss_difficulty);
                            boss_in_game.put(obj_counter.get("boss"), boss);
                            boss_in_game.get(obj_counter.get("boss")).
                                    load_projectile();
                        }
                        current_wave.boss_spawned = true;
                    }

                }
            } else if (count_obj_in_screen > 0) {
            } else {
                end_wave();
                acc_time = 0;

            }
        } else if (acc_time > current_wave.wave_wait) {
            new_wave_begins = true;
            acc_time = 0;
            tutorial_intro_new_enemy();
//            Log.d(TAG, "update: wave " + Integer.toString(wave_counter)+" start!");
        } else {
        }
    }

    @Override
    public void draw(Canvas canvas) {
        //draw coconuts and bananas
        for (int j = 0; j < obj_types.size(); j++) {
            if (projectile_in_game.get(obj_types.get(j)) == null) {
            } else {
                //Iterator it = projectile_in_game.get(obj_types.get(j)).entrySet().iterator();
                for (Map.Entry<Integer, Projectile> entry : projectile_in_game.get(obj_types.get(j)).entrySet()) {
                    if (entry.getValue().out || !entry.getValue().spawned) {
                    } else {
                        entry.getValue().draw(canvas);
                    }
                }
            }
        }

        for (Map.Entry<Integer, Projectile_heart> entry : heart_in_game.entrySet()) {
            if (entry.getValue().out) {
            } else {
                entry.getValue().draw(canvas);
            }
        }
        for (Map.Entry<Integer, Projectile_bomb> entry : bomb_in_game.entrySet()) {
            if (entry.getValue().out) {
            } else {
                entry.getValue().draw(canvas);
            }
        }

        for (Map.Entry<Integer, Projectile_stage1_boss> entry : boss_in_game.entrySet()) {
            if (entry.getValue().out) {
            } else {
                entry.getValue().draw_boss(canvas);
            }
        }

    }

    @Override
    public void draw_ink(Canvas canvas){
        for (Map.Entry<Integer, Projectile_ink> entry : ink_in_game.entrySet()) {
            if (entry.getValue().out) {
            } else {
                entry.getValue().draw_ink(canvas,entry.getValue().paint);
            }
        }
    }

    @Override
    public void adjust_game_difficulty() {
    }

    @Override
    public Bitmap getBitmap(int drawableRes) {
        Drawable drawable = ResourcesCompat.getDrawable(resources,drawableRes,null);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    @Override
    public void kaboom() {
        for (int j = 0; j < obj_types.size(); j++) {
            for (Map.Entry<Integer, Projectile> entry : projectile_in_game.get(obj_types.get(j)).entrySet()) {
                if (entry.getValue().dead) {
                } else {
                    entry.getValue().destroyed(game);
                }

            }
        }
    }

    @Override
    public HashMap<String, HashMap<String, Integer>> getMultiProjectile() {
        return multiProjectile;
    }

    @Override
    public Wave getCurrent_wave() {
        return current_wave;
    }

    //creating projectiles and objects
    //create hearts
    @Override
    public void createLives(int k) {
        live_creation_count += k;
    }

    //create bomb
    @Override
    public void createBombs(Integer i) {
        bomb_creation_count += i;
    }

    //create inks
    @Override
    public void createInks(Integer j) {
        ink_creation_count =Math.min(1,ink_creation_count+j);
    }

    //get obj_counter and types
    @Override
    public HashMap<String, Integer> getObj_counter() {
        return obj_counter;
    }

    @Override
    public ArrayList<String> getObj_types() {
        return obj_types;
    }

    //projectiles_in_game
    @Override
    public HashMap<String, ConcurrentHashMap<Integer, Projectile>> getProjectile_in_game() {
        return projectile_in_game;
    }

    @Override
    public ConcurrentHashMap<Integer, Projectile_heart> getHeart_in_game() {
        return heart_in_game;
    }

    @Override
    public ConcurrentHashMap<Integer, Projectile_bomb> getBomb_in_game() {
        return bomb_in_game;
    }

    @Override
    public ConcurrentHashMap<Integer, Projectile_stage1_boss> getstage1_Boss_in_game() {
        return boss_in_game;
    }
    @Override
    public ConcurrentHashMap<Integer, Projectile_stage2_boss> getstage2_Boss_in_game() {
       return null;
    }

    @Override
    public ConcurrentHashMap<Integer, Projectile_laser> getLaser_in_game() {
        return null;
    }

    //set ball
    @Override
    public void setBall(Ball ball) {
        this.ball = ball;
    }

    @Override
    public int getCount_obj_in_screen() {
        return count_obj_in_screen;
    }

    @Override
    public void setCount_obj_in_screen(int count_obj_in_screen) {
        this.count_obj_in_screen = count_obj_in_screen;
    }

    //get wave
    @Override
    public int getWave_counter() {
        return wave_counter;
    }

    @Override
    public void setWave_counter(int wave_counter) {
        this.wave_counter = wave_counter;
    }

    //get random
    @Override
    public MyRandom getMy_random() {
        return my_random;
    }

    @Override
    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    @Override
    public void setKill_qty_tracker(int kill_qty_tracker) {
        this.kill_qty_tracker = kill_qty_tracker;
    }
    @Override
    public void setScore_tracker(int score_tracker) {
        this.score_tracker = score_tracker;
    }

    //bananas and coconuts
    public void check_collision(Projectile enemy) {
        if (enemy.dead) {
        } else {
            Rect screenRect = enemy.getScreenRect();
            for (int j = 1; j < game.char_offense_modes_count; j++) {
                for (Map.Entry<Integer, Projectile_char_offense> entry : game.char_offense.get(j).entrySet()) {
                    if (entry.getValue().dead) {
                    } else {
                        if (entry.getValue().getX() + entry.getValue().getScreenRect().width() / 2 >= screenRect.left &&
                                entry.getValue().getX() + entry.getValue().getScreenRect().width() / 2 <= screenRect.right &&
                                entry.getValue().getY() + entry.getValue().getScreenRect().height() / 2 >= screenRect.top &&
                                entry.getValue().getY() + entry.getValue().getScreenRect().height() / 2 <= screenRect.bottom) {

                            enemy.destroyed(game);
                            return;
                        }
                    }
                }
            }


            if (ball.getX() + ball.getRect().width() / 2 >= screenRect.left &&
                    ball.getX() + ball.getRect().width() / 2 <= screenRect.right &&
                    ball.getY() + ball.getRect().height() / 2 >= screenRect.top &&
                    ball.getY() + ball.getRect().height() / 2  / 2 <= screenRect.bottom) {
                enemy.destroyed_by_collision(game);
            }
        }
    }
    //boss
    public void check_boss_collision(Projectile_stage1_boss enemy) {
        if (enemy.dead) {
        } else {
            Rect screenRect = enemy.getScreenRect();
            if (ball.getX() + ball.getScreenRect().width() / 2 >= screenRect.left &&
                    ball.getX() + ball.getScreenRect().width() / 2 <= screenRect.right &&
                    ball.getY() + ball.getScreenRect().height() / 2 >= screenRect.top &&
                    ball.getY() + ball.getScreenRect().height() / 2 <= screenRect.bottom) {
                enemy.destroyed_by_collision(game);
            }
        }
    }

    public void check_heart_collision(Projectile_heart enemy) {
        if (enemy.dead) {
        } else {
            Rect screenRect = enemy.getScreenRect();
            if (screenRect.left >= ball.getScreenRect().left + error_tolerance &&
                    screenRect.right <= ball.getScreenRect().right - error_tolerance &&
                    screenRect.bottom <= ball.getScreenRect().bottom - error_tolerance &&
                    screenRect.top >= ball.getScreenRect().top + error_tolerance) {
                enemy.destroyed_by_collision(game);
            }
        }
    }


    //wave setting
    public void create_waves() {
        Log.d(TAG, "create_waves: starting to create waves");
        total_wave_count = 1;

        float base_proj_speed  = 0.5f;
        int base_wave_interval = 1000;
        int base_max_projectiles = 7;
        float phase_1_speed_multiplier = 1f;
        float phase_2_speed_multiplier = 1.05f;
        float phase_3_speed_multiplier = 1.1f;
        float phase_4_speed_multiplier = 1.2f;
        //create wave according to difficulty
        //12
        create_coconut_waves(3, 3, null, base_proj_speed,"top",base_max_projectiles,base_wave_interval);
        create_banana_waves(3, 2,null, base_proj_speed,"top",base_max_projectiles,base_wave_interval);
        create_coconut_black_waves(2,5,null, base_proj_speed,"top_bottom",10,base_wave_interval);
        create_boss_wave(1, 0,5,null, base_proj_speed,base_max_projectiles,base_wave_interval);
        //probabilities -> normal, small, big
        create_coconut_black_waves(1,5,new double[] {0.5,0.4,0.1}, base_proj_speed,"top_bottom",base_max_projectiles,base_wave_interval);
        create_boss_wave(2, 0,5,null, base_proj_speed,base_max_projectiles,base_wave_interval);

        //8
        create_mixed_waves(1, 3,1,1,0,0, 0, new double[] { 0.8,0.2,0.1 }, base_proj_speed*phase_2_speed_multiplier,"top",base_max_projectiles,base_wave_interval);
        create_mixed_waves(1, 3,1,1,0,0, 1, new double[] { 0.8,0.2,0.1 }, base_proj_speed*phase_2_speed_multiplier,"top_bottom",base_max_projectiles,base_wave_interval);
        create_boss_wave(1, 0,10,new double[] { 0,0.8,0.2 }, base_proj_speed*phase_2_speed_multiplier,base_max_projectiles,base_wave_interval);
        create_boss_wave(2, 0,1,new double[] { 0.5,0.2,0.3 }, base_proj_speed*phase_2_speed_multiplier,base_max_projectiles,base_wave_interval);
        create_coconut_black_waves(2,2,new double[] {0.0,0.9,0.1}, base_proj_speed*phase_2_speed_multiplier,"top_bottom",base_max_projectiles,base_wave_interval);
        create_boss_wave(3, 0,10,new double[] { 0.8,0.2,0.1 }, base_proj_speed*phase_2_speed_multiplier,base_max_projectiles,base_wave_interval);

        //15
        create_coconut_angry_waves(3,3, new double[] { 0.8,0.2,0.1 }, base_proj_speed*phase_3_speed_multiplier,"top_left_right",base_max_projectiles,base_wave_interval);
        create_angry_banana_waves(1,3, new double[] { 0.8,0.2,0.1 }, base_proj_speed*phase_3_speed_multiplier,"top_left_right",base_max_projectiles,base_wave_interval);
        create_mixed_waves(2, 2,1,0,1,0,0, new double[] { 0.0,0.5,0.5 }, base_proj_speed*phase_3_speed_multiplier,"top_left_right",base_max_projectiles,base_wave_interval);
        create_mixed_waves(1, 2,0,1,0,1, 0,  new double[] { 0.5,0.0,0.5 }, base_proj_speed*phase_3_speed_multiplier,"top_left_right",base_max_projectiles,base_wave_interval);
        create_boss_wave(0, 1,5,new double[] { 0.0,0.0,1.0 }, base_proj_speed*phase_3_speed_multiplier,base_max_projectiles,base_wave_interval);
        create_coconut_black_waves(2,3,new double[] {0.0,0.2,0.8}, base_proj_speed*phase_3_speed_multiplier,"top_bottom",base_max_projectiles,base_wave_interval);
        create_boss_wave(0, 2,10,null, base_proj_speed*phase_3_speed_multiplier,base_max_projectiles,base_wave_interval);
        create_boss_wave(3, 1,5,new double[] { 0.1,0.8,0.1 }, base_proj_speed*phase_3_speed_multiplier,base_max_projectiles,base_wave_interval);
        create_coconut_angry_waves(2,3, new double[] { 0.8,0.2,0.1 }, base_proj_speed*phase_3_speed_multiplier,"top_bottom",base_max_projectiles,base_wave_interval);
        create_boss_wave(4, 0,1,null, base_proj_speed*phase_3_speed_multiplier,base_max_projectiles,base_wave_interval);

        //10
        create_coconut_waves(1, 20, new double[] { 0.3,0.3,0.4 }, base_proj_speed*phase_4_speed_multiplier,"top_bottom",base_max_projectiles,base_wave_interval);
        create_mixed_waves(1, 1,1,1,0,0,0, new double[] { 0.5,0.25,0.25 }, base_proj_speed*phase_4_speed_multiplier,"bottom",base_max_projectiles,base_wave_interval);
        create_mixed_waves(1, 1,1,0,1,1,0, new double[] { 0.5,0.25,0.25 }, base_proj_speed*phase_4_speed_multiplier,"top_bottom",base_max_projectiles,base_wave_interval);
        create_banana_waves(2, 3, new double[] { 0.3,0.5,0.2 }, base_proj_speed*phase_4_speed_multiplier,"bottom",base_max_projectiles,base_wave_interval);
        create_boss_wave(3, 0,20,new double[] { 0.0,0.0,1.0 }, base_proj_speed*phase_4_speed_multiplier,base_max_projectiles,base_wave_interval);
        create_boss_wave(0, 2,20,new double[] { 0.3,0.4,0.3 }, base_proj_speed*phase_4_speed_multiplier,base_max_projectiles,base_wave_interval);
        create_mixed_waves(1, 1,1,1,1,1,1, new double[] { 0.4,0.3,0.3 }, base_proj_speed*phase_4_speed_multiplier,"top_down_left_right",base_max_projectiles,base_wave_interval);
        create_boss_wave(0, 3,1,new double[] { 0.3,0.4,0.3 }, base_proj_speed*phase_4_speed_multiplier,base_max_projectiles,base_wave_interval);
        create_boss_wave(5,2,10,new double[] { 0.3,0.4,0.3 }, base_proj_speed*phase_4_speed_multiplier,base_max_projectiles,base_wave_interval);
    //total waves = 45
    }

    //can actially optmise this part ot make it more efficient however, the number is still problematic
    public void create_coconut_waves(int coconut_waves,
                                     int start_count,
                                     double[] size_probabilities,
                                     float projectile_speed,
                                     String spawn_start,
                                     int max_projectiles,
                                     int wave_interval) {
        for (int x = 1; x < coconut_waves + 1; x++) {
            HashMap<String, Integer> wave_completion = set_wave_completion(
                    Math.max((x - 1) * randomNumberInRange(4, 7) * start_count + 1 + start_count,x*20),
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0);
//            Log.d(TAG, "create_waves: finished set_wave_completion");
            Wave wave = new Wave(
                    wave_completion,
                    projectile_speed,
                    set_spawn_rate(min(1 + (x - 1 + start_count) * randomNumberInRange(3, 5), 7), 0,0,0,0),
                    randomNumberInRange(max_projectiles-2,max_projectiles+1),
                    randomNumberInRange(wave_interval-500, wave_interval+500),
                    spawn_start,
                    1000,
                    my_random,
                    size_probabilities
            );

            //create random spawn rates
            //create random spawn rates
            add_random_spawn_rate_to_random_map(wave);
            wave_info.put(total_wave_count, wave);
            total_wave_count = total_wave_count + 1;
        }
    }

    public void create_banana_waves(int banana_waves,
                                    int start_count,
                                    double[] size_probabilities,
                                    float projectile_speed,
                                    String spawn_start,
                                    int max_projectiles,
                                    int wave_interval) {
        for (int b = 1; b < banana_waves + 1; b++) {
            HashMap<String, Integer> wave_completion = set_wave_completion(
                    0,
                    Math.max((b - 1) * randomNumberInRange(4, 7) * start_count + 1 + start_count,b*15),
                    0,
                    0,
                    0,
                    0,
                    0,
                    0);
            Wave wave = new Wave(
                    wave_completion,
                    projectile_speed,
                    set_spawn_rate(0,
                            min(1 + (b - 1 + start_count) * randomNumberInRange(3, 5), 5),0,0,0),
                    randomNumberInRange(max_projectiles-2,max_projectiles+1),
                    randomNumberInRange(wave_interval-500, wave_interval+500),
                    spawn_start,
                    1000
                    ,my_random,
                    size_probabilities
            );
            //create random spawn rates
            add_random_spawn_rate_to_random_map(wave);
            wave_info.put(total_wave_count, wave);
            total_wave_count = total_wave_count + 1;
        }

    }

    public void create_coconut_angry_waves(int coconut_angry_waves,
                                           int start_count,
                                           double[] size_probabilities,
                                           float projectile_speed,
                                           String spawn_start,
                                           int max_projectiles,
                                           int wave_interval) {
        for (int x = 1; x < coconut_angry_waves + 1; x++) {
            HashMap<String, Integer> wave_completion = set_wave_completion(
                    0,
                    0,
                    Math.max((x - 1) * randomNumberInRange(4, 7) * start_count + 1 + start_count,x*10),
                    0,
                    0,
                    0,
                    0,
                    0);
//            Log.d(TAG, "create_waves: finished set_wave_completion");
            Wave wave = new Wave(
                    wave_completion,
                    projectile_speed,
                    set_spawn_rate(0, 0,min(1 + (x - 1 + start_count) * randomNumberInRange(3, 5), 7),0,0),
                    randomNumberInRange(max_projectiles-2,max_projectiles+1),
                    randomNumberInRange(wave_interval-500, wave_interval+500),
                    spawn_start,
                    1000,
                    my_random,
                    size_probabilities
            );

            //create random spawn rates
            //create random spawn rates
            add_random_spawn_rate_to_random_map(wave);
            wave_info.put(total_wave_count, wave);
            total_wave_count = total_wave_count + 1;
        }
    }

    public void create_coconut_black_waves(int coconut_black_waves,
                                           int start_count,
                                           double[] size_probabilities,
                                           float projectile_speed,
                                           String spawn_start,
                                           int max_projectiles,
                                           int wave_interval) {
        for (int x = 1; x < coconut_black_waves + 1; x++) {
            HashMap<String, Integer> wave_completion = set_wave_completion(
                    0,
                    0,
                    0,
                    0,
                    Math.max((x - 1) * randomNumberInRange(4, 7) * start_count + 1 + start_count,x*10),
                    0,
                    0,
                    0);
//            Log.d(TAG, "create_waves: finished set_wave_completion");
            Wave wave = new Wave(
                    wave_completion,
                    projectile_speed,
                    set_spawn_rate(0, 0,0,0,min(1 + (x - 1 + start_count) * randomNumberInRange(3, 5), 7)),
                    randomNumberInRange(max_projectiles-2,max_projectiles+1),
                    randomNumberInRange(wave_interval-500, wave_interval+500),
                    spawn_start,
                    1000,
                    my_random,
                    size_probabilities
            );

            //create random spawn rates
            add_random_spawn_rate_to_random_map(wave);
            wave_info.put(total_wave_count, wave);
            total_wave_count = total_wave_count + 1;
        }
    }

    public void create_angry_banana_waves(int angry_banana_waves,
                                          int start_count,
                                          double[] size_probabilities,
                                          float projectile_speed,
                                          String spawn_start,
                                          int max_projectiles,
                                          int wave_interval) {
        for (int b = 1; b < angry_banana_waves + 1; b++) {
            HashMap<String, Integer> wave_completion = set_wave_completion(
                    0,
                    0,
                    0,
                    Math.max((b - 1) * randomNumberInRange(4, 7) * start_count + 1 + start_count,b*10),
                    0,
                    0,
                    0,
                    0);
            Wave wave = new Wave(
                    wave_completion,
                    projectile_speed,
                    set_spawn_rate(0,
                            0,
                            0,
                            (b - 1) * randomNumberInRange(4, 7) + 1 + start_count,
                            0),
                    randomNumberInRange(max_projectiles-2,max_projectiles+1),
                    randomNumberInRange(wave_interval-500, wave_interval+500),
                    spawn_start,
                    1000
                    ,my_random,
                    size_probabilities
            );
            //create random spawn rates
            add_random_spawn_rate_to_random_map(wave);
            wave_info.put(total_wave_count, wave);
            total_wave_count = total_wave_count + 1;
        }

    }

    public void create_mixed_waves(int mix_waves, int start_count,
                                   int coconut, int banana,
                                   int coconut_angry, int angry_banana,
                                   int coconut_black,
                                   double[] size_probabilities,
                                   float projectile_speed,
                                   String spawn_start,
                                   int max_projectiles,
                                   int wave_interval) {
        for (int m = 1; m < mix_waves + 1; m++) {
            HashMap<String, Integer> wave_completion = set_wave_completion(
                    randomNumberInRange(10, 15) * (m + start_count) * coconut,
                    randomNumberInRange(4, 8) * (m + start_count) * banana,
                    randomNumberInRange(5, 8) * (m + start_count) * coconut_angry,
                    randomNumberInRange(2, 4) * (m + start_count) * angry_banana,
                    randomNumberInRange(1,2) *(m+start_count)* coconut_black,
                    25 * (m + start_count),
                    0,
                    0);
            Wave wave = new Wave(
                    wave_completion,
                    projectile_speed,
                    set_spawn_rate(min(1 + (m + start_count) * randomNumberInRange(2, 3), 5)* coconut,
                            min(1+ (m + start_count) * randomNumberInRange(1,2) , 3)* banana,
                            min(1 + (m + start_count) * randomNumberInRange(2, 3), 3)* coconut_angry,
                            min(1+ (m + start_count) * randomNumberInRange(1,2) , 3) * angry_banana,
                            randomNumberInRange(1 , 2) * coconut_black),
                    randomNumberInRange(max_projectiles-2,max_projectiles+1),
                    randomNumberInRange(wave_interval-500, wave_interval+500),
                    spawn_start,
                    1000,
                    my_random,
                    size_probabilities
            );

            //create random spawn rates
            add_random_spawn_rate_to_random_map(wave);

            wave_info.put(total_wave_count, wave);
//            Log.d(TAG, "create_waves: created wave " + Integer.toString(total_wave_count));
            total_wave_count = total_wave_count + 1;
        }
    }

    public void create_boss_wave(int tree_boss_count, int urchin_boss_count, int boss_difficulty, double[] size_probabilities, float projectile_speed,
                                 int max_projectiles,
                                 int wave_interval) {
//        for (int x = 1 ; x <= boss_waves; x++){
        HashMap<String, Integer> wave_completion = set_wave_completion(
                0,
                0,
                0,
                0,
                0,
                0,
                tree_boss_count,
                urchin_boss_count);
//        Log.d(TAG, "create_waves: finished set_wave_completion");
        Wave wave = new Wave(
                wave_completion,
                projectile_speed,
                set_spawn_rate(randomNumberInRange(3, 4) - 1, randomNumberInRange(1, 2) - 1,
                        randomNumberInRange(3, 4) - 1, randomNumberInRange(1, 2) - 1,0),
                randomNumberInRange(max_projectiles-2,max_projectiles+1),
                randomNumberInRange(wave_interval-500, wave_interval+500),
                "irrelevant",
                4000
                ,my_random,
                size_probabilities
        );
        wave.set_stage1_bosses(tree_boss_count,urchin_boss_count, boss_difficulty);
        //create random spawn rates
        add_random_spawn_rate_to_random_map(wave);

//        Log.d(TAG, "create_waves: finished creating new boss wave");
        wave_info.put(total_wave_count, wave);
        total_wave_count = total_wave_count + 1;
//        }
    }


    public void end_wave() {
        game.loaded = false;

        //reset char offense to 1
        game.change_char_offense(-1);

//        if(wave_counter == total_wave_count-1){
//            game.stage_completed();
//        }
        if(wave_counter == 9){
            game.stage_completed();
        }
        else {
            count_obj_in_screen = 0;
            //stars
            current_wave.stars = game.getStar_calculator().eval_stars(game.score-score_tracker);
//            Log.d(TAG, "end_wave: current stars" + current_wave.stars);
            update_stars(current_wave);
            show_wave_end_log(current_wave.stars, wave_counter);

            new_wave_begins = false;
            wave_counter++;
            //stars
            game.create_star_calculator(wave_counter);
            if (wave_counter % 10 == 0) {
                createLives(randomNumberInRange(1, 2));
            }
            current_wave = wave_info.get(wave_counter);
            setup_wave_to_game(current_wave);
            //refresh randoms
            my_random.add_stage1_random_starts(5000);

            //clear
            for (int j = 0; j < obj_types.size(); j++) {
                obj_counter.put(obj_types.get(j),0);
            }
            current_wave.load_game(game);
            current_wave.get_dim(s_width, s_height);
            current_wave.load_stage1_projectiles(500, projectile_in_game);

            game.slice_animation_in_game = new ConcurrentHashMap<>();
            game.slice_effect_counter = 0;
            game.press_effect_in_game = new ConcurrentHashMap<>();
            game.press_effect_counter = 0;
        }
        System.gc();
    }

    public void setup_wave_to_game(Wave current_wave) {
        spawn_interval = current_wave.spawn_interval;
        spawn_rate = current_wave.spawn_rate;
        max_projectiles = current_wave.max_projectiles;
//        Log.d(TAG, "setup_wave_to_game: " + max_projectiles);
        projSpeed = current_wave.projSpeed;
        ((MainActivity_stage) game.getContext()).updateWaveText(wave_counter);
        ((MainActivity_stage) game.getContext()).updatereq(current_wave.wave_completion, game.current_stage);


    }

    public HashMap<String, Integer> set_wave_completion(int coconut, Integer banana,
                                                        int coconut_angry, int angry_banana,
                                                        int coconut_black,
                                                        int score,
                                                        int tree_boss, int urchin_boss
                                                        ) {
        HashMap<String, Integer> wave_completion = new HashMap<>();
        wave_completion.put("coconut", coconut);
        wave_completion.put("banana", banana);
        wave_completion.put("coconut_angry", coconut_angry);
        wave_completion.put("banana_angry", angry_banana);
        wave_completion.put("coconut_black", coconut_black);
        wave_completion.put("score", score);
        wave_completion.put("tree_boss", tree_boss);
        wave_completion.put("urchin_boss", urchin_boss);
        return wave_completion;
    }

    public HashMap<String, Integer> set_spawn_rate(int coconut, int banana,
                                                   int coconut_angry, int angry_banana,
                                                    int coconut_black) {
        HashMap<String, Integer> spawn_rate = new HashMap<>();
        spawn_rate.put("coconut", coconut);
        spawn_rate.put("banana", banana);
        spawn_rate.put("coconut_angry", coconut_angry);
        spawn_rate.put("banana_angry", angry_banana);
        spawn_rate.put("coconut_black", coconut_black);
        return spawn_rate;
    }

    public static int randomNumberInRange(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }



    public void load_stage1() {
        obj_counter.put("coconut", 0);
        obj_counter.put("coconut_angry", 0);
        obj_counter.put("banana", 0);
        obj_counter.put("banana_angry", 0);
        obj_counter.put("coconut_black",0);
        obj_counter.put("heart", 0);
        obj_counter.put("bomb", 0);
        obj_counter.put("ink", 0);
        obj_counter.put("boss", 0);


        spawn_all_start = "top_down_left_right";
        live_creation_count = 0;
        bomb_creation_count = 0;


        //zone1 files
        //fill hashmap - coconut
        HashMap<String, Integer> coconut = new HashMap<>();
        coconut.put("damage", 1);
        coconut.put("hits", 1);
        coconut.put("p_score", 1);
        coconut.put("wall_bounce_lives", 1);
        multiProjectile.put("coconut", coconut);

        //fill hashmap - coconut
        HashMap<String, Integer> coconut_angry = new HashMap<>();
        coconut_angry.put("damage", 1);
        coconut_angry.put("hits", 2);
        coconut_angry.put("p_score", 2);
        coconut_angry.put("wall_bounce_lives", 1);
        multiProjectile.put("coconut_angry", coconut_angry);

        //fill hashmap - coconut
        HashMap<String, Integer> coconut_black = new HashMap<>();
        coconut_black.put("damage", 1);
        coconut_black.put("hits", 999);
        coconut_black.put("p_score", 2);
        coconut_black.put("wall_bounce_lives", 1);
        multiProjectile.put("coconut_black", coconut_black);

        HashMap<String, Integer> banana = new HashMap<>();
        banana.put("damage", 1);
        banana.put("hits", 2);
        banana.put("p_score", 2);
        banana.put("wall_bounce_lives", 2);
        multiProjectile.put("banana", banana);

        HashMap<String, Integer> banana_angry = new HashMap<>();
        banana_angry.put("damage", 1);
        banana_angry.put("hits", 2);
        banana_angry.put("p_score", 2);
        banana_angry.put("wall_bounce_lives", 3);
        multiProjectile.put("banana_angry", banana_angry);


        HashMap<String, Integer> tree_boss = new HashMap<>();
        tree_boss.put("collision_image", null);
        tree_boss.put("damage", 1);
        tree_boss.put("hits", 20);
        tree_boss.put("p_score", 100);
        tree_boss.put("wall_bounce_lives", null);
        multiProjectile.put("tree_boss", tree_boss);

        HashMap<String, Integer> urchin_boss = new HashMap<>();
        urchin_boss.put("collision_image", null);
        urchin_boss.put("damage", 1);
        urchin_boss.put("hits", 40);
        urchin_boss.put("p_score", 200);
        urchin_boss.put("wall_bounce_lives", null);
        multiProjectile.put("urchin_boss", urchin_boss);

        HashMap<String, Integer> heart = new HashMap<>();
        heart.put("damage", -1);
        heart.put("hits", 1);
        heart.put("p_score", 0);
        heart.put("wall_bounce_lives", 10);
        multiProjectile.put("heart", heart);

        HashMap<String, Integer> bomb = new HashMap<>();
        bomb.put("damage", 0);
        bomb.put("hits", 1);
        bomb.put("p_score", 3);
        bomb.put("wall_bounce_lives", 10);
        multiProjectile.put("bomb", bomb);


        //different object type names
        obj_types.add("coconut");
        obj_types.add("banana");
        obj_types.add("coconut_angry");
        obj_types.add("banana_angry");
        obj_types.add("coconut_black");

        for (int j = 0; j < obj_types.size(); j++) {
            ConcurrentHashMap<Integer, Projectile> temp = new ConcurrentHashMap<>();
            projectile_in_game.put(obj_types.get(j), temp);
        }
    }

    public void update_stars(Wave current_wave) {
        HashMap<Integer,Integer> wave_stars = new HashMap<>();
        SharedPreferences local_db = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String stage1_wave_stars_local_store = local_db.getString("stage1_wave_stars_local_store", "");
        if (stage1_wave_stars_local_store == "") {
            for (int i = 0; i < total_wave_count; i++) {
                wave_stars.put(i + 1, 0);
            }
//            Log.d(TAG, "update_stars: cannot find local database");
            SharedPreferences.Editor editor = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
            Type type = new TypeToken<HashMap<Integer, Integer>>() {
            }.getType();
            String json = gson.toJson(wave_stars, type);
            editor.putString("stage1_wave_stars_local_store", json);
            editor.apply();
        } else {
            HashMap<Integer, Integer> stage1_wave_stars = new Gson().fromJson(stage1_wave_stars_local_store,
                    new TypeToken<HashMap<Integer, Integer>>() {}.getType());
            if(current_wave.stars > stage1_wave_stars.get(wave_counter)){
//                Log.d(TAG, "update_stars: done update for wave" + wave_counter);
                SharedPreferences.Editor editor = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                stage1_wave_stars.put(wave_counter,current_wave.stars);
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
                Type type = new TypeToken<HashMap<Integer, Integer>>() {
                }.getType();
                String json = gson.toJson(stage1_wave_stars, type);
                editor.putString("stage1_wave_stars_local_store", json);
                editor.apply();
            }
        }
    }



    public void create_wave_end_log(final int stars, final int wave_counter){
        paused = true;
        game.pause_background =true;
        //now let's do the dialog creation
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(game.getContext());
        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(game.getContext());
        View mView = inflater.inflate(R.layout.activity_wave_end, null);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();

        //set stars
        ImageView s1 = mView.findViewById(R.id.wave_end_star_achievement_1);
        ImageView s2 = mView.findViewById(R.id.wave_end_star_achievement_2);
        ImageView s3 = mView.findViewById(R.id.wave_end_star_achievement_3);
        HashMap<Integer,ImageView> go_stars = new HashMap<>();
        go_stars.put(1,s1);
        go_stars.put(2,s2);
        go_stars.put(3,s3);

        HashMap<Integer,Integer> star_images = new HashMap<>();
        star_images.put(1,R.drawable.star1);
        star_images.put(2,R.drawable.star2);
        star_images.put(3,R.drawable.star3);
        star_images.put(0,R.drawable.star0);

        for (int i = 1; i<4; i++) {
            if (i <= stars) {
                update_image_view(go_stars.get(i), star_images.get(i));
            } else {
                update_image_view(go_stars.get(i), star_images.get(0));
            }
        }

        //set statistics - score/kill/tokens
        TextView game_over_score = mView.findViewById(R.id.wave_end_score_achieved);
        game_over_score.setText(Integer.toString(game.score-score_tracker));
        TextView kill_count = mView.findViewById(R.id.wave_end_kill_qty);
        kill_count.setText(" X " +Integer.toString(game.kill_qty - kill_qty_tracker));

        //set title
        TextView wave_end_title = mView.findViewById(R.id.wave_end_title);
        wave_end_title.setText("Wave "+Integer.toString(wave_counter) + " CompletE!");

        //reset kill/score
        kill_qty_tracker = game.kill_qty;
        score_tracker = game.score;

        Button next_wave = mView.findViewById(R.id.wave_end_next_wave);
        next_wave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paused=false;
                game.pause_background =false;
                game.loaded = true;
                dialog.dismiss();

            }
        });

        Button main_menu = mView.findViewById(R.id.wave_end_end_game);
        main_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               game.record_scores();
               dialog.dismiss();
            }
        });


        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }
    private void show_wave_end_log(final int stars, final int wave_counter) {
        //initiate SharedPreferences to collect scores + name
//        Handler mHandler = new Handler(Looper.getMainLooper());
        game.activity_handler = new Handler(Looper.getMainLooper());
        game.activity_handler.postAtTime(new Runnable() {
            @Override
            public void run() {
                synchronized(game.getContext()) {
                    create_wave_end_log(stars, wave_counter);
                    Log.d(TAG, "run: handler fired! show_wave_end_log_stage1");
                }
            }
        },500);

//        Thread t = new Thread(){
//            public void run(){
//                synchronized(game.getContext()) {
//                    create_wave_end_log(stars, wave_counter);
//                }
//            }
//        };
//        t.start();

    }

    private void update_image_view(ImageView image, int id){
        image.setImageResource(id);
        image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        image.setBackgroundColor(Color.TRANSPARENT);
    }

    private void add_random_spawn_rate_to_random_map(Wave wave){
        my_random.add_random_map(Integer.toString(total_wave_count) + "coconut",
                wave.spawn_rate.get("coconut") / 2,
                wave.spawn_rate.get("coconut"), 1000);

        my_random.add_random_map(Integer.toString(total_wave_count) + "banana",
                wave.spawn_rate.get("banana") / 2,
                wave.spawn_rate.get("banana"), 1000);

        my_random.add_random_map(Integer.toString(total_wave_count) + "coconut_angry",
                wave.spawn_rate.get("coconut_angry") / 2,
                wave.spawn_rate.get("coconut_angry"), 1000);

        my_random.add_random_map(Integer.toString(total_wave_count) + "banana_angry",
                wave.spawn_rate.get("banana_angry") / 2,
                wave.spawn_rate.get("banana_angry"), 1000);

        my_random.add_random_map(Integer.toString(total_wave_count) + "coconut_black",
                wave.spawn_rate.get("coconut_black") / 2,
                wave.spawn_rate.get("coconut_black"), 200);
    }


    private void tutorial_intro_new_enemy(){
        for (Map.Entry<String,Integer> entry1 : current_wave.wave_completion.entrySet()) {
            for (Map.Entry<String,Boolean> entry2 : game.tutorial.first_time_tracker.entrySet()) {
                if(entry1.getKey() == entry2.getKey() && entry2.getValue() && entry1.getValue()>0){
                    game.tutorial.show_tutorial_log(game,entry2.getKey());
                }
                else{}
            }
        }
    }


}

//    public void check_bomb_collision(Projectile_bomb enemy) {
//        if (enemy.dead) {
//        } else {
//            Rect screenRect = enemy.getScreenRect();
//            if (screenRect.left >= ball.getScreenRect().left + error_tolerance &&
//                    screenRect.right <= ball.getScreenRect().right - error_tolerance &&
//                    screenRect.bottom <= ball.getScreenRect().bottom - error_tolerance &&
//                    screenRect.top >= ball.getScreenRect().top + error_tolerance) {
//                enemy.destroyed_by_collision(game);
//            }
//        }
//    }

//        Canvas canvas = getHolder().lockCanvas();
//        Paint paint2 = new Paint();
//        paint2.setColor(Color.WHITE);
//        paint2.setStyle(Paint.Style.FILL);
//        paint2.setAlpha(200);
//        canvas.drawPaint(paint2);
//        getHolder().unlockCanvasAndPost(canvas);

