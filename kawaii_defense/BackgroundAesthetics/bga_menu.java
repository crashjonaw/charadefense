package nexidea.kawaii_defense.BackgroundAesthetics;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.view.SurfaceHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import nexidea.kawaii_defense.Projectiles.Projectile_slice_animation;
import nexidea.kawaii_defense.Projectiles.Projectile_tap_effect;

import static nexidea.kawaii_defense.Game.randomNumberInRange;

public class bga_menu {

    private Resources resources;
    private SurfaceHolder holder;
    private Context context;
    private int width;
    private int height;
    private boolean drawing;
    private boolean drawn;
    private long spawn_interval;
    public static Random random;

    private HashMap<Integer,Projectile_tap_effect> tap_effectHashMap = new HashMap<>();
    private int tap_effect_counter;

    private HashMap<Integer,Projectile_slice_animation> slice_effectHashMap = new HashMap<>();
    private int slice_effect_counter;

    public bga_menu(Context context, int width, int height, SurfaceHolder holder, Resources resource) {
        this.resources = resource;
        this.context = context;
        this.width = width;
        this.height = height;
        this.holder = holder;
        random = new Random();
        tap_effectHashMap = new HashMap<>();
        tap_effect_counter = 0;
        slice_effectHashMap = new HashMap<>();
        slice_effect_counter = 0;
        spawn_interval = 0;
    }

    private void init(){

    }

    public void update(long elapsed){
        if(spawn_interval<0){
            int spawn_count = randomNumberInRange(0,4);
            spawn_interval = randomNumberInRange(4000,7000);
            for (int i = 1; i< spawn_count + 1; i ++){
                int x = randomNumberInRange(0,width);
                int y = randomNumberInRange(0,height);
                load_tap_effect_animation(x,y);
                int rand_small_tap_effect = randomNumberInRange(7,15);
                for(int j =1;j<rand_small_tap_effect;j++){
                    load_slice_animation(x,y,false);
                }
            }
        }
        else{
            spawn_interval -= elapsed;
            for(Map.Entry<Integer,Projectile_tap_effect> entry : tap_effectHashMap.entrySet()){
                if(entry.getValue().out) {
                }
                else{
                    entry.getValue().update(elapsed);
                }
            }
            for(Map.Entry<Integer,Projectile_slice_animation> entry : slice_effectHashMap.entrySet()){
                if(entry.getValue().out) {
                }
                else{
                    entry.getValue().update(elapsed);
                }
            }
        }
    }

    public void draw(Canvas canvas){
        for(Map.Entry<Integer,Projectile_tap_effect> entry : tap_effectHashMap.entrySet()){
            if(entry.getValue().out) {
            }
            else{
                entry.getValue().draw(canvas);
            }
        }

        for(Map.Entry<Integer,Projectile_slice_animation> entry : slice_effectHashMap.entrySet()){
            if(entry.getValue().out) {
            }
            else{
                entry.getValue().draw(canvas);
            }
        }

    }

    // generic functions (not related to the game)
    public static int randomNumberInRange(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    private boolean load_tap_effect_animation(int x, int y) {
        Projectile_tap_effect new_press2 = new Projectile_tap_effect(width, height,
                x, (int) y, 0, 0, null,true);
        tap_effect_counter++;
        tap_effectHashMap.put(tap_effect_counter, new_press2);
        tap_effectHashMap.get(tap_effect_counter).load_projectile(null);
        return true;
    }

    private void load_slice_animation(int x, int y, boolean char_move) {
        //slice animation
        Projectile_slice_animation new_slice = new Projectile_slice_animation(width, height,
                x + randomNumberInRange(-width/2, width/2), (int) y + randomNumberInRange(-width/2, width/2), 0, 0);
        slice_effect_counter++;
        slice_effectHashMap.put(slice_effect_counter,new_slice);
        slice_effectHashMap.get(slice_effect_counter).load_projectile(null,char_move,true);
    }
}
