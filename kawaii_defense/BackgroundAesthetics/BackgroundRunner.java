package nexidea.kawaii_defense.BackgroundAesthetics;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceHolder;

import nexidea.kawaii_defense.Animated_background;
import nexidea.kawaii_defense.MenuView;

public class BackgroundRunner extends Thread {
    private Animated_background bg;
    private volatile boolean running = true;
    private static BackgroundRunner backgroundRunner;
    private boolean loaded;
    private bga_menu bga_menu;

    public BackgroundRunner(Animated_background bg) {
        this.bg = bg;
        backgroundRunner = this;
        loaded=false;
        bga_menu = null;
    }
    @Override
    public void run() {
        Log.d("tag", "run: bg is ran!");
//        SurfaceHolder holder = MenuView.holder;
        long lastTime = System.currentTimeMillis();
//      bg runs here
        while (running) {
//             draw stuff.
            long now = System.currentTimeMillis();
            long elapsed = now - lastTime;
            //Log.d("delta time", "delta time: " + elapsed);
            if (elapsed < 1000) {
//                bg.update(elapsed);

                if(bga_menu!=null){

                    Canvas canvas = lock_canvas();
                    if(canvas!=null) {
                        bga_menu.update(elapsed);
                        bg.draw_bg(canvas);
                        bga_menu.draw(canvas);
                    }
                    MenuView.holder.unlockCanvasAndPost(canvas);
                }
            }
            lastTime = now;
        }
    }
    public void shutdown(){
        running= false;
    }


    private Canvas lock_canvas(){
        SurfaceHolder holder = MenuView.holder;
        if(Build.VERSION.SDK_INT >= 26) {
            Canvas canvas = holder.lockHardwareCanvas();
            return canvas;
        }else{
            Canvas canvas =  holder.lockCanvas();
            return canvas;
        }
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public static BackgroundRunner getBackgroundRunner() {
        return backgroundRunner;
    }

    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

    public void setBga_menu(nexidea.kawaii_defense.BackgroundAesthetics.bga_menu bga_menu) {
        this.bga_menu = bga_menu;
    }
}
