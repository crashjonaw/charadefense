package nexidea.kawaii_defense;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import nexidea.kawaii_defense.BackgroundAesthetics.BackgroundRunner;

import static nexidea.kawaii_defense.MenuView.MY_PREFS_NAME;
import static nexidea.kawaii_defense.MenuView.menuviewcontext;

public class MainActivity_wave_selection extends AppCompatActivity implements View.OnClickListener {
    //song stuff
    private static ArrayList<Integer> songArray;
    private static MediaPlayer mp;
    public static Timer timer;
    private int i = 0;
    float volume = 0;
    private boolean paused;
    private TimerTask play_songs;
    private boolean first_song = true;
    private int max_achieved_wave;
    private int total_waves;
    private int s_width;
    private int s_height;

    //button stuff
    private ImageButton back_button;
    private HashMap<Integer, ImageButton> wave_buttons = new HashMap<>();
    private HashMap<Integer, Integer> wave_ids = new HashMap<>();
    private HashMap<Integer, Integer> wave_stars = new HashMap<>();
    private HashMap<Integer,Integer> star_identifier = new HashMap<>();
    //intent
    private Intent go_to_wave;

    //stage
    private int stage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        //get stage
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            stage = extras.getInt("stage");
        }

        switch(stage){
            //alpha test
            case 1:
                total_waves = 9;//45;
                break;
            case 2:
                total_waves = 1;//15;
                break;

            case 3:
                total_waves = 1;//10;
                break;

            default:
                break;
        }

//        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        this.mp = MenuActivity.getMp();
        this.setContentView(R.layout.wave_selection);
//        songArray = getArrSong("stage_selection");
        timer = new Timer(true);
        back_button = findViewById(R.id.back_to_stage_selection);
        back_button.setOnClickListener(this);
        load_wave_selection_bg(stage);
        star_identifier.put(0, R.drawable.star0);
        star_identifier.put(1, R.drawable.star1);
        star_identifier.put(2, R.drawable.star2);
        star_identifier.put(3, R.drawable.star_wave_selection);
        getScreenDimensions();
        get_waves_stars(stage);
        build_dynamic_wave_selection(total_waves);
        get_unlocked_waves(stage);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.back_to_stage_selection){
            Intent stage_selection = new Intent(this,MainActivity_stage_selection.class);
            startActivity(stage_selection);
        }
        else {
            for (Map.Entry<Integer, ImageButton> entry : wave_buttons.entrySet()) {
                if (findViewById(v.getId()) == entry.getValue()) {
                    openWave(entry.getKey());
                    return;
                } else {
                }
            }
            Log.d("error", "onClick: error!!!");
            return;
        }


    }

    public void openWave(int wave) {
        BackgroundRunner.getBackgroundRunner().shutdown();
        go_to_wave = new Intent(this, MainActivity_stage.class);
        go_to_wave.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        go_to_wave.putExtra("wave", wave);
        go_to_wave.putExtra("stage",stage);
        startActivity(go_to_wave);
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        return_to_menu();
    }

    public void return_to_menu() {
        Intent intent = new Intent(this, MenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

//    public static ArrayList<Integer> getArrSong(String venue) {
//        Field[] ID_Fields = R.raw.class.getFields();
//        songArray = new ArrayList<Integer>();
//        for (int i = 0; i < ID_Fields.length; i++) {
//            if (ID_Fields[i].getName() != null &&
//                    ID_Fields[i].getName().contains(venue)) {
//                try {
//                    //Log.d("musicidfieldname",ID_Fields[i].getName());
//                    songArray.add(ID_Fields[i].getInt(null));
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        Log.d("array print", "array" + songArray.toString());
//        return songArray;
//    }

    private void startFadeIn() {
        final int FADE_DURATION = 3000; //The duration of the fade
        //The amount of time between volume changes. The smaller this is, the smoother the fade
        final int FADE_INTERVAL = 250;
        final int MAX_VOLUME = 1; //The volume will increase from 0 to 1
        int numberOfSteps = FADE_DURATION / FADE_INTERVAL; //Calculate the number of fade steps
        //Calculate by how much the volume changes each step
        final float deltaVolume = MAX_VOLUME / (float) numberOfSteps;

        //Create a new Timer and Timer task to run the fading outside the main UI thread
        final Timer timer = new Timer(true);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                fadeInStep(deltaVolume); //Do a fade step
                //Cancel and Purge the Timer if the desired volume has been reached
                if (volume >= 1f) {
                    timer.cancel();
                    timer.purge();
                }
            }
        };

        timer.schedule(timerTask, FADE_INTERVAL, FADE_INTERVAL);
    }

    private void startFadeOut() {
        final int FADE_DURATION = 3000; //The duration of the fade
        //The amount of time between volume changes. The smaller this is, the smoother the fade
        final int FADE_INTERVAL = 250;
        final int MAX_VOLUME = 1; //The volume will increase from 0 to 1
        int numberOfSteps = FADE_DURATION / FADE_INTERVAL; //Calculate the number of fade steps
        //Calculate by how much the volume changes each step
        final float deltaVolume = MAX_VOLUME / (float) numberOfSteps;

        //Create a new Timer and Timer task to run the fading outside the main UI thread
        final Timer timer = new Timer(true);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                fadeOutStep(deltaVolume); //Do a fade step
                //Cancel and Purge the Timer if the desired volume has been reached
                if (volume <= 0.5f) {
                    timer.cancel();
                    timer.purge();
                }
            }
        };

        timer.schedule(timerTask, FADE_INTERVAL, FADE_INTERVAL);
    }

    private void fadeInStep(float deltaVolume) {
        mp.setVolume(volume, volume);
        volume += deltaVolume;

    }

    private void fadeOutStep(float deltaVolume) {
        mp.setVolume(volume, volume);
        volume -= deltaVolume;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("timer", "onPause: timer canceled and purged");
        paused = true;
        if (mp != null) {
            mp.stop();
            mp.reset();
        }
        BackgroundRunner.getBackgroundRunner().shutdown();
        // Do your stuff here when you are stopping your activity
    }

    @Override
    public void onResume() {
        super.onResume();
        Game.free_hold = false;
        i = 0;
        paused = false;
//        play_songs = new TimerTask() {
//            @Override
//            public void run() {
//                if (!paused) {
//                    if (!first_song) {
//                        startFadeOut();
//                        mp.stop();
//                        mp.reset();
//                    }
//                    if (i == songArray.size()) {
//                        i = 0;
//                    } else {
//                    }
//                    mp = MediaPlayer.create(getApplicationContext(), songArray.get(i));
//                    mp.start();
//                    startFadeIn();
//                    first_song = false;
//                    i++;
//                }
//            }
//        };
//        timer.scheduleAtFixedRate(play_songs, 3000, 309000);
    }

    public static MediaPlayer getMp() {
        return mp;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }


    private void get_unlocked_waves(int stage) {
        SharedPreferences local_db = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String stage_wave_map_json_string = local_db.getString("stage_wave_local_store", "");
        if (stage_wave_map_json_string == "") {
            max_achieved_wave = 1;
        } else {
            HashMap<Integer, Integer> stage_wave_map = new Gson().fromJson(stage_wave_map_json_string,
                    new TypeToken<HashMap<Integer, Integer>>() {
                    }.getType());
            if(stage_wave_map.get(stage)!= null) {
                max_achieved_wave = stage_wave_map.get(stage);
            }
            else{
                max_achieved_wave =1;
            }
        }
        for (Map.Entry<Integer, ImageButton> entry : wave_buttons.entrySet()) {
//            if (entry.getKey() <= total_waves){//max_achieved_wave) {
            if (entry.getKey() <= max_achieved_wave){//max_achieved_wave) {
                entry.getValue().setImageResource(R.drawable.unlocked_wave_2);
                entry.getValue().setClickable(true);
                Log.d("test", "get_unlocked_waves: " + max_achieved_wave);
            } else {
                entry.getValue().setImageResource(R.drawable.lock_wave);
                entry.getValue().setClickable(false);
            }
        }
    }

    private void load_wave_selection_bg(int stage){
        LinearLayout bg = findViewById(R.id.wave_selection_bg);
        switch(stage){
            case 1:
                bg.setBackground(getDrawable(R.drawable.stage_1));
                break;

            case 2:
                bg.setBackground(getDrawable(R.drawable.stage_2));
                break;

            case 3:
                bg.setBackground(getDrawable(R.drawable.stage_3));
                break;
        }
    }

    private void get_waves_stars(int stage) {
        SharedPreferences local_db = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String stage_wave_stars_local_store = local_db.getString("stage"+Integer.toString(stage)+"_wave_stars_local_store", "");
        if (stage_wave_stars_local_store == "") {
            for (int i = 0; i< total_waves; i++) {
                wave_stars.put(i+1,0);
            }
            SharedPreferences.Editor editor = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
            Type type = new TypeToken<HashMap<Integer,Integer>>(){}.getType();
            String json = gson.toJson(wave_stars, type);
            editor.putString("stage"+Integer.toString(stage)+"_wave_stars_local_store",json);
            editor.apply();
        } else {
            HashMap<Integer, Integer> stage_wave_stars = new Gson().fromJson(stage_wave_stars_local_store,
                    new TypeToken<HashMap<Integer, Integer>>() {
                    }.getType());
            wave_stars = stage_wave_stars;
        }
        for (Map.Entry<Integer, ImageButton> entry : wave_buttons.entrySet()) {
            if (entry.getKey() <= total_waves){//max_achieved_wave) {
                entry.getValue().setImageResource(R.drawable.unlocked_wave_2);
                entry.getValue().setClickable(true);
                Log.d("test", "get_unlocked_waves: " + max_achieved_wave);
            } else {
                entry.getValue().setImageResource(R.drawable.lock_wave);
                entry.getValue().setClickable(false);
            }
        }
    }

    private void build_dynamic_wave_selection(int waves) {

        //when setting layout parameters, make sure the parameters belong to
        //the parent containing the current child that you are working on
        int waves_per_screen = 20;
        int tables = divideRoundUp(waves, waves_per_screen);
        int row_count = 5;
        int frame_count_per_row = 4;
        int waves_created =0;
        TableRow stage_wave_selection_tr = findViewById(R.id.stage1_wave_selection_tr);
        for (int i = 0; i < tables; i++){
            TableLayout inner_tr = new TableLayout(getApplicationContext());
            TableRow.LayoutParams inner_tr_params = new TableRow.LayoutParams(
                    s_width
                    ,TableRow.LayoutParams.WRAP_CONTENT);
            inner_tr_params.gravity=Gravity.CENTER_HORIZONTAL;
            inner_tr.setLayoutParams(inner_tr_params);
            for (int j = 0; j < row_count; j++) {
                TableRow tr = new TableRow(getApplicationContext());
                TableLayout.LayoutParams tr_params = new TableLayout.LayoutParams(
                        TableLayout.LayoutParams.WRAP_CONTENT,
                        TableLayout.LayoutParams.WRAP_CONTENT);
                tr.setLayoutParams(tr_params);
                tr.setGravity(Gravity.CENTER_HORIZONTAL);

                for (int k = 0; k < frame_count_per_row; k++) {
                    //frame_layout
                    FrameLayout fl = new FrameLayout(getApplicationContext());
                    TableRow.LayoutParams fl_params = new  TableRow.LayoutParams(
                            TableRow.LayoutParams.WRAP_CONTENT,
                            TableRow.LayoutParams.WRAP_CONTENT,
                            Gravity.CENTER);
                    fl.setLayoutParams(fl_params);

                    //image button
                    ImageButton ib = new ImageButton(getApplicationContext());
                    FrameLayout.LayoutParams ib_params = new FrameLayout.LayoutParams(
                            s_width/4,
                            s_width/4,
                            Gravity.CENTER);
                    ib_params.gravity = Gravity.CENTER;
                    ib.setLayoutParams(ib_params);
                    ib.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    ib.setImageResource(R.drawable.lock_wave);
                    ib.setBackgroundColor(Color.TRANSPARENT);
                    fl.addView(ib);

                    //textview
                    TextView tv = new TextView(getApplicationContext());
                    tv.setGravity(Gravity.CENTER);
                    FrameLayout.LayoutParams tv_params = new FrameLayout.LayoutParams(
                            FrameLayout.LayoutParams.MATCH_PARENT,
                            FrameLayout.LayoutParams.MATCH_PARENT,
                            Gravity.CENTER);
                    tv_params.gravity = Gravity.CENTER;
                    tv.setLayoutParams(tv_params);
                    int wave = (k + 1) + (j)*frame_count_per_row + waves_per_screen* (i);
                    tv.setText(Integer.toString(wave));
                    tv.setTextColor(Color.parseColor("#ffffff"));
                    Typeface typeface = ResourcesCompat.getFont(getApplicationContext(), R.font.ribeye_marrow);
                    tv.setTypeface(typeface);
                    tv.setClickable(false);
                    tv.setTextSize(getResources().getDimension(R.dimen._8ssp));
                    fl.addView(tv);


                    int image_button_id = View.generateViewId();
                    ib.setId(image_button_id);
                    ib.setOnClickListener(this);
                    wave_buttons.put(wave,ib);
                    wave_ids.put(wave,image_button_id);

                    LinearLayout ll = new LinearLayout(getApplicationContext());
                    FrameLayout.LayoutParams ll_params = new FrameLayout.LayoutParams(
                            FrameLayout.LayoutParams.WRAP_CONTENT,
                            FrameLayout.LayoutParams.MATCH_PARENT,
                            Gravity.CENTER_HORIZONTAL);
                    ll.setLayoutParams(ll_params);
                    ll.setGravity(Gravity.BOTTOM);
                    ll.setOrientation(LinearLayout.HORIZONTAL);

                    int stars = wave_stars.get(wave);
                    for (int s =0;s<3;s++){
                        if(s<stars) {
                            ImageView iv = new ImageView(getApplicationContext());
                            LinearLayout.LayoutParams iv_params = new LinearLayout.LayoutParams(
                                    (int) getResources().getDimension(R.dimen._15sdp),
                                    (int) getResources().getDimension(R.dimen._15sdp),
                                    Gravity.CENTER);
                            iv.setLayoutParams(iv_params);
                            iv.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                            iv.setImageResource(star_identifier.get(3));
                            iv.setBackgroundColor(Color.TRANSPARENT);
                            ll.addView(iv);
                        }
                        else{
                            ImageView iv = new ImageView(getApplicationContext());
                            LinearLayout.LayoutParams iv_params = new LinearLayout.LayoutParams(
                                    (int) getResources().getDimension(R.dimen._15sdp),
                                    (int) getResources().getDimension(R.dimen._15sdp),
                                    Gravity.CENTER);
                            iv.setLayoutParams(iv_params);
                            iv.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                            iv.setImageResource(star_identifier.get(0));
                            iv.setBackgroundColor(Color.TRANSPARENT);
                            ll.addView(iv);
                        }
                    }
                    Log.d("test", "build_dynamic_wave_selection: wave" + wave +"complete");
                    fl.addView(ll);

                    //add the frame_layout to this
                    tr.addView(fl);

                    waves_created ++;
                    if(waves_created == waves){
                        break;
                    }
                 }

            inner_tr.addView(tr);
            if(waves_created == waves){
                break;
            }
            }

        stage_wave_selection_tr.addView(inner_tr);
        if(waves_created == waves){
            break;
        }
        }

    }


    private int get_pixel_from_dp(int dp) {
        float size = dp * ((float) getApplicationContext().getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
//        Resources r = getApplicationContext().getResources();
//        int px = (int) TypedValue.applyDimension(
//                TypedValue.COMPLEX_UNIT_DIP,
//                dp,
//                r.getDisplayMetrics()
//        );
//        return px;
        return (int) size;
    }

    public static int convertDpToPixel(float dp){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }

    public static float convertDpToPixel(float dp, Context context){
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    private static int divideRoundUp(int denominator, int divisor) {
        return (denominator + divisor - 1) / divisor;
    }

    public void getScreenDimensions(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        s_width = width;
        s_height = height;

    }
}

