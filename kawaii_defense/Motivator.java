package nexidea.kawaii_defense;

import android.content.Context;
import android.graphics.Color;

import com.google.android.gms.common.util.ArrayUtils;

import java.util.HashMap;

public class Motivator {

    private long catch_duration;
    private Game game;
    private HashMap<Integer,String> mwodc = new HashMap<>(); //motivates_without_duration_consideration
    private HashMap<Integer,String> mwdc_string = new HashMap<>(); //getMotivates_with_duration_consideration
    private HashMap<Integer,Integer> mwdc_color = new HashMap<>(); //getMotivates_with_duration_consideration
    private HashMap<Integer,Integer> mwdc_size = new HashMap<>(); //getMotivates_with_duration_consideration
    private Integer motivator_score;
    private Context context;
    private int[] array;
    private boolean time_trigger;


    public Motivator(Game game, Context context) {
        this.game = game;
        this.context = context;

        array = new int[]{2,3,7,10,15,20,25,30,35,40};

        mwodc.put(1,"PERFECT");

        mwdc_string.put(array[0],"DOUBLE"); //blue
        mwdc_string.put(array[1],"TRIPLE~"); //orange
        mwdc_string.put(array[2],"FANTASTIC!"); //yellow
        mwdc_string.put(array[3],"^LUCKY^"); // green
        mwdc_string.put(array[4],"GREAT."); // pink
        mwdc_string.put(array[5],"*EXCELLENT*"); // yellow
        mwdc_string.put(array[6],"--AMAZING--"); // yellow
        mwdc_string.put(array[7],"-> PHENOMENAL <-"); // yellow
        mwdc_string.put(array[8],"RAMPAGE @#$%^&"); // yellow
        mwdc_string.put(array[9],"~~GODLIKE~~"); // yellow

//https://www.w3schools.com/colors/colors_crayola.asp
        mwdc_color.put(array[0], Color.parseColor("#ffffff")); //white
        mwdc_color.put(array[1],Color.parseColor("#FF6037")); //outrageous orange
        mwdc_color.put(array[2],Color.parseColor("#CCFF00")); //electric lime
        mwdc_color.put(array[3],Color.parseColor("#66FF66")); //screaming green
        mwdc_color.put(array[4],Color.parseColor("#FF007C")); // winter sky
        mwdc_color.put(array[5],Color.parseColor("#FDFF00")); // lemon glacier
        mwdc_color.put(array[6],Color.parseColor("#FFFF31")); // yellow
        mwdc_color.put(array[7],Color.parseColor("#E30B5C")); // razz matazz
        mwdc_color.put(array[8],Color.parseColor("#87FF2A")); //spring frost
        mwdc_color.put(array[9],Color.parseColor("#FF404C"));  // sunburn cyclops
//        mwdc_color.put(array[10],Color.parseColor("#bf9b30")); // gold

//        int adjust = 8;
//        mwdc_size.put(array[0],18 - adjust); //blue
//        mwdc_size.put(array[1],18- adjust); //orange
//        mwdc_size.put(array[2],20- adjust); //yellow
//        mwdc_size.put(array[3],20- adjust); // green
//        mwdc_size.put(array[4],20- adjust); // pink
//        mwdc_size.put(array[5],20- adjust); // green
//        mwdc_size.put(array[6],20- adjust); // white
//        mwdc_size.put(array[7],20- adjust); // yellow
//        mwdc_size.put(array[8],20- adjust); // pink
//        mwdc_size.put(array[9],20- adjust); // green

        motivator_score = 0;
    }

    public void update(long elapsed){
        catch_duration -= elapsed;
        if(catch_duration<0 && time_trigger){
            update_motivator("",Color.WHITE);
            motivator_score = 0;
            time_trigger = false;
        }
    }

    public void check_and_post(){
        motivator_score ++;
        if(game.score ==1){
            update_motivator(mwodc.get(1),Color.YELLOW);
            catch_duration = 2000;
            time_trigger = true;
        }
        if (ArrayUtils.contains(array, motivator_score)){
            update_motivator(mwdc_string.get(motivator_score),mwdc_color.get(motivator_score));
            catch_duration = 2000;
            time_trigger = true;
        }
        else{}
    }

    public void update_motivator(String string, int color) {
        //update combo
        ((MainActivity_stage) context).updateMotivatorText(string, color);
    }
}