package nexidea.kawaii_defense;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;

import android.view.SurfaceHolder;
import android.view.SurfaceView;

import nexidea.kawaii_defense.BackgroundAesthetics.BackgroundRunner;
import nexidea.kawaii_defense.BackgroundAesthetics.bga_menu;


public class MenuView extends SurfaceView implements SurfaceHolder.Callback {
    public static final String MY_PREFS_NAME = "local_db";
    private Bitmap background;
    private Animated_background background_animated;
    public static BackgroundRunner runner;
    public static Context menuviewcontext;
    public static SurfaceHolder holder;


    public MenuView(Context context, AttributeSet attrs) {
        super(context, attrs);
        holder = getHolder();
        holder.addCallback(this);
        menuviewcontext = context;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
//        if (MenuActivity.getMp().isPlaying()){}
//        else {
//            MenuActivity.getMp().start();
//        }
        background = BitmapFactory.decodeResource(getResources(), R.drawable.home_page_v5);
        background = Bitmap.createScaledBitmap(background,
                getWidth(),
                getHeight(),true);
        background_animated = new Animated_background(getContext(), getWidth(), getHeight(), holder, getResources(), background,0.1f,0);
        background_animated.getMoving_background().load_background(background,0,0);
        bga_menu menu_animation = new bga_menu(getContext(),getWidth(),getHeight(),holder,getResources());
        runner = new BackgroundRunner(background_animated);
        runner.setBga_menu(menu_animation);
        runner.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        //

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
//        MenuActivity.getMp().pause();
        if (runner != null){
            runner.shutdown();
            while(runner != null){
                try {
                    runner.join();
                    runner=null;
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public int getNavHeight(){
        //navigation bar height
        int navigationBarHeight = 0;
        int resourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            navigationBarHeight = getResources().getDimensionPixelSize(resourceId);
        }
        return navigationBarHeight;
    }
}


