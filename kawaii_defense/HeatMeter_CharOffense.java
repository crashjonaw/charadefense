package nexidea.kawaii_defense;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;

import com.google.protobuf.MapEntryLite;

import java.util.HashMap;
import java.util.Map;

import static android.support.constraint.Constraints.TAG;

public class HeatMeter_CharOffense {
    private Resources resources;
    private int increment_interval;
    private float heat_meter;
    private long cd_interval;
    private int total;
    private long overheat_time;
    private HashMap<Integer,Bitmap> meter_image = new HashMap<>();
    private int screenWidth;
    private int screenHeight;
    private int x;
    private int y;
    private Paint text_paint = new Paint();
    private HashMap<Integer,String> signal = new HashMap();
    private HashMap<Integer,Integer> signal_color = new HashMap<>();
    private String signal_text;
    private Paint bar_paint = new Paint();
    private final int arr[] = {30,70,101,1000,2000};
    public boolean overheat;
    private int text_size;
    public boolean lifted;

    public HeatMeter_CharOffense(int screenWidth, int screenHeight, Resources resources, Context context) {

        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        this.resources = resources;
        text_size = (int) resources.getDimension(R.dimen._15sdp);
        x = 0;
        y = screenHeight /10 + text_size;
        heat_meter = 1f;
        total = 100;
        cd_interval = 0;
        increment_interval =0;
        overheat_time = 0;


        signal.put(arr[0],"GOOD :)");
        signal.put(arr[1],"TAP & DRAG~~");
        signal.put(arr[2],"DRAGGING TOO MUCH :(");

        signal_color.put(arr[0],Color.GREEN);
        signal_color.put(arr[1],Color.YELLOW);
        signal_color.put(arr[2],Color.RED);
        signal_color.put(arr[3],Color.WHITE);
        signal_color.put(arr[4],Color.parseColor("#a7ffff"));

        text_paint.setColor(signal_color.get(arr[0]));
        text_paint.setStyle(Paint.Style.FILL);
        text_paint.setTextSize(text_size);
        Typeface typeface = ResourcesCompat.getFont(context, R.font.ribeye_marrow);
        Typeface typeface_bold = Typeface.create(typeface, Typeface.BOLD);
        text_paint.setTypeface(typeface_bold);
        set_bar_paint_color(signal_color.get(arr[0]));
        bar_paint.setColor(Color.BLACK);
        bar_paint.setStrokeWidth(5);
        bar_paint.setStyle(Paint.Style.STROKE);
        lifted = true;

        for(int i =1; i<total +1;i++){
            Bitmap temp_image = Bitmap.createScaledBitmap(MyBitmap.general_bitmap_hash.get("hp").get(1),
                    Math.round(this.screenWidth/3 * i/total),
                    this.screenHeight / 20, false);
            meter_image.put(i,temp_image);
            Log.d(TAG, "HeatMeter_CharOffense: image inserted: " + i);
            Log.d(TAG, "length = " + Math.round(this.screenWidth/4 * i/total));
            Log.d(TAG, "HeatMeter_CharOffense: temp image" + temp_image);
        }
        overheat = false;
    }

    public void update(long elapsed){
        cd_interval += elapsed;
        if(cd_interval>50){
            heat_meter = Math.max(heat_meter-1f,1f);
            cd_interval =0;
        }

        if(heat_meter> 70 && !lifted){
            overheat_time += elapsed;
            if(overheat_time>4000){
                overheat = true;
                overheat_time=0;
            }
        }

        if(heat_meter<arr[0]){
            overheat_time = Math.max(overheat_time-elapsed,0);
        }

        if(heat_meter<arr[0]/2){
            overheat = false;
            text_paint.setTextSize(resources.getDimension(R.dimen._15sdp));
        }
        update_signal_text();
    }

    public void pressed(){
        heat_meter = Math.min(heat_meter + 1.5f, total);
        increment_interval = 0;
        lifted = false;
    }

    public void draw(Canvas canvas){
        Log.d(TAG, "draw: heat_meter = "+ meter_image.get(Math.round(heat_meter)));
        Log.d(TAG, "draw: heat_meter = " + heat_meter + " , " + Math.round(heat_meter));
        canvas.drawText(signal_text,x + meter_image.get(Math.round(heat_meter)).getWidth()/3,y - text_size,text_paint);
        canvas.drawBitmap( meter_image.get(Math.round(heat_meter)), x, y, bar_paint);
    }

    private void update_signal_text(){
        if(!overheat) {
            for (int i = 0; i < 3; i++) {
                if (heat_meter < arr[i]) {
                    if(!lifted) {
                        signal_text = signal.get(arr[i]);
                    }
                    else {
                        signal_text = "COOLING DOWN ^.^";
                    }
                    break;
                } else {
                    continue;
                }
            }

            for (int i = 0; i < 3; i++) {
                if (heat_meter < arr[i]) {
                    if(!lifted) {
                        text_paint.setColor(signal_color.get(arr[i]));
                    }
                    else{
                        text_paint.setColor(signal_color.get(arr[4]));
                    }
                    set_bar_paint_color(signal_color.get(arr[i]));
                    break;
                } else {
                    continue;
                }
            }
        }

        else{
            signal_text = "TAP ONLY! >.<\"";
            text_paint.setTextSize(resources.getDimension(R.dimen._20sdp));
            text_paint.setColor(signal_color.get(arr[3]));
            set_bar_paint_color(signal_color.get(arr[3]));
        }
    }

    private void set_bar_paint_color(int color){
        ColorFilter filter = new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN);
        bar_paint.setColorFilter(filter);

    }




}
