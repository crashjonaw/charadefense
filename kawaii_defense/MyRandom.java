package nexidea.kawaii_defense;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import static android.content.ContentValues.TAG;

public class MyRandom {
    private int s_width;
    private int s_height;
    public HashMap<String,Iterator<Integer>> random_map = new HashMap<>();
    public HashMap<String,Iterator<float[]>> random_start_map = new HashMap<>();

    public MyRandom() {
    }

    public void add_dim(int s_width, int s_height){
        this.s_width = s_width;
        this.s_height = s_height;
    }

    public void add_random_map(String string, int min, int max, int count){
        ArrayList<Integer> random_list = new ArrayList<>();
        for (int j=0; j<count ; j++){
            random_list.add(randomNumberInRange(min,max));
        }
        random_map.put(string, random_list.iterator());
    }

    public void add_random_map_no_zeros(String string, int min, int max, int count){
        ArrayList<Integer> random_list = new ArrayList<>();
        for (int j=0; j<count ; j++){
            random_list.add(randomNumberInRange_no_zeros(min,max));
        }
        random_map.put(string, random_list.iterator());
    }

    public void add_stage1_random_starts(int count){
        HashMap<Integer,int[]> random_set_of_start_points = new HashMap<>();
        random_set_of_start_points.put(1,new int[] {1}); //top
        random_set_of_start_points.put(2,new int[] {3,4}); //left,right
        random_set_of_start_points.put(3,new int[] {2});
        random_set_of_start_points.put(4,new int[] {2,3,4});
        random_set_of_start_points.put(5,new int[] {1,2});
//        random_set_of_start_points.put(6,new int[] {1,4});
        random_set_of_start_points.put(7,new int[] {1,3,4});
        random_set_of_start_points.put(8,new int[] {1,2,3,4});
        Log.d(TAG, "create_waves: finished random_set_of_start_points");

        ArrayList<float[]> start_points_t = new ArrayList<>();
        for (int j =0; j<count; j++){
            start_points_t.add(generate_random_start(setSpawn_start_info(random_set_of_start_points.get(1))));
        }
        random_start_map.put("top",start_points_t.iterator());

        ArrayList<float[]> start_points_tlr = new ArrayList<>();
        for (int j =0; j<count; j++){
            start_points_tlr.add(generate_random_start(setSpawn_start_info(random_set_of_start_points.get(7))));
        }
        random_start_map.put("top_left_right",start_points_tlr.iterator());

        ArrayList<float[]> start_points_lr = new ArrayList<>();
        for (int j =0; j<count; j++){
            start_points_lr.add(generate_random_start(setSpawn_start_info(random_set_of_start_points.get(2))));
        }
        random_start_map.put("left_right",start_points_lr.iterator());

        ArrayList<float[]> start_points_tdlr = new ArrayList<>();
        for (int j =0; j<count; j++){
            start_points_tdlr.add(generate_random_start(setSpawn_start_info(random_set_of_start_points.get(8))));
        }
        random_start_map.put("top_down_left_right",start_points_tdlr.iterator());

        ArrayList<float[]> start_points_b = new ArrayList<>();
        for (int j =0; j<count; j++){
            start_points_b.add(generate_random_start(setSpawn_start_info(random_set_of_start_points.get(3))));
        }
        random_start_map.put("bottom",start_points_b.iterator());

        ArrayList<float[]> start_points_blr = new ArrayList<>();
        for (int j =0; j<count; j++){
            start_points_blr.add(generate_random_start(setSpawn_start_info(random_set_of_start_points.get(4))));
        }
        random_start_map.put("bottom_left_right",start_points_blr.iterator());

        ArrayList<float[]> start_points_tb = new ArrayList<>();
        for (int j =0; j<count; j++){
            start_points_tb.add(generate_random_start(setSpawn_start_info(random_set_of_start_points.get(5))));
        }
        random_start_map.put("top_bottom",start_points_blr.iterator());
    }

    public void add_stage2_random_starts(int count) {
        ArrayList<float[]> start_points_laser = new ArrayList<>();
        for (int j =0; j<count; j++){
            int type = randomNumberInRange(1,2)-1;
            start_points_laser.add(generate_random_laser(type));
        }
        random_start_map.put("random_laser_start",start_points_laser.iterator());

        HashMap<Integer,int[]> random_set_of_start_points = new HashMap<>();
        random_set_of_start_points.put(1,new int[] {1});
        random_set_of_start_points.put(2,new int[] {3,4});
        random_set_of_start_points.put(3,new int[] {3});
        random_set_of_start_points.put(4,new int[] {4});
        random_set_of_start_points.put(5,new int[] {1,3});
        random_set_of_start_points.put(6,new int[] {1,4});
        random_set_of_start_points.put(7,new int[] {1,3,4});
        random_set_of_start_points.put(8,new int[] {1,2,3,4});
        Log.d(TAG, "create_waves: finished random_set_of_start_points");

        ArrayList<float[]> start_points_t = new ArrayList<>();
        for (int j =0; j<count; j++){
            start_points_t.add(generate_random_start(setSpawn_start_info(random_set_of_start_points.get(1))));
        }
        random_start_map.put("top",start_points_t.iterator());

        ArrayList<float[]> start_points_tlr = new ArrayList<>();
        for (int j =0; j<count; j++){
            start_points_tlr.add(generate_random_start(setSpawn_start_info(random_set_of_start_points.get(7))));
        }
        random_start_map.put("top_left_right",start_points_tlr.iterator());

        ArrayList<float[]> start_points_lr = new ArrayList<>();
        for (int j =0; j<count; j++){
            start_points_lr.add(generate_random_start(setSpawn_start_info(random_set_of_start_points.get(2))));
        }
        random_start_map.put("left_right",start_points_lr.iterator());

        ArrayList<float[]> start_points_tdlr = new ArrayList<>();
        for (int j =0; j<count; j++){
            start_points_tdlr.add(generate_random_start(setSpawn_start_info(random_set_of_start_points.get(8))));
        }
        random_start_map.put("top_down_left_right",start_points_tdlr.iterator());
    }

    public void add_stage3_random_starts(int count){
        HashMap<Integer,int[]> random_set_of_start_points = new HashMap<>();
        random_set_of_start_points.put(1,new int[] {1});
        random_set_of_start_points.put(2,new int[] {3,4});
        random_set_of_start_points.put(3,new int[] {3});
        random_set_of_start_points.put(4,new int[] {4});
        random_set_of_start_points.put(5,new int[] {1,3});
        random_set_of_start_points.put(6,new int[] {1,4});
        random_set_of_start_points.put(7,new int[] {1,3,4});
        random_set_of_start_points.put(8,new int[] {1,2,3,4});
        Log.d(TAG, "create_waves: finished random_set_of_start_points");

        ArrayList<float[]> start_points_t = new ArrayList<>();
        for (int j =0; j<count; j++){
            start_points_t.add(generate_random_start(setSpawn_start_info(random_set_of_start_points.get(1))));
        }
        random_start_map.put("top",start_points_t.iterator());

        ArrayList<float[]> start_points_tlr = new ArrayList<>();
        for (int j =0; j<count; j++){
            start_points_tlr.add(generate_random_start(setSpawn_start_info(random_set_of_start_points.get(7))));
        }
        random_start_map.put("top_left_right",start_points_tlr.iterator());

        ArrayList<float[]> start_points_lr = new ArrayList<>();
        for (int j =0; j<count; j++){
            start_points_lr.add(generate_random_start(setSpawn_start_info(random_set_of_start_points.get(2))));
        }
        random_start_map.put("left_right",start_points_lr.iterator());

        ArrayList<float[]> start_points_tdlr = new ArrayList<>();
        for (int j =0; j<count; j++){
            start_points_tdlr.add(generate_random_start(setSpawn_start_info(random_set_of_start_points.get(8))));
        }
        random_start_map.put("top_down_left_right",start_points_tdlr.iterator());
    }

    public ArrayList<Integer> setSpawn_start_info(int[] spawn_start) {
        ArrayList<Integer> spawn_start_info = new ArrayList<Integer>();
        for (int i = 0 ; i <spawn_start.length; i++){
            spawn_start_info.add(spawn_start[i]);
        }
        return spawn_start_info;
    }

    public int randomNumberInRange(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    public int randomNumberInRange_no_zeros(int min, int max) {
        Random random = new Random();
        int my_rand;
        do {
            my_rand = random.nextInt((max - min) + 1) + min;
        }while(my_rand ==0);

        return my_rand;
    }

    public float[] generate_random_start(ArrayList<Integer> available_start_regions){
        float[] start_point = new float[2];
        int rand_int = randomNumberInRange(1,available_start_regions.size());
        int rand_start = available_start_regions.get(rand_int-1);
        switch(rand_start){

            case 1: //top
                start_point[1] = 0;
                start_point[0] =(float) randomNumberInRange(0,100) /100 * s_width;
                break;

            case 2: //bottom
                start_point[1] = s_height;
                start_point[0] =(float) randomNumberInRange(0,100)/100 *s_width ;
                break;

            case 3: //left
                start_point[1] = (float) randomNumberInRange(0,30)/100 *s_height;
                start_point[0] =0;
                break;

            case 4: //right
                start_point[1] = (float)randomNumberInRange(0,30)/100 *s_height ;
                start_point[0] = s_width;
                break;

            default:
                Log.d("start_point error", "generate_random_start: error!");}

        return start_point;}


    public float[] generate_random_laser (int type){
        float[] start_point = new float[8];
        //0 is mobile
        //1 is static
        if ( type == 0) {
            int rand_int = randomNumberInRange(1,4);
            switch (rand_int) {
                case 1: //top - horizontal laser
                    start_point[0] = 0; //width
                    start_point[1] = 0; // height
                    start_point[2] = 0; // x-speed
                    start_point[3] = 0.25f; // y-speed
                    start_point[4] = 0; //x-direction
                    start_point[5] = 1; //y-direction
                    start_point[6] = 1; // horizontal = 1 ,vertical = 0

                    break;

                case 2: //bottom - horizontal laser
                    start_point[0] = 0; //width
                    start_point[1] = s_height; //height
                    start_point[2] = 0; // x-speed
                    start_point[3] = 0.25f; // y-speed
                    start_point[4] = 0; //x-direction
                    start_point[5] = -1; //y-direction
                    start_point[6] = 1; // horizontal = 1 ,vertical = 0
                    break;

                case 3: //left - vertical laser
                    start_point[0] = 0; //width
                    start_point[1] = 0; // height
                    start_point[2] = 0.25f; // x-speed
                    start_point[3] = 0; // y-speed
                    start_point[4] = 1; //x-direction
                    start_point[5] = 0; //y-direction
                    start_point[6] = 0; // horizontal = 1 ,vertical = 0

                    break;

                case 4: //right - vertical
                    start_point[0] = s_width; //width
                    start_point[1] = 0; //height
                    start_point[2] = 0.25f; // x-speed
                    start_point[3] = 0; // y-speed
                    start_point[4] = -1; //x-direction
                    start_point[5] = 0; //y-direction
                    start_point[6] = 0; // horizontal = 1 ,vertical = 0
                    break;


                default:
                    Log.d("start_point error", "generate_random_start: error!");
            }
        }

        if ( type == 1) {
            int rand_int = randomNumberInRange(1,2);
            switch (rand_int) {
                case 1: //top - horizontal laser
                    start_point[0] = 0; //width
                    start_point[1] = (float) randomNumberInRange(0,100)/100 * (s_height - s_width/12); // height
                    start_point[2] = 0; // x-speed
                    start_point[3] = 0f; // y-speed
                    start_point[4] = 0; //x-direction
                    start_point[5] = 0; //y-direction
                    start_point[6] = 1; // horizontal = 1 ,vertical = 0

                    break;

                case 2:  //left - vertical laser
                    start_point[0] = (float) randomNumberInRange(0,100)/100 * (s_width - s_width/12); //width
                    start_point[1] = 0; // height
                    start_point[2] = 0; // x-speed
                    start_point[3] = 0; // y-speed
                    start_point[4] = 0; //x-direction
                    start_point[5] = 0; //y-direction
                    start_point[6] = 0; // horizontal = 1 ,vertical = 0

                    break;
                default:
                    Log.d("start_point error", "generate_random_start: error!");
            }
        }


        return start_point;}

    public float[] generate_random_start_coconut_black(int[] spawn_start, boolean hard){
        ArrayList<Integer> spawn_start_info = new ArrayList<Integer>();
        for (int i = 0 ; i <spawn_start.length; i++){
            spawn_start_info.add(spawn_start[i]);
        }

        float[] start_point = new float[4];
        int rand_int = randomNumberInRange(1,spawn_start_info.size());
        int rand_start = spawn_start_info.get(rand_int-1);
        switch(rand_start){
            case 1: //top
                start_point[1] = 0;
                start_point[0] =(float) randomNumberInRange(0,100) /100 * s_width;
                start_point[2] = hard ? 1 : 0; //x
                start_point[3] = 1; //y
                break;

            case 2: //bottom
                start_point[1] = s_height;
                start_point[0] =(float) randomNumberInRange(0,100)/100 *s_width ;
                start_point[2] = hard ? 1 : 0;//x
                start_point[3] = 1;
                break;

            case 3: //left
                start_point[1] = (float) randomNumberInRange(0,100)/100 *s_height;
                start_point[0] =0;
                start_point[2] = 1;
                start_point[3] = 1;
                break;

            case 4: //right
                start_point[1] = (float)randomNumberInRange(0,100)/100 *s_height ;
                start_point[0] = s_width;
                start_point[2] = 1;
                start_point[3] = 1;
                break;

            default:
                Log.d("start_point error", "generate_random_start: error!");}

        return start_point;}

}
