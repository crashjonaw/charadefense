package nexidea.kawaii_defense;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public class ripple {

    private int x;
    private int y;
    private int size;
    private Bitmap ripple_bitmap;
    private Paint alpha;

    public ripple (int x, int y, Bitmap bitmap, int size, Paint alpha){
        this.x = x;
        this.y = y;
        this.ripple_bitmap = bitmap;
        this.size = size;
        this.alpha = alpha;
    }

    public void draw(Canvas canvas){
            canvas.drawBitmap(ripple_bitmap,x-size/2,y-size/2,alpha);
        }
}
