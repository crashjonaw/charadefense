package nexidea.kawaii_defense;

import android.util.Log;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import nexidea.kawaii_defense.Projectiles.Projectile;
import nexidea.kawaii_defense.Projectiles.Projectile_diamond;
import nexidea.kawaii_defense.Projectiles.Projectile_laser;

import static android.content.ContentValues.TAG;

//        int[] hi = new int[] {1,2,3};

public class Wave {

    public HashMap<String,Integer> wave_completion;
    private HashMap<String,Integer> diamond_types = new HashMap<>();
    public HashMap<Integer,String> diamond_types_v2= new HashMap<>();
    public ArrayList<Integer> diamond_types_for_wave = new ArrayList<>();
    private int s_width;
    private int s_height;
    private Game game;
    public int collision;
    public float projSpeed;
    public HashMap<String, Integer> spawn_rate;
    public long spawn_interval;
    public long wave_wait;
    public long time_to_complete;
    public String spawn_start_info;
    public boolean boss_stage;
    public boolean boss_spawned;
    public int boss_count;
    public int tree_boss_count;
    public int urchin_boss_count;
    public int boss_difficulty;
    public int stars;
    public int max_projectiles;
    public double[] probabilities;
    private int ll;
    private int ul;

    private static MyRandom my_random;

    public Wave(HashMap<String,Integer> wave_completion,
                float projSpeed,
                HashMap<String,Integer> spawn_rate,
                Integer max_projectiles,
                long spawn_interval,
                String spawn_start,
                long wave_wait,
                MyRandom my_random,
                double[] probabilities
                ) {


        this.wave_completion = wave_completion;
        this.projSpeed = projSpeed;
        this.spawn_interval = spawn_interval;
        this.spawn_rate = spawn_rate;
        this.max_projectiles = max_projectiles;
        this.wave_wait = wave_wait;
        this.my_random = my_random;
        if(probabilities!=null) {
            this.probabilities = probabilities;
        }
        else{
            this.probabilities  = new double[] { 1.0,0.0,0.0 };
        }
        time_to_complete = 0;
        collision = 0;
        stars =0;
        spawn_start_info = spawn_start;
        boss_stage= false;

        ll = 80;
        ul = 150;
    }

    public void get_dim(int s_width, int s_height){
        this.s_width = s_width;
        this.s_height = s_height;
    }

    public void load_game(Game game){
        this.game = game;
    }

    public boolean check_wave_completion(){
        boolean complete = true;
        for (Map.Entry<String, Integer> entry : wave_completion.entrySet()){
            if (entry.getValue() > 0){
//                Log.d(TAG, "check_wave_completion: " + entry.getKey() + entry.getValue() + "not completed");
                return false;
            }
        }
        return complete;
    }

    public void load_stage1_projectiles(int load_count, HashMap<String,
            ConcurrentHashMap<Integer, Projectile>> projectile_in_game) {
        if(!boss_stage) {
            Log.d(TAG, "load_projectiles: start to load");
            for (int j = 0; j < load_count; j++) {
                float[] rand_start = my_random.random_start_map.get(spawn_start_info).next();
                Projectile coconut = new Projectile(
                        s_width,
                        s_height,
                        rand_start[0],
                        rand_start[1],
                        projSpeed * randomNumberInRange(ll,ul) / 100,
                        projSpeed * randomNumberInRange(ll,ul)  / 100,
                        "coconut",
                        game,this);

                projectile_in_game.get("coconut").put(j, coconut);
                Log.d(TAG, "load_projectiles: coconut loaded " + j);

                float[] rand_start2 = my_random.random_start_map.get(spawn_start_info).next();
                Projectile banana = new Projectile(
                        s_width,
                        s_height,
                        rand_start2[0],
                        rand_start2[1],
                        projSpeed * randomNumberInRange(ll,ul) * 1.5f / 100,
                        projSpeed * randomNumberInRange(ll,ul) *0.7f / 100,
                        "banana",
                        game,this);

                projectile_in_game.get("banana").put(j, banana);
                Log.d(TAG, "load_projectiles: banana loaded " + j);

                float[] rand_start3 = my_random.random_start_map.get(spawn_start_info).next();
                Projectile angry_coconut = new Projectile(
                        s_width,
                        s_height,
                        rand_start3[0],
                        rand_start3[1],
                        projSpeed * randomNumberInRange(ll,ul) / 100*0.2f,
                        projSpeed * randomNumberInRange(ll,ul)  / 100*1.4f,
                        "coconut_angry",
                        game, this);

                projectile_in_game.get("coconut_angry").put(j, angry_coconut);
                Log.d(TAG, "load_projectiles: coconut_angry loaded " + j);

                float[] rand_start4 = my_random.random_start_map.get(spawn_start_info).next();
                Projectile angry_banana = new Projectile(
                        s_width,
                        s_height,
                        rand_start4[0],
                        rand_start4[1],
                        projSpeed * randomNumberInRange(ll,ul)  / 100 * 1.2f,
                        projSpeed * randomNumberInRange(ll,ul)  / 100 * 0.35f,
                        "banana_angry",
                        game, this);

                projectile_in_game.get("banana_angry").put(j, angry_banana);
                Log.d(TAG, "load_projectiles: banana_angry loaded " + j);

                float[] rand_start5 = my_random.generate_random_start_coconut_black(new int[] {1,2},false);
                Projectile coconut_black = new Projectile(
                        s_width,
                        s_height,
                        rand_start5[0],
                        rand_start5[1],
                        projSpeed * randomNumberInRange(90,ul)  / 100 * rand_start5[2],
                        projSpeed * randomNumberInRange(90,ul)  / 100 * rand_start5[3],
                        "coconut_black",
                        game, this);

                projectile_in_game.get("coconut_black").put(j, coconut_black);
                Log.d(TAG, "load_projectiles: coconut_black loaded " + j);
            }
            Log.d(TAG, "load_projectiles: all loaded");
        }
        else{
            for (int j = 0; j < load_count; j++) {
            Projectile coconut = new Projectile(
                    s_width,
                    s_height,
                    0,
                    0,
                    projSpeed * randomNumberInRange(ll,ul)  / 100,
                    projSpeed * randomNumberInRange(ll,ul)  / 100,
                    "coconut",
                    game,this);

            projectile_in_game.get("coconut").put(j, coconut);
            Log.d(TAG, "load_projectiles: coconut loaded " + j);

            Projectile banana = new Projectile(
                    s_width,
                    s_height,
                    0,
                    0,
                    projSpeed * randomNumberInRange(ll,ul)  / 100,
                    projSpeed * randomNumberInRange(ll,ul)  / 100,
                    "banana",
                    game, this);

            projectile_in_game.get("banana").put(j, banana);
            Log.d(TAG, "load_projectiles: banana loaded " + j);

            Projectile angry_coconut = new Projectile(
                    s_width,
                    s_height,
                    0,0,
                    projSpeed * randomNumberInRange(ll,ul) / 100,
                    projSpeed * randomNumberInRange(ll,ul)  / 100,
                    "coconut_angry",
                    game, this);

            projectile_in_game.get("coconut_angry").put(j, angry_coconut);
            Log.d(TAG, "load_projectiles: coconut_angry loaded " + j);

            Projectile angry_banana = new Projectile(
                    s_width,
                    s_height,
                    0,0,
                    projSpeed * randomNumberInRange(ll,ul)  / 100,
                    projSpeed * randomNumberInRange(ll,ul)  / 100,
                    "banana_angry",
                    game, this);

            projectile_in_game.get("banana_angry").put(j, angry_banana);
            Log.d(TAG, "load_projectiles: banana_angry loaded " + j);
            }
        }
            Log.d(TAG, "load_projectiles: all loaded");}


    public void load_stage2_projectiles(int load_count, HashMap<String,
            ConcurrentHashMap<Integer, Projectile>> projectile_in_game,
            ConcurrentHashMap<Integer, Projectile_laser> laser_in_game) {
        if(!boss_stage) {
            Log.d(TAG, "load_projectiles: start to load");
            for (int j = 0; j < load_count; j++) {
                float[] rand_start = my_random.random_start_map.get(spawn_start_info).next();
                Projectile shuriken = new Projectile(
                        s_width,
                        s_height,
                        rand_start[0],
                        rand_start[1],
                        projSpeed * randomNumberInRange(ll,ul)  / 100,
                        projSpeed * randomNumberInRange(ll,ul)  / 100,
                        "shuriken",
                        game,this);

                projectile_in_game.get("shuriken").put(j, shuriken);
//                Log.d(TAG, "load_projectiles: coconut loaded " + j);

                float[] rand_start2 = my_random.random_start_map.get("top").next();
                Projectile dagger = new Projectile(
                        s_width,
                        s_height,
                        rand_start2[0],
                        rand_start2[1],
                        0,
                        projSpeed * randomNumberInRange(ll,ul) / 100 * 3.5f,
                        "dagger",
                        game,this);

                projectile_in_game.get("dagger").put(j, dagger);
//                Log.d(TAG, "load_projectiles: banana loaded " + j);


                float[] randlaser = my_random.random_start_map.get("random_laser_start").next();
                Projectile_laser new_laser = new Projectile_laser(s_width, s_height,
                        (int) randlaser[0],
                        (int) randlaser[1]
                        , randlaser[2]
                        , randlaser[3],
                        (int) randlaser[4],
                        (int) randlaser[5],
                        (int) randlaser[6],
                        game
                        );
                laser_in_game.put(j,new_laser);
//                Log.d(TAG, "load_projectiles: laser loaded " + j);
            }
            Log.d(TAG, "load_projectiles: all loaded");
        }
        for (int j = 0; j < load_count; j++) {
            float[] rand_start = my_random.random_start_map.get(spawn_start_info).next();
            Projectile dark_ball = new Projectile(
                    s_width,
                    s_height,
                    rand_start[0],
                    rand_start[1],
                    projSpeed * randomNumberInRange(ll,ul)  / 100 *1.5f,
                    projSpeed * randomNumberInRange(ll,ul)  / 100 *1.5f,
                    "dark_ball",
                    game,this);

            projectile_in_game.get("dark_ball").put(j, dark_ball);
//                Log.d(TAG, "load_projectiles: coconut loaded " + j);

//            float[] rand_start2 = my_random.random_start_map.get("top").next();
//            Projectile dagger = new Projectile(
//                    s_width,
//                    s_height,
//                    rand_start2[0],
//                    rand_start2[1],
//                    projSpeed * randomNumberInRange(ll,ul) / 100,
//                    projSpeed * randomNumberInRange(ll,ul) *2 / 100,
//                    "dagger",
//                    game);
//
//            projectile_in_game.get("dagger").put(j, dagger);
////                Log.d(TAG, "load_projectiles: banana loaded " + j);


            float[] randlaser = my_random.random_start_map.get("random_laser_start").next();
            Projectile_laser new_laser = new Projectile_laser(s_width, s_height,
                    (int) randlaser[0],
                    (int) randlaser[1]
                    , randlaser[2]
                    , randlaser[3],
                    (int) randlaser[4],
                    (int) randlaser[5],
                    (int) randlaser[6],
                    game
            );
            laser_in_game.put(j,new_laser);
//                Log.d(TAG, "load_projectiles: laser loaded " + j);
        }
            Log.d(TAG, "load_projectiles: all loaded");}

    public void load_stage3_projectiles(int load_count, HashMap<String,
        ConcurrentHashMap<Integer, Projectile>> projectile_in_game,
                                    ConcurrentHashMap<Integer, Projectile_diamond> diamond_in_game) {
        if(!boss_stage) {
            Log.d(TAG, "load_projectiles: start to load");
            for (int j = 0; j < load_count; j++) {
                float[] rand_fireball = my_random.random_start_map.get(spawn_start_info).next();
                Projectile_diamond diamond = new Projectile_diamond(
                        s_width,
                        s_height,
                        rand_fireball[0],
                        rand_fireball[1],
                        projSpeed * randomNumberInRange(ll,ul)/ 100,
                        projSpeed * randomNumberInRange(ll,ul)/ 100,
                        game,
                        this);
                diamond_in_game.put(j,diamond);
//                Log.d(TAG, "load_projectiles: laser loaded " + j);
            }
            Log.d(TAG, "load_projectiles: all loaded");
        }
        else{for (int j = 0; j < load_count; j++) {
            float[] rand_fireball = my_random.random_start_map.get(spawn_start_info).next();
            Projectile_diamond diamond = new Projectile_diamond(
                    s_width,
                    s_height,
                    rand_fireball[0],
                    rand_fireball[1],
                    projSpeed * randomNumberInRange(ll,ul)/ 100,
                    projSpeed * randomNumberInRange(ll,ul)/ 100,
                    game,
                    this);
            diamond_in_game.put(j,diamond);
//                Log.d(TAG, "load_projectiles: laser loaded " + j);
        }
            Log.d(TAG, "load_projectiles: all loaded");}
    }

    public void set_stage1_bosses(int tree_boss_count,int urchin_boss_count, int difficulty) {
        boss_stage = true;
        boss_spawned = false;
        this.tree_boss_count = tree_boss_count;
        this.urchin_boss_count = urchin_boss_count;
        boss_difficulty = difficulty;
    }

    public void set_stage2_bosses(int bosses, int difficulty) {
        boss_stage = true;
        boss_spawned = false;
        boss_count = bosses;
        boss_difficulty = difficulty;
    }

    public int randomNumberInRange(int min, int max) {
        Random random = new Random();
        int my_rand;
        do {
            my_rand = random.nextInt((max - min) + 1) + min;
        }while(my_rand ==0);

        return my_rand;
    }

    public void setMy_random(MyRandom my_random) {
        this.my_random = my_random;
    }

    public int generate_projectile_size_based_on_probability_array(){
        int[] numsToGenerate           = new int[]    {1,   2,    3};
        double[] discreteProbabilities = probabilities;

        EnumeratedIntegerDistribution distribution =
                new EnumeratedIntegerDistribution(numsToGenerate, discreteProbabilities);

        int numSamples = 1;
        return distribution.sample(numSamples)[0];
    }

    //1: red, 2: blue, 3: purple, 4: yellow, 5: green
    public void set_stage3_projectile_diamond_types(){
        diamond_types_v2.put(1,"red");
        diamond_types_v2.put(2,"blue");
        diamond_types_v2.put(3,"purple");
        diamond_types_v2.put(4,"yellow");
        diamond_types_v2.put(5,"green");

        diamond_types.put("red",1);
        diamond_types.put("blue",2);
        diamond_types.put("purple",3);
        diamond_types.put("yellow",4);
        diamond_types.put("green",5);


        diamond_types_for_wave = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : wave_completion.entrySet()) {
            if (entry.getValue() > 0) {
//                Log.d(TAG, "check_wave_completion: " + entry.getKey() + entry.getValue() + "not completed");
                diamond_types_for_wave.add(diamond_types.get(entry.getKey()));
            }
        }
    }

}





