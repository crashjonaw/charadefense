package nexidea.kawaii_defense;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import java.util.HashMap;
import java.util.Map;


public class MainActivity_stage extends AppCompatActivity implements View.OnClickListener,RewardedVideoAdListener {
    public static boolean paused;
    public static boolean game_paused;
//    private static ArrayList<Integer> songArray;
//    public static ArrayList<Integer> swordArray;
//    private int i = 0;
//    public static MediaPlayer mp;
//    float volume = 0;
//    private TimerTask play_songs;
//    private boolean first_song = true;
//    private boolean timer_first;
//    public static Timer timer;

    public  static Activity game_activity;
    private static TextView combo_meter;
    private TextView multiplier;
    public static int wave;
    public static int stage;
    private View v;

    //ads
    private RewardedVideoAd mRewardedVideoAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //ads
        //ads
        MobileAds.initialize(this, "ca-app-pub-9072284262045894~9618169643");
        // Use an activity context to get the rewarded video instance.
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);
        loadRewardedVideoAd();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        //get wave
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            wave = extras.getInt("wave");
            stage = extras.getInt("stage");
        }

//        timer_first =true;
        paused = false;
        game_paused = false;

        //add game_view to layout
        this.setContentView(R.layout.activity_main_stage);
        set_view_class(stage);
        game_activity = this;
//        songArray = getArrSong("stage1");
//        timer = new Timer(true);
        Button end_game= (Button) findViewById(R.id.end_game_1);
        end_game.setOnClickListener(this);
        Button music = (Button) findViewById(R.id.music_button_1);
        music.setOnClickListener(this);
//        combo_meter = findViewById(R.id.combo_meter);
//        multiplier = findViewById(R.id.multiplier);
    }

    private void loadRewardedVideoAd() {
        mRewardedVideoAd.loadAd("ca-app-pub-9072284262045894/8875933619",
                new AdRequest.Builder().build());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.end_game_1:
                if(stage == 1) {
                    GameView_stage1.getGame().end_game();
                }
                else if (stage ==2){
                    GameView_stage2.getGame().end_game();
                }
                else if (stage ==3){
                    GameView_stage3.getGame().end_game();
                }
                else{}
//               timer.cancel();
//               timer.purge();
                break;

            case R.id.music_button_1:
//                if(mp.isPlaying()){
//                    mp.pause();
//                }
//                else {
//                    mp.start();
//                }
                break;

        }

    }

    @Override
    public void onBackPressed() {
        return_to_menu();
    }

    public void return_to_menu(){
        Intent intent = new Intent(this, MenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

//    public static ArrayList<Integer> getArrSong(String venue) {
//        Field[] ID_Fields = R.raw.class.getFields();
//        songArray = new ArrayList<Integer>();
//        for (int i = 0; i < ID_Fields.length; i++) {
//            if (ID_Fields[i].getName()!= null &&
//                    ID_Fields[i].getName().contains(venue)){
//                try {
//                    //Log.d("musicidfieldname",ID_Fields[i].getName());
//                    songArray.add(ID_Fields[i].getInt(null));
//                }
//                catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                }}
//        }
//        Log.d("array print","array" + songArray.toString());
//        return songArray;
//    }

//    private void startFadeIn(){
//        final int FADE_DURATION = 3000; //The duration of the fade
//        //The amount of time between volume changes. The smaller this is, the smoother the fade
//        final int FADE_INTERVAL = 250;
//        final int MAX_VOLUME = 1; //The volume will increase from 0 to 1
//        int numberOfSteps = FADE_DURATION/FADE_INTERVAL; //Calculate the number of fade steps
//        //Calculate by how much the volume changes each step
//        final float deltaVolume = MAX_VOLUME / (float)numberOfSteps;
//
//        //Create a new Timer and Timer task to run the fading outside the main UI thread
//        final Timer timer = new Timer(true);
//        TimerTask timerTask = new TimerTask() {
//            @Override
//            public void run() {
//                fadeInStep(deltaVolume); //Do a fade step
//                //Cancel and Purge the Timer if the desired volume has been reached
//                if(volume>=1f){
//                    timer.cancel();
//                    timer.purge();
//                }
//            }
//        };
//
//        timer.schedule(timerTask,FADE_INTERVAL,FADE_INTERVAL);
//    }
//
//    private void startFadeOut(){
//        final int FADE_DURATION = 3000; //The duration of the fade
//        //The amount of time between volume changes. The smaller this is, the smoother the fade
//        final int FADE_INTERVAL = 250;
//        final int MAX_VOLUME = 1; //The volume will increase from 0 to 1
//        int numberOfSteps = FADE_DURATION/FADE_INTERVAL; //Calculate the number of fade steps
//        //Calculate by how much the volume changes each step
//        final float deltaVolume = MAX_VOLUME / (float)numberOfSteps;
//
//        //Create a new Timer and Timer task to run the fading outside the main UI thread
//        final Timer timer = new Timer(true);
//        TimerTask timerTask = new TimerTask() {
//            @Override
//            public void run() {
//                fadeOutStep(deltaVolume); //Do a fade step
//                //Cancel and Purge the Timer if the desired volume has been reached
//                if(volume<=0.5f){
//                    timer.cancel();
//                    timer.purge();
//                }
//            }
//        };
//
//        timer.schedule(timerTask,FADE_INTERVAL,FADE_INTERVAL);
//    }
//
//    private void fadeInStep(float deltaVolume){
////        mp.setVolume(volume, volume);
////        volume += deltaVolume;
//
//    }
//
//    private void fadeOutStep(float deltaVolume){
////        mp.setVolume(volume, volume);
////        volume -= deltaVolume;
//    }

    @Override
    public void onPause()
    {
        mRewardedVideoAd.pause(this);
        super.onPause();
        Log.d("timer", "onPause: timer canceled and purged");
        paused = true;
        game_paused = true;
//        if (mp != null && mp.isPlaying()) {
//            mp.stop();
//            mp.reset();
//        }
        GameRunner.getGameRunner().shutdown();
        // Do your stuff here when you are stopping your activity
    }
    @Override
    public void onResume() {
        mRewardedVideoAd.resume(this);
        super.onResume();
        Game.free_hold = false;
        paused = false;
//        if (mp != null && mp.isPlaying()) {
//        play_songs = new TimerTask() {
//            @Override
//            public void run() {
//                if (!paused) {
//                    if (!first_song) {
//                        startFadeOut();
//                        mp.stop();
//                        mp.reset();
//                    }
//                    if (i == songArray.size()) {
//                        i = 0;
//                    } else {
//                    }
//                    mp = MediaPlayer.create(getApplicationContext(), songArray.get(i));
//                    mp.start();
//                    startFadeIn();
//                    first_song = false;
//                    i++;
//                }
//            }
//        };
//        };
//        timer.scheduleAtFixedRate(play_songs, 3000, 180000);
//    }
    }

    @Override
    public void onDestroy() {
        mRewardedVideoAd.destroy(this);
        super.onDestroy();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    public void updateText(final int combo) {
        final String text = Integer.toString(combo);
        runOnUiThread(new Runnable() {
            public void run(){
//                TextView txtView = findViewById(R.id.combo_meter);
//                txtView.setText("/ "+text);
            }
        });
    }

    public void updateWaveText(final int wave) {
//        final String text = Integer.toString(wave_counter);
        runOnUiThread(new Runnable() {
            public void run() {
                TextView txtView = findViewById(R.id.wave_counter);
                txtView.setText("Wave " + Integer.toString(wave));
            }
        });
    }

    public void updateScoreText(final int score) {
//        final String text = Integer.toString(wave_counter);
        runOnUiThread(new Runnable() {
            public void run() {
                TextView txtView = findViewById(R.id.score);
                txtView.setText(Integer.toString(score));
            }
        });
    }

    public void update_end_ScoreText(final int score) {
//        final String text = Integer.toString(wave_counter);
        runOnUiThread(new Runnable() {
            public void run() {
                TextView txtView = findViewById(R.id.score);
                txtView.setTextColor(Color.parseColor("#FFFF00"));
            }
        });
    }

    public void reset_end_ScoreText() {
//        final String text = Integer.toString(wave_counter);
        runOnUiThread(new Runnable() {
            public void run() {
                TextView txtView = findViewById(R.id.score);
                txtView.setTextColor(Color.WHITE);
            }
        });
    }

    public void updateMultiplierText(final int multiplier) {
//        final String text = Integer.toString(wave_counter);
        runOnUiThread(new Runnable() {
            public void run() {
//                TextView txtView = findViewById(R.id.multiplier);
//                txtView.setText("x"+Integer.toString(multiplier));
            }
        });
    }

    public void updateMotivatorText(final String string, final int color) {
//        final String text = Integer.toString(wave_counter);

        runOnUiThread(new Runnable() {
            public void run() {
                TextView txtView = findViewById(R.id.motivator);
                txtView.setTextColor(color);
                txtView.setText(string);

            }
        });
    }

//    public void stylizeMotivatorText(){
//
//        final EmbossMaskFilter filter = new EmbossMaskFilter(
//                new float[]{1,5,1}, // direction of the light source
//                0.5f, // ambient light between 0 to 1
//                10, // specular highlights
//                7.5f // blur before applying lighting
//        );
//
//        runOnUiThread(new Runnable() {
//            public void run() {
//                //Emboss
//                TextView txtView = findViewById(R.id.motivator);
//                txtView.getPaint().setMaskFilter(filter);
//            }
//        });
//
//    }

    public void updateLife(final int life, final int default_life_count){
//        final ImageView heart_1 = findViewById(R.id.heart_1);
//        final ImageView heart_2 = findViewById(R.id.heart_2);
//        final ImageView heart_3 = findViewById(R.id.heart_3);
//        final ImageView heart_4 = findViewById(R.id.heart_4);
//        final ImageView heart_5 = findViewById(R.id.heart_5);
//        final ImageView heart_6 = findViewById(R.id.heart_6);
//        final ImageView heart_7 = findViewById(R.id.heart_7);
//
//        final HashMap<Integer,ImageView> hearts = new HashMap<>();
//        hearts.put(1,heart_1);
//        hearts.put(2,heart_2);
//        hearts.put(3,heart_3);
//        hearts.put(4,heart_4);
//        hearts.put(5,heart_5);
//        hearts.put(6,heart_6);
//        hearts.put(7,heart_7);
//        runOnUiThread(new Runnable() {
//            public void run() {
//
//                for(int i =1; i<8; i++){
//                    if(i<=life){
//                        hearts.get(i).setBackgroundResource(R.drawable.life_heart);
//                    }
//                    else{
//                        if(i<=default_life_count) {
//                            hearts.get(i).setBackgroundResource(R.drawable.broken_heart);
//                        }
//                        else{
//                            hearts.get(i).setBackgroundResource(R.drawable.blank_projectile);
//                        }
//                    }
//                }
//            }
//        });

    }

    public void updateShield(final int shield){
//        final ImageView shield_1 = findViewById(R.id.shield_1);
//        final ImageView shield_2 = findViewById(R.id.shield_2);
//        final ImageView shield_3 = findViewById(R.id.shield_3);
//        runOnUiThread(new Runnable() {
//            public void run() {
//                switch(shield){
//                    case 1:
//                        update_image_view(shield_1,R.drawable.shield_low);
//                        update_image_view(shield_2,R.drawable.blank_projectile);
//                        update_image_view(shield_3,R.drawable.blank_projectile);
//                        break;
//
//                    case 2:
//                        update_image_view(shield_1,R.drawable.shield_low);
//                        update_image_view(shield_2,R.drawable.shield_med);
//                        update_image_view(shield_3,R.drawable.blank_projectile);
//                        break;
//
//                    case 3:
//                        update_image_view(shield_1,R.drawable.shield_low);
//                        update_image_view(shield_2,R.drawable.shield_med);
//                        update_image_view(shield_3,R.drawable.shield_high);
//                        break;
//
//                    case 0:
//                        update_image_view(shield_1,R.drawable.blank_projectile);
//                        update_image_view(shield_2,R.drawable.blank_projectile);
//                        update_image_view(shield_3,R.drawable.blank_projectile);
//                        break;
//
//                    default:
//                        break;
//                }
//            }
//        });
    }

    public void updateChar_offense(final int char_offense){
//        final ImageView char_offense_1 = findViewById(R.id.char_offense_1);
//        final ImageView char_offense_2 = findViewById(R.id.char_offense_2);
//        final ImageView char_offense_3 = findViewById(R.id.char_offense_3);
//        runOnUiThread(new Runnable() {
//            public void run() {
//                switch(char_offense){
//                    case 1:
//                        update_image_view(char_offense_1,R.drawable.red_mini_sword);
//                        update_image_view(char_offense_2,R.drawable.blank_projectile);
//                        update_image_view(char_offense_3,R.drawable.blank_projectile);
//                        break;
//
//                    case 2:
//                        update_image_view(char_offense_1,R.drawable.red_mini_sword);
//                        update_image_view(char_offense_2,R.drawable.yellow_mini_sword);
//                        update_image_view(char_offense_3,R.drawable.blank_projectile);
//                        break;
//
//                    case 3:
//                        update_image_view(char_offense_1,R.drawable.red_mini_sword);
//                        update_image_view(char_offense_2,R.drawable.yellow_mini_sword);
//                        update_image_view(char_offense_3,R.drawable.blue_mini_sword);
//                        break;
//
//                    case 0:
//                        update_image_view(char_offense_1,R.drawable.blank_projectile);
//                        update_image_view(char_offense_2,R.drawable.blank_projectile);
//                        update_image_view(char_offense_3,R.drawable.blank_projectile);
//                        break;
//
//                    default:
//                        break;
//                }
//            }
//        });
    }

    public void updatereq(final HashMap<String,Integer> wave_completion, final int stage) {
        final ImageView image_req_1 = findViewById(R.id.req_img_1);
        final ImageView image_req_2 = findViewById(R.id.req_img_2);
        final ImageView image_req_3 = findViewById(R.id.req_img_3);
        final TextView text_req_1 = findViewById(R.id.req_qty_1);
        final TextView text_req_2 = findViewById(R.id.req_qty_2);
        final TextView text_req_3 = findViewById(R.id.req_qty_3);
        final HashMap<Integer, ImageView> filled_image = new HashMap<>();
        filled_image.put(1,image_req_1);
        filled_image.put(2,image_req_2);
        filled_image.put(3,image_req_3);

        final HashMap<Integer, TextView> filled_text = new HashMap<>();
        filled_text.put(1,text_req_1);
        filled_text.put(2,text_req_2);
        filled_text.put(3,text_req_3);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int fill = 1;
                for (Map.Entry<String, Integer> entry : wave_completion.entrySet()) {
                    if(entry.getValue() <=0){
                    }
                    else{
                        if(fill<4){
                            update_image_view_bitmap(filled_image.get(fill),
                                    MyBitmap.projectile_image_bitmap_hash.get(entry.getKey()));
                            filled_text.get(fill).setText(Integer.toString(entry.getValue())+" x ");
                            filled_text.get(fill).setGravity(Gravity.CENTER);
                            fill++;
                        }
                        else{}
                    }
                }

                for (int k=fill; k<4;k++){
                    update_image_view_bitmap(filled_image.get(fill),
                            MyBitmap.projectile_image_bitmap_hash.get("blank"));
                    filled_text.get(fill).setText("");
                }

            }
        });

    }

    public void setViewTextColor(final int stage, final int color){
        final TextView text_field1 = findViewById(R.id.req_qty_1);
        final TextView text_field2 = findViewById(R.id.req_qty_2);
        final TextView text_field3 = findViewById(R.id.req_qty_3);
//        final TextView text_field4 = findViewById(R.id.end_game_1);
//        final TextView text_field5 = findViewById(R.id.wave_counter);

        final HashMap<Integer, TextView> text_views = new HashMap<>();
        text_views.put(1,text_field1);
        text_views.put(2,text_field2);
        text_views.put(3,text_field3);
//        text_views.put(4,text_field4);
//        text_views.put(5,text_field5);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (Map.Entry<Integer,TextView> entry : text_views.entrySet()) {
                    entry.getValue().setTextColor(color);
                }

            }
        });

    }

    public static int[] get_score_coordinates(){
        int[] location = new int[2];
//        combo_meter.getLocationInWindow(location);
        location[0] = 0;
        location[1] = 0;
        return location;
    }

    public void set_view_class(int stage){

        LayoutInflater vi = getLayoutInflater();

        //change colors accordingly
        TextView heart_text = findViewById(R.id.heart_text);
//        TextView shield_text = findViewById(R.id.shield_text);
//        TextView char_offense_text = findViewById(R.id.char_offense_text);
        TextView score_text =findViewById(R.id.score);

        //set the view dynamically
        switch(stage){
            case 1:
                v = vi.inflate(R.layout.stage1, null);
                heart_text.setTextColor(Color.WHITE);
//                shield_text.setTextColor(Color.BLACK);
//                char_offense_text.setTextColor(Color.BLACK);
                score_text.setTextColor(Color.BLACK);
                break;

            case 2:
                v = vi.inflate(R.layout.stage2, null);
                heart_text.setTextColor(Color.WHITE);
//                shield_text.setTextColor(Color.WHITE);
//                char_offense_text.setTextColor(Color.WHITE);
                break;

            case 3:
                v = vi.inflate(R.layout.stage3, null);
                break;

            default:
                break;
        }

        // insert into main view
        ConstraintLayout insertPoint = findViewById(R.id.game_stage_view);
        insertPoint.addView(v, 0, new ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT));
    }

    private void update_image_view(ImageView image, int id){
        image.setImageResource(id);
        image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        image.setBackgroundColor(Color.TRANSPARENT);
    }

    private void update_image_view_bitmap(ImageView image, Bitmap bitmap){
        image.setImageBitmap(bitmap);
        image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        image.setBackgroundColor(Color.TRANSPARENT);
    }

    public void set_gameover_stars(int stars){
        ImageView s1 = findViewById(R.id.star_achievement_1);
        ImageView s2 = findViewById(R.id.star_achievement_2);
        ImageView s3 = findViewById(R.id.star_achievement_3);
        HashMap<Integer,ImageView> go_stars = new HashMap<>();
        go_stars.put(1,s1);
        go_stars.put(2,s2);
        go_stars.put(3,s3);

        HashMap<Integer,Integer> star_images = new HashMap<>();
        star_images.put(1,R.drawable.star1);
        star_images.put(2,R.drawable.star2);
        star_images.put(3,R.drawable.star3);
        star_images.put(0,R.drawable.star0);

        for (int i = 1; i<4; i++){
            if (i<=stars){
                update_image_view(go_stars.get(i),star_images.get(i));
            }

            else{
                update_image_view(go_stars.get(i),star_images.get(0));
            }
        }


    }

    public void change_life_text(int life){
        TextView life_text = findViewById(R.id.heart_text);
        life_text.setText(Integer.toString(life));
    }

    public void change_shield_text(int shield){
//        TextView shield_text = findViewById(R.id.shield_text);
//        shield_text.setText(Integer.toString(shield));
    }

    public void change_char_offense_text(int char_offense){
//        TextView char_offense_text = findViewById(R.id.char_offense_text);
//        char_offense_text.setText(Integer.toString(char_offense));
    }

    //ads stuff

    public boolean check_ad(){
        return mRewardedVideoAd.isLoaded();
    }

    public void show_ad(){
        runOnUiThread(new Runnable() {
            public void run(){
                if (mRewardedVideoAd.isLoaded()) {
                    mRewardedVideoAd.show();
                    //                    Log.d("ad", "show_ad: ad is loaded!");
                }
            }
        });
//        if (mRewardedVideoAd.isLoaded()) {
//            mRewardedVideoAd.show();
//            Log.d("ad", "show_ad: ad is loaded!");
//        }
    }

    @Override
    public void onRewarded(RewardItem reward) {
//        Toast.makeText(this, "onRewarded! currency: " + reward.getType() + "  amount: " +
//                reward.getAmount(), Toast.LENGTH_SHORT).show();
        // Reward the user.
        GameRunner.setPaused(false);
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
//        Toast.makeText(this, "onRewardedVideoAdLeftApplication",
//                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdClosed() {
//        Toast.makeText(this, "onRewardedVideoAdClosed", Toast.LENGTH_SHORT).show();
        // Load the next rewarded video ad.
        loadRewardedVideoAd();
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int errorCode) {
//        Toast.makeText(this, "onRewardedVideoAdFailedToLoad", Toast.LENGTH_SHORT).show();
          Toast.makeText(this, "Please enable your internet connection for CONTINUE(video) option :)", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onRewardedVideoAdLoaded() {
//        Toast.makeText(this, "onRewardedVideoAdLoaded", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdOpened() {
//        Toast.makeText(this, "onRewardedVideoAdOpened", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoStarted() {
//        Toast.makeText(this, "onRewardedVideoStarted", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoCompleted() {
//        Toast.makeText(this, "onRewardedVideoCompleted", Toast.LENGTH_SHORT).show();
    }
}