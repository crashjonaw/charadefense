package nexidea.kawaii_defense;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.protobuf.MapEntryLite;

import java.util.HashMap;
import java.util.Map;

public class Tutorial {

    private HashMap<String,HashMap<String,String>> tutorial_info = new HashMap<>();
    public static HashMap<String,Boolean> first_time_tracker = new HashMap<>();

    //can add a tracker to local_db if needed in the future

    private Resources resources;
    private int tutorial_counter;


    public Tutorial(Resources resources) {
        this.resources = resources;
        //teaching players how to make full use of the game and play it well!
        // first a foremost by creating the popups needed to go through the game
        // popups will be the same but just splitting into two kinds of popups:
        // 1. those popups without images (out_game)
        // 2. those popups with images (in_game) - new enemy, hearts, bombs, swords, shields, lives, broken heart
        // 2. - those with 3 images needed - heart/shield/attack

        HashMap<String,String> out_game = new HashMap<>();
        //explanations - out game
        //intro to w-coins
        out_game.put("w-coins","W-Coins are the main currency in the game.\nThey can be used to purchase in-game items such as:\n\nNew Characters\nIn-game Power Ups\nNew stages\nNew waves");

        //choosing a stage to enter
        out_game.put("choose_stage","As a new adventurer, you can start off with stage 1 unlock other stages thereafter!\nWithin each stage, there are waves to clear and you have to complete most of the waves before unlocking the next stage!\n\nAll the best!\nAdventurer!");

        //red waves means locked
        out_game.put("waves_intro","Blue bubbles are waves that have been unlocked and the stars represents how well you scored in the wave!\n\n You will unlock the next wave once completing the previous one!\nGood luck :)");

        //new tokens
        out_game.put("new_tokens","Congratulations, you can now use your newly obtained tokens to purchase new characters and items to enhance your performance when fighting!");

        HashMap<String,String> in_game = new HashMap<>();
        //explanations - in game
        //3 images - hearts/ shields/ swords
        in_game.put("heart","Welcome new adventurer!\n\n Dodge by dragging your character around!");
        in_game.put("shield","Shields provides extra defense against attackers!");
        in_game.put("sword","Tap & Drag to eliminate enemies!\n\n Have fun :)");

//        in_game.put("heart","Welcome new adventurer!\nYou begin with 3 lives at the beginning of wave you have chosen.\n\nAvoid objects hitting you with your shields off!");
//        in_game.put("shield","As an added bonus, each character begins with 1 shield, the red one!\n\n Subsequently, you can attain 2 more shields (yellow and blue) once you reach a certain combo count!");
//        in_game.put("sword","Lastly, each character begins with 1 sword!\nThis indicates that every tap on the enemy monsters will deal 1 damage it.\n\nPlus, each sword represents a new layer of defense for your character.\n\nYou can attain up to 2 more swords by gaining hearts after you reached your max life :)");

        in_game.put("broken_heart","A broken heart is shown when you have taken hit from a monster with shields down!");
        in_game.put("coconut","Beware of falling coconuts!\n\n They die when they touch the ground!\n\n");
        in_game.put("coconut_angry","The coconuts have become angry.\n\nRage!\nRage!\nRage!");
        in_game.put("coconut_black","Just let them be.\n They die after awhile!\n\n");
        in_game.put("banana","Bounce!\n\nBouncee!\n\nDie after two bounces!\n");
        in_game.put("banana_angry","Angry bananas bouncing!\n\nDie after four bounces!");
        in_game.put("tree_boss","1st Boss!\n\n Tap it before it rages!\n");
        in_game.put("urchin_boss","2nd Boss!!\n\n Tap it before it rages!\n");
        in_game.put("shuriken","Unforgiving Shurikens!\n\nDie after three bounces!");
        in_game.put("dagger","Watch out for daggers!\n\nBreaks all shields upon hit!");
        in_game.put("laser","Press!\n\n Before it fires!\n");
        in_game.put("boss","Prepare to fight!\n\nShow them who's boss!");
        in_game.put("fireball","Behind these rocks!\n\nDIAMONDS!\n\nLet them bounce!");
        tutorial_info.put("in_game",in_game);
        tutorial_info.put("out_game",out_game);

        for (Map.Entry<String,String> entry : in_game.entrySet()) {
            first_time_tracker.put(entry.getKey(),true);
        }
        for (Map.Entry<String,String> entry : out_game.entrySet()) {
            first_time_tracker.put(entry.getKey(),true);
        }
        tutorial_counter = 0;
    }

    private void popup_with_images(final Game game, String reference){
        //now let's do the dialog creation
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(game.getContext());
        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(game.getContext());
        View mView = inflater.inflate(R.layout.popup_new_image, null);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        ImageView s1 = mView.findViewById(R.id.new_image_1);
        ImageView s2 = mView.findViewById(R.id.new_image_2);
        ImageView s3 = mView.findViewById(R.id.new_image_3);
        TextView title = mView.findViewById(R.id.popup_new_image_title);
        TextView text = mView.findViewById(R.id.popup_new_image_text);

        if (reference == "heart" || reference == "shield" || reference == "sword") {

            switch (reference) {
                case "heart":
                    title.setText("Game Info");
                    update_image_view(s1, R.drawable.life_heart);
                    update_image_view(s2, R.drawable.life_heart);
                    update_image_view(s3, R.drawable.life_heart);
                    text.setText(tutorial_info.get("in_game").get(reference));
                    break;

                case "shield":
                    title.setText("Game Info");
                    update_image_view(s1, R.drawable.shield_low);
                    update_image_view(s2, R.drawable.shield_med);
                    update_image_view(s3, R.drawable.shield_high);
                    text.setText(tutorial_info.get("in_game").get(reference));
                    break;

                case "sword":
                    title.setText("Game Info");
                    update_image_view(s1, R.drawable.red_mini_sword);
                    update_image_view(s2, R.drawable.yellow_mini_sword);
                    update_image_view(s3, R.drawable.blue_mini_sword);
                    text.setText(tutorial_info.get("in_game").get(reference));
                    break;
            }
        } else {
            title.setText("New Enemy");
            update_image_view_bitmap(s2, MyBitmap.projectile_image_bitmap_hash.get(reference));
            text.setText(tutorial_info.get("in_game").get(reference));
        }


        Button understood = mView.findViewById(R.id.understood);
        understood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(--tutorial_counter==0) {
                    game.getGame_activity().setPaused(false);
                }
                else{}
                dialog.dismiss();

            }
        });


        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    public void show_tutorial_log(final Game game, final String reference) {
        if(first_time_tracker.get(reference)) {
            tutorial_counter++;
            game.getGame_activity().setPaused(true);
            first_time_tracker.put(reference, false);
//            Handler mHandler = new Handler(Looper.getMainLooper());
            game.tutorial_handler = new Handler(Looper.getMainLooper());
            game.tutorial_handler.post(new Runnable() {
                @Override
                public void run() {
                    popup_with_images(game, reference);
                }
            });
        }
        else{}
    }

    private void update_image_view(ImageView image, int id){
        image.setImageResource(id);
        image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        image.setBackgroundColor(ResourcesCompat.getColor(resources,android.R.color.transparent,null));
    }

    private void update_image_view_bitmap(ImageView image, Bitmap bitmap){
        image.setImageBitmap(bitmap);
        image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        image.setBackgroundColor(ResourcesCompat.getColor(resources,android.R.color.transparent,null));
    }

}
