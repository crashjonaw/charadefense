package nexidea.kawaii_defense;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class PreMenuView extends SurfaceView implements SurfaceHolder.Callback {
    private Bitmap background;
    private Animated_background background_animated;
    private BackgroundRunner_PreMenu runner;
    private Context context;
    private boolean waiting;


    public PreMenuView(Context context, AttributeSet attrs) {
        super(context, attrs);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        this.context = context;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
//        if (PreMenuActivity.getMp().isPlaying()){}
//        else {
//            PreMenuActivity.getMp().start();
//        }
        background = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
        background = Bitmap.createScaledBitmap(
                background,
                getWidth(),
                getHeight(),
                true);
        background_animated = new Animated_background(getContext(), getWidth(), getHeight(), holder, getResources(), background,0,0);
        background_animated.getMoving_background().load_background(background,0,0);
        runner = new BackgroundRunner_PreMenu(background_animated,context,getResources(),getWidth(),getHeight());
        runner.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        //

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
//        PreMenuActivity.getMp().pause();
        if (runner != null){
            runner.shutdown();
            while(runner != null){
                try {
                    runner.join();
                    runner=null;
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public int getNavHeight(){
        //navigation bar height
        int navigationBarHeight = 0;
        int resourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            navigationBarHeight = getResources().getDimensionPixelSize(resourceId);
        }
        return navigationBarHeight;
    }
}


