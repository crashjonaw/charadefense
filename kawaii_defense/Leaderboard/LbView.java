package nexidea.kawaii_defense.Leaderboard;

import android.content.Context;
import android.graphics.Bitmap;

import android.graphics.Paint;

import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import nexidea.kawaii_defense.Animated_background;
import nexidea.kawaii_defense.BackgroundAesthetics.BackgroundRunner;


public class LbView extends SurfaceView implements SurfaceHolder.Callback {
    private Paint bhs_paint = new Paint();
    private Bitmap background;
    private Animated_background background_animated;
    private BackgroundRunner runner;
    private int prev_bhs;
    public static Context lbviewcontext;


    public LbView(Context context, AttributeSet attrs) {
        super(context, attrs);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (LbActivity.getMp().isPlaying()){}
        else {
            LbActivity.getMp().start();
        }
//        background = BitmapFactory.decodeResource(getResources(), R.drawable.stage_1);
//        background = Bitmap.createScaledBitmap(background, background.getWidth()*getHeight()/background.getHeight(), getHeight(),true);
//        background_animated = new Animated_background(getContext(), getWidth(), getHeight(), holder, getResources(), background,0.1f,0);
//        background_animated.getMoving_background().load_background(background,0.1f,0);
//        runner = new BackgroundRunner(background_animated);
//        runner.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        LbActivity.getMp().pause();
        if (runner != null){
            runner.shutdown();
            while(runner != null){
                try {
                    runner.join();
                    runner=null;
                } catch (InterruptedException e) {
                }
            }
        }
    }

}


