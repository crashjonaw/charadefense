package nexidea.kawaii_defense.Leaderboard;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nexidea.kawaii_defense.MenuActivity;
import nexidea.kawaii_defense.MenuView;
import nexidea.kawaii_defense.R;

import static android.content.ContentValues.TAG;


public class LbActivity extends AppCompatActivity implements View.OnClickListener{
    private Button button_lib;
    private Button button_beach;
    private Button return_home;
    private Map<String, Integer> lb = new HashMap<String, Integer>();
    private static MediaPlayer mp;
    private List<Map.Entry<String, Integer>> sorted_list;
    private final String MY_PREFS_NAME = "local_db";
    private CollectionReference db = FirebaseFirestore.getInstance().collection("players");

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lb);
//        mp = MediaPlayer.create(getApplicationContext(), R.raw.lb_final);
//        mp.setLooping(true);
//        mp.start();
        ImageButton button_lb_home = findViewById(R.id.button_lb_home);
        button_lb_home.setOnClickListener(this); // calling onClick() method
        final TableLayout t1 = findViewById(R.id.t1);

        //get firestore data
        Query query = db.orderBy("highscore",Query.Direction.DESCENDING).limit(10);
        query.get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                //Log.d("object loop", "surfaceCreated: looping:" + entry.getKey() + entry.getValue().toString());
                                TableRow tr = new TableRow(getApplicationContext());
                                TableLayout.LayoutParams tr_params = new TableLayout.LayoutParams(
                                        TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT,3
                                );
                                tr.setLayoutParams(tr_params);


                                TextView scr1 = new TextView(getApplicationContext());
                                TextView scr2 = new TextView(getApplicationContext());
                                TextView scr3 = new TextView(getApplicationContext());

                                TableRow.LayoutParams scr_params = new TableRow.LayoutParams(
                                        0,(int) getResources().getDimension(R.dimen._40sdp),1
                                );
                                scr1.setLayoutParams(scr_params);
                                scr2.setLayoutParams(scr_params);
                                scr3.setLayoutParams(scr_params);

                                //player_name
                                scr1.setText((String) document.getData().get("name"));
                                scr1.setTextSize(getResources().getDimension(R.dimen._5ssp));
                                scr1.setTextColor(Color.BLACK);
                                scr1.setGravity(Gravity.CENTER);
                                scr1.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.cell_shape_player,null));
                                //scr1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

                                //stage
                                scr2.setText(document.getData().get("stage").toString());
                                scr2.setTextSize(getResources().getDimension(R.dimen._5ssp));
                                scr2.setTextColor(Color.parseColor("#ffffff"));
                                scr2.setGravity(Gravity.CENTER);
                                scr2.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.cell_shape_stage,null));

                                //score
                                scr3.setText(document.getData().get("highscore").toString());
                                scr3.setTextSize(getResources().getDimension(R.dimen._5ssp));
                                scr3.setTextColor(Color.parseColor("#ffffff"));
                                scr3.setGravity(Gravity.CENTER);
                                scr3.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.cell_shape_score,null));
                                //scr2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));


                                tr.addView(scr1);
                                tr.addView(scr2);
                                tr.addView(scr3);
                                t1.addView(tr);
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });


        SharedPreferences prefs = MenuView.menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

        int personal_best = prefs.getInt("personal_best",0);
        TextView personal_best_view = findViewById(R.id.personal_best);
        personal_best_view.setText(" Personal Best: "+Integer.toString(personal_best));

        int latest_score = prefs.getInt("latest_score",0);
        TextView latest_score_view = findViewById(R.id.latest_score);
        latest_score_view.setText(" Latest Score: "+Integer.toString(latest_score));

        t1.requestLayout();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_lb_home:
//                mp.stop();
                return_to_menu();
                break;
        }
    }


    public void return_to_menu(){
        Intent intent = new Intent(this,MenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
//        mp.stop();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public static MediaPlayer getMp() {
        return mp;
    }
}
