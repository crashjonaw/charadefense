package nexidea.kawaii_defense;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;

import java.util.HashMap;

public class MyBitmap {

    private Resources resources;
    private Context context;
    private int s_width;
    private int s_height;

    public static HashMap<String,Bitmap> projectile_image_bitmap_hash = new HashMap<>();
    public static HashMap<String,HashMap<String,Bitmap>> character_image_bitmap_hash = new HashMap<>();
    public static HashMap<String, HashMap<Integer, Bitmap>> general_bitmap_hash = new HashMap<>();
    public static HashMap<Integer,Bitmap> ink_bitmap_hash = new HashMap<>();
    public static HashMap<String, HashMap<String, Bitmap>> stage1_bitmap_hash = new HashMap<>();
    public static HashMap<String, HashMap<String, Bitmap>> stage2_bitmap_hash = new HashMap<>();
    public static HashMap<String, HashMap<String, Bitmap>> stage3_bitmap_hash = new HashMap<>();
    public static HashMap<Integer,Bitmap> stage3_diamond_bitmap_hash = new HashMap<>();
    public static HashMap<Integer,Bitmap> char_offense_bitmap_hash = new HashMap<>();
    public static HashMap<String,Bitmap> char_offense_bitmap_hashv2 = new HashMap<>();
    public static HashMap<String,Integer> char_offense_attributes = new HashMap<>();
    public static HashMap<String,HashMap<Integer,Bitmap>> char_press_effect = new HashMap<>();
    public static HashMap<String,HashMap<Integer,Bitmap>> char_press_effect_mini = new HashMap<>();
    public static HashMap<String,HashMap<Integer,Bitmap>> char_tap_effect = new HashMap<>();
    public static HashMap<String, Bitmap[]> drag_effect_bitmap_inventory = new HashMap<>();
    public static HashMap<Integer,Bitmap> stage1_bg_hash = new HashMap<>();

    public MyBitmap(Resources resources, Context context, int s_width, int s_height) {
        this.resources = resources;
        this.context = context;
        this.s_width = s_width;
        this.s_height = s_height;
        float game_scale = 0.5f;
        long now = System.currentTimeMillis();
        long TotalLoadTime = PreMenuActivity.local_db.getLong("TotalLoadTime",10000);
        ((PreMenuActivity) context).setLoadedComponent("Starting to load...");
        load_scores();
        display_log(now,TotalLoadTime,"loaded scores.");
        load_health_bars();
        display_log(now,TotalLoadTime,"loaded health bars.");
        load_combo_effects(game_scale);
        display_log(now,TotalLoadTime,"loaded combo effects");
        load_stage1_bitmap(game_scale);
        display_log(now,TotalLoadTime,"loaded stage 1 components.");
        load_stage2_bitmaps(1);
        display_log(now,TotalLoadTime,"loaded stage 2 components.");
        load_stage3_bitmaps(1);
        display_log(now,TotalLoadTime,"loaded stage 3 components.");
        load_tap_effects();
        display_log(now,TotalLoadTime,"loaded tap effects.");
        load_character_bitmap_hash();
        display_log(now,TotalLoadTime,"loaded characters.");
        load_all_req_projectiles(game_scale);
        display_log(now,TotalLoadTime,"loaded projectiles.");
        load_char_offense_bitmaps();
        display_log(now,TotalLoadTime,"loaded offense effects.");
        load_ink_bitmaps();
        ((PreMenuActivity) context).setLoadedComponent("loaded ink.");
        ((PreMenuActivity) context).setProgressNumber(1);
        TotalLoadTime = System.currentTimeMillis() - now;
        ((PreMenuActivity) context).setTotalTimeTaken(TotalLoadTime);
        PreMenuActivity.editor.putLong("TotalLoadTime", TotalLoadTime);
        PreMenuActivity.editor.apply();

        //load bgs
//        load_stage1_bgs();
    }

    //convert a drawable to a Bitmap image
    private Bitmap getBitmap(int drawableRes) {
        Drawable drawable = ResourcesCompat.getDrawable(resources,drawableRes,null);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_4444);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public void load_health_bars(){
        Bitmap health_bar = Bitmap.createScaledBitmap(getBitmap(R.drawable.health_bar)
                , s_width/5, s_height/30, true);
        HashMap<Integer,Bitmap> health_bar_map = new HashMap<>();
        health_bar_map.put(1,health_bar);
        general_bitmap_hash.put("hp", health_bar_map);
    }

    public void load_scores() {
        //put score bitmaps
        Bitmap plus_1 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.plus_1),
                s_width / 6, s_width / 7, true);
        Bitmap plus_2 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.plus_2),
                s_width / 6, s_width / 7, true);
        Bitmap plus_3 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.plus_3),
                s_width / 6, s_width / 7, true);
        Bitmap plus_4 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.plus_4),
                s_width / 6, s_width / 7, true);
        Bitmap plus_8 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.plus_8),
                s_width / 6, s_width / 7, true);
        Bitmap plus_16 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.plus_16),
                s_width / 6, s_width / 7, true);
        Bitmap plus_32 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.plus_32),
                s_width / 6, s_width / 7, true);
        Bitmap plus_64 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.plus_64),
                s_width / 6, s_width / 7, true);
        Bitmap plus_128 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.plus_128),
                s_width / 5, s_width / 6, true);
        Bitmap plus_100 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.plus_100),
                s_width / 3, s_width / 6, true);
        Bitmap plus_200 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.plus_200),
                s_width / 3, s_width / 6, true);
        Bitmap plus_400 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.plus_400),
                s_width / 3, s_width / 6, true);
        Bitmap plus_800 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.plus_800),
                s_width / 3, s_width / 6, true);
        Bitmap plus_1600 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.plus_1600),
                s_width / 2, s_width / 6, true);
        Bitmap plus_3200 = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.plus_3200),
                s_width / 2, s_width / 6, true);

        HashMap<Integer, Bitmap> score_bitmap_inventory = new HashMap<>();
        score_bitmap_inventory.put(1, plus_1);
        score_bitmap_inventory.put(2, plus_2);
        score_bitmap_inventory.put(3, plus_3);
        score_bitmap_inventory.put(4, plus_4);
        score_bitmap_inventory.put(8, plus_8);
        score_bitmap_inventory.put(16, plus_16);
        score_bitmap_inventory.put(32, plus_32);
        score_bitmap_inventory.put(64, plus_64);
        score_bitmap_inventory.put(128, plus_128);
        score_bitmap_inventory.put(100, plus_100);
        score_bitmap_inventory.put(200, plus_200);
        score_bitmap_inventory.put(400, plus_400);
        score_bitmap_inventory.put(800, plus_800);
        score_bitmap_inventory.put(1600, plus_1600);
        score_bitmap_inventory.put(3200, plus_3200);

        general_bitmap_hash.put("score", score_bitmap_inventory);
    }

    public void load_combo_effects(float game_scale) {
        //score_effect bitmaps
//        int combo_effect_scale = 10;
//        HashMap<Integer, Bitmap> combo_effect_bitmap_inventory = new HashMap<>();
//        Bitmap combo_effect_1 = Bitmap.createScaledBitmap(getBitmap(R.drawable.square_effect_black)
//                , (int) (s_width / combo_effect_scale * 1.2 * game_scale), (int) (s_width / combo_effect_scale * 1.2 * game_scale), true);
//        combo_effect_bitmap_inventory.put(1, combo_effect_1);
//        Bitmap combo_effect_2 = Bitmap.createScaledBitmap(getBitmap(R.drawable.square_effect_blue)
//                , (int) (s_width / combo_effect_scale * 1.2 * game_scale), (int) (s_width / combo_effect_scale * 1.2 * game_scale), true);
//        combo_effect_bitmap_inventory.put(2, combo_effect_2);
//        Bitmap combo_effect_3 = Bitmap.createScaledBitmap(getBitmap(R.drawable.square_effect_gold)
//                , (int) (s_width / combo_effect_scale * 1.2 * game_scale), (int) (s_width / combo_effect_scale * 1.2 * game_scale), true);
//        combo_effect_bitmap_inventory.put(3, combo_effect_3);
//        Bitmap combo_effect_4 = Bitmap.createScaledBitmap(getBitmap(R.drawable.square_effect_green)
//                , (int) (s_width / combo_effect_scale * 1.2 * game_scale), (int) (s_width / combo_effect_scale * 1.2 * game_scale), true);
//        combo_effect_bitmap_inventory.put(4, combo_effect_4);
//        Bitmap combo_effect_5 = Bitmap.createScaledBitmap(getBitmap(R.drawable.square_effect_red)
//                , (int) (s_width / combo_effect_scale * 1.2 * game_scale), (int) (s_width / combo_effect_scale * 1.2 * game_scale), true);
//        combo_effect_bitmap_inventory.put(5, combo_effect_5);
//        general_bitmap_hash.put("combo effect", combo_effect_bitmap_inventory);
    }

    public void load_tap_effects() {
        int slice_size = 50;
        Bitmap white_s = Bitmap.createScaledBitmap(getBitmap(R.drawable.slice_white)
                , s_width / slice_size, s_width / slice_size, true);
        Bitmap grey_s = Bitmap.createScaledBitmap(getBitmap(R.drawable.slice_grey)
                , s_width / slice_size, s_width / slice_size, true);
        Bitmap green_s = Bitmap.createScaledBitmap(getBitmap(R.drawable.slice_green)
                , s_width / slice_size, s_width / slice_size, true);
        Bitmap red_s = Bitmap.createScaledBitmap(getBitmap(R.drawable.slice_red)
                , s_width / slice_size, s_width / slice_size, true);
        Bitmap blue_s = Bitmap.createScaledBitmap(getBitmap(R.drawable.slice_blue)
                , s_width / slice_size, s_width / slice_size, true);
        Bitmap yellow_s = Bitmap.createScaledBitmap(getBitmap(R.drawable.slice_yellow)
                , s_width / slice_size, s_width / slice_size, true);
        Bitmap purple_s = Bitmap.createScaledBitmap(getBitmap(R.drawable.slice_purple)
                , s_width / slice_size, s_width / slice_size, true);
        Bitmap gold_s = Bitmap.createScaledBitmap(getBitmap(R.drawable.slice_gold)
                , s_width / slice_size, s_width / slice_size, true);
        Bitmap black_s = Bitmap.createScaledBitmap(getBitmap(R.drawable.slice_black)
                , s_width / slice_size, s_width / slice_size, true);
//      puffy tap - white grey black
//      yuri tap - red, yellow, gold
//      summer yuri tap - blue, white, purple
        Bitmap[] puffy = new Bitmap[3];
        puffy[0] = white_s;
        puffy[1] = grey_s;
        puffy[2] = black_s;
        drag_effect_bitmap_inventory.put("puffy", puffy);
        Bitmap[] yuri = new Bitmap[3];
        yuri[0] = red_s;
        yuri[1] = yellow_s;
        yuri[2] = gold_s;
        drag_effect_bitmap_inventory.put("yuri", yuri);
        Bitmap[] summer_yuri = new Bitmap[3];
        summer_yuri[0] = blue_s;
        summer_yuri[1] = purple_s;
        summer_yuri[2] = white_s;
        drag_effect_bitmap_inventory.put("summer_yuri", summer_yuri);
    }

    public void load_stage1_bitmap(float game_scale) {

        float boss_game_scale = game_scale *1.5f;
        //bitmap - coconut
        HashMap<String, Bitmap> coconut = new HashMap<>();
        coconut.put("image_1", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.coconut),
                (int) (s_width / 6 * game_scale*1.2), (int) (s_width / 6 * game_scale*1.2), true));

        coconut.put("image_2", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.coconut),
                (int) (s_width / 8 * game_scale*1.2), (int) (s_width / 8 * game_scale*1.2), true));

        coconut.put("image_3", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.coconut),
                (int) (s_width / 4 * game_scale*1.2), (int) (s_width / 4 * game_scale*1.2), true));

        coconut.put("dead_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.broken_heart),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));
        coconut.put("collision_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.broken_heart),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));
        stage1_bitmap_hash.put("coconut", coconut);

        //coconut_angry
        HashMap<String, Bitmap> coconut_angry = new HashMap<>();
        coconut_angry.put("image_1", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.coconut_angry),
                (int) (s_width / 6 * game_scale *1.2), (int) (s_width / 6 * game_scale *1.2), true));

        coconut_angry.put("image_2", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.coconut_angry),
                (int) (s_width / 8 * game_scale *1.2), (int) (s_width / 8 * game_scale *1.2), true));

        coconut_angry.put("image_3", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.coconut_angry),
                (int) (s_width / 4 * game_scale *1.2), (int) (s_width / 4 * game_scale *1.2), true));

        coconut_angry.put("dead_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.broken_heart),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));
        coconut_angry.put("collision_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.broken_heart),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));
        stage1_bitmap_hash.put("coconut_angry", coconut_angry);

        //coconut_black
        HashMap<String, Bitmap> coconut_black = new HashMap<>();
        coconut_black.put("image_1", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.coconut_black),
                (int) (s_width / 6 * game_scale *1.2), (int) (s_width / 6 * game_scale *1.2), true));

        coconut_black.put("image_2", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.coconut_black),
                (int) (s_width / 8 * game_scale *1.2), (int) (s_width / 8 * game_scale *1.2), true));

        coconut_black.put("image_3", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.coconut_black),
                (int) (s_width / 4 * game_scale *1.2), (int) (s_width / 4 * game_scale *1.2), true));

        coconut_black.put("dead_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.broken_heart),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));
        coconut_black.put("collision_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.broken_heart),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));
        stage1_bitmap_hash.put("coconut_black", coconut_black);

        //bitmap - banana
        HashMap<String, Bitmap> banana = new HashMap<>();
        banana.put("image_1", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.banana),
                (int) (s_width / 5 * game_scale*1.2), (int) (s_width / 6 * game_scale*1.2), true));

        banana.put("image_2", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.banana),
                (int) (s_width / 6 * game_scale), (int) (s_width / 7 * game_scale), true));

        banana.put("image_3", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.banana),
                (int) (s_width / 3 * game_scale), (int) (s_width / 4 * game_scale), true));

        banana.put("dead_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.broken_heart),
                (int) (s_width / 5 * game_scale), (int) (s_width / 6 * game_scale), true));
        banana.put("collision_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.broken_heart),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));
        stage1_bitmap_hash.put("banana", banana);

        HashMap<String, Bitmap> banana_angry = new HashMap<>();
        banana_angry.put("image_1", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.banana_angry),
                (int) (s_width / 5 * game_scale*1.2), (int) (s_width / 6 * game_scale*1.2), true));
        banana_angry.put("image_2", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.banana_angry),
                (int) (s_width / 6 * game_scale), (int) (s_width /7 * game_scale), true));
        banana_angry.put("image_3", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.banana_angry),
                (int) (s_width / 3 * game_scale), (int) (s_width / 4 * game_scale), true));

        banana_angry.put("dead_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.broken_heart),
                (int) (s_width / 5 * game_scale), (int) (s_width / 6 * game_scale), true));

        banana_angry.put("collision_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.broken_heart),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));
        stage1_bitmap_hash.put("banana_angry", banana_angry);

        //hearts and bombs bitmap
        HashMap<String, Bitmap> heart = new HashMap<>();
        heart.put("image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.life_heart),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));
        heart.put("dead_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.life_heart_delivered),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));
        heart.put("collision_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.life_heart_delivered),
                (int) (s_width / 5 * game_scale), (int) (s_width / 5 * game_scale), true));
        stage1_bitmap_hash.put("heart", heart);


        //hearts and bombs bitmap
        HashMap<String, Bitmap> bomb = new HashMap<>();
        bomb.put("image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.star),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));
        bomb.put("dead_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.bomb_broken_1),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));
        bomb.put("collision_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.bomb_broken_1),
                (int) (s_width / 5 * game_scale), (int) (s_width / 5 * game_scale), true));
        stage1_bitmap_hash.put("bomb", bomb);

        //boss bitmap
        HashMap<String, Bitmap> tree_boss = new HashMap<>();
        tree_boss.put("rage_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.stage1_tree_boss_rage),
                (int) (s_height / 4 * 0.77 * boss_game_scale), (int) (s_height / 4 * boss_game_scale), true));
        tree_boss.put("calm_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.stage1_tree_boss_calm),
                (int) (s_height / 4 * 0.77 * boss_game_scale), (int) (s_height / 4 * boss_game_scale), true));
        tree_boss.put("dead_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.stage1_tree_boss_dead),
                (int) (s_height / 4 * 0.77 * boss_game_scale), (int) (s_height / 4 * boss_game_scale), true));
        tree_boss.put("x_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.x_mark2),
                (int) (s_height / 6 * game_scale), (int) (s_height / 6 * game_scale), true));
        stage1_bitmap_hash.put("tree_boss", tree_boss);

        //urchin_boss bitmap
        HashMap<String, Bitmap> urchin_boss = new HashMap<>();
        urchin_boss.put("rage_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.sea_urchin_boss_base_rage),
                (int) (s_height / 4 * boss_game_scale ), (int) (s_height / 4 * boss_game_scale), true));
        urchin_boss.put("rage_image_eyes", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.sea_urchin_boss_eyes_rage),
                (int) (s_height / 4 * boss_game_scale), (int) (s_height / 4 * boss_game_scale), true));
        urchin_boss.put("calm_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.sea_urchin_boss_base),
                (int) (s_height / 4  * boss_game_scale), (int) (s_height / 4 * boss_game_scale), true));
        urchin_boss.put("calm_image_eyes", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.sea_urchin_boss_eyes),
                (int) (s_height / 4  * boss_game_scale), (int) (s_height / 4 * boss_game_scale), true));
        urchin_boss.put("dead_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.sea_urchin_boss_base_dead),
                (int) (s_height / 4  * boss_game_scale), (int) (s_height / 4 *boss_game_scale), true));
        urchin_boss.put("x_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.x_mark),
                (int) (s_height / 6 * game_scale), (int) (s_height / 6 * game_scale), true));
        stage1_bitmap_hash.put("urchin_boss", urchin_boss);

        //shuriken
        HashMap<String, Bitmap> shuriken = new HashMap<>();
        shuriken.put("image_1", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.house_projectile_1),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));
        shuriken.put("image_2", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.house_projectile_1),
                (int) (s_width / 6 * game_scale * 0.8), (int) (s_width / 6 * game_scale * 0.8), true));
        shuriken.put("image_3", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.house_projectile_1),
                (int) (s_width / 6 * game_scale * 1.5), (int) (s_width / 6 * game_scale * 1.5), true));
        shuriken.put("dead_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.blank_projectile),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));
        shuriken.put("collision_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.broken_heart),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));
        stage1_bitmap_hash.put("shuriken", shuriken);

        //dagger
        HashMap<String, Bitmap> dagger = new HashMap<>();
        dagger.put("image_1", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.house_projectile_2),
                (int) (s_width / 9 * game_scale), (int) (s_width / 3 * game_scale), true));
        dagger.put("image_2", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.house_projectile_2),
                (int) (s_width / 9 * game_scale*0.8), (int) (s_width / 3 * game_scale*0.5), true));
        dagger.put("image_3", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.house_projectile_2),
                (int) (s_width / 9 * game_scale*1.5), (int) (s_width / 3 * game_scale*1.5), true));
        dagger.put("dead_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.blank_projectile),
                (int) (s_width / 9 * game_scale), (int) (s_width / 3 * game_scale), true));
        dagger.put("collision_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.broken_heart),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));
        stage1_bitmap_hash.put("dagger", dagger);
    }

    public void load_stage2_bitmaps(float game_scale) {
        float ball_game_scale = 0.5f;
        int inverse_size = 60;
        Bitmap blue_laser_v = Bitmap.createScaledBitmap(getBitmap(R.drawable.laser_blue_v)
                , s_width / inverse_size, s_height, true);
        Bitmap red_laser_v = Bitmap.createScaledBitmap(getBitmap(R.drawable.laser_red_v)
                , s_width / inverse_size, s_height, true);
        Bitmap off_laser_v = Bitmap.createScaledBitmap(getBitmap(R.drawable.laser_off)
                , s_width / inverse_size, s_height, true);
        Bitmap score_laser_v = Bitmap.createScaledBitmap(getBitmap(R.drawable.laser_green)
                , s_width / inverse_size, s_height, true);
        Bitmap blue_laser_h = Bitmap.createScaledBitmap(getBitmap(R.drawable.laser_blue_h)
                , s_width, s_width / inverse_size, true);
        Bitmap red_laser_h = Bitmap.createScaledBitmap(getBitmap(R.drawable.laser_red_h)
                , s_width, s_width / inverse_size, true);
        Bitmap off_laser_h = Bitmap.createScaledBitmap(getBitmap(R.drawable.laser_off)
                , s_width, s_width / inverse_size, true);
        Bitmap score_laser_h = Bitmap.createScaledBitmap(getBitmap(R.drawable.laser_green)
                , s_width, s_width / inverse_size, true);

        //vertical warning sign
        Bitmap v_warning = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.vertical_warning),
                s_width / 6, s_width / 6, true);

        Bitmap v_warning_flash = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.flash_vertical_warning),
                s_width / 6, s_width / 6, true);

        //horizontal warning sign
        Bitmap h_warning = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.horizontal_warning),
                s_width / 6, s_width / 6, true);

        Bitmap h_warning_flash = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.flash_horizontal_warning),
                s_width / 6, s_width / 6, true);

        Bitmap warning_flash = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.warning_flash),
                s_width / 6, s_width / 6, true);

        HashMap<String, Bitmap> horizontal_laser_bitmap = new HashMap<>();
        horizontal_laser_bitmap.put("warning_image", h_warning);
        horizontal_laser_bitmap.put("warning_flash", h_warning_flash);
        horizontal_laser_bitmap.put("blue_image", blue_laser_h);
        horizontal_laser_bitmap.put("red_image", red_laser_h);
        horizontal_laser_bitmap.put("dead_image", off_laser_h);
        horizontal_laser_bitmap.put("score_image", score_laser_h);
        stage2_bitmap_hash.put("horizontal", horizontal_laser_bitmap);

        HashMap<String, Bitmap> vertical_laser_bitmap = new HashMap<>();
        vertical_laser_bitmap.put("warning_image", v_warning);
        vertical_laser_bitmap.put("warning_flash", v_warning_flash);
        vertical_laser_bitmap.put("blue_image", blue_laser_v);
        vertical_laser_bitmap.put("red_image", red_laser_v);
        vertical_laser_bitmap.put("dead_image", off_laser_v);
        vertical_laser_bitmap.put("score_image", score_laser_v);
        stage2_bitmap_hash.put("vertical", vertical_laser_bitmap);

        //boss bitmap
        HashMap<String, Bitmap> boss = new HashMap<>();
        boss.put("rage_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.stage2_char_boss_rage),
                (int) (s_height / 3  * game_scale), (int) (s_height / 3 * 0.65 * game_scale), true));
        boss.put("calm_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.stage2_char_boss_neutral),
                (int) (s_height / 3  * game_scale), (int) (s_height / 3 * 0.65* game_scale), true));
        boss.put("dead_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.stage2_boss_dead),
                (int) (s_height / 3  * game_scale), (int) (s_height / 3 * 0.65 * game_scale), true));
        boss.put("injured_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                    resources, R.drawable.stage2_char_boss_injured),
                (int) (s_height / 3  * game_scale), (int) (s_height / 3 * 0.65 * game_scale), true));
        boss.put("dead_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.stage2_boss_dead),
                (int) (s_height / 4 * 0.77 * game_scale), (int) (s_height / 4 * game_scale), true));
        boss.put("preview_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.stage2_char_boss_preview),
                (int) (s_height / 3  * 1.05 * game_scale), (int) (s_height / 3 * 1.05 * 0.65 * game_scale), true));
        boss.put("sp_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                    resources, R.drawable.stage2_char_boss),
                (int) (s_height / 3  * 1.05 * game_scale), (int) (s_height / 3 * 1.05 * 0.65 * game_scale), true));
        boss.put("x_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.stage2_char_boss_target),
                (int) (s_height / 6 * 0.75), (int) (s_height / 6 * 0.75f * 0.65f), true));

        boss.put("dark_ball_11", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.dark_ball),
                (int) (s_width / 6 * ball_game_scale), (int) (s_width / 6 * ball_game_scale), true));
        boss.put("dark_ball_12", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.dark_ball),
                (int) (s_width / 4 * ball_game_scale), (int) (s_width / 4 * ball_game_scale), true));
        boss.put("dark_ball_13", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.dark_ball),
                (int) (s_width / 8 * ball_game_scale), (int) (s_width / 8 * ball_game_scale), true));
        stage2_bitmap_hash.put("boss", boss);
        boss.put("dark_ball_21", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.dark_ball_type2),
                (int) (s_width / 6 * ball_game_scale), (int) (s_width / 6 * ball_game_scale), true));
        boss.put("dark_ball_22", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.dark_ball_type2),
                (int) (s_width / 4 * ball_game_scale), (int) (s_width / 4 * ball_game_scale), true));
        boss.put("dark_ball_23", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.dark_ball_type2),
                (int) (s_width / 8 * ball_game_scale), (int) (s_width / 8 * ball_game_scale), true));
        boss.put("dark_ball_31", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.dark_ball_type3),
                (int) (s_width / 4 * ball_game_scale * 0.5), (int) (s_width / 4 * ball_game_scale * 0.5), true));
        boss.put("dark_ball_32", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.dark_ball_type3),
                (int) (s_width / 4 * ball_game_scale), (int) (s_width / 4 * ball_game_scale), true));
        boss.put("dark_ball_33", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.dark_ball_type3),
                (int) (s_width / 4 * ball_game_scale * 1.25), (int) (s_width / 4 * ball_game_scale * 1.25), true));
        stage2_bitmap_hash.put("boss", boss);

    }

    public void load_stage3_bitmaps(float game_scale) {
        //zone3 files

        Bitmap red = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.red),(int)(s_width/6*game_scale),(int)(s_width/6*game_scale),true);
        Bitmap blue = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.blue),(int)(s_width/6*game_scale),(int)(s_width/6*game_scale),true);
        Bitmap purple = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.purple),(int)(s_width/6*game_scale),(int)(s_width/6*game_scale),true);
        Bitmap yellow = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.yellow),(int)(s_width/6*game_scale),(int)(s_width/6*game_scale),true);
        Bitmap green = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.green),(int)(s_width/6*game_scale),(int)(s_width/6*game_scale),true);
        // dead_fire suppose to be dead_fire_diamond.png but for now, its ugly, so use blank instead

        HashMap<String, Bitmap> fireball = new HashMap<>();
        fireball.put("image_1", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.fireball),
                (int) (s_width / 6 * game_scale*0.9), (int) (s_width / 6 * game_scale*0.9), true));
        fireball.put("image_2", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.fireball),
                (int) (s_width / 6 * game_scale*0.5), (int) (s_width / 6 * game_scale*0.5), true));
        fireball.put("image_3", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.fireball),
                (int) (s_width / 6 * game_scale*1.2), (int) (s_width / 6 * game_scale*1.2), true));
        fireball.put("dead_image", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.blank_projectile),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));
        fireball.put("broken_heart", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.broken_heart),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));

        stage3_diamond_bitmap_hash.put(1,red);
        stage3_diamond_bitmap_hash.put(2,blue);
        stage3_diamond_bitmap_hash.put(3,purple);
        stage3_diamond_bitmap_hash.put(4,yellow);
        stage3_diamond_bitmap_hash.put(5,green);
        stage3_bitmap_hash.put("fireball",fireball);

        projectile_image_bitmap_hash.put("red",red);
        projectile_image_bitmap_hash.put("blue",blue);
        projectile_image_bitmap_hash.put("purple",purple);
        projectile_image_bitmap_hash.put("yellow",yellow);
        projectile_image_bitmap_hash.put("green",green);

        //1: red, 2: blue, 3: purple, 4: yellow, 5: green

    }

    public void load_all_req_projectiles(float game_scale){
        projectile_image_bitmap_hash.put("coconut", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.coconut),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));

        projectile_image_bitmap_hash.put("banana", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.banana),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));

        projectile_image_bitmap_hash.put("coconut_angry", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.coconut_angry),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));

        projectile_image_bitmap_hash.put("coconut_black", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.coconut_black),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));

        projectile_image_bitmap_hash.put("banana_angry", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.banana_angry),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));

        projectile_image_bitmap_hash.put("shuriken", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.house_projectile_1),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));

        projectile_image_bitmap_hash.put("dagger", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.house_projectile_2),
                (int) (s_width / 9 * game_scale), (int) (s_width / 3 * game_scale), true));

        projectile_image_bitmap_hash.put("tree_boss", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.stage1_tree_boss_dead),
                (int) (s_height / 4 * 0.77 * game_scale), (int) (s_height / 4 * game_scale), true));

        projectile_image_bitmap_hash.put("urchin_boss", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.sea_urchin_boss_base_dead),
                (int) (s_height / 4 * game_scale), (int) (s_height / 4 * game_scale), true));

        projectile_image_bitmap_hash.put("boss", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.stage2_char_boss_preview_v2),
                (int) (s_height / 4 * game_scale), (int) (s_height / 4 * game_scale), true));

        projectile_image_bitmap_hash.put("score", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.star),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));

        projectile_image_bitmap_hash.put("laser", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.warning_2),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));

        projectile_image_bitmap_hash.put("blank", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.blank_projectile),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));

        projectile_image_bitmap_hash.put("broken_heart", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.broken_heart),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));

        projectile_image_bitmap_hash.put("heart", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.life_heart),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));

        projectile_image_bitmap_hash.put("star", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.star),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));

        projectile_image_bitmap_hash.put("fireball", Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.fireball),
                (int) (s_width / 6 * game_scale), (int) (s_width / 6 * game_scale), true));

    }

    public void load_character_bitmap_hash(){

        //girl - yuri
        HashMap<String,Bitmap> yuri = new HashMap<>();

        float scale = 7f;
        int boss_scale = 15;
        int mini_scale = 30;
        int tap_scale = 6;
        //girl - yuri
        Bitmap happy = getBitmap(R.drawable.vanilla_happy_1);
        yuri.put(
                "happy_1",
                Bitmap.createScaledBitmap(happy,
                        (int) ((float) s_width /scale*1.07),
                        (int) ((float) s_width/scale * happy.getHeight()/happy.getWidth()),true));

        Bitmap cry = getBitmap(R.drawable.vanilla_cry_1);
        yuri.put(
                "cry_1",
                Bitmap.createScaledBitmap(cry,
                        (int) ((float) s_width /scale*1.07),
                        (int) ((float) s_width/scale * cry.getHeight()/cry.getWidth()),true));

        Bitmap angry = getBitmap(R.drawable.vanilla_angry_1);
        yuri.put(
                "angry_1",
                Bitmap.createScaledBitmap(angry,
                        (int) ((float) s_width /scale*1.07),
                        (int) ((float) s_width/scale * angry.getHeight()/angry.getWidth()),true));

        Bitmap shy = getBitmap(R.drawable.vanilla_shy_1);
        yuri.put(
                "shy_1",
                Bitmap.createScaledBitmap(shy,
                        (int) ((float) s_width /scale*1.07),
                        (int) ((float) s_width/scale * shy.getHeight()/shy.getWidth()),true));

        Bitmap happy2 = getBitmap(R.drawable.vanilla_happy_2);
        yuri.put(
                "happy_2",
                Bitmap.createScaledBitmap(happy2,
                        (int) ((float) s_width /scale*1.07),
                        (int) ((float) s_width/scale * happy.getHeight()/happy.getWidth()),true));

        Bitmap cry2 = getBitmap(R.drawable.vanilla_cry_2);
        yuri.put(
                "cry_2",
                Bitmap.createScaledBitmap(cry2,
                        (int) ((float) s_width /scale*1.07),
                        (int) ((float) s_width/scale * cry.getHeight()/cry.getWidth()),true));

        Bitmap angry2 = getBitmap(R.drawable.vanilla_angry_2);
        yuri.put(
                "angry_2",
                Bitmap.createScaledBitmap(angry2,
                        (int) ((float) s_width /scale*1.07),
                        (int) ((float) s_width/scale * angry.getHeight()/angry.getWidth()),true));

        Bitmap shy2 = getBitmap(R.drawable.vanilla_shy_2);
        yuri.put(
                "shy_2",
                Bitmap.createScaledBitmap(shy2,
                        (int) ((float) s_width /scale*1.07),
                        (int) ((float) s_width/scale * shy.getHeight()/shy.getWidth()),true));

        character_image_bitmap_hash.put("yuri", yuri);
        char_offense_attributes.put("yuri",2);

        //press effect - yuri
        HashMap<Integer,Bitmap> yuri_press_effect = new HashMap<>();
        yuri_press_effect.put(1, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.yuri_press_effect_1),
                (int) (s_width / boss_scale), (int) (s_width / boss_scale), true));
        yuri_press_effect.put(2, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.yuri_press_effect_2),
                (int) (s_width / boss_scale * 1.2f), (int) (s_width / boss_scale * 1.2f), true));
        yuri_press_effect.put(3, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.yuri_press_effect_3),
                (int) (s_width / boss_scale* 1.5f), (int) (s_width / boss_scale* 1.5f), true));
        char_press_effect.put("yuri",yuri_press_effect);

        //press effect - yuri
        HashMap<Integer,Bitmap> yuri_press_effect_mini = new HashMap<>();
        yuri_press_effect_mini.put(1, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.yuri_press_effect_1),
                (int) (s_width / mini_scale), (int) (s_width / mini_scale), true));
        yuri_press_effect_mini.put(2, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.yuri_press_effect_2),
                (int) (s_width / mini_scale * 1.2f), (int) (s_width / mini_scale * 1.2f), true));
        yuri_press_effect_mini.put(3, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.yuri_press_effect_3),
                (int) (s_width / mini_scale* 1.5f), (int) (s_width / mini_scale* 1.5f), true));
        char_press_effect_mini.put("yuri",yuri_press_effect_mini);

        //press effect - yuri
        HashMap<Integer,Bitmap> yuri_tap_effect = new HashMap<>();
        yuri_tap_effect.put(1, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.hq_yuri_press_effect_1),
                (int) (s_width / tap_scale), (int) (s_width / tap_scale), true));
        yuri_tap_effect.put(2, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.hq_yuri_press_effect_2),
                (int) (s_width / tap_scale), (int) (s_width / tap_scale), true));
        yuri_tap_effect.put(3, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.hq_yuri_press_effect_3),
                (int) (s_width / tap_scale), (int) (s_width / tap_scale), true));
        char_tap_effect.put("yuri",yuri_tap_effect);

        //girl - summer_yuri
        HashMap<String,Bitmap> summer_yuri = new HashMap<>();
        Bitmap happy_summer_yuri = getBitmap(R.drawable.summer_happy_1);
        summer_yuri.put(
                "happy_1",
                Bitmap.createScaledBitmap(happy_summer_yuri,
                        (int) ((float) s_width /scale*1.07),
                        (int) ((float) s_width/scale * happy.getHeight()/happy.getWidth()),true));

        Bitmap cry_summer_yuri = getBitmap(R.drawable.summer_cry_1);
        summer_yuri.put(
                "cry_1",
                Bitmap.createScaledBitmap(cry_summer_yuri,
                        (int) ((float) s_width /scale*1.07),
                        (int) ((float) s_width/scale * cry.getHeight()/cry.getWidth()),true));

        Bitmap angry_summer_yuri = getBitmap(R.drawable.summer_angry_1);
        summer_yuri.put(
                "angry_1",
                Bitmap.createScaledBitmap(angry_summer_yuri,
                        (int) ((float) s_width /scale*1.07),
                        (int) ((float) s_width/scale * angry.getHeight()/angry.getWidth()),true));

        Bitmap shy_summer_yuri = getBitmap(R.drawable.summer_shy_1);
        summer_yuri.put(
                "shy_1",
                Bitmap.createScaledBitmap(shy_summer_yuri,
                        (int) ((float) s_width /scale*1.07),
                        (int) ((float) s_width/scale * shy.getHeight()/shy.getWidth()),true));

        Bitmap happy2_summer_yuri = getBitmap(R.drawable.summer_happy_2);
        summer_yuri.put(
                "happy_2",
                Bitmap.createScaledBitmap(happy2_summer_yuri,
                        (int) ((float) s_width /scale*1.07),
                        (int) ((float) s_width/scale * happy.getHeight()/happy.getWidth()),true));

        Bitmap cry2_summer_yuri = getBitmap(R.drawable.summer_cry_2);
        summer_yuri.put(
                "cry_2",
                Bitmap.createScaledBitmap(cry2_summer_yuri,
                        (int) ((float) s_width /scale*1.07),
                        (int) ((float) s_width/scale * cry.getHeight()/cry.getWidth()),true));

        Bitmap angry2_summer_yuri = getBitmap(R.drawable.summer_angry_2);
        summer_yuri.put(
                "angry_2",
                Bitmap.createScaledBitmap(angry2_summer_yuri,
                        (int) ((float) s_width /scale*1.07),
                        (int) ((float) s_width/scale * angry.getHeight()/angry.getWidth()),true));

        Bitmap shy2_summer_yuri = getBitmap(R.drawable.summer_shy_2);
        summer_yuri.put(
                "shy_2",
                Bitmap.createScaledBitmap(shy2_summer_yuri,
                        (int) ((float) s_width /scale*1.07),
                        (int) ((float) s_width/scale * shy.getHeight()/shy.getWidth()),true));


        character_image_bitmap_hash.put("summer_yuri", summer_yuri);
        char_offense_attributes.put("summer_yuri",3);
        //press effect - summer_yuri
        HashMap<Integer,Bitmap> summer_yuri_press_effect = new HashMap<>();
        summer_yuri_press_effect.put(1, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.summer_yuri_press_effect_1),
                (int) (s_width / boss_scale), (int) (s_width / boss_scale), true));
        summer_yuri_press_effect.put(2, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.summer_yuri_press_effect_2),
                (int) (s_width / boss_scale * 1.2f), (int) (s_width / boss_scale * 1.2f), true));
        summer_yuri_press_effect.put(3, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.summer_yuri_press_effect_3),
                (int) (s_width / boss_scale* 1.5f), (int) (s_width / boss_scale* 1.5f), true));
        char_press_effect.put("summer_yuri",summer_yuri_press_effect);

        //press effect - summer_yuri
        HashMap<Integer,Bitmap> summer_yuri_press_effect_mini = new HashMap<>();
        summer_yuri_press_effect_mini.put(1, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.summer_yuri_press_effect_1),
                (int) (s_width / mini_scale), (int) (s_width / mini_scale), true));
        summer_yuri_press_effect_mini.put(2, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.summer_yuri_press_effect_2),
                (int) (s_width / mini_scale * 1.2f), (int) (s_width / mini_scale * 1.2f), true));
        summer_yuri_press_effect_mini.put(3, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.summer_yuri_press_effect_3),
                (int) (s_width / mini_scale* 1.5f), (int) (s_width / mini_scale* 1.5f), true));
        char_press_effect_mini.put("summer_yuri",summer_yuri_press_effect_mini);

        //tap effect - summer_yuri
        HashMap<Integer,Bitmap> summer_yuri_tap_effect = new HashMap<>();
        summer_yuri_tap_effect.put(1, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.hq_summer_yuri_press_effect_1),
                (int) (s_width / tap_scale),(int) (s_width / tap_scale), true));
        summer_yuri_tap_effect.put(2, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.hq_summer_yuri_press_effect_2),
                (int) (s_width / tap_scale), (int) (s_width / tap_scale), true));
        summer_yuri_tap_effect.put(3, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.hq_summer_yuri_press_effect_3),
                (int) (s_width / tap_scale), (int) (s_width / tap_scale), true));
        char_tap_effect.put("summer_yuri",summer_yuri_tap_effect);


        //pet - puffy
        float scale_puffy = 4;
        HashMap<String,Bitmap> puffy = new HashMap<>();
        Bitmap happy_puffy = getBitmap(R.drawable.puffy_neutral);
        puffy.put(
                "happy_1",
                Bitmap.createScaledBitmap(happy_puffy,
                        (int) ((float) s_width /scale_puffy),
                        (int) ((float) s_width/scale_puffy),true));

        Bitmap cry_puffy = getBitmap(R.drawable.puffy_sad);
        puffy.put(
                "cry_1",
                Bitmap.createScaledBitmap(cry_puffy,
                        (int) ((float) s_width /scale_puffy),
                        (int) ((float) s_width/scale_puffy),true));

        Bitmap angry_puffy = getBitmap(R.drawable.puffy_neutral);
        puffy.put(
                "angry_1",
                Bitmap.createScaledBitmap(angry_puffy,
                        (int) ((float) s_width /scale_puffy),
                        (int) ((float) s_width/scale_puffy),true));

        Bitmap shy_puffy = getBitmap(R.drawable.puffy_neutral);
        puffy.put(
                "shy_1",
                Bitmap.createScaledBitmap(shy_puffy,
                        (int) ((float) s_width /scale_puffy),
                        (int) ((float) s_width/scale_puffy ),true));

        Bitmap happy_puffy2 = getBitmap(R.drawable.puffy_neutral);
        puffy.put(
                "happy_2",
                Bitmap.createScaledBitmap(happy_puffy2,
                        (int) ((float) s_width /scale_puffy),
                        (int) ((float) s_width/scale_puffy),true));

        Bitmap cry_puffy2 = getBitmap(R.drawable.puffy_neutral);
        puffy.put(
                "cry_2",
                Bitmap.createScaledBitmap(cry_puffy2,
                        (int) ((float) s_width /scale_puffy),
                        (int) ((float) s_width/scale_puffy),true));

        Bitmap angry_puffy2 = getBitmap(R.drawable.puffy_sad);
        puffy.put(
                "angry_2",
                Bitmap.createScaledBitmap(angry_puffy2,
                        (int) ((float) s_width /scale_puffy),
                        (int) ((float) s_width/scale_puffy),true));

        Bitmap shy_puffy2 = getBitmap(R.drawable.puffy_neutral);
        puffy.put(
                "shy_2",
                Bitmap.createScaledBitmap(shy_puffy2,
                        (int) ((float) s_width /scale_puffy),
                        (int) ((float) s_width/scale_puffy ),true));

        character_image_bitmap_hash.put("puffy", puffy);
        char_offense_attributes.put("puffy",1);
        //press effect - puffy
        HashMap<Integer,Bitmap> puffy_press_effect = new HashMap<>();
        puffy_press_effect.put(1, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.puffy_press_effect_1),
                (int) (s_width / boss_scale), (int) (s_width / boss_scale), true));
        puffy_press_effect.put(2, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.puffy_press_effect_2),
                (int) (s_width / boss_scale * 1.2f), (int) (s_width / boss_scale * 1.2f), true));
        puffy_press_effect.put(3, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.puffy_press_effect_3),
                (int) (s_width / boss_scale* 1.5f), (int) (s_width / boss_scale* 1.5f), true));
        char_press_effect.put("puffy",puffy_press_effect);

        //mini
        HashMap<Integer,Bitmap> puffy_press_effect_mini = new HashMap<>();
        puffy_press_effect_mini.put(1, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.puffy_press_effect_1),
                (int) (s_width / mini_scale), (int) (s_width / mini_scale), true));
        puffy_press_effect_mini.put(2, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.puffy_press_effect_2),
                (int) (s_width / mini_scale * 1.2f), (int) (s_width / mini_scale * 1.2f), true));
        puffy_press_effect_mini.put(3, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.puffy_press_effect_3),
                (int) (s_width / mini_scale* 1.5f), (int) (s_width / mini_scale* 1.5f), true));
        char_press_effect_mini.put("puffy",puffy_press_effect_mini);

        //tap_effect
        HashMap<Integer,Bitmap> puffy_tap_effect = new HashMap<>();
        puffy_tap_effect.put(1, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.hq_puffy_press_effect_1),
                (int) (s_width / tap_scale), (int) (s_width / tap_scale), true));
        puffy_tap_effect.put(2, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.hq_puffy_press_effect_2),
                (int) (s_width / tap_scale), (int) (s_width / tap_scale), true));
        puffy_tap_effect.put(3, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                resources, R.drawable.hq_puffy_press_effect_3),
                (int) (s_width / tap_scale), (int) (s_width / tap_scale), true));
        char_tap_effect.put("puffy",puffy_tap_effect);
    }

    public void load_char_offense_bitmaps(){
        float scale =0.5f;
        Bitmap off_shield_0 = Bitmap.createScaledBitmap(getBitmap(R.drawable.shield_zero)
                , s_width / 16, s_width / 16, true);
        Bitmap off_shield_1 = Bitmap.createScaledBitmap(getBitmap(R.drawable.shield_offense_1)
                , (int) (s_width / 8 * scale), (int) (s_width / 8 * scale), true);
//        Bitmap off_shield_2 = Bitmap.createScaledBitmap(getBitmap(R.drawable.shield_offense_2)
//                , s_width / 12, s_width / 12, true);
        Bitmap off_shield_3 = Bitmap.createScaledBitmap(getBitmap(R.drawable.shield_offense_3)
                , s_width / 8, s_width / 8, true);
        char_offense_bitmap_hash.put(0,off_shield_0);
        char_offense_bitmap_hash.put(1,off_shield_1);
//        char_offense_bitmap_hash.put(2,off_shield_2);
        char_offense_bitmap_hash.put(3,off_shield_3);

        Bitmap off_shield_puffy = Bitmap.createScaledBitmap(getBitmap(R.drawable.shield_offense_white)
                , (int) (s_width / 8 * scale), (int) (s_width / 8 * scale), true);
        Bitmap off_shield_summer_yuri = Bitmap.createScaledBitmap(getBitmap(R.drawable.shield_offense_skyblue)
                , (int) (s_width / 8 * scale), (int) (s_width / 8 * scale), true);

        char_offense_bitmap_hashv2.put("yuri",off_shield_1);
        char_offense_bitmap_hashv2.put("puffy",off_shield_puffy);
        char_offense_bitmap_hashv2.put("summer_yuri",off_shield_summer_yuri);


    }

    public void load_ink_bitmaps(){
        Bitmap ink1 = Bitmap.createScaledBitmap(getBitmap(R.drawable.ink1_black)
                , s_width / 2, s_width /2, true);
        Bitmap ink2 = Bitmap.createScaledBitmap(getBitmap(R.drawable.ink1_black)
                , (int) ((float) s_width / 2 * 1.5f),  (int) ((float) s_width / 2 * 1.2f), true);
        Bitmap ink3 = Bitmap.createScaledBitmap(getBitmap(R.drawable.ink1_black)
                , (int) ((float) s_width / 2 * 2f),  (int) ((float) s_width / 2 * 1.5f), true);
        ink_bitmap_hash.put(1,ink1);
        ink_bitmap_hash.put(2,ink2);
        ink_bitmap_hash.put(3,ink3);
    }

//    public void load_stage1_bgs() {
//        add_to_bg_hash(1, R.drawable.stage1_scroll_bg_1, 7.5f);
//        add_to_bg_hash(2, R.drawable.stage1_scroll_bg_1, 7.5f);
//        add_to_bg_hash(3, R.drawable.stage1_scroll_bg_1, 7.5f);
//        add_to_bg_hash(4, R.drawable.stage1_scroll_bg_1, 7.5f);
//        add_to_bg_hash(5, R.drawable.stage1_scroll_bg_1, 7.5f);
//        add_to_bg_hash(6, R.drawable.stage1_scroll_bg_1, 7.5f);
//    }
//
//
//    private void add_to_bg_hash(int index, int bg, float height_multiplier){
//        Bitmap background = BitmapFactory.decodeResource(resources, bg);
//        Bitmap background_scaled = Bitmap.createScaledBitmap(background,
//                s_width,
//                Math.round(s_height*height_multiplier),true);
//        stage1_bg_hash.put(index,background_scaled);
//    }
    private Bitmap convert(Bitmap bitmap, Bitmap.Config config) {
        Bitmap convertedBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), config);
        Canvas canvas = new Canvas(convertedBitmap);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        canvas.drawBitmap(bitmap, 0, 0, paint);
        return convertedBitmap;
    }

    private void display_log(long now, long TotalLoadTime, String text){
        ((PreMenuActivity) context).setLoadedComponent(text);
        ((PreMenuActivity) context).setProgressNumber(Math.min((double) (System.currentTimeMillis()-now)/TotalLoadTime,0.99));
    }

}