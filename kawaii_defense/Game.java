package nexidea.kawaii_defense;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;

import android.graphics.drawable.Drawable;

import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import	android.view.View;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import nexidea.kawaii_defense.GameActivities.GameActivity_stage1;
import nexidea.kawaii_defense.GameActivities.GameActivity_stage2;
import nexidea.kawaii_defense.GameActivities.GameActivity_stage3;
import nexidea.kawaii_defense.GameActivities.game_interface;
import nexidea.kawaii_defense.Projectiles.Ball;
import nexidea.kawaii_defense.Projectiles.Projectile_background;
import nexidea.kawaii_defense.Projectiles.Projectile_char_offense;
import nexidea.kawaii_defense.Projectiles.Projectile_press_effect;
import nexidea.kawaii_defense.Projectiles.Projectile_score_effect;
import nexidea.kawaii_defense.Projectiles.Projectile_slice_animation;
import nexidea.kawaii_defense.Projectiles.Projectile_tap_effect;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;
import static nexidea.kawaii_defense.MenuView.MY_PREFS_NAME;
import static nexidea.kawaii_defense.MenuView.menuviewcontext;

//credit diamonds: <a href='https://www.freepik.com/free-vector/colorful-gemstones_968024.htm'>Designed by 0melapics</a>
//sound icon credit: <div>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>

public class Game {
    private long time_tracker;

    //holder i.e. the layout and resources
    private SurfaceHolder holder;
    public Resources resources;
    public boolean loaded = false;
    private Context context;
    public MediaPlayer mp;
    public boolean ended = false;
    private int[] score_meter_loc = new int[2];
    private GameRunner runner;
    public static Handler handler;
    public static Handler activity_handler;
    public static Handler tutorial_handler;

    //Upgrades
    public int spin_upgrade;
    private int default_lives;
    private int default_shields;

    //current character selection
    public String selected_char;

    //Cloud FireStore
    private CollectionReference db = FirebaseFirestore.getInstance().collection("players");
    public Integer current_stage;
    private HashMap<Integer, Class> activity_map = new HashMap<>();

    //game scaling in terms of size
    private final float game_scale = 0.75f;
    private final int error_tolerance = 0;

    //game random
    public MyRandom game_random_map;

    //background game settings
    public HashMap<String, Integer> game_bg = new HashMap<>();
    public Boolean pause_background;

    //background + dimensions
    private int s_width;
    private int s_height;
//    public HashMap<Integer,Bitmap> bg_picture_hash;
//    public ConcurrentHashMap<Integer,Projectile_background> bg_map = new ConcurrentHashMap<>();
//    public int bg_map_counter;
    public HashMap<String, Bitmap> bg_hash;
    private Projectile_background bg;


    //character and shield
    public Ball ball;
    public HashMap<Integer, Bitmap> shield_bitmap_inventory = new HashMap<>();

    //stage triggers
    private game_interface game_activity;

    //stage completion scores
    public HashMap<Integer, Integer> stage_completion_score = new HashMap<>();

    //char_expressions
    public HashMap<String,Bitmap> char_expression_map = new HashMap<>();

    //game parameters
    private float charSpeed;
    private int ball_shield_count;

    //char offense projectile storage
    public HashMap<Integer, HashMap<Integer, Projectile_char_offense>> char_offense = new HashMap<>();
    public int char_offense_mode;
    private int default_char_offense_level;

    //HeatMeter_char_offense
    public static HeatMeter_CharOffense HeatMeter;

    //score effect
    public ConcurrentHashMap<Integer, Projectile_score_effect> score_effect_in_game = new ConcurrentHashMap<>();
    public int score_effect_counter = 0;

    //score effect
    public ConcurrentHashMap<Integer, Projectile_tap_effect> tap_effect_in_game = new ConcurrentHashMap<>();
    public int tap_effect_counter = 0;

    //dragging animation stuff
    public ConcurrentHashMap<Integer, Projectile_slice_animation> slice_animation_in_game = new ConcurrentHashMap<>();
    public int slice_effect_counter = 0;

    //press animation stuff
    public ConcurrentHashMap<Integer, Projectile_press_effect> press_effect_in_game = new ConcurrentHashMap<>();
    public int press_effect_counter = 0;

    //Motivator
    private Motivator motivator;


    //score/life recording
    public int life;
    public int score;
    public int kill_qty;
    public int token_qty;
    private Star_calculator star_calculator;
    //combo counter
    public int combo;
    public int multiplier;
    public int max_multiplier = 16;

    //free hold button
    public static boolean free_hold;

    //game tutorial
    public Tutorial tutorial;

    //char_offense_modes
    public int char_offense_modes_count = 4;

    //constructor
    public Game(Context context, int width, int height, SurfaceHolder holder, Resources resource,
                HashMap<String,Bitmap> bg_hash,
                int score, int lives) {

        //game startup parameters
        this.holder = holder;
        this.resources = resource;
        this.context = context;
        this.s_height = height;
        this.s_width = width;
        this.bg_hash = bg_hash;

        //setup_tutorial
        tutorial = new Tutorial(resources);

        //setup Motivator
        motivator = new Motivator(this,context);

        //set up randoms
        game_random_map = new MyRandom();

        //activity_map
        activity_map.put(0, MenuActivity.class);
        activity_map.put(1, MainActivity_stage.class);
        activity_map.put(2, MainActivity_wave_selection.class);
        activity_map.put(3, MainActivity_shop.class);

        //background game storage
        game_bg.put("life", life);
        game_bg.put("score", score);
        game_bg.put("combo", combo);


        //load moving background
        bg = new Projectile_background(s_width, s_height, 0, 0, 0, 0,this);
        bg.load_game_background(bg_hash, 0, 0);


        //setup heatmeter
        HeatMeter = new HeatMeter_CharOffense(s_width,s_height,resources,context);

        handler = new Handler(Looper.getMainLooper());

        reset_game();
    }

    public void init() {
        //initialise character
        load_char_expression_map(selected_char);
        ball = new Ball(s_width, s_height, s_width / 2, s_height / 2, charSpeed, charSpeed, this);
        create_shield_bitmap();
        ball.init_shield(this, ball_shield_count);
        load_projectiles();
        game_activity.setBall(ball);
//        change_char_offense(default_char_offense_level);
        change_char_offense(-3);
        activate_char_offense();
//        activate_char_offense();
//        activate_char_offense();
//        show_char_offense();
        update_lives();
        update_shield();
        update_char_offense();
        Log.d("starting", "init: finally starting game");
        loaded = true;
    }

    public void load_projectiles() {
        game_activity.load_projectiles();
    }

    public void update(long elapsed) {
        //background load
        //update Motivator
        motivator.update(elapsed);

        HeatMeter.update(elapsed);

        bg.update_game_bg(elapsed);

        //update background
//        moving_background.update_game_bg(elapsed);
//        if(!pause_background) {
//            for (Map.Entry<Integer, Projectile_background> entry : bg_map.entrySet()) {
//                if (entry.getValue().out) {
//                } else {
//                    //Log.d("null", "update: null pointer" + entry.getKey() + entry.getValue());
//                    entry.getValue().update(elapsed);
//                }
//            }
//        }

        //character movement
        ball.update(elapsed);

        //offense update
        for (int i = 1; i < char_offense_modes_count; i++) {
            for (Map.Entry<Integer, Projectile_char_offense> entry : char_offense.get(i).entrySet()) {
                entry.getValue().update(elapsed);
            }
        }

        //slice animation
        if (slice_animation_in_game != null) {
            for (Map.Entry<Integer, Projectile_slice_animation> entry : slice_animation_in_game.entrySet()) {
                if (entry.getValue().out) {
                    slice_animation_in_game.remove(entry.getKey());
                } else {
                    //Log.d("null", "update: null pointer" + entry.getKey() + entry.getValue());
                    entry.getValue().update(elapsed);
                }
            }
        }

        //score effect animation
//        if (score_effect_in_game != null) {
//            for (Map.Entry<Integer, Projectile_score_effect> entry : score_effect_in_game.entrySet()) {
//                if (entry.getValue().out) {
//                    score_effect_in_game.remove(entry.getKey());
//                } else {
//                    //Log.d("null", "update: null pointer" + entry.getKey() + entry.getValue());
//                    entry.getValue().update(elapsed);
//                }
//            }
//        }

        //press effect animation
        if (press_effect_in_game != null) {
            for (Map.Entry<Integer, Projectile_press_effect> entry : press_effect_in_game.entrySet()) {
                if (entry.getValue().out) {
                    press_effect_in_game.remove(entry.getKey());
                } else {
                    //Log.d("null", "update: null pointer" + entry.getKey() + entry.getValue());
                    entry.getValue().update(elapsed);
                }
            }
        }

        //tap effect animation
        if (tap_effect_in_game != null) {
            for (Map.Entry<Integer, Projectile_tap_effect> entry : tap_effect_in_game.entrySet()) {
                if (entry.getValue().out) {
                    tap_effect_in_game.remove(entry.getKey());
                } else {
                    //Log.d("null", "update: null pointer" + entry.getKey() + entry.getValue());
                    entry.getValue().update(elapsed);
                }
            }
        }


        game_activity.update(elapsed);
    }

    private Canvas lock_canvas(){
        if(Build.VERSION.SDK_INT >= 26) {
            Canvas canvas = holder.lockHardwareCanvas();
            return canvas;
        }else{
            Canvas canvas =  holder.lockCanvas();
            return canvas;
        }
    }

    public void draw() {
        Canvas canvas = lock_canvas();
        if (canvas != null) {
            time_tracker = System.currentTimeMillis();
//            for (Map.Entry<Integer, Projectile_background> entry : bg_map.entrySet()) {
//                if (entry.getValue().out) {
//                } else {
//                    //Log.d("null", "update: null pointer" + entry.getKey() + entry.getValue());
//                    entry.getValue().draw(canvas);
//                }
//            }
            bg.draw_background(canvas, null);
//            if((System.currentTimeMillis()-time_tracker)>7.0) {
//                Log.d(TAG, "draw: background " + Long.toString(System.currentTimeMillis() - time_tracker));
//            }

            time_tracker = System.currentTimeMillis();
            ball.draw(canvas);
//            if((System.currentTimeMillis()-time_tracker)>7.0) {
//                Log.d(TAG, "draw: character " + Long.toString(System.currentTimeMillis() - time_tracker));
//            }

            time_tracker = System.currentTimeMillis();
            draw_char_offense(canvas);
//            if((System.currentTimeMillis()-time_tracker)>7.0) {
//                Log.d(TAG, "draw: offense mode " + Long.toString(System.currentTimeMillis()-time_tracker));
//            }
            time_tracker = System.currentTimeMillis();
            game_activity.draw(canvas);
            game_activity.draw_ink(canvas);
//            if((System.currentTimeMillis()-time_tracker)>7.0) {
//                Log.d(TAG, "draw: game_activity " + Long.toString(System.currentTimeMillis() - time_tracker));
//            }


//            time_tracker = System.currentTimeMillis();
            //slice animation - draw
            if (slice_animation_in_game != null) {
                for (Map.Entry<Integer, Projectile_slice_animation> entry : slice_animation_in_game.entrySet()) {
                    if (entry.getValue().out) {
                    } else {
                        if (entry.getValue().getDraw_image() != null) {
                            entry.getValue().draw(canvas);
                        } else {
//                            Log.d("null", "update: null slice pointer" + entry.getKey() + entry.getValue());
                        }
                    }
                }
            }

            //score effect - draw
//            if (score_effect_in_game != null) {
//                for (Map.Entry<Integer, Projectile_score_effect> entry : score_effect_in_game.entrySet()) {
//                    if (entry.getValue().out) {
//                    } else {
//                        if (entry.getValue().getDraw_image() != null) {
//                            entry.getValue().draw_score_effect(canvas);
//                        } else {
////                            Log.d("null", "update: null slice pointer" + entry.getKey() + entry.getValue());
//                        }
//                    }
//                }
//            }
//            Log.d(TAG, "draw: others " + Long.toString(System.currentTimeMillis()-time_tracker));

            //press effect animation
            if (press_effect_in_game != null) {
                for (Map.Entry<Integer, Projectile_press_effect> entry : press_effect_in_game.entrySet()) {
                    if (entry.getValue().out) {
                        press_effect_in_game.remove(entry.getKey());
                    } else {
                        if (entry.getValue().getDraw_image() != null) {
                            entry.getValue().draw_press_effect(canvas);
                        } else {
//                            Log.d("null", "update: null slice pointer" + entry.getKey() + entry.getValue());
                        }
                    }
                }
            }

            //tap effect animation
            if (tap_effect_in_game != null) {
                for (Map.Entry<Integer, Projectile_tap_effect> entry : tap_effect_in_game.entrySet()) {
                    if (entry.getValue().out) {
                        tap_effect_in_game.remove(entry.getKey());
                    } else {
                        if (entry.getValue().getDraw_image() != null) {
                            entry.getValue().draw_press_effect(canvas);
                        } else {
//                            Log.d("null", "update: null slice pointer" + entry.getKey() + entry.getValue());
                        }
                    }
                }
            }

            HeatMeter.draw(canvas);

//            time_tracker = System.currentTimeMillis();
            holder.unlockCanvasAndPost(canvas);
//            Log.d(TAG, "draw: posting time " + Long.toString(System.currentTimeMillis()-time_tracker));

        }
//        Log.d(TAG, "draw: hardware accelerated:"  + canvas.isHardwareAccelerated());
    }

    public void setScore(int my_score) {
        if (life > 0 && my_score != score) {
            score = my_score;

            //score text
            update_score();
//
//            //score_effect
//            score_effect_counter += 1;
//            Projectile_score_effect new_score_effect =
//                    new Projectile_score_effect(s_width, s_height,
//                            score_meter_loc[0],
//                            score_meter_loc[1],
//                            0, 0);
//            score_effect_in_game.put(score_effect_counter, new_score_effect);
//            score_effect_in_game.get(score_effect_counter).load_projectile(this);

            //Motivator
            motivator.check_and_post();
        }
        System.gc();
    }

    public void addCombo(int x) {
        if(combo%5 ==0 && combo>0){
            ball.change_expression(char_expression_map.get("angry_"+randomNumberInRange(1,2)));
        }

        combo = combo + x;
        if (combo % 50 == 0 && combo > 0) {
            game_activity.createLives(1);
            if(ball_shield_count<3) {
                ball.setShield_count(this, ball.shield_count + 1);
            }

        }
        if (combo % 100 == 0 && combo > 0) {
            multiplier = Math.min(multiplier * 2, max_multiplier);
            game_activity.createBombs(1);
            update_multiplier();
        }
        update_combo();
        update_shield();
    }

    public void setLife(int new_life) {
        if (new_life >= life) {
            ball.adjust_char_paint_alpha(new_life - life);
            life = Math.min(new_life, default_lives);
            if(new_life>3){
//                activate_char_offense();
                change_char_offense(1);
                update_char_offense();
                ball.change_expression(char_expression_map.get("angry_"+randomNumberInRange(1,2)));
            }
        } else {
            if (ball.vulnerable) {
                combo = 0;
                ball.adjust_char_paint_alpha(new_life - life);
                star_calculator.addChar_hits();
//                moving_background.hit = true;
                life = new_life;
                multiplier = 1;
                update_combo();
                update_multiplier();
                if (life <= 0) {
                    continue_to_play();
                }
            }
            update_char_offense();
            bg.hit_bg();
            ball.setShield_count(this, ball.shield_count - 1);
            ball_shield_count = Math.max(ball.shield_count, 0);
            ball.change_expression(char_expression_map.get("cry_"+randomNumberInRange(1,2)));
        }
        update_lives();
        update_shield();
    }

    //useful functions that are more or less final
    public void end_game() {
        //set yellow final score
        //setScore(score + life);
        are_you_sure();
    }

    // generic functions (not related to the game)
    public static int randomNumberInRange(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    public void record_scores() {
        update_end_score();
        ended = true;
        ball.kill();
        GameRunner.getGameRunner().shutdown();
        MainActivity_stage.paused = false;
        //initiate SharedPreferences to collect scores + name
//        Handler mHandler = new Handler(Looper.getMainLooper());
//        handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                synchronized(context) {
                    create_game_over_dialog(context);
                }
            }
        });

    }

    public void create_game_over_dialog(final Context context) {
        //in the mean time, lets store some personal_best and latest_score into the prefs with editor
        SharedPreferences local_db = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();

        token_qty = score/5 + kill_qty + combo*3;
        //tokens
        editor.putInt("token", local_db.getInt("token",0)+token_qty);

        //latest_score
        editor.putInt("latest_score", score);

        //personal_best
        if (local_db.getInt("personal_best", 0) < score) {
            editor.putInt("personal_best", score);
        } else {
        }
        String stage_wave_map_json_string = local_db.getString("stage_wave_local_store", "");
        if (stage_wave_map_json_string == "") {
            HashMap<Integer, Integer> stage_wave_map = new HashMap<>();
            stage_wave_map.put(current_stage, getGame_activity().getWave_counter());
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
            Type type = new TypeToken<HashMap<Integer, Integer>>() {
            }.getType();
            String json = gson.toJson(stage_wave_map, type);
            editor.putString("stage_wave_local_store", json);
        } else {
            HashMap<Integer, Integer> stage_wave_map = new Gson().fromJson(stage_wave_map_json_string,
                    new TypeToken<HashMap<Integer, Integer>>() {
                    }.getType());

            if (stage_wave_map.get(current_stage) == null) {
                stage_wave_map.put(current_stage, getGame_activity().getWave_counter());
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
                Type type = new TypeToken<HashMap<Integer, Integer>>() {
                }.getType();
                String json = gson.toJson(stage_wave_map, type);
                editor.putString("stage_wave_local_store", json);
            } else if (stage_wave_map.get(current_stage) < getGame_activity().getWave_counter()) {
                stage_wave_map.put(current_stage, getGame_activity().getWave_counter());
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
                Type type = new TypeToken<HashMap<Integer, Integer>>() {
                }.getType();
                String json = gson.toJson(stage_wave_map, type);
                editor.putString("stage_wave_local_store", json);
            }
        }
        editor.apply();

        //now let's do the dialog creation
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(context);
        View mView = inflater.inflate(R.layout.popup_gameover, null);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();

        //set stars
        ImageView s1 = mView.findViewById(R.id.star_achievement_1);
        ImageView s2 = mView.findViewById(R.id.star_achievement_2);
        ImageView s3 = mView.findViewById(R.id.star_achievement_3);
        HashMap<Integer,ImageView> go_stars = new HashMap<>();
        go_stars.put(1,s1);
        go_stars.put(2,s2);
        go_stars.put(3,s3);

        HashMap<Integer,Integer> star_images = new HashMap<>();
        star_images.put(1,R.drawable.star1);
        star_images.put(2,R.drawable.star2);
        star_images.put(3,R.drawable.star3);
        star_images.put(0,R.drawable.star0);

        for (int i = 1; i<4; i++) {
            if (i <= star_calculator.stars) {
                update_image_view(go_stars.get(i), star_images.get(i));
            } else {
                update_image_view(go_stars.get(i), star_images.get(0));
            }
        }

        //set statistics - score/kill/tokens
        TextView game_over_score = mView.findViewById(R.id.score_achieved);
        game_over_score.setText(Integer.toString(score));
        TextView kill_count = mView.findViewById(R.id.kill_qty);
        kill_count.setText(" X " +Integer.toString(kill_qty));

        //token qty formula
        TextView tokens = mView.findViewById(R.id.tokens_earned);
        tokens.setText(" X " + Integer.toString(token_qty));


        Button retry = mView.findViewById(R.id.retry);
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reset_game();
                set_stage(current_stage,game_activity.getWave_counter());
                game_activity.init();
                load_projectiles();
                start_game_view_runner();
                dialog.dismiss();
//                context.startActivity(intent);
                SharedPreferences local_db = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                final String global_id = local_db.getString("global_id", "");
                DocumentReference docRef = db.document(global_id);
                docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
//                                Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                                if (score > (long) document.getData().get("highscore")) {
                                    db.document(global_id).update("highscore", score);
                                    db.document(global_id).update("stage", current_stage);
                                }

                            } else {
//                                Log.d(TAG, "No such document");
                            }
                        } else {
//                            Log.d(TAG, "get failed with ", task.getException());
                        }
                    }
                });
            }
        });

        Button main_menu = mView.findViewById(R.id.main_menu);
        main_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, activity_map.get(0)); //0 represents main menu
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                dialog.dismiss();
                context.startActivity(intent);
            }
        });

        Button wave_select = mView.findViewById(R.id.wave_select);
        wave_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, activity_map.get(2)); //0 represents main menu
                intent.putExtra("stage", current_stage);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                dialog.dismiss();
                context.startActivity(intent);
            }
        });

        Button upgrades = mView.findViewById(R.id.upgrades);
        upgrades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, activity_map.get(3)); //0 represents main menu
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                dialog.dismiss();
                context.startActivity(intent);
            }
        });


        final String global_id = local_db.getString("global_id", "");
        DocumentReference docRef = db.document(global_id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
//                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        if (score > (long) document.getData().get("highscore")) {
                            db.document(global_id).update("highscore", score);
                            db.document(global_id).update("stage", current_stage);
                        }

                    } else {
//                        Log.d(TAG, "No such document");
                    }
                } else {
//                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });


        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    public void are_you_sure(){
        //initiate SharedPreferences to collect scores + name
//        Handler mHandler = new Handler(Looper.getMainLooper());
//        handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                synchronized(context) {
                    if(!GameRunner.isPaused()) {
                        GameRunner.setPaused(true);
                        create_dialog_are_you_sure(context);
//                        Log.d(TAG, "run: handler fired! are-you-sure");
                    }
                    else{
                        create_dialog_continue_to_play(context);
                    }
                }
            }
        });
//        Thread t = new Thread(){
//            public void run(){
//                synchronized(context) {
//                    create_dialog_are_you_sure(context);
//                }
//            }
//        };
//        t.start();
    }

    public void create_dialog_are_you_sure(final Context context) {
       //now let's do the dialog creation
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(context);
        View mView = inflater.inflate(R.layout.popup_are_you_sure, null);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();

        Button no = mView.findViewById(R.id.are_you_sure_no);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GameRunner.setPaused(false);
                dialog.dismiss();
                    }
            }
        );

        Button yes = mView.findViewById(R.id.are_you_sure_yes);
        yes.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View view) {
                                       GameRunner.setPaused(false);
                                       record_scores();
                                       dialog.dismiss();
                                   }
                               }
        );

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }


    public void continue_to_play() {
        //initiate SharedPreferences to collect scores + name
//        Handler mHandler = new Handler(Looper.getMainLooper());
//        handler = new Handler(Looper.getMainLooper());
        GameRunner.setPaused(true);
        try {
            handler.postAtTime(new Runnable() {
                @Override
                public void run() {
                    synchronized (context) {
//                        Log.d(TAG, "run: handler fired! continue-to-play");
                        create_dialog_continue_to_play(context);
                    }
                }
            },500);
        }
        catch(Exception exception)
        {
//            Log.d(TAG, "continue_to_play: error" + exception);
        }
    }
//        Thread t = new Thread(){
//            public void run(){
//                synchronized(context) {
//                    create_dialog_continue_to_play(context);
//                }
//            }
//        };
//        t.start();

    public void create_dialog_continue_to_play(final Context context) {

        SharedPreferences local_db = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        final int tokens = local_db.getInt("token",0);
//        Log.d(TAG, "create_dialog_continue_to_play: moving to dialog creation");
        //now let's do the dialog creation
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(context);
        View mView = inflater.inflate(R.layout.popup_continue, null);
        mBuilder.setView(mView);
//        Log.d(TAG, "create_dialog_continue_to_play: builded mView");
        final AlertDialog dialog = mBuilder.create();
        ImageButton pay_in_coins = mView.findViewById(R.id.pay_in_coins);
        final TextView tokens_avail = mView.findViewById(R.id.continue_tokens_available);
        tokens_avail.setText(Integer.toString(tokens));


        pay_in_coins.setOnClickListener(new View.OnClickListener() {
                                  @Override
                                  public void onClick(View view) {
                                      SharedPreferences.Editor editor = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                      int restart_fee = 5000;
                                      if(tokens < 5000){
                                          Toast.makeText(context, "Not enough tokens. Need "+ Integer.toString(5000-tokens) +" more.", Toast.LENGTH_SHORT).show();
                                      }
                                      else{
                                          editor.putInt("token", tokens - restart_fee);
                                          editor.apply();
                                          reset_and_continue_game();
                                          ball.reset_alpha();
                                          GameRunner.setPaused(false);
                                          ((MainActivity_stage) context).reset_end_ScoreText();
//                                          show_char_offense();
                                          update_lives();
                                          update_shield();
                                          update_char_offense();
                                          dialog.dismiss();
                                      }
                                  }
                              }
        );

        Button watch_a_video = mView.findViewById(R.id.watch_a_video);
        watch_a_video.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View view) {
                                       ((MainActivity_stage) context).show_ad();
                                       reset_and_continue_game();
                                       ball.reset_alpha();
//                                       GameRunner.setPaused(false);
                                       ((MainActivity_stage) context).reset_end_ScoreText();
//                                       show_char_offense();
                                       update_lives();
                                       update_shield();
                                       update_char_offense();
                                       dialog.dismiss();
                                   }
                               }
        );

        ImageButton no = mView.findViewById(R.id.continue_no);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GameRunner.setPaused(false);
                record_scores();
                dialog.dismiss();
            }
        });

        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(false);
        dialog.show();
    }

    public void stage_completed(){
        update_end_score();
        ended = true;
        GameRunner.getGameRunner().shutdown();
        ball.kill();
        MainActivity_stage.paused = false;
        //initiate SharedPreferences to collect scores + name
//        Handler mHandler = new Handler(Looper.getMainLooper());
//        handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                synchronized(context) {
                    create_stage_completed_dialog(context);
                }
            }
        });
//        Thread t = new Thread(){
//            public void run(){
//                synchronized(context) {
//                    create_stage_completed_dialog(context);
//                }
//            }
//        };
//        t.start();
    }

    public void create_stage_completed_dialog(final Context context) {
        //in the mean time, lets store some personal_best and latest_score into the prefs with editor
        SharedPreferences local_db = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();

        token_qty = score/5 + kill_qty + combo*3;
        //tokens
        editor.putInt("token", local_db.getInt("token",0) + token_qty);

        //latest_score
        editor.putInt("latest_score", score);

        //personal_best
        if (local_db.getInt("personal_best", 0) < score) {
            editor.putInt("personal_best", score);
        } else {
        }
        String stage_wave_map_json_string = local_db.getString("stage_wave_local_store", "");
        if (stage_wave_map_json_string == "") {
            HashMap<Integer, Integer> stage_wave_map = new HashMap<>();
            stage_wave_map.put(current_stage, getGame_activity().getWave_counter());
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
            Type type = new TypeToken<HashMap<Integer, Integer>>() {
            }.getType();
            String json = gson.toJson(stage_wave_map, type);
            editor.putString("stage_wave_local_store", json);
        } else {
            HashMap<Integer, Integer> stage_wave_map = new Gson().fromJson(stage_wave_map_json_string,
                    new TypeToken<HashMap<Integer, Integer>>() {
                    }.getType());

            if (stage_wave_map.get(current_stage) == null) {
                stage_wave_map.put(current_stage, getGame_activity().getWave_counter());
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
                Type type = new TypeToken<HashMap<Integer, Integer>>() {
                }.getType();
                String json = gson.toJson(stage_wave_map, type);
                editor.putString("stage_wave_local_store", json);
            } else if (stage_wave_map.get(current_stage) < getGame_activity().getWave_counter()) {
                stage_wave_map.put(current_stage, getGame_activity().getWave_counter());
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
                Type type = new TypeToken<HashMap<Integer, Integer>>() {
                }.getType();
                String json = gson.toJson(stage_wave_map, type);
                editor.putString("stage_wave_local_store", json);
            }
        }
        editor.apply();

        //now let's do the dialog creation
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(context);
        View mView = inflater.inflate(R.layout.popup_gameover, null);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();

        //change title
        TextView game_over_title = mView.findViewById(R.id.game_over_title);
        game_over_title.setText("Congratulations!");

        //add comment below
        TextView flavour_text = mView.findViewById(R.id.GameOverFlavourText);
        flavour_text.setText("Completed Stage " + current_stage+"!");
        flavour_text.setTextSize(TypedValue.COMPLEX_UNIT_PT, 15);

        //set stars
        ImageView s1 = mView.findViewById(R.id.star_achievement_1);
        ImageView s2 = mView.findViewById(R.id.star_achievement_2);
        ImageView s3 = mView.findViewById(R.id.star_achievement_3);
        HashMap<Integer,ImageView> go_stars = new HashMap<>();
        go_stars.put(1,s1);
        go_stars.put(2,s2);
        go_stars.put(3,s3);

        HashMap<Integer,Integer> star_images = new HashMap<>();
        star_images.put(1,R.drawable.star1);
        star_images.put(2,R.drawable.star2);
        star_images.put(3,R.drawable.star3);
        star_images.put(0,R.drawable.star0);

        for (int i = 1; i<4; i++) {
                update_image_view(go_stars.get(i), star_images.get(i));
        }

        //set statistics - score/kill/tokens
        TextView game_over_score = mView.findViewById(R.id.score_achieved);
        game_over_score.setText(Integer.toString(score));
        TextView kill_count = mView.findViewById(R.id.kill_qty);
        kill_count.setText(" X " +Integer.toString(kill_qty));

        //token qty formula
        TextView tokens = mView.findViewById(R.id.tokens_earned);
        tokens.setText(" X " + Integer.toString(token_qty));


        Button retry = mView.findViewById(R.id.retry);
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reset_game();
                set_stage(current_stage,game_activity.getWave_counter());
                game_activity.init();
                load_projectiles();
                start_game_view_runner();
                dialog.dismiss();
                SharedPreferences local_db = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                final String global_id = local_db.getString("global_id", "");
                DocumentReference docRef = db.document(global_id);
                docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
//                                Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                                if (score > (long) document.getData().get("highscore")) {
                                    db.document(global_id).update("highscore", score);
                                    db.document(global_id).update("stage", current_stage);
                                }

                            } else {
//                                Log.d(TAG, "No such document");
                            }
                        } else {
//                            Log.d(TAG, "get failed with ", task.getException());
                        }
                    }
                });
            }
        });

        Button main_menu = mView.findViewById(R.id.main_menu);
        main_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, activity_map.get(0)); //0 represents main menu
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                dialog.dismiss();
                context.startActivity(intent);
            }
        });
        final String global_id = local_db.getString("global_id", "");
        DocumentReference docRef = db.document(global_id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
//                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        if (score > (long) document.getData().get("highscore")) {
                            db.document(global_id).update("highscore", score);
                            db.document(global_id).update("stage", current_stage);
                        }

                    } else {
//                        Log.d(TAG, "No such document");
                    }
                } else {
//                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });


        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    public SurfaceHolder getHolder() {
        return holder;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

    //convert a drawable to a Bitmap image
    private Bitmap getBitmap(int drawableRes) {
        Drawable drawable = context.getDrawable(drawableRes);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_4444);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    //convert a drawable to a Bitmap image
    private Bitmap getBitmap_LQ(int drawableRes) {
        Drawable drawable = context.getDrawable(drawableRes);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.RGB_565);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    //set stages
    public void set_stage(int stage, int start_wave) {
        switch (stage) {
            case 1:
                current_stage = 1;
                combo = 0;
                multiplier = 1;
                update_combo();
                update_multiplier();
                loaded = false;
                free_hold = true;
                score_meter_loc[0] = MainActivity_stage.get_score_coordinates()[0];
                score_meter_loc[1] = MainActivity_stage.get_score_coordinates()[1];
                ((MainActivity_stage) context).setViewTextColor(stage, Color.BLACK);
                ((MainActivity_stage) context).updateMotivatorText("Stage " +current_stage, Color.WHITE);
                game_activity = new GameActivity_stage1(
                        s_width,
                        s_height,
                        resources,
                        this,
                        error_tolerance,
                        game_scale,
                        start_wave);
                break;

            case 2:
//                Log.d(TAG, "set_stage: 2");
                current_stage = 2;
                combo = 0;
                multiplier = 1;
                update_combo();
                update_multiplier();
                loaded = false;
                free_hold = true;
                score_meter_loc[0] = MainActivity_stage.get_score_coordinates()[0];
                score_meter_loc[1] = MainActivity_stage.get_score_coordinates()[1];
                ((MainActivity_stage) context).setViewTextColor(stage, Color.WHITE);
                ((MainActivity_stage) context).updateMotivatorText("Stage " +current_stage, Color.WHITE);
                game_activity = new GameActivity_stage2(
                        s_width,
                        s_height,
                        resources,
                        holder,
                        this,
                        error_tolerance,
                        game_scale,
                        start_wave);
                break;

            case 3:
                current_stage = 3;
                combo = 0;
                multiplier = 1;
                update_combo();
                update_multiplier();
                loaded = false;
                free_hold = true;
                score_meter_loc[0] = MainActivity_stage.get_score_coordinates()[0];
                score_meter_loc[1] = MainActivity_stage.get_score_coordinates()[1];
                ((MainActivity_stage) context).setViewTextColor(stage, Color.WHITE);
                ((MainActivity_stage) context).updateMotivatorText("Stage " +current_stage, Color.WHITE);
                game_activity = new GameActivity_stage3(
                        s_width,
                        s_height,
                        resources,
                        holder,
                        this,
                        error_tolerance,
                        game_scale,
                        start_wave);
                break;
            default:
                break;
        }

//        load_background();

    }

//    private void load_background(){
//        //load moving background
//        bg_map_counter =1;
//        if(current_stage==1) {
//            Projectile_background moving_background = new Projectile_background(s_width, s_height, 0, -1*Math.round(6.5f*s_height), 0, 0,this);
//            bg_map.put(bg_map_counter,moving_background);
//            bg_map_counter++;
//            pause_background = false;
//            moving_background.load_game_background(bg_picture_hash, 0, 0.2f);
//        }
//        else{
//            Projectile_background moving_background = new Projectile_background(s_width, s_height, 0, 0, 0, 0,this);
//            bg_map.put(bg_map_counter,moving_background);
//            bg_map_counter++;
//            pause_background = false;
//            moving_background.load_game_background(bg_picture_hash, 0, 0);
//        }
//    }

    public void update_combo() {
        //update combo
//        ((MainActivity_stage) context).updateText(combo);
    }
    public void update_score() {
        //update combo
        ((MainActivity_stage) context).updateScoreText(score);
    }
    public void update_end_score() {
         ((MainActivity_stage) context).update_end_ScoreText(score);
    }
    public void update_multiplier() {
        //update combo
//        ((MainActivity_stage) context).updateMultiplierText(multiplier);
    }
    public void update_lives() {
        //update combo
        ((MainActivity_stage) context).change_life_text(life);
    }
    public void update_shield() {
        //update combo
//        ((MainActivity_stage) context).change_shield_text(ball.shield_count);
    }
    public void update_char_offense() {
        //update combo
//        ((MainActivity_stage) context).change_char_offense_text(char_offense_mode);
    }

    public int getNavHeight() {
        //navigation bar height
        int navigationBarHeight = 0;
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            navigationBarHeight = resources.getDimensionPixelSize(resourceId);
        }
        return navigationBarHeight;
    }

    public game_interface getGame_activity() {
        return game_activity;
    }

    public HashMap<Integer, Integer> getStage_completion_score() {
        return stage_completion_score;
    }

    public void setHolder(SurfaceHolder holder) {
        this.holder = holder;
    }

    public Context getContext() {
        return context;
    }

    public void create_star_calculator(int wave) {
        star_calculator = new Star_calculator(wave, current_stage);
    }

    public Star_calculator getStar_calculator() {
        return star_calculator;
    }

    public void create_shield_bitmap() {
        //initialise  shield
        Bitmap shield_high = Bitmap.createScaledBitmap(getBitmap(R.drawable.shield_high)
                , (int) (ball.getRect().height() * 1.2), (int) (ball.getRect().height() * 1.2), true);
        Bitmap shield_med = Bitmap.createScaledBitmap(getBitmap(R.drawable.shield_med)
                , (int) (ball.getRect().height() * 1.2), (int) (ball.getRect().height() * 1.2), true);
        Bitmap shield_low = Bitmap.createScaledBitmap(getBitmap(R.drawable.shield_low)
                , (int) (ball.getRect().height() * 1.2), (int) (ball.getRect().height() * 1.2), true);
        Bitmap shield_zero = Bitmap.createScaledBitmap(getBitmap(R.drawable.shield_zero)
                , (int) (ball.getRect().height() * 1.2), (int) (ball.getRect().height() * 1.2), true);

        shield_bitmap_inventory.put(3, shield_high);
        shield_bitmap_inventory.put(2, shield_med);
        shield_bitmap_inventory.put(1, shield_low);
        shield_bitmap_inventory.put(0, shield_zero);

//        for (int j = 1; j < char_offense_modes_count; j++) {
//            char_offense.put(j, new HashMap<Integer, Projectile_char_offense>());
//            for (int i = 1; i < 5; i++) { // 90 angle difference each
//                if(j==1){
//                    if(i==1){
//                        char_offense.get(j).put(i, new Projectile_char_offense(
//                                s_width, s_height, 0, 0, 0, 0,
//                                i, this, 3.14159f * 2 / (float) (Math.min(Math.pow(4, j - 1), 4)) * i + 3.14159f / 4 * (j - 1),
//                                shield_bitmap_inventory.get(1).getHeight() / 2 * 1.1f + Math.min((j - 1), 1) * shield_bitmap_inventory.get(5).getHeight() * 0.9f,
//                                j));
//                    }
//                }
//                else {
//                    char_offense.get(j).put(i, new Projectile_char_offense(
//                            s_width, s_height, 0, 0, 0, 0,
//                            i, this, 3.14159f * 2 / (float) (Math.min(Math.pow(4, j - 1), 4)) * i + 3.14159f / 4 * (j - 1),
//                            shield_bitmap_inventory.get(1).getHeight() / 2 + Math.min((j - 1), 1) * shield_bitmap_inventory.get(5).getHeight() * 0.9f,
//                            j));
//                }
////                Log.d(TAG, "create_shield_bitmap: distance is " + (ball.getRect().height() * 0.75f + ball.getRect().height() *(j-1)*0.35f));
//            }
//        }

        for (int j = 1; j < char_offense_modes_count; j++) {
            char_offense.put(j, new HashMap<Integer, Projectile_char_offense>());
            for (int i = 1; i < MyBitmap.char_offense_attributes.get(selected_char) +1; i++) { // 90 angle difference each
                char_offense.get(j).put(i, new Projectile_char_offense(
                        s_width, s_height, 0, 0, 0, 0,
                        i, this, 360 / 3 * (j - 1) +
                        360/MyBitmap.char_offense_attributes.get(selected_char) * (i-1),
                        Math.round(shield_bitmap_inventory.get(2).getHeight() / 2 * 1.05f),
                        1));

//                Log.d(TAG, "create_shield_bitmap: distance is " + (ball.getRect().height() * 0.75f + ball.getRect().height() *(j-1)*0.35f));
            }
        }

    }

    public void change_char_offense(int i) {
        char_offense_mode = Math.min(Math.max(Math.min(char_offense_mode + i, 3),1),default_char_offense_level);
    }

    public void load_char_expression_map(String character_name){
        char_expression_map = MyBitmap.character_image_bitmap_hash.get(character_name);

    }

    private void update_image_view(ImageView image, int id){
        image.setImageResource(id);
        image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        image.setBackgroundColor(ResourcesCompat.getColor(resources,android.R.color.transparent,null));
    }

    private void reset_game(){
        SharedPreferences local_db = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        selected_char = local_db.getString("selected_char","puffy");
        pause_background =false;
        change_char_offense(-3);
        life = local_db.getInt("life",3);
        default_lives = local_db.getInt("life",3);
        ball_shield_count = local_db.getInt("shield",1);
        default_shields = local_db.getInt("shield",3);
        default_char_offense_level = local_db.getInt("char_offense",1);
        spin_upgrade = local_db.getInt("spin_upgrade",1);
        if(ball!=null) {
            ball.init_shield(this, ball_shield_count);
        }
        score = 0;
        token_qty = 0;
        kill_qty = 0;
        ((MainActivity_stage) context).reset_end_ScoreText();
        ((MainActivity_stage) context).updateScoreText(score);
    }

    private void reset_and_continue_game(){
        SharedPreferences local_db = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        selected_char = local_db.getString("selected_char","puffy");
        life = local_db.getInt("life",3);
        ball_shield_count =local_db.getInt("shield",1);
        default_char_offense_level = local_db.getInt("char_offense",1);
//        change_char_offense(default_char_offense_level);
        change_char_offense(-3);
        spin_upgrade = local_db.getInt("spin_upgrade",1) *2;
        ball.init_shield(this, ball_shield_count);

        pause_background =false;
    }

    private void start_game_view_runner(){
        switch(current_stage){
            case 1:
                runner = GameView_stage1.runner;
                break;

            case 2:
                runner = GameView_stage2.runner;
                break;

            case 3:
                runner = GameView_stage3.runner;
                break;

            default:
                runner = GameView_stage1.runner;
                break;
        }
        runner = new GameRunner(this);
        runner.init_game();
        runner.start();
    }

    public void show_char_offense() {
        for (int j = 1; j < char_offense_modes_count; j++) {
            for (Map.Entry<Integer, Projectile_char_offense> entry : char_offense.get(j).entrySet()) {
                if (entry.getValue().dead) {
                } else {
                    entry.getValue().load_projectile();
                }
            }
        }
    }

    public boolean check_all_char_offense_dead_for_mode(int mode){
        boolean all_dead =true;
        for (Map.Entry<Integer, Projectile_char_offense> entry : char_offense.get(mode).entrySet()) {
            if(!entry.getValue().dead){
                all_dead = false;
                return all_dead;
            }
        }
        return all_dead;
    }

    public void draw_char_offense(Canvas canvas){
        for (int i = 1; i < char_offense_modes_count; i++) {
            for (Map.Entry<Integer, Projectile_char_offense> entry : char_offense.get(i).entrySet()) {
                if (entry.getValue().dead) {
                } else {
                    entry.getValue().draw_char_offense(canvas);
                }
            }
        }
    }


    public void update_char_offense_mode() {
        int count = 0;
        for (int j = 1; j < char_offense_modes_count; j++) {
            if(check_all_char_offense_dead_for_mode(j)){
            }
            else{
                count++;
            }
        }
        char_offense_mode = count;
        update_char_offense();
    }

    public void activate_char_offense() {
        for (int j = 1; j < char_offense_modes_count; j++) {
            if(check_all_char_offense_dead_for_mode(j)){
                for (Map.Entry<Integer, Projectile_char_offense> entry : char_offense.get(j).entrySet()) {
                    entry.getValue().load_projectile();
                }
//                Log.d(TAG, "activate_char_offense: mode activated is " + j);
                return;
            }
            else{
//                Log.d(TAG, "activate_char_offense: nothing happened + " +j);
            }
        }
    }
}
//
//
//
//
