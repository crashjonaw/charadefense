package nexidea.kawaii_defense;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import nexidea.kawaii_defense.BackgroundAesthetics.BackgroundRunner;

public class Stage_selection_View extends SurfaceView implements SurfaceHolder.Callback {
    public static final String MY_PREFS_NAME = "best_score_storage";
    private Bitmap background;
    private Animated_background background_animated;
    private BackgroundRunner runner;

    public Stage_selection_View(Context context, AttributeSet attrs) {
        super(context, attrs);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
//        if (MainActivity_stage_selection.getMp().isPlaying()){}
//        else {
//            MainActivity_stage_selection.getMp().start();
//        }
//        background = BitmapFactory.decodeResource(getResources(), R.drawable.stage_selection_map_color);
//        background = Bitmap.createScaledBitmap(background,
//                getWidth(),
//                getHeight(),true);
//        background_animated = new Animated_background(getContext(), getWidth(), getHeight(), holder, getResources(), background,0.1f,0);
//        //Log.d("tag", "surfaceCreated: runner started");
//        background_animated.getMoving_background().load_background(background,0,0);
//        runner = new BackgroundRunner(background_animated);
//        runner.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
//        MenuActivity.getMp().pause();
        if (runner != null){
            runner.shutdown();
            while(runner != null){
                try {
                    runner.join();
                    runner=null;
                } catch (InterruptedException e) {
                }
            }
        }
    }
    public int getNavHeight(){
        //navigation bar height
        int navigationBarHeight = 0;
        int resourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            navigationBarHeight = getResources().getDimensionPixelSize(resourceId);
        }
        return navigationBarHeight;
    }

}


