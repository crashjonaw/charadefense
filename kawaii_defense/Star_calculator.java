package nexidea.kawaii_defense;

public class Star_calculator {

    private int char_hits;
    private int bottom_bounces;
    public int stars;
    private int current_stage;


    public Star_calculator(int wave, int current_stage) {
        stars=0;
        char_hits = 0;     // 1 or less hits awards a star
        bottom_bounces =0; // 10 or less bottom bounces award a star
        this.current_stage = current_stage;
    }

    public void addChar_hits() {
        char_hits++;
    }

    public void addBottom_bounces() {
        bottom_bounces ++;
    }

    public int eval_stars(int score){
        //star given for completion
        stars++;
        switch(current_stage){
            case 1:
                if (char_hits < 2) {
                    stars++;
                }

                if (bottom_bounces < 10) {
                    stars++;
                }

                if(score>1000){
                    stars++;
                }

//                if(rotating_hits>7){
//                    stars++;
//                }
                break;

            case 2:
                if (char_hits < 2) {
                    stars++;
                }

                if (bottom_bounces < 10) {
                    stars++;
                }

                if(score>200){
                    stars++;
                }

//                if(rotating_hits>7){
//                    stars++;
//                }
                break;

            case 3:
                if (char_hits < 2) {
                    stars++;
                }

                if (bottom_bounces < 10) {
                    stars++;
                }

                if(score>100){
                    stars++;
                }

//                if(rotating_hits>7){
//                    stars++;
//                }
                break;
        }


        return Math.min(stars,3);
    }
}
