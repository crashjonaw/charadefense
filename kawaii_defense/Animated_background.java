package nexidea.kawaii_defense;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.SurfaceHolder;

import nexidea.kawaii_defense.Projectiles.Projectile_background;

//credit diamonds: <a href='https://www.freepik.com/free-vector/colorful-gemstones_968024.htm'>Designed by 0melapics</a>
//sound icon credit: <div>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>

public class Animated_background {
    //holder i.e. the layout and resources
    private SurfaceHolder holder;
    public  Resources resources;
    public boolean loaded = false;
    private Context context;

    private int s_width;
    private int s_height;
    private Bitmap background;
    private Projectile_background moving_background;
    private Projectile_background blank;
    private Paint paint;

    private static Bitmap blank_bg;

    //constructor
    public Animated_background(Context context, int width, int height, SurfaceHolder holder, Resources resource, Bitmap background, float speedX, float speedY) {

        //game startup parameters
        this.holder = holder;
        this.resources = resource;
        this.context = context;
        this.s_height = height;
        this.s_width = width;
        this.background = background;
        paint = new Paint();
        paint.setAlpha(255);
        //load moving background
        moving_background = new Projectile_background(s_width, s_height, 0, 0, speedX, speedY,null);
        blank = new Projectile_background(s_width,s_height,0,0,0,0,null);

        if(blank_bg!=null){
        }
        else{
            blank_bg = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                    resources, R.drawable.bomb_broken_1),
                    s_width, s_height, true);
        }
        blank.load_background(blank_bg,0,0);
//        blank.load_background(getBitmap(R.drawable.logo_blank),0,0);

    }
    public void init(){
    }

    public Paint getPaint() {
        return paint;
    }

    public void update(long elapsed) {
        //update background
        moving_background.update(elapsed);

        }

    public void update_alpha(long elapsed) {
        //update background
        moving_background.setPaint_alpha(Math.max(moving_background.paint.getAlpha()-2,5));
        moving_background.update(elapsed);
    }

    public void draw() {
        Canvas canvas = holder.lockCanvas();
        if (canvas != null) {
            moving_background.draw(canvas);
            holder.unlockCanvasAndPost(canvas);}
    }

    public void draw_bg(Canvas canvas) {
        moving_background.draw(canvas);
    }

    public void draw_premenu() {
        Canvas canvas = holder.lockCanvas();
        if (canvas != null) {
            blank.draw(canvas);
            moving_background.draw_background(canvas,moving_background.paint);
            holder.unlockCanvasAndPost(canvas);}
    }


    //convert a drawable to a Bitmap image
//    private Bitmap getBitmap(int drawableRes) {
//        Drawable drawable = context.getDrawable(drawableRes);
//        Canvas canvas = new Canvas();
//        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
//        canvas.setBitmap(bitmap);
//        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
//        drawable.draw(canvas);
//
//        return bitmap;
//    }


    public Projectile_background getMoving_background() {
        return moving_background;
    }
}
