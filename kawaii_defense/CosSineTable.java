package nexidea.kawaii_defense;

public class CosSineTable {
    float[] cos = new float[361];
    float[] sin = new float[361];
    private static CosSineTable table = new CosSineTable();

    private CosSineTable() {
        for (int i = 0; i <= 360; i++) {
            cos[i] = (float) Math.cos(Math.toRadians(i));
            sin[i] = (float) Math.sin(Math.toRadians(i));
        }
    }

    public float getSine(int angle) {
        int angleCircle = angle % 360;
        return sin[angleCircle];
    }

    public float getCos(int angle) {
        int angleCircle = angle % 360;
        return cos[angleCircle];
    }

    public static CosSineTable getTable() {
        return table;
    }
}