package nexidea.kawaii_defense;

import android.util.Log;

import static android.content.ContentValues.TAG;

public class GameRunner extends Thread {
    private Game game;
    private volatile boolean running = true;
    private static GameRunner gameRunner;
    private static boolean paused;
    private long start_time;
    private long total_time;
    private long fps;
    private long optimal_time;

    public GameRunner(Game game) {
        gameRunner = this;
        fps = 55;
        optimal_time = 1000 / fps;
        this.game = game;
    }

    public void init_game(){
        game.init();
    }

    @Override
    public void run() {
        start_time = System.currentTimeMillis();
        long lastTime = System.currentTimeMillis();
//      game runs here
        while (running) {
//             draw stuff.
            long now = System.currentTimeMillis();
            long elapsed = now - lastTime;
            lastTime = now;
            if (elapsed<1000) {
                if (!paused) {
//                    long time = System.currentTimeMillis();
                    game.update(elapsed);
                    game.draw();
//                    Log.d(TAG, "fps" +  Long.toString(1000/(System.currentTimeMillis() - time)));
                }
                else{}
            }

            try {
                Log.d(TAG, "run: sleep time = " + Long.toString(lastTime-System.currentTimeMillis()+optimal_time));
                Thread.sleep(Math.max(lastTime-System.currentTimeMillis() + optimal_time,0));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void shutdown(){
        running= false;
    }
    public void resume_runner() {
        running= true;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public static GameRunner getGameRunner() {
        return gameRunner;
    }

    public static void setPaused(boolean paused) {
        GameRunner.paused = paused;
    }

    public static boolean isPaused() {
        return paused;
    }
}

