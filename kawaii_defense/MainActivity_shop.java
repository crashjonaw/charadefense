package nexidea.kawaii_defense;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import nexidea.kawaii_defense.BackgroundAesthetics.BackgroundRunner;

import static nexidea.kawaii_defense.MenuView.MY_PREFS_NAME;
import static nexidea.kawaii_defense.MenuView.menuviewcontext;

public class MainActivity_shop extends AppCompatActivity implements View.OnClickListener {
    //song stuff
    private static ArrayList<Integer> songArray;
    private static MediaPlayer mp;
    public static Timer timer;
    private int i = 0;
    float volume = 0;
    private boolean paused;
    private TimerTask play_songs;
    private boolean first_song = true;
    private int max_achieved_wave;
    private int total_waves;
    private int s_width;
    private int s_height;

    //button stuff
    private HashMap<String,ImageButton> buy_buttons = new HashMap<>();

    //purchase type -> level -> cost
    private HashMap<String, HashMap<Integer,Integer>> shop_details = new HashMap<>();
    private int life;
    private int total_available_lives;
    private int shield;
    private int total_available_shield;
    private int spin_upgrade;
    private int total_available_spin_upgrade;
    private int char_offense;
    private int total_available_char_offense;

    //current inventory
    private HashMap<String,Integer> inventory = new HashMap<>();

    private HashMap<String,Integer> shop_items = new HashMap<>();

    private HashMap<String,String> shop_item_names = new HashMap<>();

    private ArrayList<String> order_shop_items = new ArrayList<>();

    //character selection and purchase
    private HashMap<String, Integer> char_price = new HashMap<>();

    private HashMap<String, Integer> char_preview = new HashMap<>();

    private HashMap<String, Boolean> char_inventory = new HashMap<>();

    private String selected_character;

    private HashMap<String,ImageButton> char_not_yet_selected_buttons = new HashMap<>();

    private HashMap<String,ImageButton> char_not_yet_bought_buttons = new HashMap<>();

    private ArrayList<String> order_characters = new ArrayList<>();

    private Context context;

    private ImageButton back_to_menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        SharedPreferences local_db = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        this.mp = MenuActivity.getMp();
        this.setContentView(R.layout.activity_shop);

        //set shop tokens
        set_shop_tokens();

        //Character shop - prices
        char_price.put("yuri",20000);
        char_price.put("puffy",0);
        char_price.put("summer_yuri",40000);

        //Character shop - Bitmaps
        char_preview.put("summer_yuri",R.drawable.summer_angry_1);
        char_preview.put("yuri",R.drawable.vanilla_happy_1);
        char_preview.put("puffy",R.drawable.puffy_neutral);

        order_characters.add("puffy");
        order_characters.add("yuri");
        order_characters.add("summer_yuri");

        //get character inventory
        get_char_inventory();
        //get current character selection
        selected_character = local_db.getString("selected_char","puffy");
        build_dynamic_character_shop_selection();


        //SHOP - UPGRADES
        //set total available
        total_available_char_offense = 3;
        total_available_shield = 3;
        total_available_lives = 7;
        total_available_spin_upgrade =  5;


        shop_items.put("life",total_available_lives);
        shop_items.put("shield",total_available_shield);
        shop_items.put("char_offense",total_available_char_offense);
        shop_items.put("spin_upgrade",total_available_spin_upgrade);

        shop_item_names.put("life", "Lives Count");
        shop_item_names.put("shield", "Shield Count");
        shop_item_names.put("spin_upgrade", "Defence Spin Rate");
        shop_item_names.put("char_offense", "Attack Damage");

        order_shop_items.add("life");
        order_shop_items.add("shield");
        order_shop_items.add("char_offense");
        order_shop_items.add("spin_upgrade");


        //build shop - upgrades
        build_shop_details();
        //step 1 - get info on current buy-ins
        get_inventory_info();
        build_dynamic_upgrades_shop_selection();

        context = this;

        back_to_menu = findViewById(R.id.back_to_menu_from_shop);
        back_to_menu.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.back_to_menu_from_shop){
            return_to_menu();
        }

        SharedPreferences local_db = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();

        //upgrades
        final LinearLayout shop = findViewById(R.id.shop);
        final int tokens = local_db.getInt("token",0);
        for (Map.Entry<String,ImageButton> entry : buy_buttons.entrySet()) {
            if (findViewById(v.getId()) == entry.getValue()){
                final int amount_needed = shop_details.get(entry.getKey()).get(inventory.get(entry.getKey())+1);
                final String upgrade_type = entry.getKey();
                if(tokens < amount_needed){
                    Toast.makeText(this, "Not enough tokens. Need "+ Integer.toString(amount_needed-tokens) +" more.", Toast.LENGTH_SHORT).show();
                    return;
                }
                else{
                    Handler mHandler = new Handler(Looper.getMainLooper());
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
                            // Get the layout inflater
                            LayoutInflater inflater = LayoutInflater.from(context);
                            View mView = inflater.inflate(R.layout.popup_buy_confirmation, null);
                            mBuilder.setView(mView);

                            TextView pre = mView.findViewById(R.id.pre_purchase_amt);
                            pre.setText(Integer.toString(tokens));
                            TextView cost = mView.findViewById(R.id.cost_of_item);
                            cost.setText(Integer.toString(amount_needed));
                            TextView post = mView.findViewById(R.id.post_purchase_amt);
                            post.setText(Integer.toString(tokens-amount_needed));

                            final AlertDialog dialog = mBuilder.create();

                            Button no = mView.findViewById(R.id.purchase_no);
                            no.setOnClickListener(new View.OnClickListener() {
                                                      @Override
                                                      public void onClick(View view) {
                                                          dialog.dismiss();
                                                      }
                                                  }
                            );

                            Button yes = mView.findViewById(R.id.purchase_yes);
                            yes.setOnClickListener(new View.OnClickListener() {
                                                       @Override
                                                       public void onClick(View view) {
                                                           editor.putInt(upgrade_type,inventory.get(upgrade_type)+1);
                                                           editor.putInt("token",tokens-amount_needed);
                                                           editor.apply();
                                                           shop.removeAllViews();
                                                           get_inventory_info();
                                                           build_dynamic_upgrades_shop_selection();
                                                           set_shop_tokens();
                                                           dialog.dismiss();
                                                       }
                                                   }
                            );

                            dialog.setCanceledOnTouchOutside(false);
                            dialog.setCancelable(false);
                            dialog.show();
                        }
                    });
                }
            }
        }

        //select character
        final LinearLayout char_shop = findViewById(R.id.character_shop);
        for (Map.Entry<String,ImageButton> entry : char_not_yet_selected_buttons.entrySet()) {
            if (findViewById(v.getId()) == entry.getValue()){
                selected_character = entry.getKey();
                editor.putString("selected_char",entry.getKey());
                editor.apply();
                char_shop.removeAllViews();
                get_char_inventory();
                build_dynamic_character_shop_selection();
                return;
            }
        }

        //buying new character
        for (Map.Entry<String,ImageButton> entry : char_not_yet_bought_buttons.entrySet()) {
            if (findViewById(v.getId()) == entry.getValue()){
                final int amount_needed = char_price.get(entry.getKey());
                if(tokens < amount_needed){
                    Toast.makeText(this, "Not enough tokens. Need "+ Integer.toString(amount_needed-tokens) +" more.", Toast.LENGTH_SHORT).show();
                    return;
                }
                else{
                    final String purchased_character = entry.getKey();
                    Handler mHandler = new Handler(Looper.getMainLooper());
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
                            // Get the layout inflater
                            LayoutInflater inflater = LayoutInflater.from(context);
                            View mView = inflater.inflate(R.layout.popup_buy_confirmation, null);
                            mBuilder.setView(mView);

                            TextView pre = mView.findViewById(R.id.pre_purchase_amt);
                            pre.setText(Integer.toString(tokens));
                            TextView cost = mView.findViewById(R.id.cost_of_item);
                            cost.setText(Integer.toString(amount_needed));
                            TextView post = mView.findViewById(R.id.post_purchase_amt);
                            post.setText(Integer.toString(tokens-amount_needed));

                            final AlertDialog dialog = mBuilder.create();

                            Button no = mView.findViewById(R.id.purchase_no);
                            no.setOnClickListener(new View.OnClickListener() {
                                                      @Override
                                                      public void onClick(View view) {
                                                          dialog.dismiss();
                                                      }
                                                  }
                            );

                            Button yes = mView.findViewById(R.id.purchase_yes);
                            yes.setOnClickListener(new View.OnClickListener() {
                                                       @Override
                                                       public void onClick(View view) {
                                                           char_inventory.put(purchased_character,true);
                                                           editor.putInt("token",tokens-amount_needed);
                                                           GsonBuilder builder = new GsonBuilder();
                                                           Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
                                                           Type type = new TypeToken<HashMap<String, Boolean>>() {
                                                           }.getType();
                                                           String json = gson.toJson(char_inventory, type);
                                                           editor.putString("char_inventory", json);
                                                           editor.apply();
                                                           char_shop.removeAllViews();
                                                           get_char_inventory();
                                                           build_dynamic_character_shop_selection();
                                                           dialog.dismiss();
                                                       }
                                                   }
                            );
                            dialog.setCanceledOnTouchOutside(false);
                            dialog.setCancelable(false);
                            dialog.show();
                        }
                    });
                }
            }
        }

    }

    @Override
    public void onBackPressed() {
        return_to_menu();
    }

    public void return_to_menu() {
        Intent intent = new Intent(this, MenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("timer", "onPause: timer canceled and purged");
        paused = true;
        if (mp != null) {
            mp.stop();
            mp.reset();
        }
        BackgroundRunner.getBackgroundRunner().shutdown();
        // Do your stuff here when you are stopping your activity
    }

    @Override
    public void onResume() {
        super.onResume();
        Game.free_hold = false;
        i = 0;
        paused = false;
//        play_songs = new TimerTask() {
//            @Override
//            public void run() {
//                if (!paused) {
//                    if (!first_song) {
//                        startFadeOut();
//                        mp.stop();
//                        mp.reset();
//                    }
//                    if (i == songArray.size()) {
//                        i = 0;
//                    } else {
//                    }
//                    mp = MediaPlayer.create(this, songArray.get(i));
//                    mp.start();
//                    startFadeIn();
//                    first_song = false;
//                    i++;
//                }
//            }
//        };
//        timer.scheduleAtFixedRate(play_songs, 3000, 309000);
    }

    public static MediaPlayer getMp() {
        return mp;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    private void get_inventory_info(){
        SharedPreferences local_db = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        life = local_db.getInt("life",3);
        shield = local_db.getInt("shield",1);
        spin_upgrade =  local_db.getInt("spin_upgrade",1);
        char_offense = local_db.getInt("char_offense",1);
        inventory.put("life",life);
        inventory.put("shield",shield);
        inventory.put("spin_upgrade",spin_upgrade);
        inventory.put("char_offense",char_offense);
    }


    private void build_dynamic_upgrades_shop_selection() {
        LinearLayout shop = findViewById(R.id.shop);

        for (int k =0; k< order_shop_items.size(); k ++){
//
//        }
//        for (Map.Entry<String,Integer> entry : shop_items.entrySet()) {
            LinearLayout item = new LinearLayout(this);
            LinearLayout.LayoutParams item_layout_params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            item.setLayoutParams(item_layout_params);
            item.setOrientation(LinearLayout.VERTICAL);
            item.setPadding(0,0,0,(int) getResources().getDimension(R.dimen._10sdp));

            //create textview for the title of the item
            TextView item_name = new TextView(this);
            LinearLayout.LayoutParams item_name_layout_params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT
            );
            item_name.setGravity(Gravity.CENTER);
            item_name.setTextColor(Color.WHITE);
            item_name.setLayoutParams(item_name_layout_params);
            item_name.setText(shop_item_names.get(order_shop_items.get(k)));
            item_name.setTextSize(getResources().getDimension(R.dimen._5ssp));
            Typeface typeface = ResourcesCompat.getFont(this, R.font.ribeye_marrow);
            item_name.setTypeface(typeface);

            //add item name to itemview
            item.addView(item_name);

            //create the level bars and purchase selection
            LinearLayout item_levels_and_purchase_ll = new LinearLayout(this);
            LinearLayout.LayoutParams ilp_params = new LinearLayout.LayoutParams(
                   LinearLayout.LayoutParams.MATCH_PARENT, (int) getResources().getDimension(R.dimen._40sdp)
            );
            item_levels_and_purchase_ll.setLayoutParams(ilp_params);
            item_levels_and_purchase_ll.setOrientation(LinearLayout.HORIZONTAL);
            item_levels_and_purchase_ll.setWeightSum(10f);

            for(int i = 1; i < shop_items.get(order_shop_items.get(k)) + 1; i++){
                if(i <= inventory.get(order_shop_items.get(k))){
                    ImageView filled_level = new ImageView(this);
                    LinearLayout.LayoutParams fl_params = new LinearLayout.LayoutParams(
                            0, LinearLayout.LayoutParams.MATCH_PARENT,(float) 7/ shop_items.get(order_shop_items.get(k))
                    );
                    filled_level.setLayoutParams(fl_params);
                    filled_level.setBackgroundResource(R.drawable.click_button3);
                    item_levels_and_purchase_ll.addView(filled_level);
//                    Log.d("weight", "build_dynamic_shop_selection: filled " + Float.toString((float) 7/entry.getValue()));
                }
                else{
                    ImageView filled_level2 = new ImageView(this);
                    LinearLayout.LayoutParams fl_params2 = new LinearLayout.LayoutParams(
                            0, LinearLayout.LayoutParams.MATCH_PARENT,(float) 7/ shop_items.get(order_shop_items.get(k))
                    );
                    filled_level2.setLayoutParams(fl_params2);
                    filled_level2.setBackgroundResource(R.drawable.click_button);
                    item_levels_and_purchase_ll.addView(filled_level2);
//                    Log.d("weight", "build_dynamic_shop_selection: not filled " + Float.toString((float) 7/entry.getValue()));
                }
            }

            //buy_button
            FrameLayout buy_screen = new FrameLayout(this);
            LinearLayout.LayoutParams bs_lp = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT,3f
            );
            buy_screen.setLayoutParams(bs_lp);
            buy_screen.setBackgroundResource(R.drawable.background_with_stroke_home_bottom);
//            Log.d("t", "build_dynamic_shop_selection: total weight" + Float.toString((float) 7/entry.getValue() * entry.getValue() + 3f));

            //add imagebutton to it -whatever works
            ImageButton buy = new ImageButton(this);
            FrameLayout.LayoutParams buy_params = new FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT
            );
            buy.setLayoutParams(buy_params);
            buy.setBackgroundColor(Color.TRANSPARENT);
            buy_screen.addView(buy);

            //create linear layout with cost price and W token image;
            LinearLayout cost_price_and_image_ll = new LinearLayout(this);
            FrameLayout.LayoutParams cost_price_and_image_ll_params = new FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT
            );
            cost_price_and_image_ll.setLayoutParams(cost_price_and_image_ll_params);
            cost_price_and_image_ll.setGravity(Gravity.RIGHT);
            cost_price_and_image_ll.setOrientation(LinearLayout.HORIZONTAL);
            cost_price_and_image_ll.setPadding(0,0,(int) getResources().getDimension(R.dimen._5sdp),0);

            //add textview to it
            TextView cost = new TextView(this);
            LinearLayout.LayoutParams cost_params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT
            );

            cost.setLayoutParams(cost_params);
            cost.setGravity(Gravity.CENTER);
            if(inventory.get(order_shop_items.get(k))==shop_items.get(order_shop_items.get(k))){
                cost.setText("MAX");
                cost.setTextSize(getResources().getDimension(R.dimen._5ssp));
                cost.setTextColor(Color.YELLOW);
            }
            else {
                cost.setText(Integer.toString(shop_details.get(order_shop_items.get(k)).get(inventory.get(order_shop_items.get(k)) + 1)));
                cost.setTextSize(getResources().getDimension(R.dimen._5ssp));
                cost.setTextColor(Color.WHITE);
            }
            cost.setPadding(0,0,(int) getResources().getDimension(R.dimen._2sdp),0);
            Typeface typeface2 = ResourcesCompat.getFont(this, R.font.ribeye_marrow);
            cost.setTypeface(typeface2);
            cost_price_and_image_ll.addView(cost);


            if(inventory.get(order_shop_items.get(k))==shop_items.get(order_shop_items.get(k))){
                //add the gap to it
                ImageView gap = new ImageView(this);
                LinearLayout.LayoutParams gap_params = new LinearLayout.LayoutParams(
                        (int) getResources().getDimension(R.dimen._15sdp),(int) getResources().getDimension(R.dimen._15sdp)
                );
                gap.setLayoutParams(gap_params);
                cost_price_and_image_ll.addView(gap);
            }
            else {
                //add the w token image to it
                ImageView w_token = new ImageView(this);
                LinearLayout.LayoutParams w_token_params = new LinearLayout.LayoutParams(
                        (int) getResources().getDimension(R.dimen._25sdp), (int) getResources().getDimension(R.dimen._25sdp)
                );
                w_token_params.gravity = Gravity.CENTER;
                w_token.setLayoutParams(w_token_params);
                w_token.setPadding((int) getResources().getDimension(R.dimen._5sdp),
                        (int) getResources().getDimension(R.dimen._5sdp),
                        (int) getResources().getDimension(R.dimen._5sdp),
                        (int) getResources().getDimension(R.dimen._5sdp));
                w_token.setBackgroundResource(R.drawable.token);

                cost_price_and_image_ll.addView(w_token);
            }

            //add cost_price_and_image_ll to buy_screen
            buy_screen.addView(cost_price_and_image_ll);
            if(inventory.get(order_shop_items.get(k))==shop_items.get(order_shop_items.get(k))) {
            }
            else {
                int image_button_id = View.generateViewId();
                buy.setId(image_button_id);
                buy.setOnClickListener(this);
                buy_buttons.put(order_shop_items.get(k), buy);
            }


            //add buy screen to item_levels_and_purchase_ll
            item_levels_and_purchase_ll.addView(buy_screen);

            //add item_levels_and_purchase_ll to item
            item.addView(item_levels_and_purchase_ll);

            //add item to shop
            shop.addView(item);

            Log.d("tag", "build_dynamic_shop_selection: " + order_shop_items.get(k)+ " added");
        }
    }

    private void build_shop_details(){
        //lives
        HashMap<Integer,Integer> life_shop_details = new HashMap<>();
        life_shop_details.put(4,10000);
        life_shop_details.put(5,10000);
        life_shop_details.put(6,10000);
        life_shop_details.put(7,10000);
        life_shop_details.put(8,0);
        shop_details.put("life",life_shop_details);

        HashMap<Integer,Integer> shield_shop_details = new HashMap<>();
        shield_shop_details.put(2,8000);
        shield_shop_details.put(3,10000);
        shield_shop_details.put(4,12000);
        shield_shop_details.put(5,14000);
        shield_shop_details.put(6,0);
        shop_details.put("shield",shield_shop_details);

        HashMap<Integer,Integer> char_offense_shop_details = new HashMap<>();
        char_offense_shop_details.put(2,20000);
        char_offense_shop_details.put(3,40000);
        char_offense_shop_details.put(4,0);
        shop_details.put("char_offense",char_offense_shop_details);

        HashMap<Integer,Integer> spin_upgrade_shop_details = new HashMap<>();
        spin_upgrade_shop_details.put(2,5000);
        spin_upgrade_shop_details.put(3,6000);
        spin_upgrade_shop_details.put(4,7000);
        spin_upgrade_shop_details.put(5,8000);
        spin_upgrade_shop_details.put(6,0);
        shop_details.put("spin_upgrade",spin_upgrade_shop_details);

    }

    public void set_shop_tokens(){
        SharedPreferences local_db = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        TextView user_tokens = findViewById(R.id.shop_tokens);
        user_tokens.setText(Integer.toString(local_db.getInt("token",0)));
    }

    private void get_char_inventory(){
        SharedPreferences local_db = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = menuviewcontext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        String char_inventory_json_serial = local_db.getString("char_inventory", "");
        if (char_inventory_json_serial == "") {
            for (Map.Entry<String,Integer> entry : char_price.entrySet()) {
                if(entry.getKey() == "puffy"){
                    char_inventory.put(entry.getKey(),true);
                }
                else{
                    char_inventory.put(entry.getKey(),false);
                }
            }
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
            Type type = new TypeToken<HashMap<String,Boolean>>() {
            }.getType();
            String json = gson.toJson(char_inventory, type);
            editor.putString("char_inventory", json);
        } else {
            char_inventory = new Gson().fromJson(char_inventory_json_serial,
                    new TypeToken<HashMap<String, Boolean>>() {
                    }.getType());
        }
        editor.apply();
    }

    private void build_dynamic_character_shop_selection() {
        LinearLayout char_shop = findViewById(R.id.character_shop);

        for(int k =0; k < order_characters.size();k++){
//
//        }
//        for(Map.Entry<String,Integer> entry: char_preview.entrySet()){
            //create container to host character_shop
            LinearLayout container = new LinearLayout(this);
            LinearLayout.LayoutParams container_params = new LinearLayout.LayoutParams(
                    (int) getResources().getDimension(R.dimen._90sdp),LinearLayout.LayoutParams.MATCH_PARENT
            );
            container.setLayoutParams(container_params);
            container.setGravity(Gravity.CENTER);
            container.setOrientation(LinearLayout.VERTICAL);

            //selected character
            Log.d("tag", "build_dynamic_character_shop_selection: " + order_characters.get(k) + "/ selected char:" + selected_character);
            if(order_characters.get(k).equals(selected_character)){
                //selected text
                TextView selected = new TextView(this);
                LinearLayout.LayoutParams selected_params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT
                );
                selected.setLayoutParams(selected_params);
                selected.setGravity(Gravity.CENTER);
                selected.setText("Selected");
                selected.setTextSize(getResources().getDimension(R.dimen._5ssp));
                selected.setTextColor(Color.WHITE);
                Typeface typeface2 = ResourcesCompat.getFont(this, R.font.ribeye_marrow);
                selected.setTypeface(typeface2);
                container.addView(selected);

                //frame_for_character and boundary
                FrameLayout frame = new FrameLayout(this);
                LinearLayout.LayoutParams frame_param = new LinearLayout.LayoutParams(
                        (int) getResources().getDimension(R.dimen._80sdp), LinearLayout.LayoutParams.WRAP_CONTENT
                );
                frame.setLayoutParams(frame_param);

                //image button->frame
                ImageButton button = new ImageButton(this);
                FrameLayout.LayoutParams button_params = new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT
                );
                button.setBackgroundColor(Color.TRANSPARENT);
                button.setLayoutParams(button_params);
                frame.addView(button);

                //character_image -> frame
                ImageView char_preview_image = new ImageView(this);
                FrameLayout.LayoutParams char_preview_image_params = new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT
                );
                char_preview_image.setLayoutParams(char_preview_image_params);
                char_preview_image.setPadding(
                        (int) getResources().getDimension(R.dimen._3sdp),
                        (int) getResources().getDimension(R.dimen._3sdp),
                        (int) getResources().getDimension(R.dimen._3sdp),
                        (int) getResources().getDimension(R.dimen._3sdp)
                        );
                char_preview_image.setImageResource(char_preview.get(order_characters.get(k)));
                char_preview_image.setBackgroundColor(Color.TRANSPARENT);
                char_preview_image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                frame.addView(char_preview_image);

                //character image border
                ImageView char_preview_image_border = new ImageView(this);
                FrameLayout.LayoutParams char_preview_image_border_params = new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT
                );
                char_preview_image_border.setLayoutParams(char_preview_image_border_params);
                char_preview_image_border.setBackground(getDrawable(R.drawable.char_selected_green));
                frame.addView(char_preview_image_border);

                container.addView(frame);

                char_shop.addView(container);

                Log.d("tag", "build_dynamic_character_shop_selection: builded selected char");
            }

            else if (char_inventory.get(order_characters.get(k))){ //available: true for available, false for not available
                //selected text
                TextView selected = new TextView(this);
                LinearLayout.LayoutParams selected_params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT
                );
                selected.setLayoutParams(selected_params);
                selected.setGravity(Gravity.CENTER);
                selected.setText("");
                selected.setTextSize(getResources().getDimension(R.dimen._5ssp));
                selected.setTextColor(Color.WHITE);
                Typeface typeface2 = ResourcesCompat.getFont(this, R.font.ribeye_marrow);
                selected.setTypeface(typeface2);
                container.addView(selected);

                //frame_for_character and boundary
                FrameLayout frame = new FrameLayout(this);
                LinearLayout.LayoutParams frame_param = new LinearLayout.LayoutParams(
                        (int) getResources().getDimension(R.dimen._80sdp), LinearLayout.LayoutParams.WRAP_CONTENT
                );
                frame.setLayoutParams(frame_param);

                //image button->frame
                ImageButton button = new ImageButton(this);
                FrameLayout.LayoutParams button_params = new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT
                );
                button.setBackgroundColor(Color.TRANSPARENT);
                button.setLayoutParams(button_params);
                frame.addView(button);

                //character_image -> frame
                ImageView char_preview_image = new ImageView(this);
                FrameLayout.LayoutParams char_preview_image_params = new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT
                );
                char_preview_image.setLayoutParams(char_preview_image_params);
                char_preview_image.setPadding(
                        (int) getResources().getDimension(R.dimen._3sdp),
                        (int) getResources().getDimension(R.dimen._3sdp),
                        (int) getResources().getDimension(R.dimen._3sdp),
                        (int) getResources().getDimension(R.dimen._3sdp)
                );
                char_preview_image.setImageResource(char_preview.get(order_characters.get(k)));
                char_preview_image.setBackgroundColor(Color.TRANSPARENT);
                char_preview_image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                frame.addView(char_preview_image);

                //character image border
                ImageView char_preview_image_border = new ImageView(this);
                FrameLayout.LayoutParams char_preview_image_border_params = new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT
                );
                char_preview_image_border.setLayoutParams(char_preview_image_border_params);
                char_preview_image_border.setBackground(getDrawable(R.drawable.char_selected_yellow));
                frame.addView(char_preview_image_border);

                //add frame to container
                container.addView(frame);

                //add buttons
                int image_button_id = View.generateViewId();
                button.setId(image_button_id);
                button.setOnClickListener(this);
                char_not_yet_selected_buttons.put(order_characters.get(k),button);

                char_shop.addView(container);
            }

            else{
                FrameLayout button_frame = new FrameLayout(this);
                LinearLayout.LayoutParams button_frame_params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT
                );
                button_frame.setLayoutParams(button_frame_params);

                //add image button to button_frame
                //image button->button_frame
                ImageButton button = new ImageButton(this);
                FrameLayout.LayoutParams button_params = new FrameLayout.LayoutParams(
                        (int) getResources().getDimension(R.dimen._80sdp),
                        (int) getResources().getDimension(R.dimen._140sdp),
                        Gravity.CENTER
                );
                button.setBackgroundColor(Color.TRANSPARENT);
                button.setLayoutParams(button_params);
                button_frame.addView(button);


                //ll for char_item_ll
                LinearLayout char_item_ll = new LinearLayout(this);
                FrameLayout.LayoutParams char_item_ll_params = new FrameLayout.LayoutParams(
                        (int) getResources().getDimension(R.dimen._90sdp),
                        FrameLayout.LayoutParams.MATCH_PARENT,
                        Gravity.CENTER
                );
                char_item_ll.setLayoutParams(char_item_ll_params);
                char_item_ll.setOrientation(LinearLayout.VERTICAL);

                //frame_for price
                FrameLayout price_frame = new FrameLayout(this);
                LinearLayout.LayoutParams price_frame_param = new LinearLayout.LayoutParams(
                        (int) getResources().getDimension(R.dimen._80sdp),
                        LinearLayout.LayoutParams.MATCH_PARENT
                );
                price_frame.setLayoutParams(price_frame_param);
                price_frame.setBackground(getDrawable(R.drawable.background_with_stroke_home_bottom));

                //Linear layout for price and coin
                LinearLayout price_coin_ll = new LinearLayout(this);
                FrameLayout.LayoutParams price_coin_ll_params = new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT,
                        FrameLayout.LayoutParams.MATCH_PARENT
                );
                price_coin_ll.setLayoutParams(price_coin_ll_params);
                price_coin_ll.setGravity(Gravity.CENTER);
                price_coin_ll.setOrientation(LinearLayout.HORIZONTAL);

                //textview for price
                TextView price = new TextView(this);
                LinearLayout.LayoutParams price_params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT
                );
                price.setLayoutParams(price_params);
                price.setText(Integer.toString(char_price.get(order_characters.get(k))));
                price.setTextSize(getResources().getDimension(R.dimen._5ssp));
                price.setTextColor(Color.WHITE);
                Typeface typeface2 = ResourcesCompat.getFont(this, R.font.ribeye_marrow);
                price.setTypeface(typeface2);
                price_coin_ll.addView(price);

                //token picture
                ImageView token = new ImageView(this);
                LinearLayout.LayoutParams token_params = new LinearLayout.LayoutParams(
                        (int) getResources().getDimension(R.dimen._15sdp),
                        (int) getResources().getDimension(R.dimen._15sdp)
                );
                token_params.gravity=Gravity.CENTER;
                token.setLayoutParams(token_params);
                token.setBackground(getDrawable(R.drawable.token));
                price_coin_ll.addView(token);

                price_frame.addView(price_coin_ll);

                char_item_ll.addView(price_frame);

                //frame_for_character and boundary
                FrameLayout frame = new FrameLayout(this);
                LinearLayout.LayoutParams frame_param = new LinearLayout.LayoutParams(
                        (int) getResources().getDimension(R.dimen._80sdp), LinearLayout.LayoutParams.WRAP_CONTENT
                );
                frame.setLayoutParams(frame_param);

                //character_image -> frame
                ImageView char_preview_image = new ImageView(this);
                FrameLayout.LayoutParams char_preview_image_params = new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT
                );
                char_preview_image.setLayoutParams(char_preview_image_params);
                char_preview_image.setPadding(
                        (int) getResources().getDimension(R.dimen._3sdp),
                        (int) getResources().getDimension(R.dimen._3sdp),
                        (int) getResources().getDimension(R.dimen._3sdp),
                        (int) getResources().getDimension(R.dimen._3sdp)
                );
                char_preview_image.setImageResource(char_preview.get(order_characters.get(k)));
                char_preview_image.setBackgroundColor(Color.TRANSPARENT);
                char_preview_image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                frame.addView(char_preview_image);

                //character image border
                ImageView char_preview_image_border = new ImageView(this);
                FrameLayout.LayoutParams char_preview_image_border_params = new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT
                );
                char_preview_image_border.setLayoutParams(char_preview_image_border_params);
                char_preview_image_border.setBackground(getDrawable(R.drawable.char_selected_black));
                frame.addView(char_preview_image_border);

                char_item_ll.addView(frame);

                button_frame.addView(char_item_ll);

                int image_button_id = View.generateViewId();
                button.setId(image_button_id);
                button.setOnClickListener(this);
                char_not_yet_bought_buttons.put(order_characters.get(k),button);

                char_shop.addView(button_frame);
            }

        }
    }

    public void getScreenDimensions(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        s_width = width;
        s_height = height;

    }


//    public static ArrayList<Integer> getArrSong(String venue) {
//        Field[] ID_Fields = R.raw.class.getFields();
//        songArray = new ArrayList<Integer>();
//        for (int i = 0; i < ID_Fields.length; i++) {
//            if (ID_Fields[i].getName() != null &&
//                    ID_Fields[i].getName().contains(venue)) {
//                try {
//                    //Log.d("musicidfieldname",ID_Fields[i].getName());
//                    songArray.add(ID_Fields[i].getInt(null));
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        Log.d("array print", "array" + songArray.toString());
//        return songArray;
//    }

    private void startFadeIn() {
        final int FADE_DURATION = 3000; //The duration of the fade
        //The amount of time between volume changes. The smaller this is, the smoother the fade
        final int FADE_INTERVAL = 250;
        final int MAX_VOLUME = 1; //The volume will increase from 0 to 1
        int numberOfSteps = FADE_DURATION / FADE_INTERVAL; //Calculate the number of fade steps
        //Calculate by how much the volume changes each step
        final float deltaVolume = MAX_VOLUME / (float) numberOfSteps;

        //Create a new Timer and Timer task to run the fading outside the main UI thread
        final Timer timer = new Timer(true);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                fadeInStep(deltaVolume); //Do a fade step
                //Cancel and Purge the Timer if the desired volume has been reached
                if (volume >= 1f) {
                    timer.cancel();
                    timer.purge();
                }
            }
        };

        timer.schedule(timerTask, FADE_INTERVAL, FADE_INTERVAL);
    }

    private void startFadeOut() {
        final int FADE_DURATION = 3000; //The duration of the fade
        //The amount of time between volume changes. The smaller this is, the smoother the fade
        final int FADE_INTERVAL = 250;
        final int MAX_VOLUME = 1; //The volume will increase from 0 to 1
        int numberOfSteps = FADE_DURATION / FADE_INTERVAL; //Calculate the number of fade steps
        //Calculate by how much the volume changes each step
        final float deltaVolume = MAX_VOLUME / (float) numberOfSteps;

        //Create a new Timer and Timer task to run the fading outside the main UI thread
        final Timer timer = new Timer(true);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                fadeOutStep(deltaVolume); //Do a fade step
                //Cancel and Purge the Timer if the desired volume has been reached
                if (volume <= 0.5f) {
                    timer.cancel();
                    timer.purge();
                }
            }
        };

        timer.schedule(timerTask, FADE_INTERVAL, FADE_INTERVAL);
    }

    private void fadeInStep(float deltaVolume) {
        mp.setVolume(volume, volume);
        volume += deltaVolume;

    }

    private void fadeOutStep(float deltaVolume) {
        mp.setVolume(volume, volume);
        volume -= deltaVolume;
    }

    //convert a drawable to a Bitmap image
    private Bitmap getBitmap(int drawableRes) {
        Drawable drawable = ResourcesCompat.getDrawable(getResources(),drawableRes,null);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_4444);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }
}

