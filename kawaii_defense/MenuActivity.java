package nexidea.kawaii_defense;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import nexidea.kawaii_defense.Leaderboard.LbActivity;


public class MenuActivity extends AppCompatActivity implements View.OnClickListener{
    public static final String MY_PREFS_NAME = "local_db";
    private static MediaPlayer mp;
    private ImageButton play_to_stage_selection;
    private ImageButton leaderboard;
    private ImageButton shop;
    public static SharedPreferences local_db;
    private boolean internet;
    private CollectionReference db = FirebaseFirestore.getInstance().collection("players");

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        local_db = this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        Log.d("dimensions", "onCreate: W/H " + getScreenWidth() + getScreenHeight());
        setContentView(R.layout.activity_menu);
//        mp = MediaPlayer.create(getApplicationContext(), R.raw.menu_song_final);
//        mp.setLooping(true);
//        mp.start();

        play_to_stage_selection = findViewById(R.id.play_to_stage_selection);
        play_to_stage_selection.setOnClickListener(this);
        play_to_stage_selection.startAnimation(gen_rotate_animation(0,360 * 1.5f,5000,200,Animation.INFINITE,1f,true,2));

        leaderboard = findViewById(R.id.button_leaderboard);
        leaderboard.setOnClickListener(this);
        leaderboard.startAnimation(gen_rotate_animation(360* 1.25f,0,5000,200,-1,1f,true,2));

        shop = findViewById(R.id.button_upgrade);
        shop.setOnClickListener(this);
        shop.startAnimation(gen_rotate_animation(0,360* 1.25f,5000,200,-1,1f,true,2));

        FadeInButtons(5000,play_to_stage_selection,leaderboard,shop,findViewById(R.id.credits));

        TextView user = findViewById(R.id.user_check);

//        clear any local storage
//        SharedPreferences.Editor editor_clear = this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
//        editor_clear.clear();
//        editor_clear.putInt("token",0);
//        editor_clear.apply();

        TextView menu_tokens = findViewById(R.id.menu_tokens);
        menu_tokens.setText(Integer.toString(local_db.getInt("token",0)));


        if(local_db.getInt("personal_best",0) == 0){
            SharedPreferences.Editor editor = this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putInt("personal_best",0);
            editor.apply();
        }

        if(local_db.getInt("latest_score",0) == 0){
            SharedPreferences.Editor editor = this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putInt("latest_score",0);
            editor.apply();
        }

        //check for global_id and create_dialog();
        if(local_db.getString("global_id", "") == ""){
            if(isNetworkConnected()){
                create_dialog(this);}
            else{
                user.setText("not connected to internet, scores will not be saved.");
            }
        }
        else{
            user.setText(local_db.getString("global_id","error!"));
            //pass
        }

        //add tokens for testing
//        SharedPreferences.Editor editor = this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
//        editor.putInt("token",999999);
//        editor.putInt("life",3);
//        editor.putInt("spin_upgrade",1);
//        editor.apply();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(MenuView.runner != null) {
            MenuView.runner.shutdown();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if(MenuView.runner!=null) {
            MenuView.runner.run();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.play_to_stage_selection:
                MenuView.runner.shutdown();
                openGame("stage_selection");
                break;

            case R.id.button_leaderboard:
                MenuView.runner.shutdown();
                openGame("leaderboard");
                break;

            case R.id.button_upgrade:
                MenuView.runner.shutdown();
                openGame("upgrade");
                break;

            default:
                break;
        }
    }

    public void openGame(String scene) {
//        mp.stop();
        switch (scene) {
            case "stage_selection":
                Intent intent2 = new Intent(this, MainActivity_stage_selection.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;

            case "leaderboard":
                Intent intent3 = new Intent(this,LbActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent3);
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;

            case "upgrade":
                Intent intent4 = new Intent(this,MainActivity_shop.class);
                intent4.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent4);
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            default:
                break;
        }
    }

    public void return_to_menu(){
        Intent intent = new Intent(this, MenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
//        mp.stop();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public static MediaPlayer getMp() {
        return mp;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }


    public void create_dialog(final Context context) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(context);
        View mView = inflater.inflate(R.layout.popup_new_player,null);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();

        final EditText username = (EditText) mView.findViewById(R.id.username);



        Button submit =  mView.findViewById(R.id.submit_new_id);


        submit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if (!username.getText().toString().isEmpty()){
                    final String name = username.getText().toString();

                    //get list of all names already on the firestore db
                    db.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    Log.d("namecheck", "onComplete: matching " + document.getId() + " name:" +name +"check"+name.equals(document.getId()));
                                    if (name.equals(document.getId()))
                                        {
                                            Log.d("collision", "onComplete: collision of names in firestore");
                                            Toast.makeText(context,
                                            "Name already taken. Please use another name.",
                                            Toast.LENGTH_SHORT).show();

                                            String number = Integer.toString(randomNumberInRange(1,100000));
                                            Toast.makeText(context,
                                                    "Hi there, you can try " +name+number+" , its probably not taken ;)",
                                                    Toast.LENGTH_SHORT).show();
                                        username.setText("");}
                                }

                                if(!username.getText().toString().isEmpty()) {
                                    Toast.makeText(context,
                                            "New player id created!",
                                            Toast.LENGTH_SHORT).show();
                                    SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                    editor.putString("global_id", name);
                                    editor.apply();
                                    Map<String, Object> vanilla_player = new HashMap<>();
                                    vanilla_player.put("name", name);
                                    vanilla_player.put("highscore", 0);
                                    vanilla_player.put("stage", 1);
                                    db.document(name).set(vanilla_player);
                                    TextView user = findViewById(R.id.user_check);
                                    user.setText(name);
                                    dialog.dismiss();
                                }
                                else{}
                            } else {
                                Log.d("list_of_names", "Error getting documents: ", task.getException());
                            }
                        }
                    });
                }
                else {
                    Toast.makeText(context,
                            "Please fill in a name. Thank you.",
                            Toast.LENGTH_SHORT).show();}

            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static int randomNumberInRange(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    public static RotateAnimation gen_rotate_animation(float from, float to, long duration, long offset, int repeat_count, float accel_factor,boolean accel, int repeat_mode){
        RotateAnimation rotate = new RotateAnimation(
                from, to, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(duration);
        if(accel) {
            rotate.setInterpolator(new AccelerateInterpolator(accel_factor));
        }
        rotate.setRepeatMode(repeat_mode);
        rotate.setStartOffset(offset);
        rotate.setFillAfter(true);
        rotate.setRepeatCount(repeat_count);
        return rotate;
    }

    public static void FadeInButtons(long duration, View ... views){

        for (View v : views)
        {
            v.setAlpha(0); // make invisible to start
        }

        for (View v : views)
        {
            // 3 second fade in time
            v.animate().alpha(1.0f).setDuration(duration).start();
        }
    }

}
