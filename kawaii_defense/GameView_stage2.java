package nexidea.kawaii_defense;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;

import java.util.HashMap;
import java.util.Map;

import nexidea.kawaii_defense.Projectiles.Projectile;
import nexidea.kawaii_defense.Projectiles.Projectile_bomb;
import nexidea.kawaii_defense.Projectiles.Projectile_heart;
import nexidea.kawaii_defense.Projectiles.Projectile_laser;
import nexidea.kawaii_defense.Projectiles.Projectile_press_effect;
import nexidea.kawaii_defense.Projectiles.Projectile_slice_animation;
import nexidea.kawaii_defense.Projectiles.Projectile_stage2_boss;
import nexidea.kawaii_defense.Projectiles.Projectile_tap_effect;

import static android.content.ContentValues.TAG;
import static nexidea.kawaii_defense.Game.randomNumberInRange;
import static nexidea.kawaii_defense.MainActivity_stage.game_paused;


public class GameView_stage2 extends SurfaceView implements SurfaceHolder.Callback {
    public static GameRunner runner;
    private static Game game;
    private int _xDelta;
    private int _yDelta;
    private double tolerance = 0.2;
    private SparseArray<PointF> mActivePointers;
    private static HashMap<Integer,Bitmap> bg_picture_hash = new HashMap<>();
    private int holding_id;
    private int wave;

    public GameView_stage2(Context context, AttributeSet attrs) {
        super(context, attrs);
        mActivePointers = new SparseArray<>();
        //Log.d("dimensions", "onCreate: width/height " + getWidth() + ',' + getHeight());
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        this.wave = MainActivity_stage.wave;
    }



    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();

        // get pointer index from the event object
        int pointerIndex = event.getActionIndex();

        // get pointer ID
        int pointerId = event.getPointerId(pointerIndex);

        int maskedAction = event.getActionMasked();

        if (game.loaded) {
            switch (maskedAction) {
                case MotionEvent.ACTION_DOWN:
//                    _xDelta = X;
//                    _yDelta = Y;
                    PointF f = new PointF();
                    f.x = event.getX(pointerIndex);
                    f.y = event.getY(pointerIndex);
                    mActivePointers.put(pointerId, f);

                    if (f.x > game.ball.getScreenRect().left && f.x < game.ball.getScreenRect().right
                            && f.y < game.ball.getScreenRect().bottom && f.y > game.ball.getScreenRect().top
                            ) {
                        holding_id = pointerId;
                        _xDelta = new Integer((int) f.x);
                        _yDelta = new Integer((int) f.y);
                    } else {
                        load_tap_effect_animation(f);
                    }

                    if (check_projectile(f)) {
                        return true;
                    }

                    if (check_heart_and_bomb(f)) {
                        return true;
                    }


                    if (check_laser(f)) {
                        return true;
                    }

                    if (check_boss(f)) {
                        load_press_effect_animation(f);
                        return true;
                    }
                case MotionEvent.ACTION_POINTER_DOWN:
                    if (event.getPointerCount() < 3) {
                        PointF g = new PointF();
                        g.x = event.getX(pointerIndex);
                        g.y = event.getY(pointerIndex);
                        mActivePointers.put(pointerId, g);

                        if (g.x > game.ball.getScreenRect().left && g.x < game.ball.getScreenRect().right
                                && g.y < game.ball.getScreenRect().bottom && g.y > game.ball.getScreenRect().top
                                ) {
                            holding_id = pointerId;
                            _xDelta = new Integer((int) g.x);
                            _yDelta = new Integer((int) g.y);
                        } else {
                            load_tap_effect_animation(g);
                        }

                        if (check_projectile(g)) {
                            return true;
                        }

                        if (check_heart_and_bomb(g)) {
                            return true;
                        }


                        if (check_laser(g)) {
                            return true;
                        }

                        if (check_boss(g)) {
                            load_press_effect_animation(g);
                            return true;
                        }


                    }


                case MotionEvent.ACTION_MOVE:
                    if (event.getPointerCount() < 3) {
                        int size = event.getPointerCount();
                        Log.d(TAG, "onTouchEvent: number of touches =" + size);
                        for (int i = 0; i < size; i++) {
                            Log.d(TAG, "onTouchEvent: moving pointer =" + size);
                            pointerIndex = i;
                            PointF point = new PointF();
                            point.x = event.getX(pointerIndex);
                            point.y = event.getY(pointerIndex);
                            if (event.getPointerId(i) == holding_id) {
                                if (point.x > game.ball.getScreenRect().left && point.x < game.ball.getScreenRect().right
                                        && point.y < game.ball.getScreenRect().bottom + game.ball.getScreenRect().height() / 2 && point.y > game.ball.getScreenRect().top) {
                                    //game.draw_char_ripple(X, Y);
                                    game.ball.setX(Math.max(Math.min(Math.max(
                                            Math.min(game.ball.getX() + (point.x - _xDelta), point.x - game.ball.getScreenRect().width() / 2)
                                            , point.x - game.ball.getScreenRect().width() / 2), getWidth() - game.ball.getScreenRect().width()), 0));//- game.getBall().getScreenRect().width()/2);
                                    game.ball.setY(Math.max(Math.min(Math.max(
                                            Math.min(game.ball.getY() + (point.y - _yDelta), point.y - game.ball.getScreenRect().height() * 5 / 6)
                                            , point.y - game.ball.getScreenRect().height() * 5 / 6), getHeight() - game.ball.getScreenRect().height()), 0));// );
                                    //Log.d("on touch", "onTouchEvent: detect point" + (game.ball.getX() + game.ball.getScreenRect().width()) + ", X position = " + X);
                                    load_slice_animation(point, true);
                                    continue;
                                } else {
                                    if (!game.HeatMeter.overheat) {
                                        load_slice_animation(point, false);
                                        game.HeatMeter.pressed();
                                    } else {
                                    }
                                }
                            } else {
                                if (!game.HeatMeter.overheat) {
                                    load_slice_animation(point, false);
                                    game.HeatMeter.pressed();
                                } else {
                                    continue;
                                }
                            }
                            if (!game.HeatMeter.overheat) {
                                if (check_projectile(point)) {
                                    continue;
                                }

                                if (check_heart_and_bomb(point)) {
                                    continue;
                                }
                                if (check_boss(point)) {
                                    load_press_effect_animation(point);
                                    return true;
                                }
                            } else {
                                continue;
                            }

                        }
                        return true;
                    }

                case MotionEvent.ACTION_UP:
                    game.HeatMeter.lifted = true;
                    break;

//                    mActivePointers.remove(pointerId);
//                    break;

                case MotionEvent.ACTION_POINTER_UP:
                    game.HeatMeter.lifted = true;
                    break;
//                    mActivePointers.remove(pointerId);
//                    break;

                case MotionEvent.ACTION_CANCEL: {
//                    mActivePointers.remove(pointerId);
//                    break;
                }

            }
        }
        return true;
    }

    @Override
// To make sure that application can handle eventualities such as going to the homescreen
// when the application is running
    public void surfaceCreated(SurfaceHolder holder) {

        if (!game_paused) {
//             load new game
            game = new Game(getContext(), getWidth(), getHeight(), getHolder(), getResources(),
                    set_bg_hash(R.drawable.stage2_bg_black,R.drawable.stage2_bg_red_black), 0, 0);
            game.set_stage(2, wave);
            Log.d("tag", "surfaceCreated: runner started");
            game.getGame_activity().init();
            runner = new GameRunner(game);
            runner.init_game();
            runner.start();
        } else {
            runner = new GameRunner(game);
            runner.start();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d("JWP","changed");

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
//        MainActivity_beach.mp.stop();
        //mp.stop();
        if (runner != null){
            runner.shutdown();
            while(runner != null){
                try {
                    runner.join();
                    runner=null;
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public static Game getGame() {
        return game;
    }

    public HashMap<String, Bitmap> set_bg_hash(int normal, int hit){
        Bitmap background_n = BitmapFactory.decodeResource(getResources(), normal);
        Bitmap background_h = BitmapFactory.decodeResource(getResources(), hit);
        Bitmap background_normal = Bitmap.createScaledBitmap(background_n,
                getWidth(),
                getHeight(),true);
        Bitmap background_hit = Bitmap.createScaledBitmap(background_h,
                getWidth(),
                getHeight(),true);
        HashMap<String,Bitmap> bg_hash = new HashMap<>();
        bg_hash.put("bg_norm", background_normal);
        bg_hash.put("bg_hit", background_hit);
        return bg_hash;
    }

    private void load_slice_animation(PointF point, boolean char_move) {
        //slice animation
        Projectile_slice_animation new_slice = new Projectile_slice_animation(getWidth(), getHeight(),
                (int) point.x + randomNumberInRange(-50, 50), (int) point.y + randomNumberInRange(-50, 50), 0, 0);
        game.slice_effect_counter++;
        game.getGame_activity().getObj_counter().put("slice_animation", game.slice_effect_counter + 1);
        game.slice_animation_in_game.put(game.getGame_activity().getObj_counter().get("slice_animation"), new_slice);
        game.slice_animation_in_game.get(game.getGame_activity().getObj_counter().get("slice_animation")).load_projectile(game,char_move,false);
    }

    private boolean load_press_effect_animation(PointF g) {
        Projectile_press_effect new_press2 = new Projectile_press_effect(getWidth(), getHeight(),
                (int) g.x, (int) g.y, 0, 0, game);
        game.press_effect_counter++;
        game.press_effect_in_game.put(game.press_effect_counter, new_press2);
        game.press_effect_in_game.get(game.press_effect_counter).load_projectile(game);
        return true;
    }

    private boolean load_tap_effect_animation(PointF g) {
        Projectile_tap_effect new_press2 = new Projectile_tap_effect(getWidth(), getHeight(),
                (int) g.x, (int) g.y, 0, 0, game,false);
        game.tap_effect_counter++;
        game.tap_effect_in_game.put(game.tap_effect_counter, new_press2);
        game.tap_effect_in_game.get(game.tap_effect_counter).load_projectile(game);
        return true;
    }

    private boolean check_heart_and_bomb(PointF g) {
        //hearts and bombs
        for (Map.Entry<Integer, Projectile_heart> entry : game.getGame_activity().getHeart_in_game().entrySet()) {
            if (g.x >= entry.getValue().getX() * (1 - tolerance) &&
                    g.x < (entry.getValue().getX() + entry.getValue().getRect().width() * (1 + tolerance))
                    && g.y >= entry.getValue().getY() * (1 - tolerance) &&
                    g.y < (entry.getValue().getY() + entry.getValue().getRect().height() * (1 + tolerance))
                    && !entry.getValue().dead) {
                entry.getValue().destroyed(game);
                return true;
            }
        }
        for (Map.Entry<Integer, Projectile_bomb> entry : game.getGame_activity().getBomb_in_game().entrySet()) {
            if (g.x >= entry.getValue().getX() * (1 - tolerance) &&
                    g.x < (entry.getValue().getX() + entry.getValue().getRect().width() * (1 + tolerance))
                    && g.y >= entry.getValue().getY() * (1 - tolerance) &&
                    g.y < (entry.getValue().getY() + entry.getValue().getRect().height() * (1 + tolerance))
                    && !entry.getValue().dead) {
                entry.getValue().destroyed(game);
                return true;
            }
        }
        return false;
    }

    private boolean check_projectile(PointF g) {
        for (int j = game.getGame_activity().getObj_types().size() - 1; j > -1; j--) {
            for (Map.Entry<Integer, Projectile> entry : game.getGame_activity().getProjectile_in_game().get(game.getGame_activity().getObj_types().get(j)).entrySet()) {
                if (g.x >= entry.getValue().getX() * (1 - tolerance) &&
                        g.x < (entry.getValue().getX() + entry.getValue().getRect().width() * (1 + tolerance))
                        && g.y >= entry.getValue().getY() * (1 - tolerance) &&
                        g.y < (entry.getValue().getY() + entry.getValue().getRect().height() * (1 + tolerance))
                        && !entry.getValue().dead) {
                    entry.getValue().get_hit(game.char_offense_mode);
                    return true;
                }
            }
        }
        return false;
    }

    private boolean check_laser(PointF g){
        for (Map.Entry<Integer, Projectile_laser> entry : game.getGame_activity().getLaser_in_game().entrySet()) {
            if (g.x >= entry.getValue().getX() * (1 - tolerance) &&
                    g.x < (entry.getValue().getX() + entry.getValue().getRect().width() * (1 + tolerance))
                    && g.y >= entry.getValue().getY() * (1 - tolerance) &&
                    g.y < (entry.getValue().getY() + entry.getValue().getRect().height() * (1 + tolerance))
                    && !entry.getValue().dead) {
                entry.getValue().destroyed(game);
                return true;
            }
        }
        return false;
    }

    private boolean check_boss(PointF g) {
        for (Map.Entry<Integer, Projectile_stage2_boss> entry : game.getGame_activity().getstage2_Boss_in_game().entrySet()) {
            if (g.x >= entry.getValue().getX() * (1 - tolerance) &&
                    g.x < (entry.getValue().getX() + entry.getValue().getRect().width() * (1 + tolerance))
                    && g.y >= entry.getValue().getY() * (1 - tolerance) &&
                    g.y < (entry.getValue().getY() + entry.getValue().getRect().height() * (1 + tolerance))
                    && !entry.getValue().dead
                    && !entry.getValue().rage) {
                entry.getValue().get_hit(game.char_offense_mode);
                return true;
            }
        }
        return false;
    }

}
