package nexidea.kawaii_defense;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

public class BackgroundRunner_PreMenu extends Thread {
    private Animated_background bg;
    private volatile boolean running = true;
    private static BackgroundRunner_PreMenu backgroundRunner;
    private boolean loaded;
    private Context context;
    private Resources resources;
    private int width;
    private int height;

    public BackgroundRunner_PreMenu(Animated_background bg, Context context, Resources resources, int width, int height) {
        this.bg = bg;
        backgroundRunner = this;
        loaded=false;
        this.context = context;
        this.resources = resources;
        this.width = width;
        this.height = height;
    }
    @Override
    public void run() {
        Log.d("tag", "run: bg is ran!");
        long lastTime = System.currentTimeMillis();
//      bg runs here
        while (running) {
//             draw stuff.
            long now = System.currentTimeMillis();
            long elapsed = now - lastTime;
            //Log.d("delta time", "delta time: " + elapsed);
            if (elapsed < 1000) {
                if(!loaded) {
                    bg.update(elapsed);
                    bg.draw_premenu();
                    MyBitmap boot = new MyBitmap(resources,context, width, height);
                    loaded = true;
                }
                else{
                    bg.update_alpha(elapsed);
                    bg.draw_premenu();
                    if (bg.getMoving_background().paint.getAlpha() == 5){
                        ((PreMenuActivity) context).openGame();
                        shutdown();
                    }
                }
            }
            lastTime = now;
        }
    }
    public void shutdown(){
        running= false;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public static BackgroundRunner_PreMenu getBackgroundRunner() {
        return backgroundRunner;
    }

    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }
}
