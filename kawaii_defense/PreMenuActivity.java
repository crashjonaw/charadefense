package nexidea.kawaii_defense;

import android.content.Intent;

import android.content.SharedPreferences;
import android.media.MediaPlayer;

import android.os.Build;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import java.text.DecimalFormat;


public class PreMenuActivity extends AppCompatActivity{
    private static MediaPlayer mp;
    private static TextView progressNumber;
    private static TextView loadedComponent;
    private static TextView TotalTimeTakenTextView;
    public static final String MY_PREFS_NAME = "premenu_db";
    public static SharedPreferences local_db;
    public static SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        setContentView(R.layout.pre_activity_menu);
        progressNumber = findViewById(R.id.ProgressNumber);
        loadedComponent = findViewById(R.id.LoadedComponent);
        TotalTimeTakenTextView = findViewById(R.id.TotalTimeTaken);

        local_db = this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

        editor = this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();

//        mp = MediaPlayer.create(getApplicationContext(), R.raw.menu_song_final);
//        mp.setLooping(true);
//        mp.start();
//        openGame();
    }

    public void openGame() {
//        mp.stop();
        Intent intent = new Intent(this, MenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void setProgressNumber(final double i){
        final DecimalFormat df2 = new DecimalFormat(".##");
        if(progressNumber!=null) {
            runOnUiThread(new Runnable() {
                public void run() {
                    progressNumber.setText(df2.format(i*100) + " %");
                }
            });
        }
    }

    public void setLoadedComponent(final String comment){
        if(loadedComponent!=null) {
            runOnUiThread(new Runnable() {
                public void run() {
                    loadedComponent.setText(comment);
                }
            });
        }
    }

    public void setTotalTimeTaken(final long i){
        final DecimalFormat df2 = new DecimalFormat(".##");
        if(TotalTimeTakenTextView!=null) {
            runOnUiThread(new Runnable() {
                public void run() {
                    TotalTimeTakenTextView.setText("Loading Time: " + df2.format(i / 1000.0) + " seconds");
                }
            });
        }
    }



    @Override
    public void onBackPressed() {
//        mp.stop();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    public static MediaPlayer getMp() {
        return mp;
    }


}
