package nexidea.kawaii_defense;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import nexidea.kawaii_defense.BackgroundAesthetics.BackgroundRunner;

public class MainActivity_stage_selection extends AppCompatActivity implements View.OnClickListener {
    //song stuff
    private static ArrayList<Integer> songArray;
    private static MediaPlayer mp;
    public static Timer timer;
    private int i = 0;
    float volume = 0;
    private boolean paused;
    private TimerTask play_songs;
    private boolean first_song = true;

    //button stuff
    private ImageButton back_button;
    private ImageButton stage1;
    private ImageButton stage2;
    private ImageButton stage3;

    //intent
    private Intent go_to_stage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        this.mp = MenuActivity.getMp();
        this.setContentView(R.layout.activity_stage_selection);
//        songArray = getArrSong("stage_selection");
        timer = new Timer(true);
        back_button = findViewById(R.id.back_to_menu);
        back_button.setOnClickListener(this);
        stage1 = findViewById(R.id.stage1);
        stage1.setOnClickListener(this);
        stage2 = findViewById(R.id.stage2);
        stage2.setOnClickListener(this);
        stage3 = findViewById(R.id.stage3);
        stage3.setOnClickListener(this);
        MenuActivity.FadeInButtons(5000,stage1,stage2,stage3);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.back_to_menu:
                return_to_menu();
                break;

            case R.id.stage1:
                openGame(1);
                break;


            case R.id.stage2:
                openGame(2);
                break;


            case R.id.stage3:
                openGame(3);
                break;

            default:
                break;
        }
    }

    public void openGame(int stage) {
//        BackgroundRunner.getBackgroundRunner().shutdown();
        go_to_stage = new Intent(this,MainActivity_wave_selection.class);
        go_to_stage.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        go_to_stage.putExtra("stage", stage);
        startActivity(go_to_stage);
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        return_to_menu();
    }

    public void return_to_menu(){
        Intent intent = new Intent(this, MenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

//    public static ArrayList<Integer> getArrSong(String venue) {
//        Field[] ID_Fields = R.raw.class.getFields();
//        songArray = new ArrayList<Integer>();
//        for (int i = 0; i < ID_Fields.length; i++) {
//            if (ID_Fields[i].getName()!= null &&
//                    ID_Fields[i].getName().contains(venue)){
//                try {
//                    //Log.d("musicidfieldname",ID_Fields[i].getName());
//                    songArray.add(ID_Fields[i].getInt(null));
//                }
//                catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                }}
//        }
//        Log.d("array print","array" + songArray.toString());
//        return songArray;
//    }

    private void startFadeIn(){
        final int FADE_DURATION = 3000; //The duration of the fade
        //The amount of time between volume changes. The smaller this is, the smoother the fade
        final int FADE_INTERVAL = 250;
        final int MAX_VOLUME = 1; //The volume will increase from 0 to 1
        int numberOfSteps = FADE_DURATION/FADE_INTERVAL; //Calculate the number of fade steps
        //Calculate by how much the volume changes each step
        final float deltaVolume = MAX_VOLUME / (float)numberOfSteps;

        //Create a new Timer and Timer task to run the fading outside the main UI thread
        final Timer timer = new Timer(true);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                fadeInStep(deltaVolume); //Do a fade step
                //Cancel and Purge the Timer if the desired volume has been reached
                if(volume>=1f){
                    timer.cancel();
                    timer.purge();
                }
            }
        };

        timer.schedule(timerTask,FADE_INTERVAL,FADE_INTERVAL);
    }

    private void startFadeOut(){
        final int FADE_DURATION = 3000; //The duration of the fade
        //The amount of time between volume changes. The smaller this is, the smoother the fade
        final int FADE_INTERVAL = 250;
        final int MAX_VOLUME = 1; //The volume will increase from 0 to 1
        int numberOfSteps = FADE_DURATION/FADE_INTERVAL; //Calculate the number of fade steps
        //Calculate by how much the volume changes each step
        final float deltaVolume = MAX_VOLUME / (float)numberOfSteps;

        //Create a new Timer and Timer task to run the fading outside the main UI thread
        final Timer timer = new Timer(true);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                fadeOutStep(deltaVolume); //Do a fade step
                //Cancel and Purge the Timer if the desired volume has been reached
                if(volume<=0.5f){
                    timer.cancel();
                    timer.purge();
                }
            }
        };

        timer.schedule(timerTask,FADE_INTERVAL,FADE_INTERVAL);
    }

    private void fadeInStep(float deltaVolume){
        mp.setVolume(volume, volume);
        volume += deltaVolume;

    }

    private void fadeOutStep(float deltaVolume){
        mp.setVolume(volume, volume);
        volume -= deltaVolume;
    }

    @Override
    public void onPause()
    {
        super.onPause();
        Log.d("timer", "onPause: timer canceled and purged");
        paused = true;
        if(mp!=null) {
            mp.stop();
            mp.reset();
        }
        BackgroundRunner.getBackgroundRunner().shutdown();
        // Do your stuff here when you are stopping your activity
    }
    @Override
    public void onResume()
    {
        super.onResume();
        Game.free_hold = false;
        i=0;
        paused = false;
//        play_songs = new TimerTask() {
//            @Override
//            public void run() {
//                if (!paused) {
//                    if (!first_song) {
//                         startFadeOut();
//                         mp.stop();
//                         mp.reset();
//                    }
//                    if (i == songArray.size()) {
//                        i = 0;
//                    } else {
//                    }
//                    mp = MediaPlayer.create(getApplicationContext(), songArray.get(i));
//                    mp.start();
//                    startFadeIn();
//                    first_song = false;
//                    i++;
//                }
//            }
//        };
//        timer.scheduleAtFixedRate(play_songs, 3000, 309000);
    }
    public static MediaPlayer getMp() {
        return mp;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }
}
