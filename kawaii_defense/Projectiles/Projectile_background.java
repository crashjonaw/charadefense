package nexidea.kawaii_defense.Projectiles;

import android.graphics.Bitmap;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;

import java.util.HashMap;

import nexidea.kawaii_defense.Game;

public class Projectile_background extends Sprite {
    //variables
    private int directionX = 1;
    private int directionY = -1;
    public Paint paint;
    private int paint_alpha;
    private ColorFilter color;
    private long hit_time;
    private final long default_hit_time = 2000;
    public boolean hit;
    private HashMap<String,Bitmap> game_bg = new HashMap<>();
    private Game game;

    public Projectile_background(int screenWidth, int screenHeight, int x, int y, float speedX, float speedY, Game game) {
        super(screenWidth, screenHeight, x ,y, speedX, speedY);
        paint = new Paint();
        paint_alpha =255;
        paint.setAlpha(paint_alpha);
        hit_time =default_hit_time;
        hit = false;
//        paint.setARGB(255,225,255,255);
    }

    public void load_background (Bitmap background, float speedX, float speedY){
        init(background,false);
        setSpeedX(speedX);
        setSpeedY(speedY);
    }

    public void load_game_background (HashMap<String,Bitmap> background, float speedX, float speedY){
        game_bg = background;
        init(game_bg.get("bg_norm"),false);
        setSpeedX(speedX);
        setSpeedY(speedY);
    }

    public void update_game_bg(long elapsed){
        if(!hit){}
        else{
            init(game_bg.get("bg_hit"),false);
            hit_time -= elapsed;
            if(hit_time <0){
                init(game_bg.get("bg_norm"),false);
                hit = false;
                hit_time = default_hit_time;
            }
        }
    }

    public void hit_bg(){
        hit = true;
    }

    public void update(long elapsed){
        float x = getX();
        float y = getY();
        int angle = getAngle();

        Rect screenRect = getScreenRect();
        if(screenRect.left >=0){
            directionX = -1;
        }
        else if(screenRect.right <= getScreenWidth()){
            directionX= 1;
        }

        if (screenRect.top < 0){
            directionY =1;
        }
        else if (screenRect.bottom >= getScreenHeight()){
            directionY = -1;
        }

        x += directionX *(getSpeedX()*elapsed);
        y += directionY *(getSpeedY()*elapsed);

        setX(x);
        setY(y);
    }

    public void setPaint_alpha(int paint_alpha) {
        this.paint_alpha = Math.max(paint_alpha,0);
        paint.setAlpha(paint_alpha);
    }
}

