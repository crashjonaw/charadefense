package nexidea.kawaii_defense.Projectiles;

import android.graphics.Bitmap;
import android.graphics.Paint;

import java.util.Random;

import nexidea.kawaii_defense.Game;
import nexidea.kawaii_defense.MyBitmap;
import nexidea.kawaii_defense.Wave;


public class Projectile_ink extends Sprite {
    //variables
    private Game game;

    //music

    //display
    public Bitmap image;
    public Bitmap dead_image;
    public long alive_time;
    public int projectile_difficulty;
    public Paint paint;

    //properties
    public int damage;
    public int hits;
    public String type;

    public boolean dead;
    public boolean out;

    public Projectile_ink(int screenWidth, int screenHeight,
                          float x, float y, float speedX, float speedY, Game game,
                          Wave wave) {
        super(screenWidth, screenHeight, x, y, speedX, speedY);
        projectile_difficulty = wave.generate_projectile_size_based_on_probability_array();
        image = MyBitmap.ink_bitmap_hash.get(randomNumberInRange(1,3));
        dead_image = MyBitmap.stage1_bitmap_hash.get("shuriken").get("dead_image");
        dead = true;
        out = true;
        setAngle(randomNumberInRange(1,360));
        paint = new Paint();
        paint.setAlpha(255);
        setX(game.ball.getX()+game.ball.getRect().width()/2 - image.getWidth()/2);
        setY(game.ball.getY()+game.ball.getRect().height()/2 - image.getHeight()/2);
    }

    public void load_projectile() {
        dead = false;
        out = false;
        alive_time = 5000;
        init(image, dead);
    }

    public void update(long elapsed) {
        alive_time -= elapsed;
        if (alive_time <0) {
            destroyed();
        }
    }

    public void destroyed() {
        dead = true;
        out=true;
    }

    public int randomNumberInRange(int min, int max) {
        Random random = new Random();
        int my_rand;
        do {
            my_rand = random.nextInt((max - min) + 1) + min;
        }while(my_rand ==0);

        return my_rand;
    }
}

