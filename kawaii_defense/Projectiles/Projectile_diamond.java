package nexidea.kawaii_defense.Projectiles;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.Log;

import java.util.Random;

import nexidea.kawaii_defense.Game;
import nexidea.kawaii_defense.MainActivity_stage;
import nexidea.kawaii_defense.MyBitmap;
import nexidea.kawaii_defense.Wave;

import static android.content.ContentValues.TAG;

public class Projectile_diamond extends Sprite {
    //variables
    private Wave wave;

    //display
    public Bitmap fire_image;
    public Bitmap collision_image;
    public Bitmap image;
    public Bitmap dead_image;
    public Bitmap score_image;
    public long score_time;
    public long wait_enter_screen_time;

    //properties
    public int damage;
    public int hits;
    private int default_hits;
    public int p_score;
    public int fire_bounces;
    public String type;
    private int projectile_difficulty;

    //motion
    private final int spin = randomNumberInRange(-10,10);
    private int directionX = randomNumberInRange(-1,1);
    private int directionY = randomNumberInRange(-1,1);

    //check projectile
    public boolean fire;
    public boolean dead;
    public boolean out;

    private long spawn_timer;
    public boolean spawned;


    //game
    private Game game;

    public Projectile_diamond(int screenWidth, int screenHeight, float x, float y, float speedX, float speedY, Game game, Wave wave) {
        super(screenWidth, screenHeight, x ,y, speedX, speedY);

        projectile_difficulty = wave.generate_projectile_size_based_on_probability_array();
        fire_image = MyBitmap.stage3_bitmap_hash.get("fireball").get("image_"+projectile_difficulty);
        dead_image = MyBitmap.stage3_bitmap_hash.get("fireball").get("dead_image");
        collision_image = MyBitmap.stage1_bitmap_hash.get("coconut").get("collision_image");


        damage = game.getGame_activity().getMultiProjectile().get("fireball").get("damage");
        default_hits = game.getGame_activity().getMultiProjectile().get("fireball").get("hits")*projectile_difficulty;
        hits = default_hits;

        score_time = 2000;
        wait_enter_screen_time = 1000;
        dead = true;
        out = true;
        type = "";
        this.game = game;
        this.wave = wave;
        setSpeedX(speedX);
        setSpeedY(speedY);

        spawn_timer = (long) randomNumberInRange(0,1000);
        spawned = false;

    }

    public void load_projectile (){
        if (randomNumberInRange(1,2) == 1 ){
            int fb_int = wave.diamond_types_for_wave.get(randomNumberInRangeIncludeZero(0,wave.diamond_types_for_wave.size()-1));
            type = wave.diamond_types_v2.get(fb_int);
            Log.d(TAG, "update: " + fb_int + type + "size" + wave.diamond_types_for_wave.size() );
            image = MyBitmap.stage3_diamond_bitmap_hash.get(
                    fb_int);
            fire = false;
            fire_bounces = 0;
            init(image, dead);
        }
        else {
            type = "";
            fire = true;
            fire_bounces = randomNumberInRange(2,5);
            init(fire_image, dead);
        }
        dead=false;
        out=false;

    }

    public void update(long elapsed){
        float x = getX();
        float y = getY();
        int angle = getAngle();

        if(!spawned){
            spawn_timer -= elapsed;
            if(spawn_timer <= 0){
                spawned = true;
            }
            return;
        }
        else{
        }


        if(!dead) {
            Rect screenRect = getScreenRect();
            if (screenRect.left <= 0) {
                directionX = 1;
                if (wait_enter_screen_time < 0) {
                    fire_bounces--;
                }
            } else if (screenRect.right >= getScreenWidth()) {
                directionX = -1;
                if (wait_enter_screen_time < 0) {
                    fire_bounces--;
                }
            }

            if (screenRect.top < 0) {
                directionY = 1;
                if (wait_enter_screen_time < 0) {
                    fire_bounces--;
                }

            } else if (screenRect.bottom >= getScreenHeight()) {
                directionY = -1;
                if (wait_enter_screen_time < 0) {
                    fire_bounces--;
                }
            }

            if (fire_bounces ==0) {
               if(fire){
                   int fb_int = wave.diamond_types_for_wave.get(randomNumberInRangeIncludeZero(0,wave.diamond_types_for_wave.size()-1));
                   type = wave.diamond_types_v2.get(fb_int);
                   Log.d(TAG, "update: " + fb_int + type + "size" + wave.diamond_types_for_wave.size() );
                   image = MyBitmap.stage3_diamond_bitmap_hash.get(
                           fb_int);
                   init_diamond(image);
                   wait_enter_screen_time = 500;
                   fire_bounces = randomNumberInRange(3, 5);
                   fire = false;
               }
               else{
                   type = "";
                   init_diamond(fire_image);
                   wait_enter_screen_time = 500;
                   fire_bounces = randomNumberInRange(3, 5);
                   fire = true;
               }
            }
            angle += spin;
            wait_enter_screen_time -= elapsed;

        }
        else if(dead && !out) {
            Rect screenRect = getScreenRect();
            score_time -= elapsed;
            if (score_time < 0) {
                init(dead_image, dead);
            }
            if (screenRect.top < 0) {
                directionY = 1;
                if (wait_enter_screen_time < 0) {
                    fire_bounces--;
                }
                out = true;
                lock = false;
                game.getGame_activity().setCount_obj_in_screen(game.getGame_activity().getCount_obj_in_screen() - 1);
                return;
                }

        }
        x += directionX * (getSpeedX() * elapsed);
        y += directionY * (getSpeedY() * elapsed);
        setX(x);
        setY(y);
        setSpeedX(getSpeedX());
        setSpeedY(getSpeedY());
        setAngle(angle);
    }

    public void destroyed_by_collision(Game game){
        if(!fire) {
           destroyed(game);
        }
        else{
            init_diamond(collision_image);
            dead = true;
            init(collision_image,dead);
            game.setLife(game.life - damage);
            damage = 0;
            hits = 0;
            directionX = 0;
            directionY = -1;
            addSpeedY(0.5f);
        }
    }

    public void destroyed(Game game) {
        p_score = game.getGame_activity().getMultiProjectile().get("fireball").get("p_score")*game.multiplier;
        score_image = MyBitmap.general_bitmap_hash.get("score").get(p_score);
        game.setScore(game.score + p_score);
        game.getGame_activity().getCurrent_wave().wave_completion.put("score",
                game.getGame_activity().getCurrent_wave().wave_completion.get("score")-p_score);
        init(score_image,dead);
        dead = true;
        damage = 0;
        hits = 0;
        directionX = 0;
        directionY = -1;
        fire = false;
        fire_bounces=0;
        if(type!= "") {
            Log.d(TAG, "destroyed: " + type );
            game.getGame_activity().getCurrent_wave().wave_completion.put(type,
                    game.getGame_activity().getCurrent_wave().wave_completion.get(type) - 1);
        }
        ((MainActivity_stage) game.getContext()).updatereq(game.getGame_activity().getCurrent_wave().wave_completion,
                game.current_stage);
        addSpeedY(1);
        setAngle(0);
        game.addCombo(1);
        game.kill_qty ++;
    }


    public int randomNumberInRange(int min, int max) {
        Random random = new Random();
        int my_rand;
        do {
            my_rand = random.nextInt((max - min) + 1) + min;
        }while(my_rand ==0);

        return my_rand;
    }
    public int randomNumberInRangeIncludeZero(int min, int max) {
        Random random = new Random();
        int my_rand;
        my_rand = random.nextInt((max - min) + 1) + min;

        return my_rand;
    }

}


//                Canvas canvas = game.getHolder().lockCanvas();
//                Paint paint2 = new Paint();
//                paint2.setColor(Color.RED);
//                paint2.setStyle(Paint.Style.FILL);
//                paint2.setAlpha(150);
//                canvas.drawPaint(paint2);
//                game.getHolder().unlockCanvasAndPost(canvas);  }


//            final Timer timer = new Timer(true);
//            TimerTask task = new TimerTask() {
//                int run_count = 1;
//                @Override
//                public void run() {
                    //FLASH COLOUR
//                    if (++run_count ==2){
//                                        timer.cancel();
//                                        timer.purge();}
//                }
//            };
//            timer.schedule(task, 0, 1);
//        }

//        else{}

