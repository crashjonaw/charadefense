package nexidea.kawaii_defense.Projectiles;

import android.graphics.Bitmap;

import java.util.HashMap;
import java.util.Random;

import nexidea.kawaii_defense.Game;

public class Ball extends Sprite_char {

    private int s_width;
    private int s_height;
    private int directionX = 1;
    private int directionY = 1;
    private float accelerationX = 0f;
    private float accelerationY = 0f;
    public int shield_count;
    private final int spin = 10;
    public boolean vulnerable;
    private Game game;
    public HashMap<Integer,HashMap<String,Double>> char_offense_xy_hash;
    private long char_expression_time;
    private boolean current_expression;
    private boolean default_expression;
    private boolean default_expression_2;
    private long default_expression_time;
    private HashMap<Boolean,String> char_express_map = new HashMap<>();
    private boolean hit;



    public Ball(int screenWidth, int screenHeight,int x, int y,float speedX, float speedY, Game game){
        super(screenWidth, screenHeight,x,y,speedX,speedY);

        this.game = game;
        char_offense_xy_hash = new HashMap<>();
        s_width = screenWidth;
        s_height = screenHeight;
        default_expression = true;
        default_expression_2 = false;
        char_express_map.put(true,"shy_");
        char_express_map.put(false,"happy_");

        current_expression = default_expression;
        default_expression_time = 3000;
        init(game.char_expression_map.get("shy_"+randomNumberInRange(1,2)));
    }

    public void change_expression(Bitmap expression){
        init(expression);
        char_expression_time = 2000;
        hit = true;
    }
    public void change_default_expression(boolean expression){
        init(game.char_expression_map.get(char_express_map.get(expression)+ randomNumberInRange(1,2)));
        default_expression_time = 3000;
        current_expression = expression;
        hit = false;
    }

    public void init_shield (Game game, int shield_count){
        if(shield_count > 0){
            this.shield_count = Math.min(shield_count,3);
            vulnerable = false;
        }
        else{
            vulnerable = true;
        }
        setShield_image(game.shield_bitmap_inventory.get(shield_count));
    }

    public void setShield_count(Game game, int shield_count) {
       this.shield_count = Math.max(Math.min(3,shield_count),0);
        if (this.shield_count == 0){
            vulnerable = true;
        }
        else{
            vulnerable =false;
        }
        setShield_image(game.shield_bitmap_inventory.get(this.shield_count));
    }

    public void update(long elapsed){
//        setShield_angle(getShield_angle() + spin);

        if(!hit) {
            default_expression_time -= elapsed;
            if (default_expression_time < 0) {
                if(current_expression) {
                    change_default_expression(default_expression_2);
                }
                else{
                    change_default_expression(default_expression);
                    }

            }
        }
        else{
            char_expression_time -= elapsed;
            if(char_expression_time<0){
                change_default_expression(default_expression);
            }
        }

    }

    public void kill(){
        setX(9999);
        setY(9999);
        setSpeedX(0);
        setSpeedY(0);
    }

    public void setDirectionX(int directionX) {
        this.directionX = directionX;
    }

    public void setDirectionY(int directionY) {
        this.directionY = directionY;
    }

    public int randomNumberInRange(int min, int max) {
        Random random = new Random();
        int my_rand;
        do {
            my_rand = random.nextInt((max - min) + 1) + min;
        }while(my_rand ==0);

        return my_rand;
    }
}
