package nexidea.kawaii_defense.Projectiles;

import android.graphics.Bitmap;
import android.graphics.Rect;

import java.util.Random;

import nexidea.kawaii_defense.Game;
import nexidea.kawaii_defense.MyBitmap;

public class Projectile_heart extends Sprite {
    //variables
    private Game game;

    //music

    //display
    public Bitmap image;
    public Bitmap dead_image;
    public Bitmap score_image;
    public Bitmap collision_image;
    public long score_time;

    //properties
    public int damage;
    public int hits;
    public int p_score;
    public String type;
    public int wall_bounce_lives;

    //motion
    private int spin;
    private int directionX;
    private int directionY;
    public float gravityX = 0f;
    public float gravityY = 0f;

//    check projectile
//    public boolean in_game = false;
    public boolean dead;
    public boolean out;
    public Projectile_heart(int screenWidth, int screenHeight, float x, float y, float speedX, float speedY) {
        super(screenWidth, screenHeight, x ,y, speedX, speedY);
    }

    public void load_projectile (String type, Game game){

        spin = randomNumberInRange(-10,10);
        directionX = randomNumberInRange(-1,1);
        directionY = randomNumberInRange(-1,1);


        image =  MyBitmap.stage1_bitmap_hash.get(type).get("image");
        dead_image =  MyBitmap.stage1_bitmap_hash.get(type).get("dead_image");
        collision_image =  MyBitmap.stage1_bitmap_hash.get(type).get("collision_image");
        damage = game.getGame_activity().getMultiProjectile().get(type).get("damage");
        hits = game.getGame_activity().getMultiProjectile().get(type).get("hits");
        p_score = game.getGame_activity().getMultiProjectile().get(type).get("p_score");
        wall_bounce_lives = game.getGame_activity().getMultiProjectile().get(type).get("wall_bounce_lives");
        score_image = MyBitmap.general_bitmap_hash.get("score").get(p_score);
        score_time = 1800;
        dead = false;
        out = false;

        this.game = game;
        init(image,dead);
    }

    public void update(long elapsed){
        float x = getX();
        float y = getY();
        int angle = getAngle();

        Rect screenRect = getScreenRect();
        if(screenRect.left <=0){
            directionX = 1;
        }
        else if(screenRect.right >= getScreenWidth()){
            directionX=-1;
        }

        if (screenRect.top < 0) {
            if (!dead) {
                directionY = 1;
                 }
            else if (dead && !out) {
                        out = true;
                        lock = false;
                        return;
                    }
            }
        else if (screenRect.bottom >= getScreenHeight()){
                directionY = -1;
            }

        x += directionX *(getSpeedX()*elapsed);
        y += directionY *(getSpeedY()*elapsed);
        angle += spin;

        setX(x);
        setY(y);
        setSpeedX(getSpeedX());
        setSpeedY(getSpeedY());

        if(!dead) {
            setAngle(angle);
        }

        else if(dead && !out) {
            score_time -= elapsed;
            if (score_time < 0) {
                init(dead_image, dead);
                setAngle(angle);
            }
        }

    }

    public void destroyed(Game game){
            game.setLife(game.life - damage);
            init(dead_image,dead);
            dead = true;
            damage = 0;
            hits = 0;
            directionX = 0;
            directionY = -1;
            addSpeedY(1);
            setAngle(0); }

//        if (game.mp.isPlaying()){
//            game.mp.stop();
//            game.mp.start();
//        }
//        else {
//            game.mp.start();
//        }

    public void destroyed_by_collision(Game game){
        game.setLife(game.life - damage);
        dead = true;
        init(collision_image,dead);
        damage = 0;
        hits = 0;
        directionX = 0;
        directionY = -1;
        addSpeedY(0.5f);
    }

    public void set_dead(){
        dead = true;
        init(dead_image, dead);
        damage = 0;
        hits = 0;
        directionX = 0;
        directionY = -1;
        addSpeedY(1);
    }

    public void setGravityX(float gravityX) {
        this.gravityX = gravityX;
    }

    public void setGravityY(float gravityY) {
        this.gravityY = gravityY;
    }

    public float getGravityX() {
        return gravityX;
    }

    public float getGravityY() {
        return gravityY;
    }

    public int randomNumberInRange(int min, int max) {
        Random random = new Random();
        int my_rand;
        do {
            my_rand = random.nextInt((max - min) + 1) + min;
        }while(my_rand ==0);

        return my_rand;
    }
}
//        if (game.mp.isPlaying()){
//            game.mp.stop();
//            game.mp.start();
//        }
//        else {
//            game.mp.start();
//        }

//                Canvas canvas = game.getHolder().lockCanvas();
//                Paint paint2 = new Paint();
//                paint2.setColor(Color.RED);
//                paint2.setStyle(Paint.Style.FILL);
//                paint2.setAlpha(150);
//                canvas.drawPaint(paint2);
//                game.getHolder().unlockCanvasAndPost(canvas);  }


//            final Timer timer = new Timer(true);
//            TimerTask task = new TimerTask() {
//                int run_count = 1;
//                @Override
//                public void run() {
                    //FLASH COLOUR
//                    if (++run_count ==2){
//                                        timer.cancel();
//                                        timer.purge();}
//                }
//            };
//            timer.schedule(task, 0, 1);
//        }

//        else{}

