package nexidea.kawaii_defense.Projectiles;

import android.graphics.Bitmap;
import android.graphics.Rect;

import java.util.Random;

import nexidea.kawaii_defense.Game;
import nexidea.kawaii_defense.MainActivity_stage;
import nexidea.kawaii_defense.MyBitmap;

public class Projectile_laser extends Sprite {
    //variables

    //display
    public Bitmap warning_image;
    public Bitmap warning_flash;
    public boolean trigger;
    public Bitmap image;
    public Bitmap dead_image;
    public Bitmap score_image;

    //properties
    public int damage;
    public int hits;
    public int p_score;
    public long warning_time;
    public long warning_flash_time;
    public long exist_time;
    public long dead_time;
    public String type;
    public String direction;

    //motion
    private int directionX;
    private int directionY;

    //check projectile
    //public boolean in_game = false;
    public boolean exist;
    public boolean dead;
    public boolean out;
    private Game game;

    public Projectile_laser(int screenWidth, int screenHeight, int x, int y, float speedX, float speedY,
                            int directionX,int directionY, int direction,
                            Game game) {
        super(screenWidth, screenHeight, x, y, speedX, speedY);

        if(speedX ==0 && speedY==0){
            type = "static_laser";
        }
        else{
            type = "mobile_laser";
        }
        this.game = game;
        this.directionX = directionX;
        this.directionY = directionY;
        // direction: horizontal = 1 ,vertical = 0
        this.direction = direction == 1 ? "horizontal" : "vertical";
        trigger = false;
        int random_color = randomNumberInRange(1,2);
        String rc = random_color == 1 ? "red_image" : "blue_image";
        warning_image = MyBitmap.stage2_bitmap_hash.get(this.direction).get("warning_image");
        warning_flash = MyBitmap.stage2_bitmap_hash.get(this.direction).get("warning_flash");
        image = MyBitmap.stage2_bitmap_hash.get(this.direction).get(rc);
        dead_image = MyBitmap.stage2_bitmap_hash.get(this.direction).get("dead_image");
        damage = game.getGame_activity().getMultiProjectile().get(type).get("damage");
        hits = game.getGame_activity().getMultiProjectile().get(type).get("hits");
        exist_time = type == "static_laser" ? (long)randomNumberInRange(500,1000): (long) randomNumberInRange(500,1000);
        exist = false;
        dead = true;
        out = true;
        warning_time = randomNumberInRange(2000,4000);
        warning_flash_time = warning_time/10;
        dead_time = 1000;
    }

    public void load_projectile (){
        dead = false;
        out = false;
        init(warning_image,dead);
    }

    public void update(long elapsed){
        float x = getX();
        float y = getY();

        Rect screenRect = getScreenRect();
        if (direction == "vertical") {
            if (screenRect.left <= 0) {
                directionX = 1;
            } else if (screenRect.right >= getScreenWidth()) {
                directionX = -1;
            }
        }

        if(direction == "horizontal") {
            if (screenRect.top < 0) {
                directionY = 1;
            } else if (screenRect.bottom >= getScreenHeight()) {
                directionY = -1;
            }
        }

        x += directionX *(getSpeedX()*elapsed);
        y += directionY *(getSpeedY()*elapsed);

        setX(x);
        setY(y);
        if (!exist) {
            warning_time -= elapsed;
            if (!trigger) {
                warning_flash_time -= elapsed;
                if (warning_flash_time <0) {
                    init(warning_flash, dead);
                    trigger = true;
                    warning_flash_time = warning_time / 10;
                }
            }
            else {
                warning_flash_time -= elapsed;
                if (warning_flash_time < 0) {
                    init(warning_image, dead);
                    trigger = false;
                    warning_flash_time = warning_time / 10;
                }
            }
            if (warning_time < 0) {
                exist = true;
                init(image,dead);
                if (direction == "vertical") {
                    setX(getX() + getScreenWidth() / 12 - getScreenWidth()/120);
                }
                else {
                    setY(getY() + getScreenWidth() / 12 - getScreenWidth()/120);
                }
            }
        }

        if (exist) {
            exist_time -= elapsed;
            if (exist_time < 0 && !dead) {
                this.destroyed(game);
            }
        }

        if (dead){
            dead_time -= elapsed;
            if (dead_time<0 && !out){
                out = true;
                lock = false;
            }
        }
    }

    public void destroyed(Game game){
        p_score = game.getGame_activity().getMultiProjectile().get(type).get("p_score");
        score_image = MyBitmap.general_bitmap_hash.get("score").get(p_score);
        game.setScore(game.score + p_score);
        dead = true;
        out = false;
        init(score_image, dead);
        damage = 0;
        hits = 0;
        game.getGame_activity().setCount_obj_in_screen(game.getGame_activity().getCount_obj_in_screen() -1);
        game.getGame_activity().getCurrent_wave().wave_completion.put("laser",
                game.getGame_activity().getCurrent_wave().wave_completion.get("laser")-1);
        ((MainActivity_stage) game.getContext()).updatereq(game.getGame_activity().getCurrent_wave().wave_completion,
                game.current_stage);
        game.kill_qty ++;
    }

    public void destroyed_by_collision(Game game){
        game.setLife(game.life - damage);
        dead = true;
        out = false;
        init(dead_image, dead);
        damage = 0;
        hits = 0;
        game.getGame_activity().setCount_obj_in_screen(game.getGame_activity().getCount_obj_in_screen() -1);
        if (game.ball.shield_count==0) {
            game.combo = 0;
            game.update_combo();
        }
    }


    public int randomNumberInRange(int min, int max) {
        Random random = new Random();
        int my_rand;
        do {
            my_rand = random.nextInt((max - min) + 1) + min;
        }while(my_rand ==0);

        return my_rand;
    }

}

