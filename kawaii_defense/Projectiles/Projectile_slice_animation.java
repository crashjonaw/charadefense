package nexidea.kawaii_defense.Projectiles;

import android.graphics.Bitmap;
import java.util.Random;

import nexidea.kawaii_defense.Game;
import nexidea.kawaii_defense.MenuActivity;
import nexidea.kawaii_defense.MenuView;
import nexidea.kawaii_defense.MyBitmap;
import nexidea.kawaii_defense.PreMenuActivity;

public class Projectile_slice_animation extends Sprite {


    //variables
    private int directionX;
    private int directionY;

    //display
    public Bitmap image;
    public Bitmap dead_image;
    public long alive_time;
    public long score_time;
    private long default_score_time;

    //motion
    private int spin;


    //check projectile
    public boolean dead;
    public boolean out;

    public Projectile_slice_animation(int screenWidth, int screenHeight, int x, int y, float speedX, float speedY) {
        super(screenWidth, screenHeight, x, y, speedX, speedY);
    }

    public void load_projectile(Game game, boolean char_move, boolean bga) {
        if(char_move) {
            Random r = new Random();
            image = MyBitmap.drag_effect_bitmap_inventory.get(game.selected_char)[r.nextInt(MyBitmap.drag_effect_bitmap_inventory.get(game.selected_char).length)];
            alive_time = 200;
            score_time = 200;
            default_score_time = 200;
        }
        else{
            if(bga){
                image = MyBitmap.char_press_effect_mini.get(MenuActivity.local_db.getString("selected_char","puffy")).get(randomNumberInRange(1,3));
                alive_time = 3000;
                score_time = 200;
                default_score_time = 200;
            }
            else {
                image = MyBitmap.char_press_effect_mini.get(game.selected_char).get(game.char_offense_mode);
                alive_time = 200;
                score_time = 200;
                default_score_time = 200;
            }
        }
        spin = randomNumberInRange(-10,10);
        setAngle(randomNumberInRange(1,360));
        init(image,dead);
        dead = false;
        out = false;

    }

    public void update(long elapsed) {
        float x = getX();
        float y = getY();
        int angle = getAngle();

        if (!dead) {
            alive_time -= elapsed;
            if(alive_time < 0){
                //init(dead_image, dead);
                dead = true;
            }
        }

        if (dead && !out) {
            score_time -= elapsed;
            directionX = randomNumberInRange(-1,1);
            directionY = randomNumberInRange(-1,1);
            setSpeedX(Math.abs(default_score_time-score_time)/100000);
            setSpeedY(Math.abs(default_score_time-score_time)/100000);
            x += directionX *(getSpeedX()*elapsed);
            y += directionY *(getSpeedY()*elapsed);
            setX(x + randomNumberInRange(-50,50));
            setY(y + randomNumberInRange(-50,50));
            if (score_time < 0) {
                out = true;
                lock = false;
            }
        }
        angle += spin;
        setAngle(angle);
    }

    public int randomNumberInRange(int min, int max) {
        Random random = new Random();
        int my_rand;
        do {
            my_rand = random.nextInt((max - min) + 1) + min;
        } while (my_rand == 0);

        return my_rand;
    }
}
