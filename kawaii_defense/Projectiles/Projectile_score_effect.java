package nexidea.kawaii_defense.Projectiles;

import android.graphics.Bitmap;

import java.util.Random;

import nexidea.kawaii_defense.Game;
import nexidea.kawaii_defense.MyBitmap;

public class Projectile_score_effect extends Sprite {

    //display
    public Bitmap image;
    public long alive_time;
    private final long alive_total_time = 1000;

    //motion
    private final int spin = 10;

    //check projectile
    public boolean dead;
    public boolean out;

    public Projectile_score_effect(int screenWidth, int screenHeight, int x, int y, float speedX, float speedY) {
        super(screenWidth, screenHeight, x, y, speedX, speedY);
    }

    public void load_projectile(Game game) {
        image = MyBitmap.general_bitmap_hash.get("combo effect").get(randomNumberInRange(1,5));
        init(image,dead);
        alive_time = alive_total_time;
        setAngle(randomNumberInRange(1,360));
        dead = false;
        out = false;

    }

    public void update(long elapsed) {
        if (!dead) {
            alive_time -= elapsed;
            setAngle(getAngle()+spin);
            setScale((float) alive_time/alive_total_time);
            if(alive_time < 0){
                dead = true;
                out = true;
                lock = false;
                return;
            }
        }
    }

    public int randomNumberInRange(int min, int max) {
        Random random = new Random();
        int my_rand;
        do {
            my_rand = random.nextInt((max - min) + 1) + min;
        } while (my_rand == 0);

        return my_rand;
    }
}
