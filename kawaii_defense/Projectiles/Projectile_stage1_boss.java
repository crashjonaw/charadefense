package nexidea.kawaii_defense.Projectiles;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;

import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import nexidea.kawaii_defense.Game;
import nexidea.kawaii_defense.MainActivity_stage;
import nexidea.kawaii_defense.MyBitmap;
import nexidea.kawaii_defense.MyRandom;

import static android.content.ContentValues.TAG;

public class Projectile_stage1_boss extends Sprite {
    //variables
    private Game game;

    //music

    //display
    public Bitmap rage_image;
    public Bitmap calm_image;
    public Bitmap dead_image;
    public Bitmap x_image;
    public Bitmap score_image;
    public long score_time;
    public Bitmap health_bar_image;

    //for uchin
    private Bitmap rage_image_eyes;
    private Bitmap calm_image_eyes;

    //properties
    public int damage;
    public int hits;
    public int default_hits;
    public int p_score;
    public String type;

    //timers
    public long rage_time;
    public long vulnerable_time;
    private int avg_vul_time; // to convert to long later
    private int avg_rage_time;
    private int random_spread;
    private long acc_time;
    private long spawn_interval;

    // target
    private float targ_x;
    private float targ_y;
    private float x_dist_to_targ;
    private float y_dist_to_targ;
    private boolean target;
    private int avg_target_time;
    private long target_time;
    private long hit_vul_time;

    //booleans
    public boolean rage;

    //collision
    public boolean collide_ability;
    public long collide_time;

    //motion
    private int spin;
    private int max_spin;
    private int directionX;
    private int directionY;
    public float gravityX = 0f;
    public float gravityY = 0f;
    public float decelX;
    public float decelY;
    public boolean dead;
    public boolean out;

    private int difficulty;


    //show that it is being hit
    private int paint_alpha;

    public Projectile_stage1_boss(int screenWidth,
                                  int screenHeight,
                                  float x,
                                  float y,
                                  float speedX,
                                  float speedY,
                                  String type,
                                  Game game) {
        super(screenWidth, screenHeight, x ,y, speedX, speedY);

        this.game = game;
        this.type = type;

        directionX = 0;
        directionY = 1;

        if (type == "urchin_boss") {
            rage_image_eyes = MyBitmap.stage1_bitmap_hash.get(type).get("rage_image_eyes");
            calm_image_eyes = MyBitmap.stage1_bitmap_hash.get(type).get("calm_image_eyes");
            spin = 0;
        }
        else {
            spin = 0;
        }
        rage_image =  MyBitmap.stage1_bitmap_hash.get(type).get("rage_image");
        calm_image =  MyBitmap.stage1_bitmap_hash.get(type).get("calm_image");
        dead_image =  MyBitmap.stage1_bitmap_hash.get(type).get("dead_image");
        x_image =  MyBitmap.stage1_bitmap_hash.get(type).get("x_image");
        health_bar_image = Bitmap.createScaledBitmap(MyBitmap.general_bitmap_hash.get("hp").get(1),
                calm_image.getWidth(),
                screenHeight/35,false);

        damage = game.getGame_activity().getMultiProjectile().get(type).get("damage");
        score_time = 1800;
        avg_rage_time = 10000;
        avg_vul_time = 5000;
        avg_target_time = 2000;
        random_spread = 2000;
        decelX = 0.005f;
        decelY = 0.005f;
        dead = true;
        out = false;
        spawn_interval = 2000;
        acc_time = 0;
        rage = false;
        vulnerable_time = 10000;
        collide_ability = true;
        score_time = 1000;
        difficulty = 1;
        init(calm_image,dead);
        hit_vul_time = 200;
    }

    public void load_projectile (){
        dead = false;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
        avg_rage_time += difficulty * 100;
        avg_target_time = Math.max(avg_target_time-difficulty*100,500);
        default_hits = game.getGame_activity().getMultiProjectile().get(type).get("hits") + difficulty*2;
        hits = default_hits;
    }

    public void update(long elapsed){
        float x = getX();
        float y = getY();
        int angle = getAngle();
        Rect screenRect = getScreenRect();
        if(!dead) {
            if(screenRect.left <=0){
                directionX = 1;
                reset_target();
            }
            else if(screenRect.right >= getScreenWidth()){
                directionX=-1;
                reset_target();
            }

            if (screenRect.top < 0) {
                directionY = 1;
                reset_target();
            }

            else if (screenRect.bottom >= getScreenHeight()){
                directionY =-1;
                reset_target();
            }


            if(rage){
                init(rage_image,dead);
                rage_time -=elapsed;
                if(!target){
                    targ_x = randomNumberInRange((int) game.ball.getX() - game.ball.getScreenRect().width(),
                            (int) game.ball.getX() + game.ball.getScreenRect().width());
                    targ_y = randomNumberInRange((int) game.ball.getY() - game.ball.getScreenRect().width(),
                            (int) game.ball.getY() + game.ball.getScreenRect().width());
                    x_dist_to_targ = Math.abs(x - targ_x);
                    y_dist_to_targ = Math.max(Math.abs(y - targ_y),screenWidth/2);
                    target_time -= elapsed;
                    if (target_time < 0) {
                        if(x_dist_to_targ<getScreenWidth()/4){
                            setGravityX(0);
                            setSpeedX(0);
                        }
                        else{
                            setGravityX(x_dist_to_targ / 50000 * Math.min(difficulty,7));
                        }
                        setGravityY(y_dist_to_targ / 50000 * Math.min(difficulty,7));
                        decelX = 0;
                        decelY = 0;
                        directionX = (int) Math.signum(targ_x-x);
                        directionY = (int) Math.signum(targ_y-y);
                        target = true;
                        Log.d(TAG, "target is set");
                        Log.d(TAG, "gravity is X: "+getGravityX() +" , Y: "+getGravityY());
                    }

                }
                else{
                    if(screenRect.right >= targ_x &&
                            screenRect.left <= targ_x &&
                            screenRect.top <= targ_y &&
                            screenRect.bottom >= targ_y
                            ){
                        Log.d(TAG, "update: boss hit target");
                        setGravityX(0);
                        setGravityY(0);
                        decelX = 0.005f;
                        decelY = 0.005f;
                        Log.d(TAG, "gravity is X: "+getGravityX() +" , Y: "+getGravityY());
                        target = false;
                        target_time = avg_target_time;
                    }
                }
                if(rage_time<0){
                    rage = false;
                    target=false;
                    target_time =avg_target_time;
                    setGravityX(decelX*0.5f);
                    setGravityY(decelY*0.5f);
                    vulnerable_time = randomNumberInRange(avg_vul_time-random_spread,avg_vul_time+random_spread);
                }

            }
            else{
                init(calm_image,dead);
                vulnerable_time -= elapsed;
                acc_time += elapsed;
                spawn_projectiles(game.getGame_activity().getMy_random(),
                        game.getGame_activity().getWave_counter(),
                        game.getGame_activity().getCount_obj_in_screen(),
                        game.getGame_activity().getObj_counter(),
                        game.getGame_activity().getProjectile_in_game(),
                        game.getGame_activity().getCurrent_wave().max_projectiles);
                if(vulnerable_time < 0){
                    if(!rage && type.equals("urchin_boss")){
                        game.getGame_activity().createInks(1);
                    }
                    rage = true;
                    rage_time = randomNumberInRange(avg_rage_time-random_spread,avg_rage_time+random_spread);
                    target = false;
                    target_time = avg_target_time;
                }
            }


            x += directionX *(getSpeedX()*elapsed);
            y += directionY *(getSpeedY()*elapsed);
            setX(x);
            setY(y);
            setSpeedX(getSpeedX()+gravityX-decelX);
            setSpeedY(getSpeedY()+gravityY-decelY);
            setAngle(getAngle()+spin);

            if(!collide_ability){
                collide_time-=elapsed;
                if (collide_time<=0){
                    collide_ability= true;
                }
            }
        }

        else if(dead){
            score_time -= elapsed;
            if (score_time < 0) {
                init(dead_image, dead);
                setAngle(0);
            }
            x += directionX *(getSpeedX()*elapsed);
            y += directionY *(getSpeedY()*elapsed);
            setX(x);
            setY(y);

            if (screenRect.top < 0) {
                if (!out) {
                    out = true;
                    lock = false;
                    game.getGame_activity().getCurrent_wave().wave_completion.put(type,
                            game.getGame_activity().getCurrent_wave().wave_completion.get(type)-1);
                    return;
                }
            }
        }
        hit_vul_time -= elapsed;
    }

    public void destroyed(Game game){
        p_score = game.getGame_activity().getMultiProjectile().get(type).get("p_score")*game.multiplier;
        score_image = MyBitmap.general_bitmap_hash.get("score").get(p_score);
        game.setScore(game.score + p_score);
        game.getGame_activity().getCurrent_wave().wave_completion.put("score",
                game.getGame_activity().getCurrent_wave().wave_completion.get("score")-p_score);
        init(score_image,dead);
        dead = true;
        damage = 0;
        hits = 0;
        directionX = 0;
        directionY = -1;
        setGravityX(0);
        setGravityY(0);
        decelY = 0;
        decelX = 0;
        setAngle(0);
        setSpeedY(0.4f);
        game.addCombo(1);
        ((MainActivity_stage) game.getContext()).updatereq(game.getGame_activity().getCurrent_wave().wave_completion,
                game.current_stage);
        game.kill_qty = game.kill_qty + 10;
    }

    public void destroyed_by_collision(Game game){
        if(collide_ability && rage) {
            game.setLife(game.life - damage);
            get_hit(1);
            collide_ability = false;
            collide_time = 2000;
        }
    }

    public void get_hit(int no_hits){
        if(hit_vul_time <0) {
            hits = hits - no_hits;
            if (!rage && hits > 0) {
                health_bar_image = Bitmap.createScaledBitmap(health_bar_image
                        , calm_image.getWidth() * hits / default_hits, screenHeight / 35, true);
            }

            if (hits <= 0) {
                destroyed(game);
                health_bar_image = MyBitmap.stage1_bitmap_hash.get("bomb").get("dead_image");
            }
            hit_vul_time = 200;
        }
        else{}
    }

    public void reset_target(){
        setSpeedX(0.2f);
        setSpeedY(0.2f);
        decelX = 0.005f;
        decelY = 0.005f;
        setGravityX(0);
        setGravityY(0);
        target = false;
        target_time = avg_target_time;
    }

    public void draw_boss(Canvas canvas){
               //draw health bar
        canvas.drawBitmap(health_bar_image,
                getX() + getRect().width() / 2 - health_bar_image.getWidth()/2,
                getY() - health_bar_image.getHeight() , null);

        canvas.save(); // save canvas in current state
        canvas.rotate(getAngle(), getX() + getRect().width() / 2, getY() + getRect().height() / 2);
        canvas.drawBitmap(getDraw_image(), getX(), getY(), null);
        canvas.restore();// restore canvas after drawing at a rotation

        if(type=="urchin_boss" && !dead){
            if(rage) {
                canvas.drawBitmap(rage_image_eyes, getX(), getY(), null);
            }
            else{
                canvas.drawBitmap(calm_image_eyes,getX(),getY(),null);
            }
        }

        //draw target
        if(target){
            canvas.drawBitmap(x_image,targ_x - x_image.getWidth()/2,targ_y- x_image.getHeight()/2,null);
        }
    }


    public int randomNumberInRange(int min, int max) {
        Random random = new Random();
        int my_rand;
        do {
            my_rand = random.nextInt((max - min) + 1) + min;
        }while(my_rand ==0);

        return my_rand;
    }

    public void spawn_projectiles(MyRandom my_random,
                                  int wave_counter,
                                  int count_obj_in_screen,
                                  HashMap<String,Integer> obj_counter,
                                  HashMap<String, ConcurrentHashMap<Integer, Projectile>> projectile_in_game,
                                  int max_projectiles) {
        if (acc_time > spawn_interval && count_obj_in_screen < max_projectiles) {
            acc_time = 0;

            if (type == "tree_boss") {
                for (int i = 0; i < my_random.random_map.get(Integer.toString(wave_counter) + "coconut").next(); i++) {
                    game.getGame_activity().setCount_obj_in_screen(game.getGame_activity().getCount_obj_in_screen()+1);
                    obj_counter.put("coconut", obj_counter.get("coconut") + 1);
                    projectile_in_game.get("coconut").get(obj_counter.get("coconut")).
                            load_boss_projectile(getX() + getScreenRect().width() / 2,
                                    getY() + getScreenRect().width() / 4);
                }

                // spawn bananas
                for (int i = 0; i < my_random.random_map.get(Integer.toString(wave_counter) + "banana").next(); i++) {
                    game.getGame_activity().setCount_obj_in_screen(game.getGame_activity().getCount_obj_in_screen()+1);
                    obj_counter.put("banana", obj_counter.get("banana") + 1);
                    projectile_in_game.get("banana").get(obj_counter.get("banana")).
                            load_boss_projectile(getX() + getScreenRect().width() / 2,
                                    getY() + getScreenRect().width() / 4);
                }
            }

            else {
                //spawn angry_coconuts
                for (int i = 0; i < my_random.random_map.get(Integer.toString(wave_counter) + "coconut_angry").next(); i++) {
                    game.getGame_activity().setCount_obj_in_screen(game.getGame_activity().getCount_obj_in_screen() + 1);
                    obj_counter.put("coconut_angry", obj_counter.get("coconut_angry") + 1);
                    projectile_in_game.get("coconut_angry").get(obj_counter.get("coconut_angry")).
                            load_boss_projectile(getX() + getScreenRect().width() / 2,
                                    getY() + getScreenRect().width() / 4);
                }

                // spawn angry_bananas
                for (int i = 0; i < my_random.random_map.get(Integer.toString(wave_counter) + "banana_angry").next(); i++) {
                    game.getGame_activity().setCount_obj_in_screen(game.getGame_activity().getCount_obj_in_screen() + 1);
                    obj_counter.put("banana_angry", obj_counter.get("banana_angry") + 1);
                    projectile_in_game.get("banana_angry").get(obj_counter.get("banana_angry")).
                            load_boss_projectile(getX() + getScreenRect().width() / 2,
                                    getY() + getScreenRect().width() / 4);
                }
            }
        }
    }


    public void setGravityX(float gravityX) {
        this.gravityX = gravityX;
    }

    public void setGravityY(float gravityY) {
        this.gravityY = gravityY;
    }

    public float getGravityX() {
        return gravityX;
    }

    public float getGravityY() {
        return gravityY;
    }



}
//        if (game.mp.isPlaying()){
//            game.mp.stop();
//            game.mp.start();
//        }
//        else {
//            game.mp.start();
//        }

//                Canvas canvas = game.getHolder().lockCanvas();
//                Paint paint2 = new Paint();
//                paint2.setColor(Color.RED);
//                paint2.setStyle(Paint.Style.FILL);
//                paint2.setAlpha(150);
//                canvas.drawPaint(paint2);
//                game.getHolder().unlockCanvasAndPost(canvas);  }


//            final Timer timer = new Timer(true);
//            TimerTask task = new TimerTask() {
//                int run_count = 1;
//                @Override
//                public void run() {
//FLASH COLOUR
//                    if (++run_count ==2){
//                                        timer.cancel();
//                                        timer.purge();}
//                }
//            };
//            timer.schedule(task, 0, 1);
//        }

//        else{}

