package nexidea.kawaii_defense.Projectiles;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class Sprite_char {
    private float x;
    private float y;
    private int screenWidth;
    private int screenHeight;
    private Bitmap image;
    private Bitmap shield_image;
    private HashMap<Integer,Bitmap> off_shield_bitmap;
    private HashMap<Integer,HashMap<String,Integer>> off_shield_coordinates;
    private int shield_angle;
    private Rect bounds;
    private int angle = 0;
    private float speedX;
    private float speedY;
    private int life;
    private Paint char_paint;
    private int x1;
    private int y1;
    private boolean offense;

    public Sprite_char(int screenWidth, int screenHeight, int x , int y, float speedX, float speedY){
        this.x = x;
        this.y = y;
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;
        this.speedX = speedX;
        this.speedY = speedY;

        char_paint = new Paint();
        char_paint.setAlpha(255); // you can change number to change the transparency level
//      render later to the pixel
    }

    public void adjust_char_paint_alpha(int lives){
        char_paint.setAlpha(max(0,min(char_paint.getAlpha() + lives*10,255)));
    }

    public void init(Bitmap image){
        this.image = image;
        bounds = new Rect(0,0,image.getWidth(),image.getHeight());
        //rotating ball
        x1 = (int) (getRect().height()* 0.6f - getRect().width()/2);
        y1 = (int) (getRect().height()* 0.1f);
    }

    public void draw(Canvas canvas){
        float x2 = Math.round(x - x1);
        float y2 = Math.round(y - y1);
        canvas.drawBitmap(image, x, y, char_paint);
        canvas.drawBitmap(shield_image, x2, y2, null);
        canvas.drawBitmapMesh(image,1,1,get_verts(),0,null,0,null);
        canvas.drawBitmapMesh(shield_image,1,1,get_shield_verts(x2,y2),0,null,0,null);
//           this rotating thing causes alot of lag
//            canvas.save(); // save canvas in current state
//            canvas.rotate(shield_angle, x + getRect().width() / 2, y + getRect().height() / 2);
            //no rotating makes it alot better

//            canvas.restore();// restore canvas after drawing at a rotation




        }


    public Rect getRect(){
        return bounds;
    }

    public Rect getScreenRect(){
        if(bounds != null) {
            return new Rect((int) x, (int) y, (int) x + getRect().width(), (int) y + getRect().height());
        }
        else
            return new Rect(0,0,0,0);
    }


    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getAngle() {
        return angle;
    }

    public void setAngle(int angle) {
        if (angle<=360){
            this.angle = angle;}
        else{
            this.angle=0;}
        }


    public int getScreenWidth() {
        return screenWidth;
    }

    public void setScreenWidth(int screenWidth) {
        this.screenWidth = screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }

    public void setScreenHeight(int screenHeight) {
        this.screenHeight = screenHeight;
    }

    public float getSpeedX() {
        return speedX;
    }

    public void setSpeedX(float speedX){
        this.speedX = max(0,speedX);
        //Log.d("speed", "setSpeedX: my speed is" + this.speedX);
    }

    public float getSpeedY() {
        return speedY;
    }

    public void setSpeedY(float speedY){
        this.speedY = max(0,speedY);
    }

    public void addSpeedX(float add){
        this.speedX = this.speedX + add;
    }

    public void addSpeedY(float add){
        this.speedY = this.speedY + add;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public void setShield_angle(int shield_angle) {
        this.shield_angle = shield_angle;
    }

    public int getShield_angle() {
        return shield_angle;
    }

    public void setShield_image(Bitmap shield_image) {
        this.shield_image = shield_image;
    }

    public HashMap<Integer, Bitmap> getOff_shield_bitmap() {
        return off_shield_bitmap;
    }

    public void reset_alpha(){
        char_paint.setAlpha(255);
    }

    public float[] get_verts(){
        float[] vert = new float[8];
        vert[0] = x; //x1
        vert[1] = y; //y1
        vert[2] = x + getRect().width();
        vert[3] = y;
        vert[4] = x ;//x3 //y3
        vert[5] = y + getRect().height();
        vert[6] = x + getRect().width();
        vert[7] = y + getRect().height();
        return vert;
    }

    public float[] get_shield_verts(float x2, float y2){
        float[] vert = new float[8];
        vert[0] = x2; //x1
        vert[1] = y2; //y1
        vert[2] = x2 + shield_image.getWidth();
        vert[3] = y2;
        vert[4] = x2 ;//x3 //y3
        vert[5] = y2 + shield_image.getHeight();
        vert[6] = x2 + shield_image.getWidth();
        vert[7] = y2 + shield_image.getHeight();
        return vert;
    }

//    HashMap<String,Integer> top = new HashMap<>();
//        top.put("x", getRect().width()/2);
//        top.put("y", (int) -(game.shield_bitmap_inventory.get(4).getHeight()*1.2));
//        off_shield_coordinates.put(1,top);
//
//    HashMap<String,Integer> bottom = new HashMap<>();
//        top.put("x", getRect().width()/2);
//        top.put("y",  getRect().height() + (game.shield_bitmap_inventory.get(4).getHeight()*2));
//        off_shield_coordinates.put(2,bottom);
//
//    HashMap<String,Integer> left = new HashMap<>();
//        top.put("x", (int) -(game.shield_bitmap_inventory.get(5).getWidth()*1.2));
//        top.put("y", getRect().height()/2);
//        off_shield_coordinates.put(3,left);
//
//    HashMap<String,Integer> right = new HashMap<>();
//        top.put("x", getRect().width() + (game.shield_bitmap_inventory.get(5).getWidth()*2));
//        top.put("y", getRect().height()/2);
//        off_shield_coordinates.put(4,right);
}
