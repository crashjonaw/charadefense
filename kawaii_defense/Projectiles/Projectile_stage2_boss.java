package nexidea.kawaii_defense.Projectiles;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import nexidea.kawaii_defense.Game;
import nexidea.kawaii_defense.MainActivity_stage;
import nexidea.kawaii_defense.MyBitmap;
import nexidea.kawaii_defense.MyRandom;

import static android.content.ContentValues.TAG;

public class Projectile_stage2_boss extends Sprite {
    //variables
    private Game game;

    //music

    //display
    public Bitmap rage_image;
    public Bitmap calm_image;
    public Bitmap dead_image;
    private Bitmap injured_image;
    private Bitmap sp_image;
    public Bitmap x_image;
    public Bitmap score_image;
    public long score_time;
    public Bitmap health_bar_image;
    private Bitmap preview_image;

    //properties
    public int damage;
    public int hits;
    public int default_hits;
    public int p_score;
    public String type;

    //timers
    public long rage_time;
    public long vulnerable_time;
    private int avg_vul_time; // to convert to long later
    private int avg_rage_time;
    private int random_spread;
    private long acc_time;
    private long spawn_interval;
    private long injured_time;
    private long default_injured_time;
    private long set_time;
    private final long default_set_time;
    private long hit_vul_time;

    //sp_mode
    public int avg_sp_time;
    public boolean sp_mode;
    public boolean sp_set;
    public long sp_time;
    public long sp_trigger_time;
    public long default_sp_trigger_time;

    // target
    private float targ_x;
    private float targ_y;
    private float x_dist_to_targ;
    private float y_dist_to_targ;
    private boolean target;
    private int avg_target_time;
    private long target_time;

    //booleans
    public boolean rage;

    //collision
    public boolean collide_ability;
    public long collide_time;

    //motion
    private int spin;
    private int max_spin;
    private int directionX;
    private int directionY;
    public float gravityX = 0f;
    public float gravityY = 0f;
    public float decelX;
    public float decelY;
    public boolean dead;
    public boolean out;

    private int difficulty;


    //show that it is being hit
    private Paint paint;
    private int paint_alpha;

    public Projectile_stage2_boss(int screenWidth,
                                  int screenHeight,
                                  float x,
                                  float y,
                                  float speedX,
                                  float speedY,
                                  String type,
                                  Game game) {
        super(screenWidth, screenHeight, x ,y, speedX, speedY);

        this.game = game;
        this.type = type;

        directionX = 0;
        directionY = 1;

        rage_image =  MyBitmap.stage2_bitmap_hash.get(type).get("rage_image");
        calm_image =  MyBitmap.stage2_bitmap_hash.get(type).get("calm_image");
        injured_image = MyBitmap.stage2_bitmap_hash.get(type).get("injured_image");
        preview_image =MyBitmap.stage2_bitmap_hash.get(type).get("preview_image");
        sp_image = MyBitmap.stage2_bitmap_hash.get(type).get("sp_image");
        dead_image =  MyBitmap.stage2_bitmap_hash.get(type).get("dead_image");
        x_image =  MyBitmap.stage2_bitmap_hash.get(type).get("x_image");
        damage = game.getGame_activity().getMultiProjectile().get(type).get("damage");
        health_bar_image = Bitmap.createScaledBitmap(MyBitmap.general_bitmap_hash.get("hp").get(1),
                calm_image.getWidth()* 4/5,
                screenHeight/40,false);
        default_hits = game.getGame_activity().getMultiProjectile().get(type).get("hits") + 50* game.getGame_activity().getCurrent_wave().boss_difficulty;
        hits = default_hits;

        spin = 5;
        max_spin = 35;
        score_time = 1800;

        //set time
        default_set_time = 700;
        set_time = default_set_time;

        //rage
        avg_rage_time = 7000;
        avg_vul_time = 5000;
        avg_target_time = 2000;
        random_spread = 2000;
        decelX = 0.005f;
        decelY = 0.005f;
        dead = true;
        out = false;
        spawn_interval = 3000;
        acc_time = 0;
        rage = false;
        vulnerable_time = 10000;
        collide_ability = true;

        //sp_mode
        sp_mode = false;
        avg_sp_time = 15000;
        sp_set=false;
        sp_time = 0;

        //injured time
        default_injured_time = 100;
        injured_time = 0;
        sp_trigger_time = avg_sp_time + avg_rage_time + avg_vul_time;

        //hit vul time
        hit_vul_time = 75;

        score_time = 1000;
        difficulty = 1;
        init(calm_image,dead);
    }

    public void load_projectile (){
        dead = false;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
        avg_rage_time += difficulty * 100;
        avg_target_time = Math.max(avg_target_time-difficulty*100,500);

    }

    public void update(long elapsed){
        float x = getX();
        float y = getY();
        int angle = getAngle();
        Rect screenRect = getScreenRect();
        if(!dead) {

            if (screenRect.left <= 0) {
                directionX = 1;
                reset_target();
            } else if (screenRect.right >= getScreenWidth()) {
                directionX = -1;
                reset_target();
            }

            if (screenRect.top < 0) {
                if(!sp_mode) {
                    directionY = 1;
                    reset_target();
                }
                else{
                    setX(getScreenWidth() / 2 - getRect().width() / 2);
                    setY(getRect().height()/2);
                }

            } else if (screenRect.bottom >= getScreenHeight()) {
                directionY = -1;
                reset_target();
            }

            if (sp_mode) {
                if (injured_time < 0) {
                    init(sp_image, dead);
                } else {
                    init(injured_image, dead);
                }
                injured_time -= elapsed;

                if(!sp_set) {
                    init(preview_image, dead);
                    targ_x = getScreenWidth() / 2 - getRect().width() / 2;
                    targ_y = getRect().height() / 2;
                    x_dist_to_targ = Math.abs(x - targ_x);
                    y_dist_to_targ = Math.max(Math.abs(y - targ_y), screenWidth / 2);
                    if(x_dist_to_targ<getScreenWidth()/6){
                        setSpeedX(0);
                    }
                    else{
                        setSpeedX(x_dist_to_targ / 1000 * difficulty);
                    }
                    setSpeedY(y_dist_to_targ / 1000 * difficulty);
                    decelX = 0;
                    decelY = 0;
                    directionX = (int) Math.signum(targ_x - x);
                    directionY = (int) Math.signum(targ_y - y);
                    x += directionX * (getSpeedX() * elapsed);
                    y += directionY * (getSpeedY() * elapsed);
                    setX(x);
                    setY(y);
                    setSpeedX(getSpeedX() + gravityX - decelX);
                    setSpeedY(getSpeedY() + gravityY - decelY);
                    set_time -= elapsed;
                    if(set_time<0){
                        init(sp_image, dead);
                        Log.d(TAG, "update: boss hit target");
                        setGravityX(0);
                        setGravityY(0);
                        setX(getScreenWidth() / 2 - getRect().width() / 2);
                        setY(getRect().height() / 2);
                        decelX = 0f;
                        decelY = 0f;
                        setSpeedX(0);
                        setSpeedY(0);
                        rage = false;
                        target = false;
                        target_time = avg_target_time;
                        sp_time = randomNumberInRange(avg_sp_time, avg_sp_time + 5000);
                        acc_time = 0;
                        sp_set = true;
                        set_time = default_set_time;
                    }
                }

//                if(screenRect.right >= targ_x &&
//                        screenRect.left <= targ_x &&
//                        screenRect.top <= targ_y &&
//                        screenRect.bottom >= targ_y) {
//                    if (!sp_set) {
//                        init(sp_image, dead);
//                        Log.d(TAG, "update: boss hit target");
//                        setGravityX(0);
//                        setGravityY(0);
//                        setX(getScreenWidth() / 2 - getRect().width() / 2);
//                        setY(getRect().height() / 2);
//                        decelX = 0f;
//                        decelY = 0f;
//                        setSpeedX(0);
//                        setSpeedY(0);
//                        rage = false;
//                        target = false;
//                        target_time = avg_target_time;
//                        sp_time = randomNumberInRange(avg_sp_time, avg_sp_time + 3000);
//                        acc_time = 0;
//                    }
//                    sp_set = true;
//                }

                if (sp_set) {
                    acc_time += elapsed;
                    sp_time -= elapsed;
                    spawn_sp_projectiles(game.getGame_activity().getMy_random(),
                            game.getGame_activity().getWave_counter(),
                            game.getGame_activity().getCount_obj_in_screen(),
                            game.getGame_activity().getObj_counter(),
                            game.getGame_activity().getProjectile_in_game(),
                            game.getGame_activity().getLaser_in_game(),
                            game.getGame_activity().getCurrent_wave().max_projectiles);
                    sp_time -= elapsed;
                    if (sp_time < 0) {
                        sp_set = false;
                        sp_mode = false;
                        setGravityX(decelX * 0.5f);
                        setGravityY(decelY * 0.5f);
                        vulnerable_time = randomNumberInRange(avg_vul_time - random_spread, avg_vul_time + random_spread);
                    }
                }

            } else {
                if (rage && !sp_mode) {
                    init(rage_image, dead);
                    rage_time -= elapsed;
                    if (!target) {
//                    targ_x = randomNumberInRange((int) game.ball.getX() - game.ball.getScreenRect().width(),
//                            (int) game.ball.getX() + game.ball.getScreenRect().width());
//                    targ_y = randomNumberInRange((int) game.ball.getY() - game.ball.getScreenRect().width(),
//                            (int) game.ball.getY() + game.ball.getScreenRect().width());
                        targ_x = (int) game.ball.getX() + game.ball.getScreenRect().width() / 2;
                        targ_y = (int) game.ball.getY() + game.ball.getScreenRect().height() / 2;
                        x_dist_to_targ = Math.abs(x - targ_x);
                        y_dist_to_targ = Math.max(Math.abs(y - targ_y), screenWidth / 2);
                        target_time -= elapsed;
                        if (target_time < 0) {
                            if(x_dist_to_targ<getScreenWidth()/3){
                                setGravityX(0);
                                setSpeedX(0);
                            }
                            else{
                                setGravityX(x_dist_to_targ / 50000 * Math.min(difficulty,3));
                            }
                            setGravityY(Math.max(y_dist_to_targ / 20000 * Math.min(difficulty,3),screenHeight/(3*20000)));
                            decelX = 0;
                            decelY = 0;
                            directionX = (int) Math.signum(targ_x - x);
                            directionY = (int) Math.signum(targ_y - y);
                            target = true;
                            Log.d(TAG, "target is set");
                            Log.d(TAG, "gravity is X: " + getGravityX() + " , Y: " + getGravityY());
                        }

                    } else {
                        if (screenRect.right >= targ_x &&
                                screenRect.left <= targ_x &&
                                screenRect.top <= targ_y &&
                                screenRect.bottom >= targ_y
                                ) {
                            Log.d(TAG, "update: boss hit target");
                            setGravityX(0);
                            setGravityY(0);
                            decelX = 0.005f;
                            decelY = 0.005f;
                            Log.d(TAG, "gravity is X: " + getGravityX() + " , Y: " + getGravityY());
                            target = false;
                            target_time = avg_target_time;

                        }
                    }
                    if (rage_time < 0) {
                        rage = false;
                        target = false;
                        target_time = avg_target_time;
                        setGravityX(decelX * 0.5f);
                        setGravityY(decelY * 0.5f);
                        vulnerable_time = randomNumberInRange(avg_vul_time - random_spread, avg_vul_time + random_spread);
                    }

                } else if (!rage && !sp_mode) {
                    if (injured_time < 0) {
                        init(calm_image, dead);
                    } else {
                        init(injured_image, dead);
                    }
                    injured_time -= elapsed;
                    vulnerable_time -= elapsed;
                    acc_time += elapsed;
                    spawn_projectiles(game.getGame_activity().getMy_random(),
                            game.getGame_activity().getWave_counter(),
                            game.getGame_activity().getCount_obj_in_screen(),
                            game.getGame_activity().getObj_counter(),
                            game.getGame_activity().getProjectile_in_game(),
                            game.getGame_activity().getCurrent_wave().max_projectiles);
                    if (vulnerable_time < 0) {
                        rage = true;
                        rage_time = randomNumberInRange(avg_rage_time - random_spread, avg_rage_time + random_spread);
                        target = false;
                        target_time = avg_target_time;
                    }
                }


                x += directionX * (getSpeedX() * elapsed);
                y += directionY * (getSpeedY() * elapsed);
                setX(x);
                setY(y);
                setSpeedX(getSpeedX() + gravityX - decelX);
                setSpeedY(getSpeedY() + gravityY - decelY);

                if (!collide_ability) {
                    collide_time -= elapsed;
                    if (collide_time <= 0) {
                        collide_ability = true;
                    }
                }

                sp_trigger_time -= elapsed;
                if (sp_trigger_time < 0) {
                    sp_mode = true;
                    sp_trigger_time = avg_sp_time + 10000;
                }
            }

            hit_vul_time -= elapsed;
        }

        else if(dead){
            score_time -= elapsed;
            if (score_time < 0) {
                init(dead_image, dead);
                setAngle(0);
            }
            x += directionX *(getSpeedX()*elapsed);
            y += directionY *(getSpeedY()*elapsed);
            setX(x);
            setY(y);

            if (screenRect.top < 0) {
                if (!out) {
                    out = true;
                    lock = false;
                    game.getGame_activity().getCurrent_wave().wave_completion.put(type,
                            game.getGame_activity().getCurrent_wave().wave_completion.get(type)-1);
                    return;
                }
            }
        }
    }

    public void destroyed(Game game){
        p_score = game.getGame_activity().getMultiProjectile().get(type).get("p_score")*game.multiplier;
        score_image = MyBitmap.general_bitmap_hash.get("score").get(p_score);
        game.setScore(game.score + p_score);
        game.getGame_activity().getCurrent_wave().wave_completion.put("score",
                game.getGame_activity().getCurrent_wave().wave_completion.get("score")-p_score);
        init(score_image,dead);
        dead = true;
        damage = 0;
        hits = 0;
        directionX = 0;
        directionY = -1;
        setGravityX(0);
        setGravityY(0);
        decelY = 0;
        decelX = 0;
        setAngle(0);
        setSpeedY(0.4f);
        game.addCombo(1);
        ((MainActivity_stage) game.getContext()).updatereq(game.getGame_activity().getCurrent_wave().wave_completion,
                game.current_stage);
        game.kill_qty = game.kill_qty + 20;
    }

    public void destroyed_by_collision(Game game){
        if(collide_ability && rage && !sp_mode) {
            game.setLife(game.life - damage);
            get_hit(1);
            collide_ability = false;
            collide_time = 2000;
        }
    }

    public void get_hit(int no_hits){

        if(hit_vul_time<0) {
            hits = hits - no_hits;
            if (hits % 3 == 0 && hits > 0 && !rage) {
                health_bar_image = Bitmap.createScaledBitmap(health_bar_image
                        , calm_image.getWidth() * 4 / 5 * hits / default_hits, screenHeight / 40, true);
                init(injured_image, dead);
                injured_time = default_injured_time;
            }

            if (hits <= 0) {
                destroyed(game);
                health_bar_image = MyBitmap.stage1_bitmap_hash.get("bomb").get("dead_image");
            }
            hit_vul_time = 75;
        }
        else
        {}
    }

    public void reset_target(){
        setSpeedX(0.2f);
        setSpeedY(0.2f);
        decelX = 0.005f;
        decelY = 0.005f;
        setGravityX(0);
        setGravityY(0);
        target = false;
        target_time = avg_target_time;
    }

    public void draw_boss(Canvas canvas){
        //draw health bar
       canvas.drawBitmap(health_bar_image,
               0 + getScreenWidth() / 2 - health_bar_image.getWidth()/2,
               0, paint);

        canvas.save(); // save canvas in current state
        canvas.rotate(getAngle(), getX() + getRect().width() / 2, getY() + getRect().height() / 2);
        canvas.drawBitmap(getDraw_image(), getX(), getY(), paint);

        //draw target
        if(target){
            canvas.drawBitmap(x_image,targ_x - x_image.getWidth()/2,targ_y- x_image.getHeight()/2,null);
        }
        canvas.restore();// restore canvas after drawing at a rotation
    }


    public int randomNumberInRange(int min, int max) {
        Random random = new Random();
        int my_rand;
        do {
            my_rand = random.nextInt((max - min) + 1) + min;
        }while(my_rand ==0);

        return my_rand;
    }

    public void spawn_projectiles(MyRandom my_random,
                                  int wave_counter,
                                  int count_obj_in_screen,
                                  HashMap<String,Integer> obj_counter,
                                  HashMap<String, ConcurrentHashMap<Integer, Projectile>> projectile_in_game,
                                  int max_projectiles) {
        if (acc_time > spawn_interval && count_obj_in_screen < max_projectiles) {
            acc_time = 0;
//            for (int i = 0; i < my_random.random_map.get(Integer.toString(wave_counter) + "c").next(); i++) {
//                game.getGame_activity().setCount_obj_in_screen(game.getGame_activity().getCount_obj_in_screen()+1);
//                obj_counter.put("dagger", obj_counter.get("dagger") + 1);
//                projectile_in_game.get("dagger").get(obj_counter.get("dagger")).
//                        load_boss_projectile(getX() + getScreenRect().width() / 2,
//                                getY() + getScreenRect().width() / 4);
//            }

            // spawn dark_balls
            for (int i = 0; i < my_random.random_map.get(Integer.toString(wave_counter) + "dark_ball").next(); i++) {
                game.getGame_activity().setCount_obj_in_screen(game.getGame_activity().getCount_obj_in_screen()+1);
                obj_counter.put("dark_ball", obj_counter.get("dark_ball") + 1);
                projectile_in_game.get("dark_ball").get(obj_counter.get("dark_ball")).
                        load_boss_projectile(getX() + getScreenRect().width() / 2,
                                getY() + getScreenRect().width() / 4);
            }
        }
    }

    public void spawn_sp_projectiles(MyRandom my_random,
                                  int wave_counter,
                                  int count_obj_in_screen,
                                  HashMap<String,Integer> obj_counter,
                                  HashMap<String, ConcurrentHashMap<Integer, Projectile>> projectile_in_game,
                                  ConcurrentHashMap<Integer,Projectile_laser> laser_in_game,
                                  int max_projectiles) {

        if (acc_time > spawn_interval && count_obj_in_screen < max_projectiles) {
            acc_time = 0;

            for (int i = 0; i < my_random.random_map.get(Integer.toString(wave_counter) + "laser").next(); i++) {
                count_obj_in_screen += 1;
                obj_counter.put("laser", obj_counter.get("laser") + 1);
                laser_in_game.get(obj_counter.get("laser")).load_projectile();
            }

            // spawn dark_balls
            for (int i = 0; i < my_random.random_map.get(Integer.toString(wave_counter) + "dark_ball").next(); i++) {
                game.getGame_activity().setCount_obj_in_screen(game.getGame_activity().getCount_obj_in_screen()+1);
                obj_counter.put("dark_ball", obj_counter.get("dark_ball") + 1);
                projectile_in_game.get("dark_ball").get(obj_counter.get("dark_ball")).
                        load_stage2_boss_projectile(0f,
                                0f);

            }
        }
    }




    public void setGravityX(float gravityX) {
        this.gravityX = gravityX;
    }

    public void setGravityY(float gravityY) {
        this.gravityY = gravityY;
    }

    public float getGravityX() {
        return gravityX;
    }

    public float getGravityY() {
        return gravityY;
    }



}
//        if (game.mp.isPlaying()){
//            game.mp.stop();
//            game.mp.start();
//        }
//        else {
//            game.mp.start();
//        }

//                Canvas canvas = game.getHolder().lockCanvas();
//                Paint paint2 = new Paint();
//                paint2.setColor(Color.RED);
//                paint2.setStyle(Paint.Style.FILL);
//                paint2.setAlpha(150);
//                canvas.drawPaint(paint2);
//                game.getHolder().unlockCanvasAndPost(canvas);  }


//            final Timer timer = new Timer(true);
//            TimerTask task = new TimerTask() {
//                int run_count = 1;
//                @Override
//                public void run() {
//FLASH COLOUR
//                    if (++run_count ==2){
//                                        timer.cancel();
//                                        timer.purge();}
//                }
//            };
//            timer.schedule(task, 0, 1);
//        }

//        else{}

