package nexidea.kawaii_defense.Projectiles;

import android.graphics.Bitmap;
import android.graphics.Paint;

import java.util.Random;

import nexidea.kawaii_defense.Game;
import nexidea.kawaii_defense.MyBitmap;

public class Projectile_press_effect extends Sprite {

    //display
    public Bitmap image;
    public long alive_time;
    private long score_time;
    private final long alive_total_time = 500;

    //variables
    private int directionX;
    private int directionY;


    //motion
    private final int spin = 10;

    //check projectile
    public boolean dead;
    public boolean out;

    public Paint paint = new Paint();

    public Projectile_press_effect(int screenWidth, int screenHeight, int x, int y, float speedX, float speedY, Game game) {
        super(screenWidth, screenHeight, x, y, speedX, speedY);
//        paint.setAlpha(255);
        image = MyBitmap.char_press_effect.get(game.selected_char).get(game.char_offense_mode);
        setX(getX()-image.getWidth()/2);
        setY(getY()-image.getHeight()/2);
        init(image,dead);
    }

    public void load_projectile(Game game) {
        alive_time = alive_total_time;
        score_time = 200;
        setAngle(randomNumberInRange(1,360));
        dead = false;
        out = false;

    }

    public void update(long elapsed) {
        float x = getX();
        float y = getY();
        int angle = getAngle();
        if (!dead) {
            alive_time -= elapsed;
            setAngle(getAngle()+spin);
            directionX = randomNumberInRange(-1,1);
            directionY = randomNumberInRange(-1,1);
            angle += spin*Math.abs(score_time);
            setSpeedX(Math.abs(200-score_time)/500000);
            setSpeedY(Math.abs(200-score_time)/500000);
            setAngle(angle);
            x += directionX *(getSpeedX()*elapsed);
            y += directionY *(getSpeedY()*elapsed);
            setX(x + randomNumberInRange(-5,5));
            setY(y + randomNumberInRange(-5,5));
            if(alive_time < 0){
                dead = true;
                out = true;
                lock = false;
                return;
            }
        }
    }

    public int randomNumberInRange(int min, int max) {
        Random random = new Random();
        int my_rand;
        do {
            my_rand = random.nextInt((max - min) + 1) + min;
        } while (my_rand == 0);

        return my_rand;
    }

    private void setAlpha(int alpha){
        paint.setAlpha(alpha);
    }
}
