package nexidea.kawaii_defense.Projectiles;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Paint;

import java.util.Random;

import nexidea.kawaii_defense.BackgroundAesthetics.bga_menu;
import nexidea.kawaii_defense.Game;
import nexidea.kawaii_defense.MenuActivity;
import nexidea.kawaii_defense.MyBitmap;

public class Projectile_tap_effect extends Sprite {

    //display
    public Bitmap image;
    public long alive_time;
    private final long alive_total_time = 700;

    //variables
    private int directionX;
    private int directionY;


    //motion
    private int spin;

    //check projectile
    public boolean dead;
    public boolean out;


    public Paint paint = new Paint();

    public Projectile_tap_effect(int screenWidth, int screenHeight,
                                 int x, int y, float speedX,
                                 float speedY,
                                 Game game,
                                 boolean rand_size) {
        super(screenWidth, screenHeight, x, y, speedX, speedY);
        paint.setAlpha(255);
        if(!rand_size) {
            image = MyBitmap.char_tap_effect.get(game.selected_char).get(game.char_offense_mode);
            spin = randomNumberInRange(-10,10);
            alive_time = alive_total_time;
        }
        else{
            Bitmap pict =  MyBitmap.char_tap_effect.get(MenuActivity.local_db.getString("selected_char","puffy")).get(randomNumberInRange(1,3));
            int length = (int)(pict.getWidth()* (bga_menu.random.nextFloat() * 2f + 0.5f));
            image = Bitmap.createScaledBitmap(pict,length,length
                    ,false);
            spin = randomNumberInRange(-10,10);
            alive_time = randomNumberInRange(4000,6000);
        }
        setX(getX()-image.getWidth()/2);
        setY(getY()-image.getHeight()/2);
        init(image,dead);

    }

    public void load_projectile(Game game) {
        setAngle(randomNumberInRange(1,360));
        dead = false;
        out = false;

    }

    public void update(long elapsed) {
        if (!dead) {
            alive_time -= elapsed;
            setAngle(getAngle()+spin);
            if(alive_time < 0){
                dead = true;
                out = true;
                lock = false;
                return;
            }
        }
    }

    public int randomNumberInRange(int min, int max) {
        Random random = new Random();
        int my_rand;
        do {
            my_rand = random.nextInt((max - min) + 1) + min;
        } while (my_rand == 0);

        return my_rand;
    }

    private void setAlpha(int alpha){
        paint.setAlpha(alpha);
    }
}
