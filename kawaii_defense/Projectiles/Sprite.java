package nexidea.kawaii_defense.Projectiles;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class Sprite {
    private float x;
    private float y;
    public int screenWidth;
    public int screenHeight;
    private Rect bounds;
    private int angle = 0;
    private float speedX;
    private float speedY;
    private float scale = 1;
    private Bitmap draw_image;
    public boolean lock = false;
    private int default_hits;
    private Bitmap health_bar_image;
    private boolean show_health_bar;

    // constructor
    public Sprite(int screenWidth, int screenHeight, float x , float y, float speedX, float speedY) {
        this.x = x;
        this.y = y;
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;
        this.speedX = speedX;
        this.speedY = speedY;
    }

    public synchronized void init(Bitmap image, boolean dead){
        if (!dead)
        {bounds = new Rect(0,0,image.getWidth(),image.getHeight());
            this.draw_image = image;
            }

        else if (dead && !lock){
            bounds = new Rect(0,0,image.getWidth(),image.getHeight());
            this.draw_image = image;
            lock = true;}

        else{}
    }

    public synchronized void init_diamond(Bitmap image) {
        bounds = new Rect(0,0,image.getWidth(),image.getHeight());
        this.draw_image = image;
    }

    public void draw(Canvas canvas){
        if(default_hits > 1 && show_health_bar){
            canvas.drawBitmap(health_bar_image,
                    getX() + getRect().width() / 2 - health_bar_image.getWidth()/2,
                    getY() - health_bar_image.getHeight() , null);
        }
        canvas.save(); // save canvas in current state
        canvas.rotate(angle, x + getRect().width() / 2, y + getRect().height() / 2);
//        canvas.drawBitmap(draw_image, x, y, null);
        canvas.drawBitmapMesh(draw_image,1,1,get_verts(),0,null,0,null);
        canvas.restore();// restore canvas after drawing at a rotation

    }

    public void draw_char_offense(Canvas canvas){
        canvas.save(); // save canvas in current state
        canvas.rotate(angle, Math.round(x + getRect().width() / 2), Math.round(y + getRect().height() / 2));
        canvas.drawBitmapMesh(draw_image,1,1,get_verts(),0,null,0,null);
        canvas.restore();// restore canvas after drawing at a rotation
    }

    public void draw_ink(Canvas canvas, Paint paint){
        canvas.save(); // save canvas in current state
        canvas.rotate(angle, x + getRect().width() / 2, y + getRect().height() / 2);
        canvas.drawBitmapMesh(draw_image,1,1,get_verts(),0,null,0,paint);
        canvas.restore();// restore canvas after drawing at a rotation
    }

    public void draw_press_effect(Canvas canvas){
        canvas.save(); // save canvas in current state
        canvas.rotate(angle, x + getRect().width() / 2, y + getRect().height() / 2);
//        canvas.drawBitmap(draw_image, x, y, null);
        canvas.drawBitmapMesh(draw_image,1,1,get_verts(),0,null,0,null);
        canvas.restore();// restore canvas after drawing at a rotation
    }


    public void draw_background(Canvas canvas, Paint paint){
//        canvas.drawBitmap(draw_image, x, y, paint);
        canvas.drawBitmapMesh(draw_image,1,1,get_verts(),0,null,0,null);
    }

    public void draw_score_effect(Canvas canvas){
        canvas.save(); // save canvas in current state
        canvas.translate((x+getRect().width()/2)*(1-scale),
                (y+getRect().width()/2)*(1-scale));
        canvas.scale(scale, scale);
        canvas.rotate(angle,x *scale+ getRect().width()/2,
                y*scale+ getRect().width()/2);
//        canvas.rotate(angle,x+ getRect().width()/2,
//                y+ getRect().width()/2);
        canvas.drawBitmap(draw_image, x, y, null);
        canvas.restore();// restore canvas after drawing at a rotation

    }

    public void draw_laser(Canvas canvas){
        canvas.save(); // save canvas in current state
        canvas.rotate(-angle, x, y);
        canvas.drawBitmapMesh(draw_image,1,1,get_verts(),0,null,0,null);
        canvas.restore();// restore canvas after drawing at a rotation

    }

    public Rect getRect(){
        if(bounds != null) {
            return bounds;}
        else{
            return new Rect(0,0,0,0);}
    }


    public Rect getScreenRect(){
        if(bounds != null) {
            return new Rect((int) x, (int) y, (int) x + getRect().width(), (int) y + getRect().height());
        }
        else{
            return new Rect(0,0,0,0);}
        }


    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getAngle() {
        return angle;
    }

    public void setAngle(int angle) {
        this.angle=angle;
    }

    public void setboss1_Angle(int angle, int spin) {
        this.angle=Math.min(Math.max(-spin,angle),spin);
    }


    public int getScreenWidth() {
        return screenWidth;
    }

    public void setScreenWidth(int screenWidth) {
        this.screenWidth = screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }

    public void setScreenHeight(int screenHeight) {
        this.screenHeight = screenHeight;
    }

    public float getSpeedX() {
        return speedX;
    }
    public void setSpeedX(float speedX) {
        this.speedX = Math.max(0,speedX);
    }

    public float getSpeedY() {
        return speedY;
    }

    public void setSpeedY(float speedY) {
        this.speedY = Math.max(0,speedY);
    }
    public void setSpeedX_G(float speedX) {
        if(Math.abs(speedX) < 0.6f) {
            this.speedX = speedX;
        }
    }
    public void setSpeedY_G(float speedY) {
        if(Math.abs(speedY) < 0.6f) {
            this.speedY = speedY;
        }
    }

    public void addSpeedX(float add){
        this.speedX = this.speedX + add;
    }

    public void addSpeedY(float add){
        this.speedY = this.speedY + add;
    }

    public Bitmap getDraw_image() {
        return draw_image;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public void setDefault_hits(int default_hits) {
        this.default_hits = default_hits;
    }

    public void setHealth_bar_image(Bitmap health_bar_image) {
        this.health_bar_image = health_bar_image;
    }

    public void setShow_health_bar(boolean show_health_bar) {
        this.show_health_bar = show_health_bar;
    }

    public boolean isShow_health_bar() {
        return show_health_bar;
    }

    public float[] get_verts(){
        float[] vert = new float[8];
        vert[0] = x; //x1
        vert[1] = y; //y1
        vert[2] = x + getRect().width();
        vert[3] = y;
        vert[4] = x ;//x3 //y3
        vert[5] = y + getRect().height();
        vert[6] = x + getRect().width();
        vert[7] = y + getRect().height();
        return vert;
    }
}
