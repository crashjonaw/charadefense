package nexidea.kawaii_defense.Projectiles;

import android.graphics.Bitmap;

import java.util.Random;

import nexidea.kawaii_defense.CosSineTable;
import nexidea.kawaii_defense.Game;
import nexidea.kawaii_defense.MyBitmap;

import static java.lang.Math.round;

public class Projectile_char_offense extends Sprite {
    //variables
    private Game game;
    private int type;
    private int offense_mode;

    //music

    //display
    public Bitmap image;
    private Bitmap dead_image;

    //motion
    private int spin;
    private int rotate_spin;
    private int rotate_spin_angle;
    private int hits;
    private int default_hit_count;

    public boolean dead;
    public boolean out;

    private int initial_angle;
    private float distance_from_origin;

    public Projectile_char_offense(int screenWidth,
                                   int screenHeight,
                                   float x,
                                   float y,
                                   float speedX,
                                   float speedY,
                                   int type,
                                   Game game,
                                   int initial_angle,
                                   float distance_from_origin,
                                   int image_type) {
        super(screenWidth, screenHeight, x ,y, speedX, speedY);
        this.game = game;
        this.type = type;

        image = MyBitmap.char_offense_bitmap_hashv2.get(game.selected_char);
        dead_image = MyBitmap.char_offense_bitmap_hash.get(0);
        dead = true;
        out = true;
        if(image_type ==1) {
            default_hit_count = 9999;
        }
        else{
            default_hit_count = 5;
        }
        offense_mode = image_type;
        rotate_spin = 0;

//        if (image_type%2 == 0){
//            rotate_spin_angle = 0.04;
//        }
//        else{
//            rotate_spin_angle = -0.04;
//        }

        if(image_type != 1){
//            rotate_spin_angle = 0.05;
        }
        else{
//            rotate_spin_angle = -0.05;
        }
        rotate_spin_angle = game.spin_upgrade;
        this.initial_angle = initial_angle;
        this.distance_from_origin = distance_from_origin;
    }

    public void load_projectile (){
        if(hits == 0 & dead) {
            hits = default_hit_count;
            spin = 10;
            dead = false;
            out = false;
        }
        init(image,dead);
    }

    public void update(long elapsed){
        if(!dead) {
//            Log.d(TAG, "update: type = " + Integer.toString(type));
//        setX(ball.char_offense_xy_hash.get(type).get("x"));
//        setY(ball.char_offense_xy_hash.get(type).get("y"));
            float new_x = game.ball.getX() + game.ball.getRect().width()/2 + CosSineTable.getTable().getSine(rotate_spin + initial_angle)*distance_from_origin  - getRect().width()/2;
            setX(round(new_x));

            float new_y = game.ball.getY() + game.ball.getRect().height()/2 + CosSineTable.getTable().getCos(rotate_spin + initial_angle)*distance_from_origin  - getRect().width()/2;
            setY(round(new_y));
            int angle = getAngle();
            angle += spin;
            setAngle(angle);
            }
            else{}
            rotate_spin += rotate_spin_angle;
    }

    public void destroyed(Game game){
        dead=true;
        init(dead_image,dead);
    }

    public void hit(int damage){
//        if(offense_mode == 1) {
            hits = hits - damage;
//            spin = spin - spin / default_hit_count;
            if (hits <= 0) {
                destroyed(game);
//                game.update_char_offense_mode();
            }

    }



    public int randomNumberInRange(int min, int max) {
        Random random = new Random();
        int my_rand;
        do {
            my_rand = random.nextInt((max - min) + 1) + min;
        }while(my_rand ==0);

        return my_rand;
    }


}
//        if (game.mp.isPlaying()){
//            game.mp.stop();
//            game.mp.start();
//        }
//        else {
//            game.mp.start();
//        }

//                Canvas canvas = game.getHolder().lockCanvas();
//                Paint paint2 = new Paint();
//                paint2.setColor(Color.RED);
//                paint2.setStyle(Paint.Style.FILL);
//                paint2.setAlpha(150);
//                canvas.drawPaint(paint2);
//                game.getHolder().unlockCanvasAndPost(canvas);  }


//            final Timer timer = new Timer(true);
//            TimerTask task = new TimerTask() {
//                int run_count = 1;
//                @Override
//                public void run() {
                    //FLASH COLOUR
//                    if (++run_count ==2){
//                                        timer.cancel();
//                                        timer.purge();}
//                }
//            };
//            timer.schedule(task, 0, 1);
//        }

//        else{}

