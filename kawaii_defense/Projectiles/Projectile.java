package nexidea.kawaii_defense.Projectiles;

import android.graphics.Bitmap;
import android.graphics.Rect;

import java.util.Random;

import nexidea.kawaii_defense.Game;
import nexidea.kawaii_defense.MainActivity_stage;
import nexidea.kawaii_defense.MyBitmap;
import nexidea.kawaii_defense.Wave;


public class Projectile extends Sprite {
    //variables
    private Game game;

    //music

    //display
    public Bitmap image;
    public Bitmap dead_image;
    public Bitmap score_image;
    public Bitmap collision_image;
    public Bitmap health_bar_image;
    public long score_time;
    public int projectile_difficulty;
    public boolean bottom_start;

    //properties
    private int default_hits;
    public int damage;
    public int hits;
    public int p_score;
    public String type;
    public int wall_bounce_lives;
    private long vul_time;

    //motion
    private int spin;
    //randomNumberInRange(-10,10);
    private int directionX;
    private int directionY;
    public float gravityX = 0f;
    public float gravityY = 0f;

    public boolean dead;
    public boolean out;
    private long spawn_timer;
    public boolean spawned;

    public Projectile(int screenWidth, int screenHeight,
                      float x, float y, float speedX,float speedY,
                      String type, Game game,
                      Wave wave) {
        super(screenWidth, screenHeight, x ,y, speedX, speedY);

       this.game = game;
       this.type = type;

       //starting from bottom
        bottom_start = y == screenHeight ? true : false;

        directionX = randomNumberInRange(-1,1);
        directionY = randomNumberInRange(-1,1);
        spin = type == "dagger" ? 0 : 5;

        if(type == "dark_ball"){
            projectile_difficulty = wave.generate_projectile_size_based_on_probability_array();
            image = MyBitmap.stage2_bitmap_hash.get("boss").get("dark_ball_"+Integer.toString(randomNumberInRange(1,2))+projectile_difficulty);
            dead_image =  MyBitmap.stage1_bitmap_hash.get("shuriken").get("dead_image");
            collision_image =  MyBitmap.stage1_bitmap_hash.get("shuriken").get("collision_image");
        }
        else {
            projectile_difficulty = wave.generate_projectile_size_based_on_probability_array();
            image = MyBitmap.stage1_bitmap_hash.get(type).get("image_"+projectile_difficulty);
            dead_image = MyBitmap.stage1_bitmap_hash.get(type).get("dead_image");
            collision_image = MyBitmap.stage1_bitmap_hash.get(type).get("collision_image");
        }

        if(type == "coconut_angry" || type == "banana_angry"){
            setGravityY((float) randomNumberInRange(-10,10)/1000);
            setGravityX((float) randomNumberInRange(-10,10)/1000);
        }

        if(type=="coconut_black" || type == "dagger"){
            setShow_health_bar(false);
        }
        else{
            setShow_health_bar(true);
        }
        health_bar_image = Bitmap.createScaledBitmap(MyBitmap.general_bitmap_hash.get("hp").get(1),
                image.getWidth(),
                screenHeight / 70, false);
        damage = game.getGame_activity().getMultiProjectile().get(type).get("damage");
        default_hits = game.getGame_activity().getMultiProjectile().get(type).get("hits")*projectile_difficulty;
        hits = default_hits;
        wall_bounce_lives = game.getGame_activity().getMultiProjectile().get(type).get("wall_bounce_lives");
        score_time = 1800;
        setDefault_hits(default_hits);
        setHealth_bar_image(health_bar_image);
        dead = true;
        out = true;

        //check if exceed screen especially for x
        if(x + image.getWidth() > screenWidth){
            setX(x - image.getWidth());
        }

        spawn_timer = (long) randomNumberInRange(0,1000);
        spawned = false;

    }

    public void load_projectile (){
        dead = false;
        out = false;
        vul_time = 75;
        init(image,dead);
    }

    public void load_boss_projectile(float boss_spawn_point_x, float boss_spawn_point_y){
        setX(boss_spawn_point_x);
        setY(boss_spawn_point_y);
        dead = false;
        out = false;
        init(image,dead);
    }

    public void load_stage2_boss_projectile(float boss_spawn_point_x, float boss_spawn_point_y){
        setX(getScreenWidth()*randomNumberInRange(1,90)/100);
        setY(boss_spawn_point_y);
        image = MyBitmap.stage2_bitmap_hash.get("boss").get("dark_ball_"+Integer.toString(3)+projectile_difficulty);
        setShow_health_bar(false);
        setSpeedX(0);
        directionY =1;
        setSpeedY(getSpeedY()*2.5f);
        hits= 1000;
        wall_bounce_lives=1;
        dead = false;
        out = false;
        init(image,dead);
    }

    public void update(long elapsed){
        if(!spawned){
            spawn_timer -= elapsed;
            if(spawn_timer <= 0){
                spawned = true;
            }
            return;
        }
        else{
        }

        if(!dead) {
            float x = getX();
            float y = getY();
            int angle = getAngle();
            Rect screenRect = getScreenRect();
            if (screenRect.left <= 0) {
                bottom_start = bottom_start ? false : false;
                directionX = 1;
                setSpeedX(Math.abs(getSpeedX()));
                setGravityX(0);
            } else if (screenRect.right >= getScreenWidth()) {
                bottom_start = bottom_start ? false : false;
                directionX = -1;
                setSpeedX(Math.abs(getSpeedX()));
                setGravityX(0);
            }

            if (screenRect.top < 0) {
                bottom_start = bottom_start ? false : false;
                setSpeedY(Math.abs(getSpeedY()));
                setGravityY(0);
                directionY = 1;

                }
            else if (screenRect.bottom >= getScreenHeight()) {
                setSpeedY(Math.abs(getSpeedY()));
                setGravityY(0);
                directionY = -1;
                game.getStar_calculator().addBottom_bounces();
                score_by_wall();
                get_hit(game.char_offense_mode);
            }

            x += directionX * (getSpeedX() * elapsed);
            y += directionY * (getSpeedY() * elapsed);
            angle += spin;
            setX(x);
            setY(y);
            setSpeedX_G(getSpeedX() + gravityX);
            setSpeedY_G(getSpeedY() + gravityY);
            setAngle(angle);
            vul_time -= elapsed;
        }
        else if (dead && !out) {
            float x = getX();
            float y = getY();
            setSpeedY(1f);
            Rect screenRect = getScreenRect();
            if (screenRect.top < 0) {
                    out = true;
                    lock = false;
                    game.getGame_activity().setCount_obj_in_screen(game.getGame_activity().getCount_obj_in_screen() - 1);
                    return;
                }
            x += directionX * (getSpeedX() * elapsed);
            y += directionY * (getSpeedY() * elapsed);
            setX(x);
            setY(y);
            setAngle(0);
        }
    }

    public void destroyed(Game game){
        setHealth_bar_image(MyBitmap.stage1_bitmap_hash.get("bomb").get("dead_image"));
        p_score = game.getGame_activity().getMultiProjectile().get(type).get("p_score")*game.multiplier;
        score_image = MyBitmap.general_bitmap_hash.get("score").get(p_score);
        game.setScore(game.score + p_score);
        game.getGame_activity().getCurrent_wave().wave_completion.put("score",
                game.getGame_activity().getCurrent_wave().wave_completion.get("score")-p_score);
        init(score_image,dead);
        dead = true;
        damage = 0;
        hits = 0;
        directionX = 0;
        directionY = -1;
        setSpeedY(1f);
        game.getGame_activity().getCurrent_wave().wave_completion.put(type,
                game.getGame_activity().getCurrent_wave().wave_completion.get(type)-1);
        ((MainActivity_stage) game.getContext()).updatereq(game.getGame_activity().getCurrent_wave().wave_completion,
                game.current_stage);

        addSpeedY(1);
        setAngle(0);
        game.addCombo(1);
        game.kill_qty ++;
    }

    public void destroyed_by_collision(Game game){
        if(type=="dagger"){
            game.ball.setShield_count(game, 0);
            game.update_shield();
        }
        setShow_health_bar(false);
        game.setLife(game.life - damage);
        dead = true;
        init(collision_image,dead);
        damage = 0;
        hits = 0;
        directionX = 0;
        directionY = -1;
        setSpeedY(0.7f);
    }

    public void set_dead(){
        dead = true;
        init(dead_image, dead);
        damage = 0;
        hits = 0;
        directionX = 0;
        directionY = -1;
        addSpeedY(1);
    }


    public int randomNumberInRange(int min, int max) {
        Random random = new Random();
        int my_rand;
        do {
            my_rand = random.nextInt((max - min) + 1) + min;
        }while(my_rand ==0);

        return my_rand;
    }

    public void get_hit(int no_hits){
        if(vul_time<0) {
            hits = hits - Math.max(no_hits, 1);
            if(isShow_health_bar()) {
                if (hits > 0) {
                    health_bar_image = Bitmap.createScaledBitmap(health_bar_image
                            , image.getWidth() * hits / default_hits, screenHeight / 70, true);
                    setHealth_bar_image(health_bar_image);
                }
                if (hits <= 0) {
                    destroyed(game);
                    setShow_health_bar(false);
                }
            }
            vul_time= 75;
        }
    }

    private void score_by_wall() {
        if (bottom_start){}
        else {
            wall_bounce_lives--;
            if (wall_bounce_lives <= 0) {
                destroyed(game);
            }
        }
    }
    public void setGravityX(float gravityX) {
        this.gravityX = gravityX;
    }

    public void setGravityY(float gravityY) {
        this.gravityY = gravityY;
    }

    public float getGravityX() {
        return gravityX;
    }

    public float getGravityY() {
        return gravityY;
    }

}
//        if (game.mp.isPlaying()){
//            game.mp.stop();
//            game.mp.start();
//        }
//        else {
//            game.mp.start();
//        }

//                Canvas canvas = game.getHolder().lockCanvas();
//                Paint paint2 = new Paint();
//                paint2.setColor(Color.RED);
//                paint2.setStyle(Paint.Style.FILL);
//                paint2.setAlpha(150);
//                canvas.drawPaint(paint2);
//                game.getHolder().unlockCanvasAndPost(canvas);  }


//            final Timer timer = new Timer(true);
//            TimerTask task = new TimerTask() {
//                int run_count = 1;
//                @Override
//                public void run() {
                    //FLASH COLOUR
//                    if (++run_count ==2){
//                                        timer.cancel();
//                                        timer.purge();}
//                }
//            };
//            timer.schedule(task, 0, 1);
//        }

//        else{}

